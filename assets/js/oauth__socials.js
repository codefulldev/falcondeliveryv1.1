var appId;
var version;
var clientid;

if ($("#socialSignin").length > 0) {

  $.get('apis', function(response){
    appId = response.appid;
    version = response.version;
    clientid = response.clientid;
  }, 'json');

  var FB;
  window.fbAsyncInit = function() {
    FB.init({
      appId      : $("[data-oauth='facebook']").data('appid'),
      cookie     : true,
      xfbml      : true,
      version    : $("[data-oauth='facebook']").data('version')
    });
    FB.AppEvents.logPageView();   

  };

  (function(d, s, id){
   var js, fjs = d.getElementsByTagName(s)[0];
   if (d.getElementById(id)) {return;} js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/en_US/sdk.js"; fjs.parentNode.insertBefore(js, fjs); }(document, 'script', 'facebook-jssdk'));
  $(document).on(
    "click", "[data-oauth='facebook']",
    function(){

      FB.login(function(response){

       if( response.status == "connected" ){

        FB.getLoginStatus(function(res){
          if( res.status == "connected" ){
           var fields = new Array(
            'name',
            'email',
            'gender',
            'picture'
            );
           FB.api('/me?fields='+fields+'', function(getUser) {
             console.log(getUser);
             var info = {
              'id'      : getUser.id,
              'token'   : res.authResponse.accessToken,
              'email'   : getUser.email,
              'name'    : getUser.name,
              'gender'  : getUser.gender,
              'picture' : getUser.picture.data.url
            };

            $("body").prepend('<div class="page"> <div class="ajax-loader"> <div class="page-loader-body"> <div class="cssload-container"> <div class="cssload-speeding-wheel"></div> </div> <p>Carregando...</p> </div> </div> </div>');

            $.ajax({
              url: 'ajaxReq/438ce62d20db72b9eb984e105dec127f2e92a181',
              type: 'post',
              dataType: 'json',
              data: info,
              success: function(response){
                console.log(response);
                if (response.autorize == true) {
                  window.localStorage.setItem('account_id', response.uid);

                  if ($("#ischeckout").length > 0) {
                    window.location = window.location.href;
                  }else{
                    window.location = response.uri;
                  }

                }
              }
            })

          });

         }
       });

      }

    }, {scope: 'public_profile,email'});
    }
    );

  var googleUser = {};
  var startApp = function() {
    gapi.load('auth2', function(){
      var $idgoogle = document.getElementById('google_oauth');

      auth2 = gapi.auth2.init({
        client_id: (clientid) ? clientid : $idgoogle.getAttribute('rel'),
        cookiepolicy: 'single_host_origin',
      });
      attachSignin($idgoogle);
    });
  };

  startApp();

  function attachSignin(element) {
    auth2.attachClickHandler(element, {},
      function(googleUser) {

        var profile = googleUser.getBasicProfile();
        var id_token = googleUser.getAuthResponse().id_token;

        var info = {
          'id'      : profile.getId(),
          'token'   : id_token,
          'email'   : profile.getEmail(),
          'picture' : profile.getImageUrl(),
          'name'    : profile.getName()
        };

        $("body").prepend('<div class="page"> <div class="ajax-loader"> <div class="page-loader-body"> <div class="cssload-container"> <div class="cssload-speeding-wheel"></div> </div> <p>Carregando...</p> </div> </div> </div>');

        $.ajax({
          url: 'ajaxReq/c855bf0613cdb97bbca3f2f5573c715a896dfba2',
          type: 'post',
          dataType: 'json',
          data: info,
          success: function(response){
            if (response.autorize == true) {
              window.localStorage.setItem('account_id', response.uid);

              if ($("#ischeckout").length > 0) {
                window.location = window.location.href;
              }else{
                window.location = response.uri;
              }

            }
          }
        })

      }, function(error) {
        console.log(JSON.stringify(error, undefined, 2));
      });
  }

}