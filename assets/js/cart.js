$(document).on('click', '.goto_checkout', function(e){
	e.preventDefault();
	var url = $(this).data('url');
	$.get('ajaxReq/254a65f77e8113e584552c28da18bea2e940438d', function(response) {
		if (response.error == false) {
			window.location = url;
		}else{
            swal('Valor mínimo!', 'Desculpa mas o valor mínimo é de R$'+response.value+'. Ligue para o nosso delivery: '+response.phone+' e saiba mais. ', 'warning');
		}
	}, 'json');
});

$(document).on('click', '.remove_item_cart', function(e){
	e.preventDefault();
	var id = $(this).data('id');
	var tr = $(this).data('tr');
	remove_item_cart(id,tr);
});

$(document).on('change', '.change_quantity', function(e){
	e.preventDefault();
	var id = $(this).data('id');
	var quantity = $(this).val();
	$.post('ajaxReq/d1df34c86ffd2e26b65631e24e7ce9d2a73b5887', {'id': id, 'quantity': quantity}, function(response){
		$(".subtotal_shopcart_"+response.index).text('R$'+response.subtotal);
		$(".total_shopcart").text('R$'+response.total);
	}, 'json');
});


document.addEventListener("DOMContentLoaded", function(event) { 

	$('.item_long_press').on('touchstart',function(e,data) {
		
		setTimeout(function(){
			var id = $(this).data('id');
			var tr = $(this).data('tr');
			remove_item_cart(id,tr);
		},1200);

	});

});

function remove_item_cart(id,tr){
	swal({   
		title: "Deseja remover?",   
		text: "O item será removido do seu carrinho.",   
		type: "warning",   
		showCancelButton: true,   
		confirmButtonColor: "#DD6B55",   
		confirmButtonText: "Sim",   
		cancelButtonText: "Não",   
		closeOnConfirm: true 
	}, function(isConfirm){   
		if(isConfirm){
			$.post('ajaxReq/2dc5cfe20a022217acfc0b2f597db5552c48a684', {'id': id}, function(response){
				$("#item"+tr).fadeOut();

				if(response.count == 0){
					$("#shopcart").empty();
					$("#shopcart").fadeIn('fast').html('<h3>O carrinho está vázio!</h3> <a href="/" class="btn btn-icon btn-icon-left btn-burnt-sienna btn-shape-circle offset-top-35"><span class="icon icon-xs mdi mdi-cart-outline"></span><span>Adicionar produto</span></a>');
				}else if(response.count == 1){
					$(".countcart").text('1 item adicionado');
				}else{
					$(".countcart").text(response.count +' itens adicionados');
				}

			},'json');
		}
	});
}