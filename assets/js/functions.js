$(document).ready(function() {

	if($(".timerCount").length){

		var date = $(".timerCount").data('date');
		
		$('.timerCount').countdown(date, function(event) {
			var totalHours = event.offset.totalDays * 24 + event.offset.hours;
			$(this).html(event.strftime(totalHours + 'h %M min %S sec'));
		});

	}
});
