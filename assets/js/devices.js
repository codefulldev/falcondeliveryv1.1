var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    }
};

$(document).ready(function() {

    if (isMobile.iOS()){
        var device = 'ios';
    }else if(isMobile.Android()){
        var device = 'android';
    }else if(isMobile.Windows()){
        var device = 'windowsphone';
    }else {
        var device = 'desktop';
    }

    $.post('device', {device: device}, function(response){
        if (response.error == false) {
            $("#download-app").attr('href', response.store_url);
        }
    }, 'json');

});