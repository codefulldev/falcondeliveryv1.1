$(document).on('submit', '[role="rclients"]', function(e){
	e.preventDefault();
	$.post('ajaxReq/ce3350785304253a0dcf8d3567a4d38884564df6', $(this).serialize(), function(response){

		if(response.error == false){
			swal("Conta criada com sucesso!", "Obrigado por nos escolher.", "success");
			setTimeout(function(){
				window.location = response.base+'account';
			},2200);
			$('[role="rclients"]')[0].reset();

		}else{
			swal("E-mail já cadastrado!", "Se o e-mail for seu, tente recuperar a senha.", "warning");
		}

	}, 'json');
});

$(document).on('submit', '[role="lclients"]', function(e){
	e.preventDefault();
	$.post('ajaxReq/51739c443435f9cef17d024bb8889b9e2c676538', $(this).serialize(), function(response){

		if(response.error == false){
			window.localStorage.setItem('account_id', response.accountID);
			window.location = response.request;
			$('[role="lclients"]')[0].reset();

		}else{
			swal("E-mail ou senha inválidos!", "Verifique se você digitou corretamente, ou recupere a senha.", "warning");
		}

	}, 'json');
});

$(document).on('click', '#recoverypassword', function(e){
	e.preventDefault();
	var target = $(this).data('target');

	$.magnificPopup.open({
		items: {
			src: target,
			midClick: true,
			closeOnBgClick: false,
			closeOnContentClick: false,
			mainClass: 'mfp-3d-unfold'
		},
		type: 'inline'
	});

});

$(document).on('click', '#recoverybutton', function(e){
	e.preventDefault();
	var email = $('input[name="recoveryemail"]');
	if (email.val().length > 0) {

		$.post('ajaxReq/d33bb21feb29e0e1cc2c677c45bc35c9cfc4110e', {'email': email.val()}, function(response){

			if(response.error == false){

				var element = '<img src="'+ response.thumbnail +'" width="30" height="30" />';
				element = element + '<h4>'+ response.name +'</h4>';
				element = element + '<p>'+ response.email +'</p>';
				element = element + '<span>Uma nova senha foi enviada para seu e-mail.</span>';
				$("#recoveryDetails").html(element);

				email.val('');
				setTimeout(function() {
					//$.magnificPopup.close($("#recoverypassword").data('target'));
					$("#recoveryDetails").empty();
				}, 5000)

			}

		}, 'json');

	}else{
		swal('E-mail inválido!', '', 'warning');
	}

});

$(document).on('blur', 'input[name="cep"]', function(e){
	e.preventDefault();

	var cep = $(this).val().replace(/\D/g, '');
	var location = $(this).data('location').trim();
	var list = $(this).data('districts');

	if (cep != "") {

		var validacep = /^[0-9]{8}$/;

		if(validacep.test(cep)) {
			$.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(response) {
				var split = list.split(",");
				if (!("erro" in response)) {
					var localidade = response.localidade;

					switch (localidade) {
						case location:
							if (split.indexOf(response.bairro) === -1) {
								var split = (list != '') ? "Bairros que entregamos: "+list.replace(",", "\n\r") : "";
								swal("Fora de area!", "Desculpe, mas nós não entregamos em seu bairro. "+split, "warning");
							}else{
								var address = response.logradouro+', '+response.bairro+', '+response.localidade+' - '+response.uf;
								var shotaddress = response.bairro+', '+response.localidade+' - '+response.uf;
								$('input[name="address"]').val(address);
							}
							break;
						default:
							$('input[name="address"]').val('');
							$('input[name="cep"]').val('');
							swal("Fora de area!", "Desculpe, mas nós só entregamos na cidade de "+ location, "warning");
							break;
					}
					
				}
				else {
					swal("CEP não encontrado!", "verifique se você digitou corretamente o cep.", "warning");
				}
			});
		}else{
			swal("CEP inválido!", "verifique o formato do cep, ex: 45200000.", "warning");
		}
	}

});

$(".removeaddress").on('click', function(e){
	var id = $(this).data('id');
	$.post('address/remove', {id: id}, function(response){
		window.location = response.url;
	}, 'json');
});

function inArray(needle, haystack) {
 var length = haystack.length;
 for (var i = 0; i < length; i++) {
 if (haystack[i] == needle)
  return true;
 }
 return false;
}

function interval(){
	setInterval(function(){
		$("#qtSemestreConcluido").val('0').blur();
		$("#vlSemestralidadeComDesc").val('3.374,80').blur();
		if ($(".close").length > 0) {
			$(".close").trigger('click');
		}
	}, 3000)
}