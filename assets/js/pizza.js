$(document).ready(function() {

	var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
	$('.js-switch').each(function() {
		new Switchery($(this)[0], $(this).data());
	});

	if ($("#getID").length > 0) {
		var id = $("#getID").data('id');
		$.post('sales/get_product', {'getID': id}, function(response){

			var pizza       = response.pizza;
			var addons      = response.addons;
			var optionals   = response.optionals;
			var ingredients = response.ingredients;

			$('.ingredients_div').hide('fast');
			$(".sidepizza-front-right").show();
			$("#ingredients-right").html(response.name+' - R$'+response.price);
			var $sideright = $(".sidepizza-front-right ul");
			$sideright.empty();

			var countSwichy = 0;
			$.each(ingredients, function(i, data) {
				$sideright.append('<li class="text-left">'+ data.name +' <input type="checkbox" id="switchery'+ data.id + countSwichy+'" name="ingredients[pizza]['+ response.id +'][]" value="'+ data.id +'" checked class="js-switch pull-right" data-color="#f96262" data-size="small" /></li>');
				var elems = document.getElementById('switchery'+ data.id + countSwichy);  
				var switchery = new Switchery(elems, {size: 'small'});
				countSwichy++;
			});

		}, 'json');
	}

});