jQuery(document).ready(function($) {

	$("input[name='verify']").on('click', function(e){

		var $this = $(this);
		
		$this.button('loading');

		$.get('payments/pagseguro', function(initialParams) {

			var session_ID   = initialParams.session[0];
			var ID           = PagSeguroDirectPayment.setSessionId(session_ID);
			var HASH         = PagSeguroDirectPayment.getSenderHash();
			var TOTAL 	     = initialParams.cartTotal.toFixed(2);
			var INSTALLMENTS = initialParams.installments;
			var CARD 	     = $("input[name='number']").val().replace(/\s+/g, '');
			var CVV 	     = $("input[name='cvc']").val().replace(/\s+/g, '');
			var EXPIRE 	     = $("input[name='expiry']").val().replace(/\s+/g, '');
			var DATES 	     = EXPIRE.split("/");

			PagSeguroDirectPayment.getPaymentMethods({
				amount: TOTAL,
				success: function(response) {
					if (response.error == false && response.paymentMethods.CREDIT_CARD) {
						PagSeguroDirectPayment.getBrand({
							cardBin: CARD,
							success: function(response) {
								if (response.brand.bin) {
									$(".brand").val(response.brand.name);
									var BRAND = response.brand.name;
									var expMonth = DATES[0].trim();
									var expYear = DATES[1].trim();

									var param = {
										cardNumber: CARD,
										brand: BRAND,
										cvv: CVV,
										expirationMonth: expMonth,
										expirationYear: expYear,
										success: function(response) {
											console.log(response);
											if (!response.card.token) {
												swal('Dados não aprovados!', 'Após uma verificação dos dados informados, não foi possível realizar esta operação. Entre em contato com a sua plataforma de crédito.', 'error');
											}else{
												var TOKEN = response.card.token;
												PagSeguroDirectPayment.getInstallments({
													amount: TOTAL,
													brand: BRAND,
													maxInstallmentNoInterest: INSTALLMENTS,
													success: function(response) {
														$("#installments").show('fast');
														$("#installments select").empty();
														$.each(response.installments.visa, function(index, val) {
															$("#installments select").append('<option value="'+ val.quantity +'|'+ val.installmentAmount +'">'+ val.quantity +'x de R$'+ val.installmentAmount +'</option>');
														});
													}
												});
												$this.button('reset');
												swal('Está tudo correndo bem', 'Agora é só continuar com o checkout e finalizar sua compra.', 'success');
												$('.creditcard_brand').val(BRAND);
												$('.creditcard_hash').val(HASH);
												$('.creditcard_token').val(TOKEN);
												$('.creditcard_session').val(ID);
												$('.creditcard_total').val(TOTAL);
											}

										}, error: function(response){
											if (response.error == true) {
												$this.button('reset');
												swal('Cartão inválido!', 'Após uma verificação dos dados informados, não foi possível realizar esta operação. Entre em contato com a sua plataforma de crédito.', 'error');
											}
										}, complete: function(response){
											console.log(response);
										}
									}
									PagSeguroDirectPayment.createCardToken(param);
								}
							}
						});
					}
				}
			});

		}, 'json');

	});

});