var $select = $('.selectric').selectric();
var $mount = $("#mount");
var $bgs = $("#pieBackground");
var $qtd;
$(function() {

	$('.chooses').on('ifChanged', function(e) {
		var qtd = $(this).val();
		var thumbnail = $(".first-flavor img").attr('src');
		$qtd = qtd;
		$(".pie").remove();

		switch (qtd) {
			case '1':
			$mount.prepend('<div id="slice-1" class="pie big" data-start="1" data-value="360"></div>');
			var style = stylesheet_body(1, thumbnail, 'center center');
			$('head').append(style);

			$(".sidepizza-front-left ul").empty();
			$(".sidepizza-front-left").hide();
			$("#second-flavor").hide();
			$(".outers-flavors").remove();

			$( "style" ).each(function(index) {
				if (index > 0) {
					if ($( this ).text().indexOf(".pie:nth-of-type(2)") !== -1) {
						$(this).remove();
					}
				}
			});

			break;
			case '2':
			var html = '<div id="slice-1" class="pie" data-start="0" data-value="180"></div>';
			html = html + '<div id="slice-2" class="pie" data-start="180" data-value="180"><i data-id="2" class="addlink fa fa-plus"></i></div>';
			$mount.prepend(html);
			var style = stylesheet_body(1, thumbnail, 'left top');
			$('head').append(style);
			break;
			case '3':
			var html = '<div id="slice-1" class="pie clip" data-start="0" data-value="120"></div>';
			html = html + '<div id="slice-2" class="pie clip" data-start="120" data-value="120"><i data-id="2" class="addlink fa fa-plus"></i></div>';
			html = html + '<div id="slice-3" class="pie clip last-three" data-start="240" data-value="120"><i data-id="3" class="addlink fa fa-plus"></i></div>';
			$mount.prepend(html);
			var style = stylesheet_body(1, thumbnail, 'left top');
			$('head').append(style);
			break;
			default:
			var html = '<div id="slice-1" class="pie" data-start="0" data-value="90"></div>';
			html = html + '<div id="slice-2" class="pie" data-start="90" data-value="90"><i data-id="2" class="addlink fa fa-plus"></i></div>';
			html = html + '<div id="slice-3"class="pie" data-start="180" data-value="90"><i data-id="3" class="addlink fa fa-plus"></i></div>';
			html = html + '<div id="slice-4" class="pie last-four" data-start="270" data-value="90"><i data-id="4" class="addlink fa fa-plus"></i></div>';
			$mount.prepend(html);
			var style = stylesheet_body(1, thumbnail, 'left top');
			$('head').append(style);
			break;
		}
		$bgs.removeAttr('class');
		$bgs.addClass('pieBackground-'+qtd);

	});


	var $list = $("#list-menu");
	var $index;
	var $product_ID = $("#product_ID").data('id');

	$(document).on('click', '.addlink', function(e)  {
		e.preventDefault();

		var $id = $(this).data('id');
		$("#choose-flavor").modal('show');
		$list.html('<p class="text-center">Carregando ...</p>');
		$index = $id;
		getJSON();

	});

	$("#search-list").on('keyup', function(e){
		var name = $(this).val();
		if(name.length > 0 && name != ''){
			$('.list-content').each(function(){
				if($(this).text().toUpperCase().indexOf(name.toUpperCase()) == -1){
					$(this).hide();
				}else{
					$(this).show();
				}
			});
		}else{
			getJSON();
		}

	});

	$(document).on('click', '.mount-button', function(e){
		e.preventDefault();
		var id = $(this).attr('id');
		var name = $("#list-" + id + " .list-header p").text();
		var thumbnail = $("#list-" + id + " img").attr('src');

		switch ($qtd) {
			case '1':
			var style = stylesheet_body($index, thumbnail, 'left top');
			$('head').append(style);
			break;
			case '2':
			var style = stylesheet_body($index, thumbnail, 'left top');
			$('head').append(style);
			break;
			case '3':
			if ($index == 2) {
				var style = stylesheet_body($index, thumbnail, 'left top');
				$('head').append(style);
			}else{
				var style = stylesheet_body($index, thumbnail, 'left top');
				$('head').append(style);
			}
			break;
			default:
			if ($index == 2) {
				var style = stylesheet_body($index, thumbnail, 'left top');
				$('head').append(style);
			}else if($index == 3){
				var style = stylesheet_body($index, thumbnail, 'left top');
				$('head').append(style);
			}else{
				var style = stylesheet_body($index, thumbnail, 'left top');
				$('head').append(style);
			}
			break;
		}

		if(!$("#choose-"+ id).length){

			if($('[data-choose="choose-'+ $index +'"]').length){
				$('[data-choose="choose-'+ $index +'"]').remove();
			}
			var html = html + '<div id="choose-'+ id +'" data-choose="choose-'+ $index +'" class="box-flavor dynamic-flavor outers-flavors">';
			html = html + '<a data-id="'+ id +'" data-index="'+ $index +'" href="#" class="remove-flavor"><i class="fa fa-times"></i></a>';
			html = html + '<div class="left-img">';
			html = html + '<img src="'+ thumbnail +'">';
			html = html + '</div>';
			html = html + '<div class="before-info">';
			html = html + '<p>'+ name +'</p>';
			html = html + '</div>';
			html = html + '<input type="hidden" name="flavors[]" value="'+ id +'">';
			html = html + '<div class="clearfix"></div>';
			html = html + '</div>';

			$("#second-flavor").show();
			$(".sidepizza-front-left").show();

			$("#second-flavor").prepend(html.replace("undefined", ""));
			$("#choose-flavor").modal('hide');

			var id          = $(this).attr('id');
			var name        = $(this).data('name');
			var price       = $(this).data('price');
			var ingredients = $("#ocultIgredients"+id).val();

			$.post('ajaxReq/a79e7133d9318261a9d8098804a4df1b9a3dca68', {'value': ingredients}, function(response){
				$("#ingredients-left").html(name+' - R$'+ price );
				var $sideleft = $(".sidepizza-front-left ul");
				$sideleft.empty();
				var countSwichy = 0;
				$.each(response, function(i, data) {
					$sideleft.append('<li>'+ data.name +' <input type="checkbox" id="switchery'+ data.id + countSwichy+'" name="ingredients[pizza]['+ id +'][]" value="'+ data.id +'" checked class="js-switch" data-color="#f96262" data-size="small" /></li>');
					var elems = document.getElementById('switchery'+ data.id + countSwichy);  
					var switchery = new Switchery(elems, {size: 'small'});
					countSwichy++;
				});
			}, 'json');

		}

	});

	$(document).on('click', '.remove-flavor', function(e){
		e.preventDefault();
		var id = $(this).data('id');
		var index = $(this).data('index');
		var content = $("#choose-"+id);
		if(content.length){

			$( "style" ).each(function() {
				if ($( this ).text().indexOf(".pie:nth-of-type("+ index +")") !== -1) {
					$(this).remove();
					content.remove();
					$(".sidepizza-front-left ul").empty();
					$(".sidepizza-front-left").hide();
					$("#second-flavor").hide();
					$("#second-flavor div:first").remove();
				}
			});
		}
	});

	function getJSON($id){
		$.getJSON('ajaxReq/b882641720be020b3a6b4cab2719d6c47b814b5a', function(json) {
			if (json.display == false) {
				$list.html('<p>Nenhuma pizza encontrada</p>');
			}else{
				$list.empty();
				$.each(json, function(index, val) {
					if (val.id != $("#product_ID").data('id')){
						if ($("#choose-"+val.id).length){
							var checked = ' button-disabled" disabled="disabled"';
							var icon = 'fa-check';
						}else{
							var checked = '"';
							var icon = 'fa-plus';
						}
						var $html = '<li id="list-'+ val.id +'" class="list-content">';
						$html = $html + '<div class="list-img"><img src="'+ val.thumbnail +'" alt="" /></div>';
						$html = $html + '<div class="list-header">';
						$html = $html + '<p>'+ val.name +' - R$'+ val.price +'</p>'; 
						$html = $html + '<input type="hidden" id="ocultIgredients'+ val.id +'" value="'+ val.ingredients_id +'" />';
						$html = $html + '<button type="button" id="'+ val.id +'" data-price="'+ val.price +'" data-name="'+val.name+'" class="mount-button btn btn-warning btn-xs pull-right '+checked+'><i class="fa '+ icon +'"></i></button>';
						$html = $html + '</div>';
						$html = $html + '<div class="clearfix"></div>';
						$html = $html + '</li>';
						$list.append($html);

					}

				});

				$(".bootstrap-tagsinput").attr('style', 'background:none;border:none;box-shadow:none;');
				$(".bootstrap-tagsinput input").attr('readyonly', 'true');
				
			}
		});
	}

	function stylesheet_body(index, url, positions){
		return '<style>.pie:nth-of-type('+ index +'):AFTER, .pie:nth-of-type('+ index +'):BEFORE {background-color: rgba(0, 0, 0, .5); cursor: pointer; text-align: center; vertical-align: middle; background-image: url('+ url +'); background-repeat: no-repeat; background-size: cover; background-position: '+ positions +'}</style>';
	}

});