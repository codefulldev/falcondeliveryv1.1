const app = angular.module('RangusApp', ['btford.socket-io']);
const uid = window.localStorage.getItem('account_id');

var ip;
var port;

app.factory('connections', ['$http', function ($http) {
  var connection = reqGet('node', 'application/json');
  $http(connection)
  .then(function(success){
    ip = success.data.ipdomain;
    port = success.data.port;
  }, function(error){});
}]);

app.factory('socket', function (socketFactory) {

  var connect = io.connect('//'+ip+':'+port+'/');
  socket = socketFactory({
    ioSocket: connect
  });
  return socket;
});


app.controller('GeneralController', ['$scope', '$http', '$interval', 'socket', function($scope, $http, $interval, socket){
	$scope.countCart = 0;
	socket.emit('user connected', uid);
}])

app.controller('CheckoutController', ['$scope', '$http', function($scope, $http){
	$scope.checkoutPageTitle = 'Finalizar Compra';
}])

function reqGet(url, content){

  var req = {
    method: 'GET',
    url: url,
    headers: {
      'Content-Type': content
    }
  }

  return req;

}