var ip = document.getElementById('pn').dataset.address;
var port = document.getElementById('pn').dataset.port;

var socket = io.connect('//' + ip + ':' + port + '/');


$(document).ready(function () {

	$("input[type='checkbox'], input[type='radio']").iCheck({

		checkboxClass: 'icheckbox_square-orange',

		radioClass: 'iradio_square-orange',

		increaseArea: '100%'

	});



	if ($("#checkout").length > 0 && $(".creditcard").length > 0) {

		var card = new Card({



			form: '#checkout',

			container: '.creditcard',



			placeholders: {

				number: '•••• •••• •••• ••••',

				name: 'Nome titular',

				expiry: '••/••',

				cvc: '•••'

			}



		})

	}



	$(document).on('click', '.calculate', function (e) {

		e.preventDefault();

        var money = $("input[name='receiver']").val();
        var delivery = 0;
        if ($("#address_types").find('.checked').length) {
            var id = $("#address_types .checked input").val();
            var delivery = $("#taxa_" + id).attr("data-pricetax");
        }
		if (money.length > 0 && money !== '') {

			$.post('ajaxReq/d1546e9584228a6c0a3c22a55518d0addb1a574f', { 'money': money, 'delivery': delivery }, function (response) {



				if (response.error == true) {

					swal("O valor total do pedido é: R$" + response.total + "", "A valor digitado precisa ser igual ou superior.", "warning");

				} else {

					$("input[name='send']").val(response.send);

				}

			}, 'json');

		}

	});



	$('.choose-address').on('ifChecked', function (event) {



		var id = $(this).attr('id');

		$(".tabs-address").hide();

		$("#tabs-" + id).show();



	});



	if ($("#address_types").find('.checked').length) {
		var id = $("#address_types .checked input").val();
		calculate_anddress(id);
	}

	$("input[name='chooseAddress']").on('ifChecked', function (e) {
		e.preventDefault();
		var id = $(this).val();
		calculate_anddress(id);
	});

	function calculate_anddress(id) {
		$.post('sales/address_calculate', { 'id': id }, function (response) {
			var shipping = response.shipping;
			$("#taxa_" + id).html("<strong>Entrega: R$" + shipping + "</strong>");
			$("#taxa_" + id).attr("data-pricetax", shipping);
		}, 'json');
	}


	$("input[name='deliveryaddress']").on('ifChecked', function (e) {
		e.preventDefault();
		var content = $(this).val();
		if (content == 'true') {
			$("#address_types input").removeAttr('disabled');
			$("#address_types").show();
		} else {
			$("#address_types input").attr('disabled', 'disabled');
			$("#address_types").hide();
		}
	});


	$('input[name="payment"]').on('ifChecked', function (event) {



		if ($(this).is(':checked')) {

			var id = $(this).attr('id');

			$(".tabs-payment").hide('fast');

			$("#tab-" + id).show('fast');



			$(".tabs-payment input").removeAttr('required');



			if (!$('#tab_pagseguro').css('display') == 'none') {

				$("#tab_pagseguro input").attr('required', 'required');

			} else {

				$("#tab-" + id + " input").attr('required', 'required');

			}



		}



	});



	$('input[name="creditcardType"]').on('ifChecked', function (event) {



		if ($(this).is(':checked')) {

			var id = $(this).attr('id');

			$(".creditcard_type").hide('fast');

			$("#tab-" + id).show('fast');



			if (!$('#select_brands').css('display') == 'none') {

				$(".creditcard_type select").removeAttr('required');

				$("#tab-" + id + " select").attr('required', 'required');

			} else {

				$(".creditcard_type input").removeAttr('required');

				$("#tab-" + id + " input").attr('required', 'required');

			}



		}



	});





	$('input[name="address"]').on('ifChecked', function (event) {



		if ($(this).is(':checked')) {

			var id = $(this).attr('id');

			$(".tabs-address").hide('fast');

			$("#tabs-" + id).show('fast');

			if (id == 'another') {

				$(".tabs-address input").attr('required', 'required');

			} else {

				$(".tabs-address input").removeAttr('required');

			}

		}



	});



	$('.btn-loader').on('click', function () {
		var money = document.getElementById("tab-delivery");
		var $this = $(this);
		if (money.style.display !== "none") {
			if ($('input[name="receiver"]').val() != '') {
				$this.button('loading');
			} else {
				swal("Valor total está vázio!", "", "warning");
			}
		} else {
			if ($('[name="creditcardType"]:checked').val() !== 'shipping') {
				var number = $('input[name="number"]').val();
				var name = $('input[name="name"]').val();
				var expiry = $('input[name="expiry"]').val();
				var cvc = $('input[name="cvc"]').val();
				var birthday = $('input[name="birthday"]').val();
				var cpf = $('input[name="cpf"]').val();
				if (number != '' && name != '' && expiry != '' && cvc != '' && birthday != '' && cpf != '') {
					$this.button('loading');
				} else {
					swal("Preechar todos os dados do cartão!", "", "warning");
				}
			}
		}

	});



	$(document).on('submit', '#checkout', function (e) {

		e.preventDefault();

		var $this = $(this);



		var cform = $("#checkout");

		var form = $this.serialize();

		$.post("ajaxReq/9f73911323c61359ac276f39934e6f60ce53d032", form, function (response) {

			if (response.checkout == true) {

				$(".btn-loader").button('reset');

				//$.magnificPopup.close(); 

				$("#processOrder").empty();

				cform[0].reset();

				cform.empty();



				display__settimeout(response.order);

				var deliveryTime = (response.shipping) ? '<p class="text-center">O seu pedido chegará em <strong>' + response.shippingDuration + '</strong> minutos. <br /> A taxa de envio para o seu bairro é de <strong>R$' + response.shipping + '</strong>.</p>' : '';

				var timer = setTimeout(function () {

					$('#lightbox--process_order').html('<p class="text-center">O seu pedido não foi atendido ainda!</p> <div id="processOrder" class="text-center"><button type="button" id="try_order" data-id="' + response.order + '" class="btn btn-primary">Tentar novamente</button></div>');

				}, 5 * 60 * 1000);



				socket.on('order status changed', function (order) {

					if (order.order == response.order) {

						clearTimeout(timer);

						switch (order.status) {

							case 1:

								cform.html('<div class="text-center"><h4>O seu pedido foi confirmado!</h4> ' + deliveryTime + ' <img src="assets/images/icon-check.png" width="30" height="30" /> <p>O seu pedido já está sendo preparado, iremos lhe notificar quando estiver pronto. Obrigado pela preferência!</p></div>');

								$('#lightbox--process_order').html('<p class="text-center">O seu pedido foi confirmado!</p> ' + deliveryTime + ' <div id="processOrder" class="text-center"><img src="assets/images/icon-check.png" width="30" height="30" /></div>');

								break;

							default:

								cform.html('<div class="text-center"><h4>O seu pedido foi cancelado!</h4> <img src="assets/images/close_red.png" width="30" height="30" /> <p>Por algum motivo, o seu pedido não pode ser confirmado, entraremos em contato com você para mais informações. Obrigado pela preferência!</p></div>');

								$('#lightbox--process_order').html('<p class="text-center">O seu pedido foi cancelado!</p> <div id="processOrder" class="text-center"><img src="assets/images/close_red.png" width="30" height="30" /></div>');

								break;

						}

					}

				});

			}



			if (!response.checkout == true && response.code != 200) {

				$(".btn-loader").button('reset');

				$('#lightbox--process_order').html('<p class="text-center">Transação não aprovada!</p> <div id="processOrder" class="text-center"><img src="assets/images/close_red.png" width="30" height="30" /><p>Parece que seu cartão não tem limite suficiênte ou está bloqueado, tente novamente!</p></div>');

			}

		}, 'json');



	});



	$(document).on('change', '#cupom', function (e) {

		e.preventDefault();

		var cupom = $(this).val();

		$.post('ajaxReq/1448e334afd96e01949ec3bc59598d3f0ea0c010', { cupom: cupom }, function (response) {

			switch (response.status) {

				case 'used':

					swal('Você já usou este cupom!', '', 'warning');

					$("#cupom").val('');

					break;

				case 'expired':

					swal('O cupom já expirou, desculpe!', '', 'warning');

					$("#cupom").val('');

					break;

				case 'invalid':

					swal('O cupom é inválido, tente outro!', '', 'warning');

					$("#cupom").val('');

					break;

				default:

					swal('Verificado!', 'O cupom é válido até ' + response.expire + ', aproveite!', 'warning');

					break;

			}

		}, 'json');

	});



	$(document).on('click', '#try_order', function (e) {

		e.preventDefault();

		var id = $(this).data('id');

		$.magnificPopup.close();

		display__settimeout(id);

	});



	!function (e, t) { "function" == typeof define && define.amd ? define("ev-emitter/ev-emitter", t) : "object" == typeof module && module.exports ? module.exports = t() : e.EvEmitter = t() }("undefined" != typeof window ? window : this, function () { function e() { } var t = e.prototype; return t.on = function (e, t) { if (e && t) { var i = this._events = this._events || {}, n = i[e] = i[e] || []; return n.indexOf(t) == -1 && n.push(t), this } }, t.once = function (e, t) { if (e && t) { this.on(e, t); var i = this._onceEvents = this._onceEvents || {}, n = i[e] = i[e] || {}; return n[t] = !0, this } }, t.off = function (e, t) { var i = this._events && this._events[e]; if (i && i.length) { var n = i.indexOf(t); return n != -1 && i.splice(n, 1), this } }, t.emitEvent = function (e, t) { var i = this._events && this._events[e]; if (i && i.length) { i = i.slice(0), t = t || []; for (var n = this._onceEvents && this._onceEvents[e], o = 0; o < i.length; o++) { var r = i[o], s = n && n[r]; s && (this.off(e, r), delete n[r]), r.apply(this, t) } return this } }, t.allOff = function () { delete this._events, delete this._onceEvents }, e }), function (e, t) { "use strict"; "function" == typeof define && define.amd ? define(["ev-emitter/ev-emitter"], function (i) { return t(e, i) }) : "object" == typeof module && module.exports ? module.exports = t(e, require("ev-emitter")) : e.imagesLoaded = t(e, e.EvEmitter) }("undefined" != typeof window ? window : this, function (e, t) { function i(e, t) { for (var i in t) e[i] = t[i]; return e } function n(e) { if (Array.isArray(e)) return e; var t = "object" == typeof e && "number" == typeof e.length; return t ? d.call(e) : [e] } function o(e, t, r) { if (!(this instanceof o)) return new o(e, t, r); var s = e; return "string" == typeof e && (s = document.querySelectorAll(e)), s ? (this.elements = n(s), this.options = i({}, this.options), "function" == typeof t ? r = t : i(this.options, t), r && this.on("always", r), this.getImages(), h && (this.jqDeferred = new h.Deferred), void setTimeout(this.check.bind(this))) : void a.error("Bad element for imagesLoaded " + (s || e)) } function r(e) { this.img = e } function s(e, t) { this.url = e, this.element = t, this.img = new Image } var h = e.jQuery, a = e.console, d = Array.prototype.slice; o.prototype = Object.create(t.prototype), o.prototype.options = {}, o.prototype.getImages = function () { this.images = [], this.elements.forEach(this.addElementImages, this) }, o.prototype.addElementImages = function (e) { "IMG" == e.nodeName && this.addImage(e), this.options.background === !0 && this.addElementBackgroundImages(e); var t = e.nodeType; if (t && u[t]) { for (var i = e.querySelectorAll("img"), n = 0; n < i.length; n++) { var o = i[n]; this.addImage(o) } if ("string" == typeof this.options.background) { var r = e.querySelectorAll(this.options.background); for (n = 0; n < r.length; n++) { var s = r[n]; this.addElementBackgroundImages(s) } } } }; var u = { 1: !0, 9: !0, 11: !0 }; return o.prototype.addElementBackgroundImages = function (e) { var t = getComputedStyle(e); if (t) for (var i = /url\((['"])?(.*?)\1\)/gi, n = i.exec(t.backgroundImage); null !== n;) { var o = n && n[2]; o && this.addBackground(o, e), n = i.exec(t.backgroundImage) } }, o.prototype.addImage = function (e) { var t = new r(e); this.images.push(t) }, o.prototype.addBackground = function (e, t) { var i = new s(e, t); this.images.push(i) }, o.prototype.check = function () { function e(e, i, n) { setTimeout(function () { t.progress(e, i, n) }) } var t = this; return this.progressedCount = 0, this.hasAnyBroken = !1, this.images.length ? void this.images.forEach(function (t) { t.once("progress", e), t.check() }) : void this.complete() }, o.prototype.progress = function (e, t, i) { this.progressedCount++ , this.hasAnyBroken = this.hasAnyBroken || !e.isLoaded, this.emitEvent("progress", [this, e, t]), this.jqDeferred && this.jqDeferred.notify && this.jqDeferred.notify(this, e), this.progressedCount == this.images.length && this.complete(), this.options.debug && a && a.log("progress: " + i, e, t) }, o.prototype.complete = function () { var e = this.hasAnyBroken ? "fail" : "done"; if (this.isComplete = !0, this.emitEvent(e, [this]), this.emitEvent("always", [this]), this.jqDeferred) { var t = this.hasAnyBroken ? "reject" : "resolve"; this.jqDeferred[t](this) } }, r.prototype = Object.create(t.prototype), r.prototype.check = function () { var e = this.getIsImageComplete(); return e ? void this.confirm(0 !== this.img.naturalWidth, "naturalWidth") : (this.proxyImage = new Image, this.proxyImage.addEventListener("load", this), this.proxyImage.addEventListener("error", this), this.img.addEventListener("load", this), this.img.addEventListener("error", this), void (this.proxyImage.src = this.img.src)) }, r.prototype.getIsImageComplete = function () { return this.img.complete && this.img.naturalWidth }, r.prototype.confirm = function (e, t) { this.isLoaded = e, this.emitEvent("progress", [this, this.img, t]) }, r.prototype.handleEvent = function (e) { var t = "on" + e.type; this[t] && this[t](e) }, r.prototype.onload = function () { this.confirm(!0, "onload"), this.unbindEvents() }, r.prototype.onerror = function () { this.confirm(!1, "onerror"), this.unbindEvents() }, r.prototype.unbindEvents = function () { this.proxyImage.removeEventListener("load", this), this.proxyImage.removeEventListener("error", this), this.img.removeEventListener("load", this), this.img.removeEventListener("error", this) }, s.prototype = Object.create(r.prototype), s.prototype.check = function () { this.img.addEventListener("load", this), this.img.addEventListener("error", this), this.img.src = this.url; var e = this.getIsImageComplete(); e && (this.confirm(0 !== this.img.naturalWidth, "naturalWidth"), this.unbindEvents()) }, s.prototype.unbindEvents = function () { this.img.removeEventListener("load", this), this.img.removeEventListener("error", this) }, s.prototype.confirm = function (e, t) { this.isLoaded = e, this.emitEvent("progress", [this, this.element, t]) }, o.makeJQueryPlugin = function (t) { t = t || e.jQuery, t && (h = t, h.fn.imagesLoaded = function (e, t) { var i = new o(this, e, t); return i.jqDeferred.promise(h(this)) }) }, o.makeJQueryPlugin(), o });

	$(document).on('click', ".mfp-close", function (e) {

		$(".title_page").hide('fast');

		var $content = $("#continue-promotions");

		var $gallery = $(".isotope");

		$content.show('fast');

		$content.prepend('<h5>Você pode comprar também:</h5>');

		$.get('continue', function (dataResponse) {



			itemsHTML = $.map(dataResponse, function (response) {

				var element = '<div id="product_' + response.id + '" data-filter="Category ' + response.category_id + '" class="col-xs-12 col-sm-6 col-md-4 isotope-item">';

				element = element + '<a href="' + response.url + '" data-photo-swipe-item="" data-size="1200x800" class="thumbnail thumbnail-variant-1">';

				element = element + '<figure><img src="' + response.thumbnail + '" height="278"> </figure>';

				element = element + '<div class="caption">';

				element = element + '<h5>' + response.name + '</h5>';

				element = element + '<hr class="hr divider-xs bg-white">';

				element = element + '<p>' + response.description + ' <br /> <strong>' + response.price + '</strong></p>';

				element = element + '<svg x="0px" y="0px" width="35px" height="35px" viewBox="0 0 127.725 127.65"><g> <defs> <rect y="0" width="127.725" height="127.65" class="SVGID_1"></rect> </defs> <clipPath class="SVGID_2_"> <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href=".SVGID_1_" overflow="visible"></use> </clipPath> <path clip-path="url(.SVGID_2_)" d="M58.613,117.275h-0.004c-15.646-0.001-30.359-6.09-41.43-17.141    C-5.702,77.289-5.73,40.088,17.115,17.208C28.193,6.112,42.93,0,58.61,0c15.645,0,30.358,6.089,41.43,17.142    c22.881,22.845,22.91,60.045,0.065,82.927C89.027,111.165,74.29,117.275,58.613,117.275 M58.61,5.235    c-14.28,0-27.701,5.565-37.79,15.672C0.014,41.744,0.041,75.623,20.879,96.427c10.082,10.067,23.482,15.612,37.731,15.612h0.003    c14.278,0,27.697-5.564,37.788-15.669c20.805-20.838,20.778-54.717-0.059-75.522C86.258,10.781,72.858,5.235,58.61,5.235"></path> <path clip-path="url(.SVGID_2_)" d="M125.108,127.65c-0.669,0-1.339-0.255-1.85-0.766l-26.855-26.812    c-1.023-1.022-1.024-2.678-0.002-3.702c1.02-1.024,2.678-1.025,3.701-0.003l26.856,26.812c1.022,1.022,1.023,2.68,0.001,3.702    C126.449,127.394,125.779,127.65,125.108,127.65"></path> <path clip-path="url(.SVGID_2_)" d="M58.636,96.435c-1.444,0-2.616-1.171-2.617-2.615l-0.054-70.361    c-0.002-1.445,1.17-2.618,2.615-2.618h0.002c1.445,0,2.617,1.17,2.618,2.614l0.054,70.361c0.001,1.445-1.17,2.618-2.616,2.619    H58.636z"></path> <path clip-path="url(.SVGID_2_)" d="M23.428,61.283c-1.444,0-2.616-1.17-2.617-2.615    c-0.002-1.447,1.169-2.618,2.615-2.619l70.36-0.056h0.002c1.446,0,2.617,1.171,2.618,2.615c0.001,1.446-1.169,2.618-2.616,2.62    l-70.36,0.055H23.428z"></path> </g> </svg>';

				element = element + '</div></a></div>';

				return element;

			});



			var $items = $(itemsHTML.join(''));

			$gallery.imagesLoaded(function () {

				$gallery.append($items).isotope('addItems', $items);

			});

			//$gallery.isotope( 'insert', $items );



		});

	});



});



function display__settimeout(order_id) {

	$("#lightbox--process_order p").remove();

	$("#processOrder").empty();

	$.magnificPopup.open({

		items: {

			src: '#lightbox--process_order',

			midClick: true,

			closeOnBgClick: false,

			closeOnContentClick: false,

			mainClass: 'mfp-3d-unfold'

		},

		type: 'inline'

	});



	var time = 5 * 60 * 1000;

	var bar = new ProgressBar.Line(processOrder, {

		strokeWidth: 4,

		easing: 'easeInOut',

		duration: time,

		color: '#FFEA82',

		trailColor: '#eee',

		trailWidth: 1,

		svgStyle: { width: '100%', height: '100%' }

	});

	bar.animate(1.0);



	$("#processOrder").prepend('<p>O seu pedido está sendo processado, aguarde um momento!</p>');



	/*setTimeout(function(){

		$.magnificPopup.close(); 

		swal('Tempo esgotado!', 'Pedimos desculpas pela demora, mas não conseguimos visualizar o seu pedido ainda. Por que não tenta mais uma vez?', 'warning');

		$("#processOrder").empty();

	}, time);*/



	socket.emit('checkout finish', order_id);

}