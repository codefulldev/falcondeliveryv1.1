window.addEventListener('load', function() {
    var editor;

	ContentTools.StylePalette.add([
		new ContentTools.Style('Muted', 'text-muted', ['p']),
		new ContentTools.Style('Primary', 'text-primary', ['p']),
		new ContentTools.Style('Success', 'text-success', ['p'])
	]);

	editor = ContentTools.EditorApp.get();
	editor.init('*[data-editable]', 'data-name');

	editor.addEventListener('saved', function (ev) {
		var name, payload, regions, xhr;


		regions = ev.detail().regions;
		if (Object.keys(regions).length == 0) {
			return;
		}

		this.busy(true);

		payload = new FormData();
		for (name in regions) {
			if (regions.hasOwnProperty(name)) {
				payload.append(name, regions[name]);
			}
		}

		function onStateChange(ev) {

			if (ev.target.readyState == 4) {
				editor.busy(false);
				if (ev.target.status == '200') {
					new ContentTools.FlashUI('ok');
					console.log(ev.target.response);
				} else {
					new ContentTools.FlashUI('no');
				}
			}
		};

		xhr = new XMLHttpRequest();
		xhr.addEventListener('readystatechange', onStateChange);
		xhr.open('POST', 'ajaxReq/cdc24e171f844992084e2128874d16cd7112df50');
		xhr.send(payload);
	});

});