var ip = document.getElementById('pn').dataset.address;
var port = document.getElementById('pn').dataset.port;

var socket = io.connect('//'+ip+':'+port+'/');



$('.chat-left-inner > .chatonline').slimScroll({
    
    height: '7000px',
    position: 'right',
    size: "0px",
    color: '#dcdcdc',

});



$(function(){



    $(document).on('click', '.do-you-need-help', function(e){

        $("#start__messages__chat").fadeToggle('fast');

        if ($(".chat-list li").length == 0) {

            var date = new Date;

            var element = '<li id="box__automatic" data-chat="0">';

            element = element + '<div class="chat-image"> <img alt="Mensagem Automática" src="assets/admin/img/avatar/no.jpg"> </div>';

            element = element + '<div class="chat-body">';

            element = element + '<div class="chat-text">';

            element = element + '<h4>Mensagem automática</h4>';

            element = element + '<p> Em que podemos ajuda-lo? </p> <b><span class="time" title="'+ date +'"></span></b> </div>';

            element = element + '</div>';

            element = element + '</li>';



            $(".chat-list").append(element);

            $(".time").timeago();

            $(this).hide();



        }



        var wtf = $('#start__messages__chat ul');

        var height = wtf[0].scrollHeight;

        wtf.scrollTop(height);

    });



    $(document).on("click", ".close-support-chat", function(e){

        $("#start__messages__chat").hide();

        $(".do-you-need-help").show();

        if ($("#box__automatic").length > 0) {

            $("#box__automatic").remove();

        }

    });



    socket.on('client receive message', function(uid){

      if($(".user_logged__"+uid).length > 0){

        $.post('chatting/client__notify', {client: uid}, function(response) {



            if ($("#start__messages__chat").css('display') == 'block') {

                $('#notifyAudio').remove();

                $('body').append('<audio id="notifyAudio"><source src="assets/sounds/conversation-tone-chat.ogg" type="audio/ogg"><source src="assets/sounds/conversation-tone-chat.mp3" type="audio/mpeg"><source src="assets/sounds/conversation-tone-chat.m4r" type="audio/m4r"></audio>');

                $('#notifyAudio')[0].play();

            }else{

                $('#notifyAudio').remove();

                $('body').append('<audio id="notifyAudio"><source src="assets/sounds/you-have-new-message.ogg" type="audio/ogg"><source src="assets/sounds/you-have-new-message.mp3" type="audio/mpeg"><source src="assets/sounds/you-have-new-message.m4r" type="audio/m4r"></audio>');

                $('#notifyAudio')[0].play();

                $("#start__messages__chat").show();

                $(".do-you-need-help").hide();

            }



            var element = '<li id="box__'+ response.message_id +'" data-chat="'+ response.admin_id +'">';

            element = element + '<div class="chat-image"> <img alt="'+ response.admin_name +'" src="'+ response.admin_thumbnail +'"> </div>';

            element = element + '<div class="chat-body">';

            element = element + '<div class="chat-text">';

            element = element + '<h4>'+ response.admin_name +'</h4>';

            element = element + '<p> '+ response.message +' </p> <b><span class="time" title="'+ response.date +'"></span></b> </div>';

            element = element + '</div>';

            element = element + '</li>';



            $(".chat-list").append(element);

            $(".time").timeago();

            document.getElementById('box__'+ response.message_id +'').scrollIntoView();

            $('#box__'+ response.message_id +'').shake();



        }, 'json');

    }

});



    $(document).on('click', '#sendbutton', function(e){

        e.preventDefault();

        var input = $("#sendtextarea").val();

        if (input.length > 0) {

            $.post('chatting/client__send', {input: input}, function(response) {



                socket.emit('client send message', response.uid);

                var element = '<li id="box__'+ response.message_id +'" data-chat="'+ response.uid +'" class="odd">';

                element = element + '<div class="chat-image"> <img alt="'+ response.name +'" src="'+ response.thumbnail +'"> </div>';

                element = element + '<div class="chat-body">';

                element = element + '<div class="chat-text">';

                element = element + '<h4>'+ response.name +'</h4>';

                element = element + '<p> '+ input +' </p> <b><span class="time" title="'+ response.date +'"></span></b> </div>';

                element = element + '</div>';

                element = element + '</li>';



                $(".chat-list").append(element);

                $("#sendtextarea").val('');

                $(".time").timeago();

                document.getElementById('box__'+ response.message_id +'').scrollIntoView();



            }, 'json');



        }

    });





});



$(function(){

    $(window).load(function(){ 

        $('.chat-list').css({'height':(($(window).height())-470)+'px'});

    });

    $(window).resize(function(){

        $('.chat-list').css({'height':(($(window).height())-470)+'px'});

    });

});



$(function() {

    $(window).load(function() {

        $('.chat-left-inner').css({

            'height': (($(window).height()) - 240) + 'px'

        });

    });

    $(window).resize(function() {

        $('.chat-left-inner').css({

            'height': (($(window).height()) - 240) + 'px'

        });

    });

});





$(".open-panel").click(function() {

    $(".chat-left-aside").toggleClass("open-pnl");

    $(".open-panel i").toggleClass("ti-angle-left");

});