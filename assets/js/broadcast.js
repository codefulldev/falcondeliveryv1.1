$(document).ready(function() {

	

	var ip = document.getElementById('pn').dataset.address;
	var port = document.getElementById('pn').dataset.port;
	var socket = io.connect('//'+ip+':'+port+'/');

	var msgs = [];

	var count__msg = 1;



	if ($(".window-chatting_is__opening").length > 0) {



		var wtf = $('.slimscroll-chat');



		if($(window).width() < 700 ) {

		 	var slimScrollRail = (($(window).height())-470);

		 	wtf.slimScroll({

				height: '210px',

				start: 'bottom',

				railVisible: true,

    			alwaysVisible: true,

    			scrollTo: '210px'

			});

		}else{

			var slimScrollRail = wtf.offsetHeight;

			wtf.slimScroll({

				height: '289px',

				start: 'bottom',

				railVisible: true,

    			alwaysVisible: true,

    			scrollTo: '299px'

			});

		}



	}

	

	socket.on('admin receive message', function(id){

		msgs[id] = count__msg++;

		$.post('chatting/client__user', {id: id}, function(response) {



			if ($("#window__starting__" + id).length > 0) {

				$.post('chatting/client_message', {id: id}, function(request){



					$('#conversationAudio').remove();

					$('body').append('<audio id="conversationAudio"><source src="assets/sounds/conversation-tone-chat.ogg" type="audio/ogg"><source src="assets/sounds/conversation-tone-chat.mp3" type="audio/mpeg"><source src="assets/sounds/conversation-tone-chat.m4r" type="audio/m4r"></audio>');

					$('#conversationAudio')[0].play();



					var element = '<li id="box__'+ request.message_id +'" data-chat="'+ request.uid +'">';

					element = element + '<div class="chat-image"> <img alt="'+ request.name +'" src="'+ request.thumbnail +'"> </div>';

					element = element + '<div class="chat-body">';

					element = element + '<div class="chat-text">';

					element = element + '<h4><strong>'+ request.name +'</strong></h4>';

					element = element + '<p> '+ request.message +' </p> <b><span class="time" title="'+ request.date +'"></span></b> </div>';

					element = element + '</div>';

					element = element + '</li>';



					$("#content__chatting__" + id).append(element);



					$('body').append('<audio id="notifyAudio"><source src="assets/sounds/solemn.ogg" type="audio/ogg"><source src="assets/sounds/solemn.mp3" type="audio/mpeg"><source src="assets/sounds/solemn.m4r" type="audio/m4r"></audio>');

					$('#notifyAudio')[0].play();



				}, 'json');

			}else{

				var element = '<div id="notification__new_message" role="notification__window--'+ response.id +'" class="myadmin-alert myadmin-alert-img alert-info myadmin-alert-bottom-right">'; 

				element = element + '<img src="'+ response.thumbnail +'" class="img" alt="img"><a href="#" data-id="'+ response.id +'" class="closed">&times;</a>';

				element = element + '<h4>Nova mensagem!</h4> <b>'+ response.name +'</b> enviou uma nova mensagem.</div>';

				$('body').append(element);



				$("#notification__new_message").fadeToggle(350);

				$('body').append('<audio id="notifyAudio"><source src="assets/sounds/solemn.ogg" type="audio/ogg"><source src="assets/sounds/solemn.mp3" type="audio/mpeg"><source src="assets/sounds/solemn.m4r" type="audio/m4r"></audio>');

				$('#notifyAudio')[0].play();

				setTimeout(function(){

					$("#notification__new_message").fadeOut('fast');

				}, 8000);

			}



		}, 'json');

		$("#notify__count").html(msgs[id]);



	});



	$(document).on('click', ".closed", function(e){

		e.preventDefault();

		var id = $(this).data('id');

		$('[role="notification__window--'+ id +'"]').fadeOut(350);

	});



	$(document).on('click', '#sendbutton', function(e){

		e.preventDefault();

		var id = $(this).data('client');

		var input = $("#sendtextarea").val();

		if (input.length > 0) {

			$.post('chatting/admin__send', {input: input, client: id}, function(response) {

				socket.emit('admin send message', id);

				var element = '<li id="box__'+ response.message_id +'" data-chat="'+ response.uid +'" class="odd">';

				element = element + '<div class="chat-image"> <img alt="'+ response.name +'" src="'+ response.thumbnail +'"> </div>';

				element = element + '<div class="chat-body">';

				element = element + '<div class="chat-text">';

				element = element + '<h4><strong>'+ response.name +'</strong></h4>';

				element = element + '<p> '+ input +' </p> <b><span class="time" title="'+ response.date +'"></span></b> </div>';

				element = element + '</div>';

				element = element + '</li>';



				$(".chat-list").append(element);

				$("#sendtextarea").val('');

				$(".time").timeago();

				document.getElementById('box__'+ response.message_id +'').scrollIntoView();



			}, 'json');



		}

	});



	socket.emit('get connections');

	socket.on('users connections', function(connections){

		if (connections.length > 0) {

			$.post('chatting/client__list', {connections: connections}, function(obj) {



				$(".chat-user__li").remove();

				$.each(obj, function(index, response) {

					var status = (response.status == true) ? '<small class="text-success">online</small>' : '<small class="text-muted">offline</small>';

					var element = '<li id="box__'+ response.id +'" class="chat-user__li"><a href="'+ response.url +'">';

					element = element + '<img src="'+ response.thumbnail +'" alt="'+ response.name +'" class="img-circle">'; 

					element = element + '<span>'+ response.name +' <span id="notify__count" class="label label-rouded label-danger pull-right"></span> '+ status +' </span></a></li>';

					$("#sidebar_chatonline").append(element);



					if ($("#listOnlineUsers").length > 0) {

						$("#status_client__" + response.id).removeClass('text-muted');

						$("#status_client__" + response.id).addClass('text-success');

						$("#status_client__" + response.id).html('online');

					}



				});



			}, 'json');

		}

	});



	socket.on('close connection', function(uid) {

		$("#box__" + uid).fadeOut('fast');

		if ($("#listOnlineUsers").length > 0) {

			$("#status_client__" + uid).removeClass('text-success');

			$("#status_client__" + uid).addClass('text-muted');

			$("#status_client__" + uid).html('offline');

		}

	});

	

	var count = 0;

	socket.on('new order', function(response){



		var timer = 5 * 60 * 1000;



		$('body').append('<audio id="notifyAudio"><source src="assets/sounds/solemn.ogg" type="audio/ogg"><source src="assets/sounds/solemn.mp3" type="audio/mpeg"><source src="assets/sounds/solemn.m4r" type="audio/m4r"></audio>');

		$('#notifyAudio')[0].play();



		$(".notify").removeClass('hidden');

		setTimeout( function(){

			$(".notify").addClass('hidden');

		}, timer);



		count++;

		$(".count--orders").html(count);



		$.post('ajaxReq/1def459b970d1a119806f3a044fc5acbdac9c029', {'id': response}, function(request) {



			var order = request[0].order;

			var client = request[1].client;

			var thumbnail = (client.thumbnail != null) ? 'assets/clients/'+ client.thumbnail : 'assets/clients/no.jpg';



			var element = '<div id="notify__order_'+ order.id +'" class="message-center">';

			element = element + '<a href="#">';

			element = element + '<div class="user-img"><img src="'+ thumbnail +'" alt="user" class="img-circle">'; 

			element = element + '<span class="profile-status online pull-right"></span> </div>';

			element = element + '<div class="mail-contnet">';

			element = element + '<h5>'+ client.name +' <button type="button" data-toggle="tooltip" data-original-title="Aceitar pedido" class="action__order btn btn-success btn-xs" data-types="accept" data-order="'+ order.id +'" data-content="#notify__order_'+ order.id +'" data-id="'+ order.id +'"><i class="fa fa-check"></i></button> <button type="button" data-toggle="tooltip" data-original-title="Recusar pedido" class="action__order btn btn-danger btn-xs" data-types="decline" data-order="'+ order.id +'" data-content="#notify__order_'+ order.id +'" data-id="'+ order.id +'"><i class="fa fa-times"></i></button></h5> <span class="mail-desc">Um novo pedido</span> <span class="time" title="'+ order.date +'></span>';

			element = element + '</div>';

			element = element + '</a>';

			element = element + '</div>';



			$(element).prependTo("#list--news_orders").delay(timer).queue(function() { 

				$(this).remove(); 

				count--;

				$(".count--orders").html(count);

			});

			$(".time").timeago();



		}, 'json');





	});



	$(document).on('click', '.cupomPrinter', function(e){

		e.preventDefault();

		var id     = $(this).data('id');

		var via    = $(this).data('via');

		var where  = $(this).data('where');

		var amount = $(this).data('amount');

		$.post('cupomPrinter', {'invoice': id, 'via': via, 'where': where, 'amount': amount}, function(response){

			w=window.open();

			w.document.write(response);

			w.print();

			w.close();

		});

	});



	$(document).on('click', '.action__order', function(e){

		e.preventDefault();

		var order   = $(this).data('order');

		var types   = $(this).data('types');

		var content = $(this).data('content');

		var status  = (types == 'decline') ? 2 : 1;

		count--;



		$.post('ajaxReq/13107df1de65ac459e9ce39ee2d304ba739ae08e',{'id':order, 'status': status}, function(response){

			if (response.error == false) {

				$(content).fadeOut('fast');

				$(".count--orders").html(count);

				socket.emit('order status', {'status': status, 'order': order});

			}

		}, 'json');



	});



});