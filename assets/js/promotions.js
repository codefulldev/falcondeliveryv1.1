$(document).on('submit', '[role="add_to_cart"]', function(e){
	e.preventDefault();
	$.post('ajaxReq/254a65f77e8113e584552c28da18bea2e940432f', $(this).serialize(), function(response){

		if(response.added == true){
			swal({   
				title: "Promoção adicionada!",   
				text: "Finalize seu pedido agora mesmo!",   
				type: "success",   
				showCancelButton: true,   
				confirmButtonColor: "#DD6B55",   
				confirmButtonText: "Ir para o carrinho",   
				cancelButtonText: "Voltar para o inicio",   
				closeOnConfirm: false 
			}, function(isConfirm){   
				if(!isConfirm){
					window.location.href = response.cart;
				}else{
					window.location.href = response.uri;
				}
			});
			$('[role="add_to_cart"]')[0].reset();
		}

	},'json');
});