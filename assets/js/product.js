$(function () {
	$('#datetimepicker2').datetimepicker({
		locale: 'pt-BR'
	});
});

const elt = $('#ingredients');

if($("#ingredients").data('ingredients') !== ''){
	var value = $("#ingredients").data('ingredients');
	$.post('ajaxReq/a79e7133d9318261a9d8098804a4df1b9a3dca68', {'value': value}, function(response){

		elt.tagsinput({ 
			allowDuplicates: false,
			tagClass: 'label label-danger',
			itemValue: 'id',
			itemText: 'name'
		});
		
		$.each(response, function(index, val) {
			elt.tagsinput('add', response[index]);
		});

		$(".bootstrap-tagsinput").attr('style', 'background:none;border:none;box-shadow:none;');
		$(".bootstrap-tagsinput input").remove();

	}, 'json');
}

$(document).on('submit', '[role="add_to_cart"]', function(e){
	e.preventDefault();
	$.post('ajaxReq/254a65f77e8113e584552c28da18bea2e940432f', $(this).serialize(), function(response){

		if(response.added == true){
			swal({   
				title: "Produto adicionado",   
				text: "Seu pedido foi realizado com sucesso!",   
				type: "success",   
				showCancelButton: true,   
				confirmButtonColor: "#DD6B55",   
				confirmButtonText: "Finalizar pedido",   
				cancelButtonText: "Continuar comprando",   
				closeOnConfirm: false 
			}, function(isConfirm){   
				if(isConfirm){
					window.location.href = response.uri;
				}else{
					window.location.href = response.cart;
				}
			});
			$('[role="add_to_cart"]')[0].reset();
		}

	},'json');
});

$("input[type='checkbox'], input[type='radio']").iCheck({
	checkboxClass: 'icheckbox_square-orange',
	radioClass: 'iradio_square-orange',
    increaseArea: '100%' // optional
});

$(document).ready(function () {
    var itemsMainDiv = ('.MultiCarousel');
    var itemsDiv = ('.MultiCarousel-inner');
    var itemWidth = "";

    $('.leftLst, .rightLst').click(function () {
        var condition = $(this).hasClass("leftLst");
        if (condition)
            click(0, this);
        else
            click(1, this)
    });

    ResCarouselSize();

    $(".fancybox").fancybox({
    	openEffect: "none",
    	closeEffect: "none"
    });

    $(window).resize(function () {
        ResCarouselSize();
    });

    //this function define the size of the items
    function ResCarouselSize() {
        var incno = 0;
        var dataItems = ("data-items");
        var itemClass = ('.item');
        var id = 0;
        var btnParentSb = '';
        var itemsSplit = '';
        var sampwidth = $(itemsMainDiv).width();
        var bodyWidth = $('body').width();
        $(itemsDiv).each(function () {
            id = id + 1;
            var itemNumbers = $(this).find(itemClass).length;
            btnParentSb = $(this).parent().attr(dataItems);
            itemsSplit = btnParentSb.split(',');
            $(this).parent().attr("id", "MultiCarousel" + id);


            if (bodyWidth >= 1200) {
                incno = itemsSplit[3];
                itemWidth = sampwidth / incno;
            }
            else if (bodyWidth >= 992) {
                incno = itemsSplit[2];
                itemWidth = sampwidth / incno;
            }
            else if (bodyWidth >= 768) {
                incno = itemsSplit[1];
                itemWidth = sampwidth / incno;
            }
            else {
                incno = itemsSplit[0];
                itemWidth = sampwidth / incno;
            }
            $(this).css({ 'transform': 'translateX(0px)', 'width': itemWidth * itemNumbers });
            $(this).find(itemClass).each(function () {
                $(this).outerWidth(itemWidth);
            });

            $(".leftLst").addClass("over");
            $(".rightLst").removeClass("over");

        });
    }


    //this function used to move the items
    function ResCarousel(e, el, s) {
        var leftBtn = ('.leftLst');
        var rightBtn = ('.rightLst');
        var translateXval = '';
        var divStyle = $(el + ' ' + itemsDiv).css('transform');
        var values = divStyle.match(/-?[\d\.]+/g);
        var xds = Math.abs(values[4]);
        if (e == 0) {
            translateXval = parseInt(xds) - parseInt(itemWidth * s);
            $(el + ' ' + rightBtn).removeClass("over");

            if (translateXval <= itemWidth / 2) {
                translateXval = 0;
                $(el + ' ' + leftBtn).addClass("over");
            }
        }
        else if (e == 1) {
            var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
            translateXval = parseInt(xds) + parseInt(itemWidth * s);
            $(el + ' ' + leftBtn).removeClass("over");

            if (translateXval >= itemsCondition - itemWidth / 2) {
                translateXval = itemsCondition;
                $(el + ' ' + rightBtn).addClass("over");
            }
        }
        $(el + ' ' + itemsDiv).css('transform', 'translateX(' + -translateXval + 'px)');
    }

    //It is used to get some elements from btn
    function click(ell, ee) {
        var Parent = "#" + $(ee).parent().attr("id");
        var slide = $(Parent).attr("data-slide");
        ResCarousel(ell, Parent, slide);
    }

});