<p>As transações realizadas atráves de nossa plataforma, é completamente segura. Todo processo de pagamento realizado via cartão de crédito/débito, é processado pelo <a href="https://pagseguro.uol.com.br">PagSeguro</a> Checkout. <br>Os dados fornecidos não ficarão armazenados em nenhuma hípotese.</p>

<div class="form-group">
  <div class="creditcard" style="margin-bottom:20px;"></div>
  
  <div class="form-group">
    <input placeholder="Número do cartão" type="text" name="number" class="form-control">
  </div>

  <div class="form-group">
    <input placeholder="Nome titular" type="text" name="name" class="form-control">
  </div class="form-group">

  <div class="form-group">
    <input placeholder="Mês/Ano" type="text" name="expiry" class="form-control">
  </div>

  <div class="form-group">
    <input placeholder="Código de Segurança" type="text" name="cvc" class="form-control">
  </div>

  <div class="form-group">
    <input placeholder="CPF do titular" type="text" name="cpf" class="form-control">
  </div>

</div>