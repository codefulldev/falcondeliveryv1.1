<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$database = 'config/config.json';
$configs = json_decode(file_get_contents($database));

$active_group = 'default';
$query_builder = TRUE;

$db['default'] = array(
	'dsn'	=> '',
	'hostname' => isset($configs->host) ? $configs->host : '',
	'username' => isset($configs->user) ? $configs->user : '',
	'password' => isset($configs->pass) ? $configs->pass : '',
	'database' => isset($configs->db) ? $configs->db : '',
	'dbdriver' => 'mysqli',
	'dbprefix' => '',
	'pconnect' => FALSE,
	'db_debug' => (ENVIRONMENT !== 'production'),
	'cache_on' => FALSE,
	'cachedir' => '',
	'char_set' => 'utf8',
	'dbcollat' => 'utf8_general_ci',
	'swap_pre' => '',
	'encrypt' => FALSE,
	'compress' => FALSE,
	'stricton' => FALSE,
	'failover' => array(),
	'save_queries' => TRUE
);
