<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller']   = 'InitController';
$route['404_override']         = '';
$route['translate_uri_dashes'] = FALSE;

$route['installer']            = 'Installer';
$route['install']              = 'Ajax/installation';

$route['apis']                 = 'Ajax/apis';
$route['node']                 = 'Ajax/node';
$route['continue']             = 'InitController/continue';

$route['a088972718e5202c662b958ac389d6b8'] = 'Ajax/a088972718e5202c662b958ac389d6b8';
$route['maintenance']       = 'InitController/maintenance';
$route['comingsoon']        = 'InitController/comingsoon';
$route['funcionally']       = 'InitController/funcionally';
$route['device']    	    = 'InitController/device';

$route['product']           = 'InitController/product';
$route['product/(:num)']    = 'InitController/product';
$route['promotions/(:num)'] = 'InitController/promotions';
$route['cart']              = 'InitController/cart';
$route['checkout']          = 'InitController/checkout';
$route['account']           = 'InitController/client_account';
$route['orders']            = 'InitController/client_orders';
$route['address']           = 'InitController/client_address';
$route['address/remove']    = 'InitController/client_address_remove';
$route['settings']          = 'InitController/client_settings';
$route['settings/delete']   = 'InitController/client_settings';
$route['logout']            = 'InitController/logout';
$route['emails']            = 'InitController/emails';
$route['datetime']          = 'InitController/datetimes';
$route['client_invoice']    = 'InitController/client_invoice';
$route['menu']    			= 'InitController/menu';

// Window Sales PDV
$route['sales/get_product']           = 'Sales/get_product';
$route['sales/add_products']          = 'Sales/add_to_cart';
$route['sales/saleslist']          	  = 'Sales/sales_list';
$route['sales/remove_item_cart']      = 'Sales/remove_item_cart';
$route['sales/sales_total']           = 'Sales/sales_total';
$route['sales/complete_sale']         = 'Sales/complete_sale';
$route['sales/cancel']         		  = 'Sales/cancel';
$route['sales/back_money'] 	 		  = 'Sales/back_money';
$route['sales/change_quantity'] 	  = 'Sales/change_quantity_item_cart';
$route['sales/create_client'] 	  	  = 'Sales/create_client';
$route['sales/create_newaddress']  	  = 'Sales/create_newaddress_client';
$route['sales/address_calculate']  	  = 'Sales/address_calculate';
$route['sales/address_client'] 	  	  = 'Sales/address_client';
$route['sales/checkout_cupom_verify'] = 'Sales/checkout_cupom_verify';
$route['checkaddress'] = 'Sales/checked';

// Payments Methods
$route['payments/pagseguro'] = 'InitController/PagSeguroMethod';

// Chat Routers
$route['chatting/client__list']   = 'Ajax/chatting_client__list';
$route['chatting/client__notify'] = 'Ajax/chatting_client__notify';
$route['chatting/client__send']   = 'Ajax/chatting_client__send';
$route['chatting/client__user']   = 'Ajax/chatting_client__details';
$route['chatting/client_message'] = 'Ajax/chatting_client_message';
$route['chatting/admin__send']    = 'Ajax/chatting_admin__send';

$route['control']                = 'Admin';
$route['control/invoice/(:num)'] = 'Admin/ecommerce_invoice';
$route['control/chat']           = 'Admin/ChatPage';
$route['control/chat/(:num)']    = 'Admin/ChatPage';
$route['control/login']          = 'Admin/login';
$route['control/logout']         = 'Admin/logout';
$route['control/sales']          = 'Admin/sales';
$route['sales']          		 = 'Admin/sales';
$route['control/inbox']          = 'Admin/inbox';
$route['control/account']        = 'Admin/account';
$route['control/inbox/(:num)']   = 'Admin/inbox_detail';
$route['control/inbox/compose']  = 'Admin/inbox_compose';

$route['control/products'] = 'Admin/ecommerce_products';
/**/ $route['ajaxReq/472b6734d7f1a3c599ee43069069c6d6ecdd0270'] = 'Ajax/ecommerce_products';
/**/ $route['ajaxReq/7a53a91a65f1e5a22a9c1d16d448b8c56f838a0e'] = 'Ajax/ecommerce_products';
/**/ $route['ajaxReq/056cf7d70d3010713f4384149c6efd1f078f90f9'] = 'Ajax/ecommerce_products_add';
/**/ $route['ajaxReq/13f5a48ed1d5991d5d0bd3c2cf598f1ff1ff4520'] = 'Ajax/ecommerce_products_edit';
/**/ $route['ajaxReq/cf38f5ec33c37247808351d567d3d31acb719699'] = 'Ajax/ecommerce_products_list';
/**/ $route['ajaxReq/2e10734440a96be1248ac60bc5655bb88762a894'] = 'Ajax/ecommerce_products_delete';
/**/ 
/**/ 
$route['control/products/edit/(:num)'] = 'Admin/ecommerce_products_edit';

$route['control/cupom'] = 'Admin/cupom';
/**/ $route['ajaxReq/3f3ddad64f6e3a77d2eb49573ccd7e0592d1b69d'] = 'Ajax/ecommerce_cupons_add';
/**/ $route['ajaxReq/dd0096da614d1e8b5d44748664173f8896287e07'] = 'Ajax/ecommerce_cupons_remove';
/**/ $route['ajaxReq/133e18fd36ada046f9e51206b34d63fa54838d45'] = 'Ajax/ecommerce_cupons_list';

$route['control/promotion'] = 'Admin/promotion';

$route['control/category'] = 'Admin/ecommerce_category';
/**/ $route['ajaxReq/308a8625716877ce43d20649e5aeb5b7022820a9'] = 'Ajax/ecommerce_category_add';
/**/ $route['ajaxReq/dd0096da614d1e8b5d44748664173f8896287e07'] = 'Ajax/ecommerce_category_remove';
/**/ $route['ajaxReq/5c192f0a5198906d65f24dcf39b3ca4d006ec70a'] = 'Ajax/ecommerce_category_list';

$route['control/addons']   = 'Admin/ecommerce_addons';
/**/ $route['ajaxReq/f037a9813090a2dae39d3640dabec66238110189'] = 'Ajax/ecommerce_addons_add';
/**/ $route['ajaxReq/5bd5524ab76a12c2232d5c9c13f03fb200fe7353'] = 'Ajax/ecommerce_addons_remove';
/**/ $route['ajaxReq/177d988094f6753022ed763cf3943f4f31054aa4'] = 'Ajax/ecommerce_addons_list';

$route['control/options'] = 'Admin/ecommerce_options';
/**/ $route['ajaxReq/7827095cec8bb21a40199ca4429d7a90a0c59c8c'] = 'Ajax/ecommerce_options_add';
/**/ $route['ajaxReq/56ddcaf23a35e960be2717844a84b1f2981e76f4'] = 'Ajax/ecommerce_options_remove';
/**/ $route['ajaxReq/88df9223964eb4cd2e67c78b41236a8cd08c7094'] = 'Ajax/ecommerce_options_list';

$route['control/ingredients']   = 'Admin/ecommerce_ingredients';
/**/ $route['ajaxReq/6f48716b9f950a4d2597c90199ed029e4290f2b1'] = 'Ajax/ecommerce_ingredients_add';
/**/ $route['ajaxReq/078798c32e5ac6b71924b425b305229c9cacd859'] = 'Ajax/ecommerce_ingredients_remove';
/**/ $route['ajaxReq/5f59d72ce004b5f3c518208b41a0e0c733995bfe'] = 'Ajax/ecommerce_ingredients_list';
/**/ $route['ajaxReq/a79e7133d9318261a9d8098804a4df1b9a3dca68'] = 'Ajax/ecommerce_ingredients_get';

$route['control/orders']   = 'Admin/ecommerce_orders';
/**/ $route['ajaxReq/13107df1de65ac459e9ce39ee2d304ba739ae08e'] = 'Admin/ecommerce_orders_change_status';

$route['control/clients']   = 'Admin/ecommerce_clients';
/**/ $route['control/clients/edit/(:num)']   = 'Admin/ecommerce_clients_edit';

$route['control/deliveryman'] = 'Admin/ecommerce_deliveryman';
$route['control/deliveryman_reports'] = 'Admin/ecommerce_deliveryman_reports';

$route['control/reports']   = 'Admin/ecommerce_reports';
/**/ 

$route['control/settings'] = 'Admin/ecommerce_settings';
/**/ $route['ajaxReq/2f06d50b643e4edd7891e1b37c636ab5542fc97e'] = 'Ajax/save_ecommerce_settings_address';
/**/ $route['ajaxReq/01309050243a5b2125d4dc1d3cff01e7c77fe4bc'] = 'Ajax/save_ecommerce_settings_general';

$route['ajaxReq/254a65f77e8113e584552c28da18bea2e940432f'] = 'Shopcart/add_to_cart';
$route['ajaxReq/254a65f77e8113e584552c28da18bea2e940438d'] = 'Shopcart/total_verify';
$route['ajaxReq/2dc5cfe20a022217acfc0b2f597db5552c48a684'] = 'Shopcart/remove_item_cart';
$route['ajaxReq/d1df34c86ffd2e26b65631e24e7ce9d2a73b5887'] = 'Shopcart/change_quantity_item_cart';
$route['ajaxReq/d1546e9584228a6c0a3c22a55518d0addb1a574f'] = 'Shopcart/checkout_item_cart';
$route['ajaxReq/1448e334afd96e01949ec3bc59598d3f0ea0c09e'] = 'Shopcart/checkout_cart_total';
$route['ajaxReq/1448e334afd96e01949ec3bc59598d3f0ea0c010'] = 'Shopcart/checkout_cupom_verify';
$route['ajaxReq/3b05e287167771f46a98582f461fcedd47387369'] = 'Shopcart/delivery_address';
$route['ajaxReq/9f73911323c61359ac276f39934e6f60ce53d032'] = 'Shopcart/close_checkout';
$route['ajaxReq/dc5712026f8e040e3cd541e0de5a5f1ba7c40861'] = 'Ajax/ecommerce_change_roles';
$route['ajaxReq/7e75d824c17cbc175a56479a7ad38b4212cbf0da'] = 'Ajax/ecommerce_change_tables';
$route['ajaxReq/a937c9941c1df0c6e917269c95700803ce1cac52'] = 'Ajax/ecommerce_change_status';
$route['ajaxReq/51739c443435f9cef17d024bb8889b9e2c676538'] = 'Ajax/ecommerce_login_client';
$route['ajaxReq/d33bb21feb29e0e1cc2c677c45bc35c9cfc4110e'] = 'Ajax/ecommerce_login_recovery';
$route['ajaxReq/ce3350785304253a0dcf8d3567a4d38884564df6'] = 'Ajax/ecommerce_register_client';
$route['ajaxReq/c9196710bc35fa7806e74c0fb9d92eba11c6ae8a'] = 'Ajax/panel_login_admin';
$route['ajaxReq/1c4239bbec8c8ca08c5007468377cac563c154f6'] = 'Ajax/recovery_password_admin';
$route['ajaxReq/6c24039a1b7fcdbae18a1957bafee95903adf179'] = 'Ajax/change_minbuy';
$route['ajaxReq/6c24039a1b7fcdbae18a1957bafee95903adf177'] = 'Ajax/change_payments';
$route['ajaxReq/0c6dc74dee68bcde2c4f57f77627a34f68f3f4ac'] = 'Ajax/change_funcionally';
$route['ajaxReq/4f62f9ef775556d839098f4946f916a0d986d639'] = 'Ajax/trash_funcionally';
$route['ajaxReq/89181b16a83f599c365eb1e44e5338e7d1ac01da'] = 'Ajax/sales_products';
$route['ajaxReq/b882641720be020b3a6b4cab2719d6c47b814b5a'] = 'Ajax/list_menu';
$route['ajaxReq/79b5318a00f32befd597920d2f4834cebfc85e48'] = 'Ajax/promotions_add';
$route['ajaxReq/a1e270684eb9551f60c7338cde7eb862f5f93bd5'] = 'Ajax/promotions_choose';
$route['ajaxReq/1def459b970d1a119806f3a044fc5acbdac9c029'] = 'Ajax/news_orders';
$route['ajaxReq/438ce62d20db72b9eb984e105dec127f2e92a181'] = 'Ajax/oauth_facebook';
$route['ajaxReq/c855bf0613cdb97bbca3f2f5573c715a896dfba2'] = 'Ajax/oauth_google';
$route['ajaxReq/79972e2375517a97ef7679d1c46dbffe238fb3da'] = 'Ajax/sales_month';
$route['ajaxReq/a2b7f359a0e3d17ddd381f07f5f1c43350826cea'] = 'Ajax/trashing_invoice';
$route['ajaxReq/a9e98764722534ba8ab10b264d44717be1a5f788'] = 'Ajax/trashing_client';
$route['ajaxReq/a9e98764722534ba8ab10b264d44717be1a5f789'] = 'Ajax/trashing_deliveryman';
$route['ajaxReq/a9e98764722534ba8ab10b264d44717be1a5f790'] = 'Ajax/change_deliveryman';
$route['ajaxReq/a9e98764722534ba8ab10b264d44717be1a5f791'] = 'Ajax/create_deliveryman';
$route['ajaxReq/1e92252363b14b6acf35ba019f1d553d83927848'] = 'Ajax/cupom_mailform';
$route['ajaxReq/9290ee36ac14af3432423b78a923ba8cd12d7a1f'] = 'Ajax/save_profile_admin_picture';
$route['ajaxReq/9290ee36ac14af3432423b78a923ba8cd12d7a1r'] = 'Ajax/save_profile_clients_picture';
$route['ajaxReq/9290ee36ac14af3432423b78a923ba8cd12d7a1s'] = 'Ajax/save_clients_edit';
$route['ajaxReq/9290ee36ac14af3432423b78a923ba8cd12d7a1t'] = 'Ajax/delete_clients_edit';
$route['ajaxReq/6bd2a7db18a5d9d9cb6a5f60dec688b08a7e6e25'] = 'Ajax/admin_account';
$route['ajaxReq/6bd2a7db18a5d9d9cb6a5f60dec688b08a7e6e26'] = 'Ajax/client_account';
$route['ajaxReq/794c3b095cb888292dac59c6ed07ca4a6e898d74'] = 'Ajax/save_email_logo';
$route['ajaxReq/5f20365ba3812df8ed36b1075959e0c10600b831'] = 'Ajax/save_email_body';
$route['ajaxReq/64336dfc11c12c7501046194505d3f77167494a5'] = 'Ajax/save_maintenance';
$route['ajaxReq/3d6c182d0fa59a0b128384c4ea4178f69819e37f'] = 'Ajax/save_comingsoon';
$route['ajaxReq/dd43b276cc26cbf4f7a9b45962c77f80a7264097'] = 'Ajax/oauthconfig';
$route['ajaxReq/0b4f7ae1da771df6b0877903560fa43528f1f9b7'] = 'Ajax/socialconfig';
$route['ajaxReq/0b4f7ae1da771df6b0877903560fa43528f1b9f8'] = 'Ajax/get_districts';
$route['ajaxReq/0b4f7ae1da771df6b0877903560fa43528f1b9f9'] = 'Ajax/add_districts';
$route['ajaxReq/0b4f7ae1da771df6b0877903560fa43528f1c1g1'] = 'Ajax/list_districts';
$route['ajaxReq/0b4f7ae1da771df6b0877903560fa43528f1b9g1'] = 'Ajax/delete_districts';
$route['ajaxReq/0b4f7ae1da771df6b0877903560fa43528f1b9g2'] = 'Ajax/deliverymen';
$route['cupomPrinter'] = 'Ajax/cupomPrinter';