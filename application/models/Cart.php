<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Model {

	public function __construct(){
		parent::__construct();
		date_default_timezone_set('America/Sao_Paulo');

		if(empty($this->session->cart)){
			$_SESSION['cart'] = array();
		}
	}

	public function add($id, $quantity, $params){
		echo 'add';
	}

	public function remove(){
		echo 'remove';
	}

	public function list(){
		echo 'list';
	}

}
