<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PagSeguro extends CI_Model {

	function get_credentials(){
		$this->db->where('payments', 'pagseguro');
		$payments = $this->db->get('config_payments');
		if ($payments->num_rows() > 0) {
			return $payments->row();
		}
	}

}