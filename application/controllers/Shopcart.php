<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Shopcart extends CI_Controller {



	public function __construct(){

		parent::__construct();

		$timezone = json_decode(file_get_contents('config/timezone.json'));

		date_default_timezone_set($timezone->timezone);

	}



	public function config_payments(){

		return $this->db->get('config_payments')->row();

	}



	public function config_emails(){

		return $this->db->get('config_emails')->row();

	}



	public function config_website(){

		return $this->db->get('config')->row();

	}



	public function add_to_cart(){



		if (!empty($_POST['productID'])) {



			$this->db->where('id', $_POST['productID']);

			$get = $this->db->get('products');



			if ($get->num_rows() > 0):

				$product = $get->row();

				$params = array();

				if (!empty($_POST['ingredients']['pizza'])) {

					$ings_arr = array();

					foreach ($_POST['ingredients']['pizza'] as $pid => $ings) {

						$this->db->where('id', $pid);

						$product_ings = $this->db->get('products');

						if ($product_ings->num_rows() > 0) {

							$product_ing = $product_ings->row();

							$ingredients = explode('|', $product_ing->ingredients);

							$diff = $ings;

							$remove = array_diff($ingredients, $diff);

							if(count($remove) > 0){

								$ings_arr[$pid][] = $remove;

							}

						}

					}

					$params[] = array('ingredients' => $ings_arr);

				}


				if (!empty($_POST['ingredients']['default'])) {
					
					$ings_arr = array();

					$ingredients = explode('|', $product->ingredients);

					$diff = $_POST['ingredients']['default'][0];
					$diff = explode(",", $diff);
					$remove = array_diff($ingredients, $diff);
					
					if(count($remove) > 0){	
						array_push($ings_arr, array_values($remove));					
					}
					
					$params[] = array('ingredients' => $ings_arr);

				}



				if(isset($_POST['addons'])){

					$params[] = array('addons' => $_POST['addons']);

				}



				if(isset($_POST['optionals'])){

					$optionals_array = array();

					foreach ($_POST['optionals'] as $idoo => $options) {

						$options_array = array();

						foreach ($options as $opt) {

							$options_array[] = $opt;

						}

						$optionals_array[] = $idoo.':'.join(",", $options_array);

					}

					$params[] = array('optionals' => join("|", $optionals_array));

				}



				if(isset($_POST['flavors'])){

					$flavors_array = array();

					foreach ($_POST['flavors'] as $flavors) {

						$flavors_array[] = $flavors;

					}

					$params[] = array('flavors' => join("|", $flavors_array));

				}







				if(isset($_POST['schendule'])){

					$params[] = array('schendule' => $_POST['schendule']);			

				}



				$this->add($product->id, (int)$_POST['quantity'], $params, base_url());



			endif;



		}else{

			$id = $_POST['promotionID'];

			$this->add($id, (int)$_POST['quantity'], array(), base_url(), true);

		}



	}



	public function total_verify(){



		$cart = $this->session->shopcart;

		$items_order = array();

		$uid = $this->session->user;

		$verify_total = 0;



		foreach ($cart as $i => $items):



			list($id, $index) = explode(":", $i);



			if($items['promotion'] == false){

				$this->db->where("id", $id);

				$product = $this->db->get('products')->row();



				if(isset($items['params']['flavors'])){

					$explore_falvors = explode("|", $items['params']['flavors']);

					$price_flavor = array();

					$price_flavor[] = $product->price / 2;

					foreach ($explore_falvors as $fid) {

						$this->db->where('id', $fid);

						$get_flavor = $this->db->get('products');

						if ($get_flavor->num_rows() > 0) {

							$flavor = $get_flavor->row();

							$price_flavor[] = $flavor->price / 2;

						}

					}

					$subtotal = array_sum($price_flavor) * $items['quantity'];

				}else{

					$subtotal = $product->price * $items['quantity'];

				}



				if(isset($items['params']['ingredients'])):

					foreach ($items['params']['ingredients'] as $p_id => $ingredients) {

						foreach ($ingredients[0] as $ing) {

							$this->db->where('id', $ing);

							$ingredient = $this->db->get('ingredients')->row();

						}

					}

				endif;



				if(isset($items['params']['optionals'])):

					$opts = explode("|", $items['params']['optionals']);

					foreach ($opts as $opt) {

						list($option_id, $odetail_id) = explode(":", $opt);

						$this->db->where('id', $option_id);

						$get_optional = $this->db->get('optionals');

						if ($get_optional->num_rows() > 0) {

							$optional = $get_optional->row();

							if($optional->price > 0){

								$subtotal += $optional->price * $items['quantity'];

							}

						}

					}

				endif;



				if(isset($items['params']['addons'])):

					foreach ($items['params']['addons'] as $addon_id) {

						$this->db->where('id', $addon_id);

						$get_addon = $this->db->get('addons');

						if($get_addon->row()->price > 0 && $get_addon->num_rows() > 0){

							$addon = $get_addon->row();

							$subtotal += $addon->price * $items['quantity'];

						}

					}

				endif;

			}else{

				$this->db->where('id', $id);

				$promotions = $this->db->get('promotions');

				if ($promotions->num_rows() > 0) {

					$promotion = $promotions->row();

					$subtotal = $promotion->promotional_price * $items['quantity'];

				}



			}

			$verify_total += $subtotal;

		endforeach;



		if ($this->helps->format_money($verify_total) < $this->helps->format_money($this->config_website()->minbuy)){

			echo json_encode(array(

				'error' => true,

				'added' => false,

				'value' => $this->helps->format_money($this->config_website()->minbuy),

				'phone' => $this->helps->formatPhone($this->config_website()->phone)

			));

		}else{

			echo json_encode(array('error' => false));

		}

	}



	public function close_checkout(){

		$cart = $this->session->shopcart;
		$items_order = array();
		$uid = $this->session->user;
		$cupom = isset($_POST['descount']) ? $_POST['descount'] : null;
		$descounted = 0;

		if (isset($_POST['cep'])) {

			$cep     = $_POST['cep'];
			$address = $_POST['address'].'|'.$_POST['address_number'];
			$reff    = $_POST['reff'];
			$this->db->where('id', $uid);
			$this->db->set('cep', $cep);
			$this->db->set('address', $address);
			$this->db->set('refer', $reff);
			$this->db->update('clients');

		}

		$deliveryaddress = ($_POST['deliveryaddress'] == 'false') ? false : true;

        $this->db->where("id >", 0);
        $this->db->limit(1);
        $configGeneral = $this->db->get("config");
        $configGeneral = ($configGeneral->num_rows() > 0) ? $configGeneral->row() : "";
        $deliverytime = ($configGeneral->deliverytime != '') ? $configGeneral->deliverytime : 30;

		if (!empty($deliveryaddress)) {

			$chooseAddress = isset($_POST['chooseAddress']) ? $_POST['chooseAddress'] : 0;
			$address_q = $this->db->get('config_address');

			$this->db->where('status', '1');
			$this->db->order_by('rand()');
			$this->db->limit(1);
			$get_deliveryman = $this->db->get('deliveryman');
			if ($get_deliveryman->num_rows() > 0) {
				$deliveryman = $get_deliveryman->row();
				$this->db->set('deliveryman_id', $deliveryman->id);
			}

			if ($address_q->num_rows() > 0) {

				$address = $address_q->row();
				$this->db->where("id", $chooseAddress);
				$address_client = $this->db->get('clients_address')->row();

				if ($addres->taxasvalue == 1) {

					$origin = $address->cep;
					$destination = $address_client->cep;
					$distance = json_decode($this->helps->delivery_calculate($origin, $destination), true);

					if (!empty($distance['duration']) && !empty($distance['distance'])) {
						$shippingPrice = $this->helps->shippingPrice($distance['distance']['value'], $address->delivery_price);
						$shippingDuration = $this->helps->shippingDuration($distance['duration']['value']);
						$this->db->set('shippingData', $distance['distance']['value'] . '|' . $distance['duration']['value']);
						$this->db->set('shippingAddress', '1');
						$this->db->set('orderType', '0');

					} else {
						$distance = null;
						$shippingPrice = null;
						$shippingDuration = null;
						$this->db->set('orderType', '0');
						$this->db->set('shippingAddress', '1');
					}

				} else {

					list($add, $district, $cities) = explode(", ", $address_client->address);

					$this->db->where('bairro', $district);
					$cepbr_bairro = $this->db->get('cepbr_bairro');
					if ($cepbr_bairro->num_rows() > 0) {
						$rs = $cepbr_bairro->row();
						$this->db->where('district_id', $rs->id_bairro);
						$rso = $this->db->get('districts');
						if ($rso->num_rows() > 0) {
							$shippingPrice = $rso->row()->price;
						} else {
							$shippingPrice = null;
						}
					} else {
						$shippingPrice = null;
					}

					$distance = null;
					$shippingDuration = null;
					$this->db->set('orderType', '0');
					$this->db->set('shippingAddress', '1');

				}

				$this->db->set('clients_address_id', $chooseAddress);
			}

		} else {

			$distance = null;

			$shippingPrice = null;

			$shippingDuration = null;

			$this->db->set('shippingAddress', '0');

			$this->db->set('orderType', '1');

		}



		$this->db->set('clients_id', $uid);

		$this->db->set('payment', $_POST['payment']);

		$this->db->set('date', date('Y-m-d H:i:s'));



		if($_POST['payment'] == 'money'){

			$this->db->set('receiver', $_POST['receiver']);

			$this->db->set('send', $_POST['send']);



			$insert = $this->db->insert('orders');

			$insert_id = $this->db->insert_id();



			if($insert_id > 0):



				foreach ($cart as $i => $items):



					list($id, $index) = explode(":", $i);

					$flavors = array();

					$take = array(); 

					$optionals = '';

					$addons = array();

					$schendule = '';



					if($items['promotion'] == false){



						$this->db->where("id", $id);

						$product = $this->db->get('products')->row();

						$stock = $product->stock - $items['quantity'];



						$this->db->where("id", $id);

						$this->db->set("stock", $stock);

						$this->db->set("sells", $items['quantity']);

						$this->db->update('products');



						if(isset($items['params']['flavors'])){

							$explore_falvors = explode("|", $items['params']['flavors']);

							$price_flavor = array();

							$price_flavor[] = $product->price / 2;

							foreach ($explore_falvors as $fid) {

								$this->db->where('id', $fid);

								$get_flavor = $this->db->get('products');

								if ($get_flavor->num_rows() > 0) {

									$flavor = $get_flavor->row();

									$price_flavor[] = $flavor->price / 2;

									$flavors[] = $fid;

								}

							}

							$subtotal = array_sum($price_flavor) * $items['quantity'];

						}else{

							$subtotal = $product->price * $items['quantity'];

						}



						if (!is_null($shippingPrice)) {

							$subtotal += $shippingPrice / count($cart);

						}



						$this->db->set('quantity', $items['quantity']);



						$this->db->where('id', $product->category_id);

						$category_get = $this->db->get('category');

						$category = ($category_get->num_rows() > 0) ? $category_get->row()->name : 'Sem categoria';



						if(isset($items['params']['ingredients'])):
							if($product->pizza > 0):

								foreach ($items['params']['ingredients'] as $p_id => $ingredients) {

									$ings_array = array();

									foreach ($ingredients[0] as $ing) {

										$this->db->where('id', $ing);

										$ingredient = $this->db->get('ingredients');

										$ings_array[] = $ing;

									}

									$take[] = $p_id.'-'.join("|", $ings_array);

								}

							else:
								if(count($items['params']['ingredients'][0]) > 0){
									$take[] = $product->id . '-' . join("|", $items['params']['ingredients'][0]);
								}
							endif;
						endif;


						if(isset($items['params']['optionals'])):

							$opts = explode("|", $items['params']['optionals']);

							foreach ($opts as $opt) {

								list($option_id, $odetail_id) = explode(":", $opt);

								$this->db->where('id', $option_id);

								$get_optional = $this->db->get('optionals');

								if ($get_optional->num_rows() > 0) {

									$optional = $get_optional->row();

									$this->db->where('id', $odetail_id);

									if($optional->price > 0){

										$subtotal += $optional->price * $items['quantity'];

									}

									$optionals = $items['params']['optionals'];

								}

							}

						endif;



						if(isset($items['params']['addons'])):

							foreach ($items['params']['addons'] as $addon_id) {

								$this->db->where('id', $addon_id);

								$get_addon = $this->db->get('addons');

								if($get_addon->row()->price > 0 && $get_addon->num_rows() > 0){

									$addon = $get_addon->row();

									$subtotal += $addon->price * $items['quantity'];

								}

								$addons[] = $addon_id;

							}

						endif;



						if(isset($items['params']['schendule'])):

							$schendule = $items['params']['schendule'];

						endif;



					}else{

						$this->db->where('id', $id);

						$promotions = $this->db->get('promotions');

						if ($promotions->num_rows() > 0) {

							$promotion = $promotions->row();

							$subtotal = $promotion->promotional_price * $items['quantity'];

							$this->db->set('promotional', $promotion->promotional_price);

							$this->db->set('quantity', $items['quantity']);

						}

					}



					if (!is_null($cupom)) {

						$this->db->where('genered', $cupom);

						$cp = $this->db->get('cupons');

						if ($cp->num_rows() > 0) {

							$descount = $cp->row()->value / 100;

							$descount_value = $subtotal * $descount;

							$final_value = $subtotal - $descount_value;

							$subtotal = $final_value;

							$descounted += $descount_value;



						}

					}





					$this->db->set('order_id', $insert_id);

					$this->db->set('product_id', $id);

					$this->db->set('addons', join('|', $addons));

					$this->db->set('optionals', $optionals);

					$this->db->set('takes', join(',', $take));

					$this->db->set('flavors', join('|', $flavors));

					$this->db->set('schendule', $schendule);

					$this->db->set('subtotal', number_format($subtotal, 2, ".", "."));

					$this->db->insert('orders_details');



					$this->session->unset_userdata('shopcart');



				endforeach;



				if ($descounted > 0) {

					$this->db->where("id", $insert_id);

					$this->db->set('descount', $this->helps->format_money($descounted));

					$this->db->update('orders');

				}


				$this->db->where('id', $uid);

				$client_emails = $this->db->get('clients')->row();

				$this->emails->receipt($client_emails->name, $client_emails->email, $this->cupomPrinter($insert_id, 'client'));


				echo json_encode(array('uid' => $uid, 'order' => $insert_id, 'checkout' => true, 'type' => $_POST['payment'], 'delivery' => $distance, 'shipping' => $shippingPrice, 'shippingDuration' => $deliverytime));



			endif;



		}else{



			if ($_POST['creditcardType'] == 'pagseguro') {



				$this->db->where('id', $this->session->user);

				$client = $this->db->get('clients')->row();



				$this->db->where('payments', 'pagseguro');

				$pagseguro = $this->db->get('config_payments')->row();



				$url_payment 	 = ($pagseguro->payments_envelopment == 1) ? 

				'https://ws.sandbox.pagseguro.uol.com.br/v2/' : 'https://ws.pagseguro.uol.com.br/v2/';



				$brand           = $_POST['brand'];

				$cpf             = $_POST['cpf'];

				$cvc             = $_POST['cvc'];

				$deliveryaddress = $_POST['deliveryaddress'];

				$expiry          = $_POST['expiry'];

				$hash            = $_POST['hash'];

				$installments    = $_POST['installments'];

				$name            = $_POST['name'];

				$number          = $_POST['number'];

				$payment         = $_POST['payment'];

				$receiver        = $_POST['receiver'];

				$send            = $_POST['send'];

				$session         = $_POST['session'];

				$token           = $_POST['token'];

				$total           = $_POST['total'];

				$birthday        = str_replace(" ", "", $_POST['birthday']);



				$phone = substr($client->phone, -(strlen($client->phone)-2));

				$ddd = substr($client->phone, 0, 2);



				list($fulladdress, $number) = explode("|", $client->address);

				list($street, $district, $fullcity) = explode(",", $fulladdress);

				list($city, $state) = explode(" - ", $fullcity);

				list($installment_quantity, $installment_value) = explode("|", $installments);

				list($dd,$mm,$yyyy) = explode("/", $birthday);

				list($address_city, $address_state) = explode(" - ", $address->city);



				if ($pagseguro->payments_envelopment == 1) {

					list($name_email, $provider_email) = explode("@", $client->email);

					$email_client = $name_email.'@sandbox.pagseguro.com.br';

				}else{

					$email_client = $client->email;

				}

				$noInterestInstallmentQuantity = ($this->config_payments()->payments_x) ? $this->config_payments()->payments_x : 1;



				$data['email'] = $pagseguro->payments_email;

				$data['token'] = $pagseguro->payments_token;

				$data['paymentMode'] = 'default';

				$data['paymentMethod'] = 'creditCard';

				$data['receiverEmail'] = $pagseguro->payments_email;

				$data['currency'] = 'BRL';

				$data['extraAmount'] = $shippingPrice;



				$data['itemId1'] = $client->id;

				$data['itemDescription1'] = 'Checkout '.$client->name;

				$data['itemAmount1'] = $total;

				$data['itemQuantity1'] = 1;



				$data['notificationURL'] = $this->config_payments()->payments_url;

				$data['reference'] = $client->id.'_'.time();

				$data['senderName'] = $client->name;

				$data['senderCPF'] = $cpf;

				$data['senderAreaCode'] = $ddd;

				$data['senderPhone'] = $phone;

				$data['senderEmail'] = $email_client;

				$data['senderHash'] = $hash;

				$data['shippingAddressStreet'] = $street;

				$data['shippingAddressNumber'] = $number;

				$data['shippingAddressComplement'] = $client->refer;

				$data['shippingAddressDistrict'] = $district;

				$data['shippingAddressPostalCode'] = $client->cep;

				$data['shippingAddressCity'] = $city;

				$data['shippingAddressState'] = $state;

				$data['shippingAddressCountry'] = 'BRA';

				$data['shippingType'] = 3;

				$data['creditCardToken'] = $token;

				$data['installmentQuantity'] = $installment_quantity;

				$data['installmentValue'] = number_format($installment_value, 2, '.', '');

				$data['creditCardHolderName'] = $name;

				$data['creditCardHolderCPF'] = $cpf;

				$data['creditCardHolderBirthDate'] = $dd.'/'.$mm.'/'.$yyyy;

				$data['creditCardHolderAreaCode'] = $ddd;

				$data['creditCardHolderPhone'] = $phone;

				$data['billingAddressStreet'] = $address->street;

				$data['billingAddressNumber'] = $address->number;

				$data['billingAddressComplement'] = $address->complement;

				$data['billingAddressDistrict'] = $address->locality;

				$data['billingAddressPostalCode'] = $address->cep;

				$data['billingAddressCity'] = $address_city;

				$data['billingAddressState'] = $address_state;

				$data['billingAddressCountry'] = 'ATA';



				$url = $url_payment . "transactions/";

				$data = http_build_query($data);



				$curl = curl_init($url);

				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

				curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

				$response = curl_exec($curl);

				$http = curl_getinfo($curl);



				$response = simplexml_load_string($response);



				if ($response != 'Unauthorized' && count($response->error) < 1) {



					$insert = $this->db->insert('orders');

					$insert_id = $this->db->insert_id();



					if($insert_id > 0):



						foreach ($cart as $i => $items):



							list($id, $index) = explode(":", $i);

							$flavors = array();

							$take = array();

							$optionals = '';

							$addons = array();

							$schendule = '';



							if($items['promotion'] == false){



								$this->db->where("id", $id);

								$product = $this->db->get('products')->row();

								$stock = $product->stock - $items['quantity'];



								$this->db->where("id", $id);

								$this->db->set("stock", $stock);

								$this->db->set("sells", $items['quantity']);

								$this->db->update('products');



								if(isset($items['params']['flavors'])){

									$explore_falvors = explode("|", $items['params']['flavors']);

									$price_flavor = array();

									$price_flavor[] = $product->price / 2;

									foreach ($explore_falvors as $fid) {

										$this->db->where('id', $fid);

										$get_flavor = $this->db->get('products');

										if ($get_flavor->num_rows() > 0) {

											$flavor = $get_flavor->row();

											$price_flavor[] = $flavor->price / 2;

											$flavors[] = $fid;

										}

									}

									$subtotal = array_sum($price_flavor) * $items['quantity'];

								}else{

									$subtotal = $product->price * $items['quantity'];

								}



								if (!is_null($shippingPrice)) {

									$subtotal += $shippingPrice / count($cart);

								}



								$this->db->set('quantity', $items['quantity']);



								$this->db->where('id', $product->category_id);

								$category_get = $this->db->get('category');

								$category = ($category_get->num_rows() > 0) ? $category_get->row()->name : 'Sem categoria';



								if(isset($items['params']['ingredients'])):

									foreach ($items['params']['ingredients'] as $p_id => $ingredients) {

										$ings_array = array();

										foreach ($ingredients[0] as $ing) {

											$this->db->where('id', $ing);

											$ingredient = $this->db->get('ingredients');

											$ings_array[] = $ing;

										}

										$take[] = $p_id.'-'.join("|", $ings_array);

									}

								endif;



								if(isset($items['params']['optionals'])):

									$opts = explode("|", $items['params']['optionals']);

									foreach ($opts as $opt) {

										list($option_id, $odetail_id) = explode(":", $opt);

										$this->db->where('id', $option_id);

										$get_optional = $this->db->get('optionals');

										if ($get_optional->num_rows() > 0) {

											$optional = $get_optional->row();

											$this->db->where('id', $odetail_id);

											if($optional->price > 0){

												$subtotal += $optional->price * $items['quantity'];

											}

											$optionals = $items['params']['optionals'];

										}

									}

								endif;



								if(isset($items['params']['addons'])):

									foreach ($items['params']['addons'] as $addon_id) {

										$this->db->where('id', $addon_id);

										$get_addon = $this->db->get('addons');

										if($get_addon->row()->price > 0 && $get_addon->num_rows() > 0){

											$addon = $get_addon->row();

											$subtotal += $addon->price * $items['quantity'];

										}

										$addons[] = $addon_id;

									}

								endif;



								if(isset($items['params']['schendule'])):

									$schendule = $items['params']['schendule'];

								endif;



							}else{

								$this->db->where('id', $id);

								$promotions = $this->db->get('promotions');

								if ($promotions->num_rows() > 0) {

									$promotion = $promotions->row();

									$subtotal = $promotion->promotional_price * $items['quantity'];

									$this->db->set('promotional', $promotion->promotional_price);

									$this->db->set('quantity', $items['quantity']);

								}

							}



							if (!is_null($cupom)) {

								$this->db->where('genered', $cupom);

								$cp = $this->db->get('cupons');

								if ($cp->num_rows() > 0) {

									$descount = $cp->row()->value / 100;

									$descount_value = $subtotal * $descount;

									$final_value = $subtotal - $descount_value;

									$subtotal = $final_value;

									$descounted += $descount_value;

								}

							}



							$this->db->set('order_id', $insert_id);

							$this->db->set('product_id', $id);

							$this->db->set('addons', join('|', $addons));

							$this->db->set('optionals', $optionals);

							$this->db->set('takes', join(',', $take));

							$this->db->set('flavors', join('|', $flavors));

							$this->db->set('schendule', $schendule);

							$this->db->set('subtotal', number_format($subtotal, 2, ".", "."));

							$this->db->insert('orders_details');



						endforeach;



						if ($descounted > 0) {

							$this->db->where("id", $insert_id);

							$this->db->set('descount', $this->helps->format_money($descounted));

							$this->db->update('orders');

						}



						$this->session->unset_userdata('shopcart');

						echo json_encode(array('uid' => $uid, 'order' => $insert_id, 'checkout' => true, 'type' => $_POST['payment'], 'delivery' => $distance, 'shipping' => $shippingPrice, 'shippingDuration' => $shippingDuration));



						$this->db->where('id', $uid);

						$client_emails = $this->db->get('clients')->row();

						$this->emails->receipt($client_emails->name, $client_emails->email, $this->cupomPrinter($insert_id, 'client'));



					endif;

				}else{



					echo json_encode(array('checkout' => false, 'code' => $response));

				}



			}else{



				$this->db->set('payment_shippingBrand', $_POST['payment_shippingBrand']);

				$insert = $this->db->insert('orders');

				$insert_id = $this->db->insert_id();



				if($insert_id > 0):



					foreach ($cart as $i => $items):



						list($id, $index) = explode(":", $i);

						$flavors = array();

						$take = array();

						$optionals = '';

						$addons = array();

						$schendule = '';



						if($items['promotion'] == false){

							$this->db->where("id", $id);

							$product = $this->db->get('products')->row();

							$stock = $product->stock - $items['quantity'];



							$this->db->where("id", $id);

							$this->db->set("stock", $stock);

							$this->db->set("sells", $items['quantity']);

							$this->db->update('products');



							if(isset($items['params']['flavors'])){

								$explore_falvors = explode("|", $items['params']['flavors']);

								$price_flavor = array();

								$price_flavor[] = $product->price / 2;

								foreach ($explore_falvors as $fid) {

									$this->db->where('id', $fid);

									$get_flavor = $this->db->get('products');

									if ($get_flavor->num_rows() > 0) {

										$flavor = $get_flavor->row();

										$price_flavor[] = $flavor->price / 2;

										$flavors[] = $fid;

									}

								}

								$subtotal = array_sum($price_flavor) * $items['quantity'];

							}else{

								$subtotal = $product->price * $items['quantity'];

							}



							if (!is_null($shippingPrice)) {

								$subtotal += $shippingPrice / count($cart);

							}



							$this->db->set('quantity', $items['quantity']);



							$this->db->where('id', $product->category_id);

							$category_get = $this->db->get('category');

							$category = ($category_get->num_rows() > 0) ? $category_get->row()->name : 'Sem categoria';



							if(isset($items['params']['ingredients'])):

								foreach ($items['params']['ingredients'] as $p_id => $ingredients) {

									$ings_array = array();

									foreach ($ingredients[0] as $ing) {

										$this->db->where('id', $ing);

										$ingredient = $this->db->get('ingredients');

										$ings_array[] = $ing;

									}

									$take[] = $p_id.'-'.join("|", $ings_array);

								}

							endif;



							if(isset($items['params']['optionals'])):

								$opts = explode("|", $items['params']['optionals']);

								foreach ($opts as $opt) {

									list($option_id, $odetail_id) = explode(":", $opt);

									$this->db->where('id', $option_id);

									$get_optional = $this->db->get('optionals');

									if ($get_optional->num_rows() > 0) {

										$optional = $get_optional->row();

										$this->db->where('id', $odetail_id);

										if($optional->price > 0){

											$subtotal += $optional->price * $items['quantity'];

										}

										$optionals = $items['params']['optionals'];

									}

								}

							endif;



							if(isset($items['params']['addons'])):

								foreach ($items['params']['addons'] as $addon_id) {

									$this->db->where('id', $addon_id);

									$get_addon = $this->db->get('addons');

									if($get_addon->row()->price > 0 && $get_addon->num_rows() > 0){

										$addon = $get_addon->row();

										$subtotal += $addon->price * $items['quantity'];

									}

									$addons[] = $addon_id;

								}

							endif;



							if(isset($items['params']['schendule'])):

								$schendule = $items['params']['schendule'];

							endif;



						}else{

							$this->db->where('id', $id);

							$promotions = $this->db->get('promotions');

							if ($promotions->num_rows() > 0) {

								$promotion = $promotions->row();

								$subtotal = $promotion->promotional_price * $items['quantity'];

								$this->db->set('promotional', $promotion->promotional_price);

								$this->db->set('quantity', $items['quantity']);

							}

						}



						if (!is_null($cupom)) {

							$this->db->where('genered', $cupom);

							$cp = $this->db->get('cupons');

							if ($cp->num_rows() > 0) {

								$descount = $cp->row()->value / 100;

								$descount_value = $subtotal * $descount;

								$final_value = $subtotal - $descount_value;

								$subtotal = $final_value;

								$descounted += $descount_value;

							}

						}



						$this->db->set('order_id', $insert_id);

						$this->db->set('product_id', $id);

						$this->db->set('addons', join('|', $addons));

						$this->db->set('optionals', $optionals);

						$this->db->set('takes', join(',', $take));

						$this->db->set('flavors', join('|', $flavors));

						$this->db->set('schendule', $schendule);

						$this->db->set('subtotal', number_format($subtotal, 2, ".", "."));

						$this->db->insert('orders_details');



					endforeach;





					if ($descounted > 0) {

						$this->db->where("id", $insert_id);

						$this->db->set('descount', $this->helps->format_money($descounted));

						$this->db->update('orders');

					}

					$this->session->unset_userdata('shopcart');

					echo json_encode(array('uid' => $uid, 'order' => $insert_id, 'checkout' => true, 'type' => $_POST['payment'], 'delivery' => $distance, 'shipping' => $shippingPrice, 'shippingDuration' => $shippingDuration));



					$this->db->where('id', $uid);

					$client_emails = $this->db->get('clients')->row();

					$this->emails->receipt($client_emails->name, $client_emails->email, $this->cupomPrinter($insert_id, 'client'));



				endif;



			}



		}



	}



	public function add($id, $quantity = 1, $params, $uri, $promotion = false){



		$session = count($this->session->shopcart) + 1;

		$index = sprintf('%s:%s', $id, $session);



		$cart_items = $this->session->userdata('shopcart');



		$array = array();



		if(count($params) > 0):

			foreach ($params as $param) {



				if(isset($param['addons'])){

					$array['addons'] = $param['addons'];

				}

				if(isset($param['ingredients'])){

					$array['ingredients'] = $param['ingredients'];	

				}

				if(isset($param['optionals'])){

					$array['optionals'] = $param['optionals'];	

				}

				if(isset($param['flavors'])){

					$array['flavors'] = $param['flavors'];	

				}

				if(isset($param['schendule'])){

					$array['schendule'] = $param['schendule'];	

				}



			}

		endif;



		$cart_items[$index] = array('quantity' => (int)$quantity, 'params' => $array, 'promotion' => $promotion);



		$storage = $this->session->set_userdata('shopcart', $cart_items);



		echo json_encode(array('added' => true, 'uri' => $uri.'cart', 'cart' => $uri));





	}



	public function remove_item_cart(){

		unset($_SESSION['shopcart'][$_POST['id']]);

		echo json_encode(array('count' => count($this->session->shopcart)));

	}



	public function change_quantity_item_cart(){



		if($_POST['quantity'] > 0){

			$indexid = $_POST['id'];

			$quantity = $_POST['quantity'];

			$session = $this->session->userdata('shopcart');

			$session[$indexid]['quantity'] = $quantity;

			$this->session->set_userdata('shopcart', $session); 



			$total = 0;

			$subtotal = 0;

			$in = 0;



			foreach ($this->session->userdata('shopcart') as $i => $items):

				list($id, $index) = explode(":", $i);



				$this->db->where("id", $id);

				$product = $this->db->get('products')->row();



				if($i == $indexid):

					$in = $index;



					if(isset($items['params']['flavors'])) {

						$explore_falvors = explode("|", $items['params']['flavors']);

						$price_flavor = array();

						$price_flavor[] = $product->price / 2;

						foreach ($explore_falvors as $flv) {

							$this->db->where('id', $flv);

							$get_flavor = $this->db->get('products');

							if ($get_flavor->num_rows() > 0) {

								$flavor = $get_flavor->row();

								$price_flavor[] = $flavor->price;

							}

						}

						$subtotal = array_sum($price_flavor) / count($price_flavor) * $items['quantity'];

					}else{

						$subtotal = $product->price * $items['quantity'];

					}

				endif;



				$take = array();

				if(isset($items['params']['ingredients'])):

					foreach ($items['params']['ingredients'] as $p_id => $ingredients) {

						foreach ($ingredients[0] as $ing) {

							$this->db->where('id', $ing);

							$ingredient = $this->db->get('ingredients');

							if($ingredient->row()->price > 0 && $ingredient->num_rows() > 0){

								$subtotal += $ingredient->row()->price * $items['quantity'];

							}

						}

					}

				endif;



				$optionals = array();

				if(isset($items['params']['optionals'])):

					$opts = explode("|", $items['params']['optionals']);

					foreach ($opts as $opt) {

						list($option_id, $odetail_id) = explode(":", $opt);

						$this->db->where('id', $option_id);

						$get_optional = $this->db->get('optionals');

						if ($get_optional->num_rows() > 0) {

							$optional = $get_optional->row();

							$this->db->where('id', $odetail_id);

							if($optional->price > 0){

								$subtotal += $optional->price * $items['quantity'];

							}

						}

					}

				endif;



				$addons = array();

				if(isset($items['params']['addons'])):

					foreach ($items['params']['addons'] as $addon_id) {

						$this->db->where('id', $addon_id);

						$addon = $this->db->get('addons')->row();

						if($addon->price > 0){

							if($i == $indexid):

								$subtotal += $addon->price * $items['quantity'];

							endif;

						}

					}

				endif;



				$total += $subtotal;



			endforeach;



			echo json_encode(

				array(

					'total' => $this->helps->format_money($total), 

					'subtotal' => $this->helps->format_money($subtotal), 

					'index' => $in

				)

			);



		}



	}



	public function checkout_item_cart(){



		$money = $_POST['money'];
		$shippingPrice = $_POST['delivery'];

		$cart = $this->session->shopcart;

		$items_order = array();

		$uid = $this->session->user;

		$verify_total = 0;



		foreach ($cart as $i => $items):



			list($id, $index) = explode(":", $i);



			if($items['promotion'] == false){

				$this->db->where("id", $id);

				$product = $this->db->get('products')->row();



				if(isset($items['params']['flavors'])){

					$explore_falvors = explode("|", $items['params']['flavors']);

					$price_flavor = array();

					$price_flavor[] = $product->price / 2;

					foreach ($explore_falvors as $fid) {

						$this->db->where('id', $fid);

						$get_flavor = $this->db->get('products');

						if ($get_flavor->num_rows() > 0) {

							$flavor = $get_flavor->row();

							$price_flavor[] = $flavor->price / 2;

						}

					}

					$subtotal = array_sum($price_flavor) * $items['quantity'];

				}else{

					$subtotal = $product->price * $items['quantity'];

				}



				if(isset($items['params']['ingredients'])):

					foreach ($items['params']['ingredients'] as $p_id => $ingredients) {

						foreach ($ingredients[0] as $ing) {

							$this->db->where('id', $ing);

							$ingredient = $this->db->get('ingredients')->row();

						}

					}

				endif;



				if(isset($items['params']['optionals'])):

					$opts = explode("|", $items['params']['optionals']);

					foreach ($opts as $opt) {

						list($option_id, $odetail_id) = explode(":", $opt);

						$this->db->where('id', $option_id);

						$get_optional = $this->db->get('optionals');

						if ($get_optional->num_rows() > 0) {

							$optional = $get_optional->row();

							if($optional->price > 0){

								$subtotal += $optional->price * $items['quantity'];

							}

						}

					}

				endif;



				if(isset($items['params']['addons'])):

					foreach ($items['params']['addons'] as $addon_id) {

						$this->db->where('id', $addon_id);

						$get_addon = $this->db->get('addons');

						if($get_addon->row()->price > 0 && $get_addon->num_rows() > 0){

							$addon = $get_addon->row();

							$subtotal += $addon->price * $items['quantity'];

						}

					}

				endif;

			}else{

				$this->db->where('id', $id);

				$promotions = $this->db->get('promotions');

				if ($promotions->num_rows() > 0) {

					$promotion = $promotions->row();

					$subtotal = $promotion->promotional_price * $items['quantity'];

				}



			}

			$verify_total += $subtotal;

		endforeach;

        $verify_total += $shippingPrice;

		if ($money < $verify_total) {

			echo json_encode(array('error' => true, 'total' => $this->helps->format_money($verify_total), 'send' => $this->helps->format_money($money - $verify_total)));

		}else{

			echo json_encode(array('error' => false, 'total' => $this->helps->format_money($verify_total), 'send' => $this->helps->format_money($money - $verify_total)));

		}



	}



	public function checkout_cart_total(){



		$total = 0;



		foreach ($this->session->userdata('shopcart') as $i => $items):

			list($id, $index) = explode(":", $i);



			$this->db->where("id", $id);

			$product = $this->db->get('products')->row();



			$subtotal = $product->price * $items['quantity'];



			$take = array();

			if(isset($items['params']['ingredients'])):

				foreach ($items['params']['ingredients'] as $p_id => $ingredients) {

					foreach ($ingredients[0] as $ing) {

						$this->db->where('id', $ing);

						$ingredient = $this->db->get('ingredients');

						if($ingredient->row()->price > 0 && $ingredient->num_rows() > 0){

							$subtotal += $ingredient->row()->price * $items['quantity'];

						}

					}

				}

			endif;



			$addons = array();

			if(isset($items['params']['addons'])):

				foreach ($items['params']['addons'] as $addon_id) {

					$this->db->where('id', $addon_id);

					$addon = $this->db->get('addons')->row();

					if($addon->price > 0){

						$subtotal += $addon->price * $items['quantity'];

					}

				}

			endif;

			$total += $subtotal;

		endforeach;



		$total;



		echo json_encode(array('total' => $total));



	}



	public function checkout_cupom_verify(){

		$uid = $this->session->user;

		$cupom = trim($_POST['cupom']);



		$this->db->where('genered', $cupom);

		$get = $this->db->get('cupons');

		if ($get->num_rows() > 0) {

			$cupom = $get->row();

			$date = date('Y-m-d', strtotime($cupom->expire));

			$now = date('Y-m-d');

			if ($date <= $now) {

				$this->db->where("clients_id", $uid);

				$this->db->where("cupom", $cupom);

				$try = $this->db->get('orders');

				if ($try->num_rows() == 0) {

					echo json_encode(

						array(

							'status' => 'free'

						)

					);

				}else{

					echo json_encode(

						array(

							'status' => 'used'

						)

					);

				}

			}else{

				echo json_encode(

					array(

						'status' => 'expired'

					)

				);

			}

		}else{

			echo json_encode(

				array(

					'status' => 'invalid'

				)

			);

		}

	}


	public function cupomPrinter($id, $via){


		$row    = '<div class="drashed"></div>';

		$spaces = '&nbsp;&nbsp;&nbsp;&nbsp;';

		$total  = 0;



		$this->db->where('id', $id);

		$orders = $this->db->get('orders');

		if ($orders->num_rows() > 0) {

			$order 	 = $orders->row();



			$this->db->where('order_id', $id);

			$orders_details = $this->db->get('orders_details');



			$config  = $this->db->get('config')->row();

			$address = $this->db->get('config_address')->row();



			$this->db->where('id', $order->clients_id);

			$client  = $this->db->get('clients')->row();

			$client_address = explode('|', $client->address);

			list($street, $locality, $city) = explode(",", $client_address[0]);



			$body['address']  = 'EMPRESA '.$spaces.': '.$config->title."\n\n\r";

			$body['address'] .= 'END. '.$spaces.': '.$address->street.', '.$address->number."\n\n\r";

			$body['address'] .= 'END. '.$spaces.': '.$address->locality.', '.$address->city."\n\n\r";

			$body['address'] .= 'TEL. '.$spaces.': '.$this->helps->formatPhone($config->phone)."\n\r";



			if ($via == 'client') {

				$body['client']   = "<strong>E N T R E G A</strong> \n\n\r";

				$body['client']  .= 'CLIENTE '.$spaces.': '.$client->name."\n\n\r";

				$body['client']  .= 'END. '.$spaces.': '.$street.', '.$client_address[0]."\n\n\r";

				$body['client']  .= 'END. '.$spaces.': '.$locality.', '.$city."\n\n\r";

				$body['client']  .= 'CEP. '.$spaces.': '.$client->cep."\n\n\r";

				$body['client']  .= 'TEL. '.$spaces.': '.$this->helps->formatPhone($client->phone)."\n\r";

			}else{

				$body['client'] = '';

			}



			$body['product'] = '';

			foreach ($orders_details->result() as $detail){

				$total += $detail->subtotal;



				if (empty($detail->promotional)) {



					$this->db->where('id', $detail->product_id);

					$product = $this->db->get('products')->row();



					$flavors = array();

					if($detail->flavors != '') {

						$explore_falvors = explode("|", $detail->flavors);

						$price_flavor = array();

						$price_flavor[] = $product->price / 2;

						foreach ($explore_falvors as $findex => $flv) {

							$this->db->where('id', $flv);

							$get_flavor = $this->db->get('products');

							if ($get_flavor->num_rows() > 0) {

								$flavor = $get_flavor->row();

								$price_flavor[] = $flavor->price / 2;

								if ($flavor->id !== $detail->product_id) {

									$flavors_index = $findex + 2;

									$flavors[] = $flavors_index.'º Sabor: <strong>'. $flavor->name .'</strong>';

								}

							}

						}



					}



					$addons = array();

					$addons_amount = 0;

					if ($detail->addons != '') {

						foreach (explode('|', $detail->addons) as $id) {

							$this->db->where('id', $id);

							$addon = $this->db->get('addons');

							if ($addon->num_rows() > 0) {

								$addons_amount += $addon->row()->price * $detail->quantity;

								$addons[] = ucfirst($addon->row()->name);

							}

						}

					}



					$optionals = array();

					$optionals_amount = 0;

					if($detail->optionals != ''):

						$opts = explode("|", $detail->optionals);

						foreach ($opts as $opt) {

							list($option_id, $odetail_id) = explode(":", $opt);

							$this->db->where('id', $option_id);

							$get_optional = $this->db->get('optionals');

							if ($get_optional->num_rows() > 0) {

								$optional = $get_optional->row();

								$this->db->where('id', $odetail_id);

								$optional_detail = $this->db->get('optionals_options')->row();

								$cost = ($optional->price > 0) ? 'R$'.$this->helps->format_money($optional->price) : 'Grátis';

								$optionals[] = $optional->name.': '.$optional_detail->name.' - '.$cost;

							}

						}

					endif;



					$takes = array();

					if ($detail->takes != '') {

						$take_separe = (explode('|', $detail->takes)) ? explode('|', $detail->takes) : $detail->takes;



						foreach (explode('|', $detail->takes) as $id) {

							$this->db->where('id', $id);

							$ingredients = $this->db->get('ingredients');

							if ($ingredients->num_rows() > 0) {

								$takes[] = ucfirst($ingredients->row()->name);

							}

						}

					}



					$flavors_group = (count($flavors) > 0) ? "<br>## ".join(", ", $flavors) : '';

					$opt_group = (count($optionals) > 0) ? "<br>".'## Opcionais: <br>'.join(", <br>", $optionals) : '';

					$addons_group = (count($addons) > 0) ? "<br>".'## Adicionais: <br>'.join(", <br>", $addons) : '';

					$take_group = (count($takes) > 0) ? "<br>".'## Tirar: <br>'.join(",<br>", $takes) : '';



					$body['product'] .= '<tr>

					<td width="50%">'. $product->name . $flavors_group . $opt_group . $addons_group . $take_group .'</td>

					<td style="text-align: right" valign="top">'. $detail->quantity .'</td>

					<td style="text-align: right" valign="top">R$'. $this->helps->format_money($detail->subtotal) .'</td>

					</tr>';



				}else{

					$this->db->where('id', $id);

					$promotions = $this->db->get('promotions');

					if ($promotions->num_rows() > 0) {

						$promotion = $promotions->row();

						$body['product'] .= '<tr>

						<td width="50%">'. $promotion->title .'</td>

						<td style="text-align: right" valign="top">'. $detail->quantity .'</td>

						<td style="text-align: right" valign="top">R$'. $this->helps->format_money($detail->subtotal) .'</td>

						</tr>';

					}

				}

			}



			if ($via == 'client') {

				$body['total']   = "<strong>T O T A L</strong> \n\n\r";

				if ($order->shippingAddress == 1) {

					list($distance, $duration) = explode("|", $order->shippingData); 

					$body['total']  .= 'TAXA DE ENTREGA '.$spaces.': R$'.$this->helps->shippingPrice($distance, $address->delivery_price)."\n\n\r";

				}

				$body['total']  .= 'TOTAL A PAGAR '.$spaces.': R$'.$total."\n\r";



				$payment_type = ($order->payment == 'delivery') ? 'Dinheiro' : 'Cartão de crédito';

				$payment_method = ($order->payment_shippingBrand == '') ? ' via PagSeguro Online' : ' via Maquineta';



				$body['payment']  = "<strong>F O R M A S &nbsp; D E &nbsp; P A G A M E N T O</strong> \n\n\r";

				$body['payment'] .= 'PAGAMENTO '.$spaces.': '.$payment_type.$payment_method."\n\n\r";

				if ($order->payment_shippingBrand != '') {

					$body['payment'] .= 'BANDEIRA. '.$spaces.': '.$order->payment_shippingBrand."\n\n\r";

					$body['payment'] .= '<center>*** LEVAR MAQUINA PARA CARTAO ***</center>'."\n\r";

				}

			}else{

				$body['total'] = '';

				$body['payment'] = '';

			}



			$html = '';

			$html .= '<style>

			body {

				width: 280px;

				font-size:12px;

				font-family: Verdana;

			}

			.drashed {

				width: 100%;

				border-bottom: 1.5px dashed #000;

			}

			.table {

				width: 100%;

				font-size: 12px;

				border-collapse: separate;

				border-spacing: 0 8px;

			}

			.table th {

				text-align: left;

				font-size: 11px;

			}

			strong {

				font-size: 11px;

			}

			</style>';



			$html .= nl2br($body['address']);



			$html .= nl2br("\n\r".$row."\n\n\r");



			$html .= '<table class="table">

			<tr>

			<th width="100">P E D I D O</th>

			<th width="140">D A T A &nbsp; E M I S S Ã O</th>

			</tr>

			<tr>

			<td></td>

			<td></td>

			</tr>

			<tr>

			<td>'; 

			for ($i = 0; $i < 10 - strlen($id); $i++) {

				$html .= '0';

			}

			$html .= $id;

			$html .= '</td> 

			<td>'. date('d/m/Y H:i', strtotime($order->date)) .'</td>

			</tr>

			</table>';



			if ($order->shippingAddress == 1) {

				$html .= nl2br("\n\r".$row."\n\n\r");

				$html .= nl2br($body['client']);

			}



			$html .= nl2br("\n\r".$row."\n\n\r");

			$html .= '<table class="table">

			<tr>

			<th>P R O D U T O</th>

			<th style="text-align: right">Q T D.</th>

			<th style="text-align: right">P R E Ç O</th>

			</tr>

			<tr>

			<td></td>

			<td></td>

			<td></td>

			</tr>';

			$html .= $body['product'];

			$html .= '</table>';

			$html .= nl2br("\n\r".$row."\n\n\r");

			$html .= nl2br($body['total']);

			$html .= nl2br("\n\r".$row."\n\n\r");

			$html .= nl2br($body['payment']);

			if ($via == 'client') {

				if ($order->shippingAddress == 1) {

					$html .= nl2br("\n\r".$row."\n\n\r");

					$html .= nl2br("<center><strong>ENTREGA NO ENDEREÇO</strong></center>\n\n\r");

				}else{

					$html .= nl2br("\n\r".$row."\n\n\r");

					$html .= nl2br("<center><strong>RETIRADA NO BALCÃO</strong></center>\n\n\r");

				}

			}

			$html .= nl2br("\n\r".$row."\n\n\r");

			$html .= nl2br("<center><strong>ESTE CUPOM NAO TEM VALIDADE FISCAL</strong></center>\n\n\r");



			$invoice = file_put_contents("invoice/order_body.html", $html);

			if ($invoice) {



				return $html;



				$ch = curl_init();

				$timeout = 0;

				curl_setopt($ch, CURLOPT_URL, base_url().'invoice/order_body.html');

				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);

				$content = curl_exec ($ch);

				curl_close($ch);



				//file_put_contents("invoice/order_client.txt", $content);



			}



		}

		

	}



}

