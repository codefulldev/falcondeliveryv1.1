<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Emails extends CI_Controller {

	function __construct(){

		parent::__construct();
		$timezone = json_decode(file_get_contents('config/timezone.json'));
		date_default_timezone_set($timezone->timezone);

	}

	public function index()
	{
		echo 'Welcome to Emails Controller';
	}

	public function save_email_logo(){
		print_r($_POST);
	}

	public function save_email_body(){

		$id = $_POST['id'];
		$body = $_POST['body'];
		$subject = $_POST['subject'];
		$save = file_put_contents('../views/emails/'.$id, $body);
		if($save){
			$this->db->where('define', $id);
			if($this->db->get('config_emails')->num_rows() > 0){
				$this->db->set('subject', $subject);
				$this->db->where('define', $id);
				$THIS->db->update('config_emails');
			}else{
				$this->db->set('subject', $subject);
				$this->db->set('define', $id);
				$this->db->insert('config_emails');
			}
			echo json_encode(array('save' => true));
		}else{
			echo json_encode(array('save' => false));
		}
	}

}