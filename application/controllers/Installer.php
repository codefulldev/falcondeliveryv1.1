<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Installer extends CI_Controller {



	var $hostname = 'localhost';

	var $username = 'falcon_username';

	var $password = 'falcon_password';

	var $database = 'falcon_database';



	public function __construct(){



		parent::__construct();

    $timezone = json_decode(file_get_contents('config/timezone.json'));

    date_default_timezone_set($timezone->timezone);

		$this->load->dbforge();



	}



	public function index(){



		if ($this->dbforge->create_database('falcon_database')){



			$database = array(

				'host' => $this->hostname,

				'user' => $this->username,

				'pass' => $this->password,

				'db'   => $this->database

			);



			file_put_contents('config.json', json_encode($database));



			$this->db->query("CREATE USER '". $this->username ."'@'localhost' IDENTIFIED BY '". $this->password ."';");

			$this->db->query("GRANT CREATE, ALTER, DELETE, INSERT, SELECT, DROP, UPDATE  ON * . * TO '". $this->username ."'@'localhost';");



			$this->execute_query();



		}



	}



	function queries_execute(){



/*		

-- ----------------------------

-- Table structure for addons

-- ----------------------------

*/

$this->ci->db->query("CREATE TABLE `addons` (

  `id` int(11) NOT NULL AUTO_INCREMENT,

  `name` varchar(70) DEFAULT NULL,

  `price` decimal(10,2) DEFAULT NULL,

  `status` enum('0','1') DEFAULT NULL,

  PRIMARY KEY (`id`)

) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8");



/*

-- ----------------------------

-- Table structure for admin

-- ----------------------------

*/

$this->ci->db->query("CREATE TABLE `admin` (

  `id` int(11) NOT NULL AUTO_INCREMENT,

  `email` varchar(70) DEFAULT NULL,

  `password` varchar(250) DEFAULT NULL,

  `name` varchar(60) DEFAULT NULL,

  `thumbnail` varchar(250) DEFAULT NULL,

  PRIMARY KEY (`id`)

) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8");



/*

-- ----------------------------

-- Table structure for entregadores

-- ----------------------------

*/

$this->ci->db->query("CREATE TABLE `deliveryman` (

  `id` int(11) NOT NULL AUTO_INCREMENT,

  `name` varchar(70) DEFAULT NULL,

  `cpf` varchar(15) DEFAULT NULL,

  `board` varchar(10) DEFAULT NULL,

  `status` enum('1','0') DEFAULT '1',

  PRIMARY KEY (`id`)

) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8");



/*

-- ----------------------------

-- Table structure for historico de entregadores

-- ----------------------------

*/

$this->ci->db->query("CREATE TABLE `deliveryman_historic` (

  `id` int(11) NOT NULL AUTO_INCREMENT,

  `deliveryman_id` int(11) unsigned NOT NULL,

  `total` varchar(20) DEFAULT NULL,

  `coast` varchar(20) DEFAULT NULL,

  `receiver` varchar(20) DEFAULT NULL,

  `period` datetime DEFAULT NULL,

  PRIMARY KEY (`id`)

) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8");



/*

-- ----------------------------

-- Table structure for bairros

-- ----------------------------

*/

$this->ci->db->query("CREATE TABLE `bairros` (

  `bairro_id` int(10) unsigned NOT NULL AUTO_INCREMENT,

  `cidade_id` int(10) unsigned NOT NULL,

  `desc_bairro` varchar(45) NOT NULL,

  PRIMARY KEY (`bairro_id`),

  KEY `cidade_id` (`cidade_id`)

) ENGINE=MyISAM AUTO_INCREMENT=28969 DEFAULT CHARSET=utf8");



/*

-- ----------------------------

-- Table structure for category

-- ----------------------------

*/

$this->ci->db->query("CREATE TABLE `category` (

  `id` int(11) NOT NULL AUTO_INCREMENT,

  `name` varchar(70) DEFAULT NULL,

  `status` enum('0','1') DEFAULT NULL,

  PRIMARY KEY (`id`)

) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8");



/*

-- ----------------------------

-- Table structure for chatting

-- ----------------------------

*/

$this->ci->db->query("CREATE TABLE `chatting` (

  `id` int(11) NOT NULL AUTO_INCREMENT,

  `client_id` int(11) NOT NULL,

  `func_id` int(11) NOT NULL,

  `message` longtext,

  `date` datetime DEFAULT NULL,

  `send` enum('1','0') DEFAULT '0',

  `answer` enum('2','1') DEFAULT NULL,

  PRIMARY KEY (`id`)

) ENGINE=MyISAM AUTO_INCREMENT=132 DEFAULT CHARSET=latin1");



/*

-- ----------------------------

-- Table structure for cidades

-- ----------------------------

*/

$this->ci->db->query("CREATE TABLE `cidades` (

  `cidade_id` int(10) unsigned NOT NULL,

  `desc_cidade` varchar(60) NOT NULL,

  `flg_estado` char(2) NOT NULL,

  PRIMARY KEY (`cidade_id`)

) ENGINE=MyISAM DEFAULT CHARSET=utf8");



/*

-- ----------------------------

-- Table structure for clients

-- ----------------------------

*/

$this->ci->db->query("CREATE TABLE `clients` (

  `id` int(11) NOT NULL AUTO_INCREMENT,

  `name` varchar(50) DEFAULT NULL,

  `email` varchar(80) DEFAULT NULL,

  `password` varchar(250) DEFAULT NULL,

  `phone` varchar(20) DEFAULT NULL,

  `status` enum('1','0') DEFAULT '1',

  `thumbnail` varchar(250) DEFAULT NULL,

  `cep` int(11) DEFAULT NULL,

  `address` text,

  `refer` text,

  `since` datetime DEFAULT NULL,

  PRIMARY KEY (`id`)

) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8");



/*

-- ----------------------------

-- Table structure for clients_cc

-- ----------------------------

*/

$this->ci->db->query("CREATE TABLE `clients_cc` (

  `id` int(11) NOT NULL AUTO_INCREMENT,

  `clients_id` int(11) NOT NULL,

  `number` int(15) DEFAULT NULL,

  `ccv` int(11) DEFAULT NULL,

  `expire` varchar(20) DEFAULT NULL,

  PRIMARY KEY (`id`)

) ENGINE=MyISAM DEFAULT CHARSET=latin1");



/*

-- ----------------------------

-- Table structure for clients_address

-- ----------------------------

*/

$this->ci->db->query("CREATE TABLE `clients_address` (

  `id` int(11) NOT NULL AUTO_INCREMENT,

  `clients_id` int(11) NOT NULL,

  `name` text,

  `address` text,

  `cep` int(11),

  PRIMARY KEY (`id`)

) ENGINE=MyISAM DEFAULT CHARSET=latin1");



/*

-- ----------------------------

-- Table structure for comingsoon

-- ----------------------------

*/

$this->ci->db->query("CREATE TABLE `comingsoon` (

  `id` int(11) NOT NULL AUTO_INCREMENT,

  `title` text,

  `period` text,

  PRIMARY KEY (`id`)

) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1");



/*

-- ----------------------------

-- Table structure for config

-- ----------------------------

*/

$this->ci->db->query("CREATE TABLE `config` (

  `id` int(11) NOT NULL AUTO_INCREMENT,

  `title` text,

  `slogan` text,

  `url` text,

  `email` text,

  `phone` text,

  `email_logo` varchar(250) DEFAULT NULL,

  `site_logo` varchar(250) DEFAULT NULL,

  `dirr` varchar(50) DEFAULT NULL,

  `android` text,

  `ios` text,

  `windowsphone` text,

  `minbuy` int(11),

  `ipdomain` VARCHAR(100) DEFAULT NULL,

  `port` int(11),
  
  `deliverytime` int(11),

  PRIMARY KEY (`id`)

) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8");



/*

-- ----------------------------

-- Table structure for config_address

-- ----------------------------

*/

$this->ci->db->query("CREATE TABLE `config_address` (

  `id` int(11) NOT NULL AUTO_INCREMENT,

  `address` text,

  `street` text,

  `number` int(11) DEFAULT NULL,

  `locality` text,

  `city` text,

  `complement` text,

  `coord_y` varchar(30) DEFAULT NULL,

  `coord_x` varchar(30) DEFAULT NULL,

  `cep` int(11) DEFAULT NULL,

  `schendule` enum('1','0') DEFAULT '0',

  `delivery_price` decimal(10,2) DEFAULT NULL,

  PRIMARY KEY (`id`)

) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8");



/*

-- ----------------------------

-- Table structure for config_emails

-- ----------------------------

*/

$this->ci->db->query("CREATE TABLE `config_emails` (

  `id` int(11) NOT NULL AUTO_INCREMENT,

  `subject` text,

  `define` varchar(70) DEFAULT NULL,

  `body` longtext,

  PRIMARY KEY (`id`)

) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8");



/*

-- ----------------------------

-- Table structure for config_payments

-- ----------------------------

*/

$this->ci->db->query("CREATE TABLE `config_payments` (

  `id` int(11) NOT NULL AUTO_INCREMENT,

  `payments` varchar(50) DEFAULT NULL,

  `payments_x` int(11) DEFAULT NULL,

  `payments_url` text,

  `payments_token` varchar(250) DEFAULT NULL,

  `payments_email` varchar(200) DEFAULT NULL,

  `payments_type` varchar(10) DEFAULT '1',

  `payments_envelopment` enum('1','0') DEFAULT '0',

  PRIMARY KEY (`id`)

) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8");



/*

-- ----------------------------

-- Table structure for cupons

-- ----------------------------

*/

$this->ci->db->query("CREATE TABLE `cupons` (

  `id` int(11) NOT NULL AUTO_INCREMENT,

  `value` int(11) DEFAULT NULL,

  `active` enum('1','0') DEFAULT '0',

  `expire` datetime DEFAULT NULL,

  `title` text,

  `genered` varchar(200),

  PRIMARY KEY (`id`)

) ENGINE=MyISAM DEFAULT CHARSET=latin1");



/*

-- ----------------------------

-- Table structure for funcionally

-- ----------------------------

*/

$this->ci->db->query("CREATE TABLE `funcionally` (

  `id` int(11) NOT NULL AUTO_INCREMENT,

  `days` text,

  `hours` text,

  PRIMARY KEY (`id`)

) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=latin1");



/*

-- ----------------------------

-- Table structure for inbox

-- ----------------------------

*/

$this->ci->db->query("CREATE TABLE `inbox` (

  `id` int(11) NOT NULL,

  `clients_id` int(11) DEFAULT NULL,

  `message` longtext,

  `date` datetime DEFAULT NULL,

  `status` enum('2','1','0') DEFAULT NULL,

  PRIMARY KEY (`id`)

) ENGINE=MyISAM DEFAULT CHARSET=latin1");



/*

-- ----------------------------

-- Table structure for ingredients

-- ----------------------------

*/

$this->ci->db->query("CREATE TABLE `ingredients` (

  `id` int(11) NOT NULL AUTO_INCREMENT,

  `name` varchar(70) DEFAULT NULL,

  `about` text,

  `price` decimal(10,2) DEFAULT NULL,

  `status` enum('0','1') DEFAULT NULL,

  PRIMARY KEY (`id`)

) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8");



/*

-- ----------------------------

-- Table structure for logradouros

-- ----------------------------

*/

$this->ci->db->query("CREATE TABLE `logradouros` (

  `num_cep` int(8) unsigned zerofill NOT NULL,

  `bairro_id` int(10) unsigned NOT NULL,

  `desc_logradouro` varchar(45) NOT NULL,

  `desc_tipo` varchar(10) NOT NULL,

  PRIMARY KEY (`num_cep`),

  KEY `bairro_id` (`bairro_id`)

) ENGINE=MyISAM DEFAULT CHARSET=utf8");



/*

-- ----------------------------

-- Table structure for maintenance

-- ----------------------------

*/

$this->ci->db->query("CREATE TABLE `maintenance` (

  `id` int(11) NOT NULL AUTO_INCREMENT,

  `active` enum('1','0') DEFAULT '0',

  `period` text,

  `title` text,

  `subtitle` text,

  PRIMARY KEY (`id`)

) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1");



/*

-- ----------------------------

-- Table structure for networks

-- ----------------------------

*/

$this->ci->db->query("CREATE TABLE `networks` (

  `id` int(11) NOT NULL AUTO_INCREMENT,

  `facebook` text,

  `twitter` text,

  `instagram` text,

  `google` text,

  PRIMARY KEY (`id`)

) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1");



/*

-- ----------------------------

-- Table structure for oauth

-- ----------------------------

*/

$this->ci->db->query("CREATE TABLE `oauth` (

  `id` int(11) NOT NULL AUTO_INCREMENT,

  `appid` text,

  `version` text,

  `clientid` text,

  PRIMARY KEY (`id`)

) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1");



/*

-- ----------------------------

-- Table structure for optionals

-- ----------------------------

*/

$this->ci->db->query("CREATE TABLE `optionals` (

  `id` int(11) NOT NULL AUTO_INCREMENT,

  `name` varchar(80) DEFAULT NULL,

  `obrigatory` enum('1','0') DEFAULT '0',

  `price` decimal(10,2) DEFAULT NULL,

  `status` enum('1','0') DEFAULT '1',

  PRIMARY KEY (`id`)

) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8");



/*

-- ----------------------------

-- Table structure for optionals_options

-- ----------------------------

*/

$this->ci->db->query("CREATE TABLE `optionals_options` (

  `id` int(11) NOT NULL AUTO_INCREMENT,

  `optionals_id` int(11) DEFAULT NULL,

  `name` varchar(70) DEFAULT NULL,

  `status` enum('1','0') DEFAULT '1',

  PRIMARY KEY (`id`)

) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=utf8");



/*

-- ----------------------------

-- Table structure for orders

-- ----------------------------

*/

$this->ci->db->query("CREATE TABLE `orders` (

  `id` int(11) NOT NULL AUTO_INCREMENT,

  `clients_id` int(11) DEFAULT NULL,

  `clients_address_id` int(11) DEFAULT NULL,

  `payment` varchar(30) DEFAULT NULL,

  `payment_shippingBrand` text,

  `receiver` decimal(10,2) DEFAULT NULL,

  `send` decimal(10,2) DEFAULT NULL,

  `date` datetime DEFAULT NULL,

  `status` enum('2','1','0') DEFAULT '0',

  `date_agend` datetime DEFAULT NULL,

  `shippingData` text,

  `shippingAddress` enum('1','0') DEFAULT NULL,

  `orderType` int(11) NOT NULL DEFAULT '0',

  `cupom` varchar(200) DEFAULT NULL,

  `promotional` decimal(10,2) DEFAULT NULL,

  `clients_address_id` int(11) DEFAULT NULL,

  `deliveryman_id` int(11) DEFAULT NULL,

  `descount` decimal(10,2) DEFAULT NULL,

  `descountComplete` decimal(10,2) DEFAULT NULL,

  PRIMARY KEY (`id`)

) ENGINE=MyISAM AUTO_INCREMENT=111 DEFAULT CHARSET=utf8");



/*

-- ----------------------------

-- Table structure for orders_details

-- ----------------------------

*/

$this->ci->db->query("CREATE TABLE `orders_details` (

  `id` int(11) NOT NULL AUTO_INCREMENT,

  `order_id` int(11) DEFAULT NULL,

  `product_id` int(11) DEFAULT NULL,

  `optionals` text,

  `addons` text,

  `takes` text,

  `flavors` text,

  `quantity` int(11) DEFAULT NULL,

  `schendule` text,

  `promotional` decimal(10,2) DEFAULT NULL,

  `subtotal` decimal(10,2) DEFAULT NULL,

  PRIMARY KEY (`id`)

) ENGINE=MyISAM AUTO_INCREMENT=92 DEFAULT CHARSET=utf8");



/* ----------------------------

-- Table structure for products

-- ----------------------------

DROP TABL

*/

$this->ci->db->query("CREATE TABLE `products` (

  `id` int(11) NOT NULL AUTO_INCREMENT,

  `category_id` int(11) NOT NULL,

  `name` varchar(50) DEFAULT NULL,

  `description` text,

  `thumbnail` varchar(250) DEFAULT NULL,

  `price` text DEFAULT NULL,

  `ingredients` varchar(100) DEFAULT NULL,

  `flavors` varchar(100) DEFAULT NULL,

  `addons` varchar(100) DEFAULT NULL,

  `optionals` varchar(100) DEFAULT NULL,

  `status` int(11) DEFAULT NULL,

  `sells` int(11) DEFAULT '0',

  `pizza` enum('1','0') DEFAULT '0',

  `stock` int(11) NOT NULL,

  `sale` enum('1','0'),

  PRIMARY KEY (`id`),

  KEY `fk_products_category1_idx` (`category_id`),

  CONSTRAINT `fk_products_category1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE

) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8");

/*

-- ----------------------------

-- Table structure for promotions

-- ----------------------------

*/

$this->ci->db->query("CREATE TABLE `promotions` (

  `id` int(11) NOT NULL AUTO_INCREMENT,

  `products_id` varchar(30) DEFAULT NULL,

  `title` text,

  `promotional_price` decimal(10,2) DEFAULT NULL,

  `date` datetime DEFAULT NULL,

  `expire` varchar(30) DEFAULT NULL,

  `active` enum('1','0') NOT NULL DEFAULT '1',

  PRIMARY KEY (`id`)

) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1");



/*

-- ----------------------------

-- Table structure for gallery

-- ----------------------------

*/

$this->ci->db->query("CREATE TABLE `gallery` (

  `id` int(11) NOT NULL AUTO_INCREMENT,

  `alt` text,

  `thumbnail` varchar(250) DEFAULT NULL,

  `products_id` int(11) NOT NULL,

  PRIMARY KEY (`id`),

  KEY `fk_gallery_products_idx` (`products_id`),

  CONSTRAINT `fk_gallery_products` FOREIGN KEY (`products_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE

) ENGINE=InnoDB DEFAULT CHARSET=utf8");





	}



}