<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Admin extends CI_Controller {



	var $theme = 'admin/';

	var $base  = 'control/';

	var $theme_folder = 'themes/';

	// URI's

	var $salesURI           = 'sales';

	var $productURI         = 'products';

	var $categoryURI        = 'category';

	var $addonsURI          = 'addons';

	var $ingredURI          = 'ingredients';

	var $optionURI          = 'options';

	var $ordersURI          = 'orders';

	var $shippingURI        = 'deliveryman';

	var $clientsURI         = 'clients';

	var $configURI          = 'settings';

	var $inbox   	        = 'inbox';

	var $finances           = 'finances';

	var $account   	        = 'account';

	var $reports   	        = 'reports';

	var $promoCupom   	    = 'cupom';

	var $promoPromotion   	= 'promotion';



	function __construct(){



		parent::__construct();

		$timezone = json_decode(file_get_contents('config/timezone.json'));

		date_default_timezone_set($timezone->timezone);



		if(!file_exists('config/config.json') OR !file_exists('config/INSTALL')){

			redirect('config');

		}

	}

    public function sess(){
        if (!$this->session->admin) {
            redirect($this->base.'/login');
        }
    }

	public function config_general(){

		return $this->db->get('config')->row();

	}



	public function config_address(){

		return $this->db->get('config_address')->row();

	}



	public function config_emails($define){

		$this->db->where('define', $define);

		return $this->db->get('config_emails')->row();

	}



	public function index()

	{

		if($this->session->admin){

			$JavaScript = array(

				$this->theme_folder.'plugins/bower_components/bootstrap-select/bootstrap-select.min.js',

				$this->theme_folder.'plugins/bower_components/switchery/dist/switchery.min.js',

				$this->theme_folder.'plugins/bower_components/waypoints/lib/jquery.waypoints.js',

				$this->theme_folder.'plugins/bower_components/counterup/jquery.counterup.min.js',

				$this->theme_folder.'plugins/bower_components/raphael/raphael-min.js',

				$this->theme_folder.'plugins/bower_components/morrisjs/morris.js',

				$this->theme_folder.'plugins/bower_components/moment/moment.js',

				$this->theme_folder.'plugins/bower_components/calendar/dist/fullcalendar.min.js',

				$this->theme_folder.'plugins/bower_components/calendar/dist/cal-init.js',

				$this->theme_folder.'plugins/bower_components/toast-master/js/jquery.toast.js',

				$this->theme_folder.'plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js',

				$this->theme_folder.'plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js',

				$this->theme_folder.'plugins/bower_components/timepicker/bootstrap-timepicker.min.js',

				$this->theme_folder.'plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js',

				$this->theme_folder.'plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js',

				$this->theme_folder.'plugins/bower_components/jquery.countdown-2.2.0/jquery.countdown.min.js'

			);

			$StyleSheet = array(

				$this->theme_folder.'plugins/bower_components/bootstrap-select/bootstrap-select.min.css',

				$this->theme_folder.'plugins/bower_components/switchery/dist/switchery.min.css',

				$this->theme_folder.'plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css',

				$this->theme_folder.'plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css',

				$this->theme_folder.'plugins/bower_components/timepicker/bootstrap-timepicker.min.css',

				$this->theme_folder.'plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css'

			);

			$ExternalJS = array(

				$this->theme_folder.'ampleadmin/js/dashboard1.js',

				'assets/js/functions.js'

			);



			if($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST['promotionID'])){

				$id = $_POST['promotionID'];

				$title = $_POST['title'];

				$price = $_POST['price'];



				if (isset($_POST['products'])) {

					$products = array();

					foreach ($_POST['products'] as $product) {

						$products[] = $product;

					}

					$this->db->set('products_id', join("|", $products));

				}

				$this->db->set('title', $title);

				$this->db->set('promotional_price', $price);

				$this->db->where('id', $id);

				$update = $this->db->update('promotions');

				if ($update) {

					redirect($this->base);

				}

			}



			$data = array(

				'page' => 'init',

				'root' => $this->theme,

				'JavaScript' => $JavaScript,

				'StyleSheet' => $StyleSheet,

				'ExternalJS' => $ExternalJS

			);



			$this->load->view('admin', $data);

		}else{
			$this->sess();
		}



	}



	public function login(){

		$JavaScript = array();

		$StyleSheet = array();

		$ExternalJS = array();



		$data = array(

			'JavaScript' => $JavaScript,

			'StyleSheet' => $StyleSheet,

			'ExternalJS' => $ExternalJS

		);

        $this->load->view($this->theme.'login', $data);


	}



	public function logout(){

		$this->session->unset_userdata('admin');

		redirect($this->base);

	}



	public function ConfigController(){

		return $this->db->get('config')->row();

	}



	public function UserController(){

		$this->db->where('id', $this->session->admin);

		return $this->db->get('admin')->row();

	}



	public function ChatPage(){

        

		$JavaScript = array();

		$StyleSheet = array();

		$ExternalJS = array(

			$this->theme_folder.'ampleadmin/js/chat.js'

		);



		$data = array(

			'page' => 'chat',

			'root' => $this->theme,

			'JavaScript' => $JavaScript,

			'StyleSheet' => $StyleSheet,

			'ExternalJS' => $ExternalJS

		);



		$this->load->view('admin', $data);



	}



	public function get_chatting($id){

		$this->db->where('client_id', $id);
		$this->db->order_by('date', 'asc');
		$get = $this->db->get('chatting');

		if ($get->num_rows() > 0) {

			return $get->result();

		}else{

			return false;

		}



	}



	public function get_chatting_users(){

		

		$get = $this->db->get('clients');

		if ($get->num_rows() > 0) {

			return $get->result();

		}



	}



	public function account(){

		$this->sess();

            $JavaScript = array();

            $StyleSheet = array();

            $ExternalJS = array(

                $this->theme_folder.'ampleadmin/js/account.js'

            );



            $data = array(

                'page' => 'account',

                'root' => $this->theme,

                'JavaScript' => $JavaScript,

                'StyleSheet' => $StyleSheet,

                'ExternalJS' => $ExternalJS

            );



            $this->load->view('admin', $data);

	}



	public function cupom(){

		$JavaScript = array();

		$StyleSheet = array();

		$ExternalJS = array(

			$this->theme_folder.'ampleadmin/js/cupons.js',

			$this->theme_folder.'ampleadmin/js/edit.js',

		);



		$data = array(

			'page' => 'cupom',

			'root' => $this->theme,

			'JavaScript' => $JavaScript,

			'StyleSheet' => $StyleSheet,

			'ExternalJS' => $ExternalJS

		);



        $this->load->view('admin', $data);
    }

	public function promotion(){

        $this->sess();

		$JavaScript = array();

		$StyleSheet = array();

		$ExternalJS = array(

			$this->theme_folder.'ampleadmin/js/account.js',

			$this->theme_folder.'ampleadmin/js/edit.js',

		);



		$data = array(

			'page' => 'promotion',

			'root' => $this->theme,

			'JavaScript' => $JavaScript,

			'StyleSheet' => $StyleSheet,

			'ExternalJS' => $ExternalJS

		);



		$this->load->view('admin', $data);

	}



	public function inbox(){

        $this->sess();


		$JavaScript = array();

		$StyleSheet = array();

		$ExternalJS = array(

			//$this->theme_folder.'ampleadmin/js/sales.js'

		);



		$data = array(

			'page' => 'inbox',

			'root' => $this->theme,

			'JavaScript' => $JavaScript,

			'StyleSheet' => $StyleSheet,

			'ExternalJS' => $ExternalJS

		);



		$this->load->view('admin', $data);

	}



	public function inbox_detail(){

        $this->sess();


		$JavaScript = array();

		$StyleSheet = array();

		$ExternalJS = array(

			//$this->theme_folder.'ampleadmin/js/sales.js'

		);



		$data = array(

			'page' => 'inbox_detail',

			'root' => $this->theme,

			'JavaScript' => $JavaScript,

			'StyleSheet' => $StyleSheet,

			'ExternalJS' => $ExternalJS

		);



		$this->load->view('admin', $data);

	}



	public function sales(){

        $this->sess();

		$JavaScript = array(

			$this->theme_folder.'plugins/bower_components/switchery/dist/switchery.min.js',

			$this->theme_folder.'plugins/bower_components/bootstrap-select/bootstrap-select.min.js',

			$this->theme_folder.'plugins/bower_components/custom-select/custom-select.min.js',

			$this->theme_folder.'plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js',

			$this->theme_folder.'plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js',

			$this->theme_folder.'plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js',

			$this->theme_folder.'plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js',

			$this->theme_folder.'plugins/bower_components/styleswitcher/jQuery.style.switcher.js',

			$this->theme_folder.'js/jquery.slimscroll.js',

			'assets/js/jquery.selectric.js',

			'assets/js/mount_sales.js',

		);

		$StyleSheet = array(

			$this->theme_folder.'plugins/bower_components/bootstrap-select/bootstrap-select.min.css',

			$this->theme_folder.'plugins/bower_components/custom-select/custom-select.css',

			$this->theme_folder.'plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css',

			$this->theme_folder.'plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css',

			$this->theme_folder.'plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css',

			$this->theme_folder.'plugins/bower_components/switchery/dist/switchery.min.css',

			'assets/css/pizza/pizza.css',

			'assets/css/pizza/style.css'

		);

		$ExternalJS = array(

            $this->theme_folder.'ampleadmin/js/sales_test.js',
            $this->theme_folder.'ampleadmin/js/deliveryman.js'

		);



		$data = array(

			'page' => 'sales',

			'root' => $this->theme,

			'JavaScript' => $JavaScript,

			'StyleSheet' => $StyleSheet,

			'ExternalJS' => $ExternalJS

		);



		$this->load->view('admin', $data);



	}



	public function recent_orders(){

		$this->db->where('status', '0');

		$this->db->limit(5);

		$this->db->order_by('date', 'DESC');

		$get = $this->db->get('orders');

		if ($get->num_rows() > 0) {

			return $get->result();

		}else{

			return null;

		}

	}



	public function ecommerce_products(){

        
		$this->sess();

		$JavaScript = array(

			$this->theme_folder.'plugins/bower_components/multiselect/js/jquery.multi-select.js',

			$this->theme_folder.'plugins/bower_components/bootstrap-select/bootstrap-select.min.js',

			'https://masonry.desandro.com/v2/jquery.masonry.min.js',

			$this->theme_folder.'plugins/bower_components/fancybox/ekko-lightbox.min.js',

			$this->theme_folder.'plugins/bower_components/mask-jquery/jquery.mask.min.js',

			$this->theme_folder.'plugins/bower_components/footable/js/footable.all.min.js'

		);

		$StyleSheet = array(

			$this->theme_folder.'plugins/bower_components/multiselect/css/multi-select.css',

			$this->theme_folder.'plugins/bower_components/bootstrap-select/bootstrap-select.min.css',

			$this->theme_folder.'plugins/bower_components/gallery/css/animated-masonry-gallery.css',

			$this->theme_folder.'plugins/bower_components/fancybox/ekko-lightbox.min.css',

			$this->theme_folder.'plugins/bower_components/footable/css/footable.core.css'

		);

		$ExternalJS = array(

			$this->theme_folder.'ampleadmin/js/products.js',

			$this->theme_folder.'ampleadmin/js/masks.js',

			$this->theme_folder.'ampleadmin/js/footable-init.js'

		);



		$data = array(

			'page' => 'ecommerce_products',

			'root' => $this->theme,

			'JavaScript' => $JavaScript,

			'StyleSheet' => $StyleSheet,

			'ExternalJS' => $ExternalJS

		);



		$this->load->view('admin', $data);

	}



	public function ecommerce_products_edit(){
        
		$this->sess();

		$JavaScript = array(

			$this->theme_folder.'plugins/bower_components/multiselect/js/jquery.multi-select.js',

			$this->theme_folder.'plugins/bower_components/bootstrap-select/bootstrap-select.min.js',

			'https://masonry.desandro.com/v2/jquery.masonry.min.js',

			$this->theme_folder.'plugins/bower_components/fancybox/ekko-lightbox.min.js',

			$this->theme_folder.'plugins/bower_components/mask-jquery/jquery.mask.min.js',

			$this->theme_folder.'plugins/bower_components/footable/js/footable.all.min.js'

		);

		$StyleSheet = array(

			$this->theme_folder.'plugins/bower_components/multiselect/css/multi-select.css',

			$this->theme_folder.'plugins/bower_components/bootstrap-select/bootstrap-select.min.css',

			$this->theme_folder.'plugins/bower_components/gallery/css/animated-masonry-gallery.css',

			$this->theme_folder.'plugins/bower_components/fancybox/ekko-lightbox.min.css',

			$this->theme_folder.'plugins/bower_components/footable/css/footable.core.css'

		);

		$ExternalJS = array(

			$this->theme_folder.'ampleadmin/js/products.js',

			$this->theme_folder.'ampleadmin/js/masks.js',

			$this->theme_folder.'ampleadmin/js/footable-init.js'

		);



		$data = array(

			'page' => 'ecommerce_products_edit',

			'root' => $this->theme,

			'JavaScript' => $JavaScript,

			'StyleSheet' => $StyleSheet,

			'ExternalJS' => $ExternalJS

		);



		$this->load->view('admin', $data);

	}



	public function ecommerce_products_get($id){

		$this->db->where('id', $id);

		$product = $this->db->get('products');



		if ($product->num_rows() > 0) {

			return $product->row();

		}else{

			redirect($this->base.$this->productURI);

		}

	}



	public function model_foreach_select($table, $where){

		$this->db->where('status', $where);

		return $this->db->get($table)->result();

	}



	public function ecommerce_category(){
        
		$this->sess();

		$JavaScript = array(

			$this->theme_folder.'plugins/bower_components/footable/js/footable.all.min.js',

			$this->theme_folder.'plugins/bower_components/tiny-editable/mindmup-editabletable.js'

		);

		$StyleSheet = array(

			$this->theme_folder.'plugins/bower_components/footable/css/footable.core.css'

		);

		$ExternalJS = array(

			$this->theme_folder.'ampleadmin/js/category.js',

			$this->theme_folder.'ampleadmin/js/edit.js',

			$this->theme_folder.'ampleadmin/js/footable-init.js'

		);



		$data = array(

			'page' => 'ecommerce_category',

			'root' => $this->theme,

			'JavaScript' => $JavaScript,

			'StyleSheet' => $StyleSheet,

			'ExternalJS' => $ExternalJS

		);



		$this->load->view('admin', $data);

	}



	public function ecommerce_addons(){

		$this->sess();
		
		$JavaScript = array(

			$this->theme_folder.'plugins/bower_components/mask-jquery/jquery.mask.min.js',

			$this->theme_folder.'plugins/bower_components/footable/js/footable.all.min.js',

			$this->theme_folder.'plugins/bower_components/tiny-editable/mindmup-editabletable.js'

		);

		$StyleSheet = array(

			$this->theme_folder.'plugins/bower_components/footable/css/footable.core.css'

		);

		$ExternalJS = array(

			$this->theme_folder.'ampleadmin/js/addons.js',

			$this->theme_folder.'ampleadmin/js/masks.js',

			$this->theme_folder.'ampleadmin/js/edit.js',

			$this->theme_folder.'ampleadmin/js/footable-init.js'

		);



		$data = array(

			'page' => 'ecommerce_addons',

			'root' => $this->theme,

			'JavaScript' => $JavaScript,

			'StyleSheet' => $StyleSheet,

			'ExternalJS' => $ExternalJS

		);



		$this->load->view('admin', $data);

	}



	public function ecommerce_options(){

		$this->sess();

		$JavaScript = array(

			$this->theme_folder.'plugins/bower_components/mask-jquery/jquery.mask.min.js',

			$this->theme_folder.'plugins/bower_components/footable/js/footable.all.min.js',

			$this->theme_folder.'plugins/bower_components/tiny-editable/mindmup-editabletable.js'

		);

		$StyleSheet = array(

			$this->theme_folder.'plugins/bower_components/footable/css/footable.core.css'

		);

		$ExternalJS = array(

			$this->theme_folder.'ampleadmin/js/optionals.js',

			$this->theme_folder.'ampleadmin/js/masks.js',

			$this->theme_folder.'ampleadmin/js/edit.js',

			$this->theme_folder.'ampleadmin/js/footable-init.js'

		);



		$data = array(

			'page' => 'ecommerce_options',

			'root' => $this->theme,

			'JavaScript' => $JavaScript,

			'StyleSheet' => $StyleSheet,

			'ExternalJS' => $ExternalJS

		);



		$this->load->view('admin', $data);

	}



	public function ecommerce_ingredients(){

		$this->sess();

		$JavaScript = array(

			$this->theme_folder.'plugins/bower_components/mask-jquery/jquery.mask.min.js',

			$this->theme_folder.'plugins/bower_components/footable/js/footable.all.min.js',

			$this->theme_folder.'plugins/bower_components/tiny-editable/mindmup-editabletable.js'

		);

		$StyleSheet = array(

			$this->theme_folder.'plugins/bower_components/footable/css/footable.core.css'

		);

		$ExternalJS = array(

			$this->theme_folder.'ampleadmin/js/ingredients.js',

			$this->theme_folder.'ampleadmin/js/masks.js',

			$this->theme_folder.'ampleadmin/js/edit.js',

			$this->theme_folder.'ampleadmin/js/footable-init.js'

		);



		$data = array(

			'page' => 'ecommerce_ingredients',

			'root' => $this->theme,

			'JavaScript' => $JavaScript,

			'StyleSheet' => $StyleSheet,

			'ExternalJS' => $ExternalJS

		);



		$this->load->view('admin', $data);

	}



	public function ecommerce_orders(){

		$this->sess();

		$JavaScript = array();

		$StyleSheet = array();

		$ExternalJS = array(

			$this->theme_folder.'ampleadmin/js/orders.js'

		);



		$data = array(

			'page' => 'ecommerce_orders',

			'root' => $this->theme,

			'JavaScript' => $JavaScript,

			'StyleSheet' => $StyleSheet,

			'ExternalJS' => $ExternalJS

		);



		$this->load->view('admin', $data);

	}



	public function ecommerce_orders_change_status(){

		$id = isset($_POST['id']) ? $_POST['id'] : 0;

		$status = isset($_POST['status']) ? $_POST['status'] : null;



		if($id > 0){

			$this->db->set('status', $status);

			$this->db->where('id', $id);

			$update = $this->db->update('orders');



			if($update){

				echo json_encode(array('error' => false));

			}else{

				echo json_encode(array('error' => true));

			}



		}



		if($status == '1'){

			$this->change_sells($id, '+');

		}



	}



	function change_sells($id, $op){



		$this->db->where('order_id', $id);

		$details = $this->db->get('orders_details');



		foreach ($details->result() as $detail) {

			$this->db->where('id', $detail->product_id);

			$product = $this->db->get('products')->row();

			$qtd = 1 * $detail->quantity;

			if ($op == '+') {

				$sells = $product->sells + $qtd;

			}else{

				$sells = ($product->sells - $qtd);

			}

			$this->db->set('sells', $sells);

			$this->db->where('id', $detail->product_id);

			$this->db->update('products');

		}



	}



	public function ecommerce_clients(){

		$this->sess();

		$JavaScript = array();
		$StyleSheet = array();
		$ExternalJS = array(
			$this->theme_folder.'ampleadmin/js/clients.js'
		);

		$data = array(
			'page' => 'ecommerce_clients',
			'root' => $this->theme,
			'JavaScript' => $JavaScript,
			'StyleSheet' => $StyleSheet,
			'ExternalJS' => $ExternalJS
		);

		$this->load->view('admin', $data);

	}


	function ecommerce_clients_list(){

		$clients = $this->db->get('clients');

		if ($clients->num_rows() > 0) {

			return $clients->result();

		}else{

			return array();

		}

	}


	public function ecommerce_deliveryman(){

		$this->sess();

		$JavaScript = array();
		$StyleSheet = array();
		$ExternalJS = array(
			$this->theme_folder.'ampleadmin/js/deliveryman.js?v=1.1'
		);

		$data = array(
			'page' => 'ecommerce_deliveryman',
			'root' => $this->theme,
			'JavaScript' => $JavaScript,
			'StyleSheet' => $StyleSheet,
			'ExternalJS' => $ExternalJS
		);

		$this->load->view('admin', $data);
	}


	function ecommerce_deliveryman_list(){

		$deliveryman = $this->db->get('deliveryman');

		if ($deliveryman->num_rows() > 0) {
			return $deliveryman->result();
		}else{
			return array();
		}

	}


	function ecommerce_deliveryman_reports(){


		$id = $_POST['id'];

		if ($_SERVER['REQUEST_METHOD'] === 'POST' && !empty($_POST['total'])) {
			$deliveryman_id = $id;
			$total          = isset($_POST['total']) ? $_POST['total'] : '';
			$coast          = isset($_POST['coast']) ? $_POST['coast'] : '';
			$receiver       = isset($_POST['receiver']) ? $_POST['receiver'] : '';
			$period         = date('Y-m-d H:i:s');

			$this->db->set('deliveryman_id', $deliveryman_id);
			$this->db->set('total', $total);
			$this->db->set('coast', $coast);
			$this->db->set('receiver', $receiver);
			$this->db->set('period', $period);
			$this->db->insert('deliveryman_historic');
			if ($this->db->insert_id() > 0) {
				echo json_encode(array('error' => false));
			}else{
				echo json_encode(array('error' => true));
			}
		}else{

			$this->db->where('deliveryman_id', $id);
			$get = $this->db->get('orders');
			if ($get->num_rows() > 0) {
				$this->load->view('admin/sources/reports', array('reports' => $get->result(), 'id' => $id));
			}else{
				$this->load->view('admin/sources/reports', array('reports' => ''));
			}
		}
	}


	function ecommerce_clients_edit(){

		$this->sess();

		$JavaScript = array();

		$StyleSheet = array();

		$ExternalJS = array(

			$this->theme_folder.'ampleadmin/js/clients.js',

			$this->theme_folder.'ampleadmin/js/clients_edit.js'

		);



		$data = array(

			'page' => 'ecommerce_clients_edit',

			'root' => $this->theme,

			'JavaScript' => $JavaScript,

			'StyleSheet' => $StyleSheet,

			'ExternalJS' => $ExternalJS

		);



		$this->load->view('admin', $data);

	}



	public function ecommerce_invoice(){

		$this->sess();

		$uri = explode("/", $_SERVER['REQUEST_URI']);



		if(end($uri) !== null && end($uri) > 0){



			$id = end($uri);



			$order = $this->invoice->invoice_order_load($id)->row();

			$this->db->where('id', $order->clients_id);

			$client = $this->db->get('clients')->row();

			$orders_details = $this->invoice->invoice_order_details($id);



			$JavaScript = array();

			$StyleSheet = array();

			$ExternalJS = array(

				$this->theme_folder.'ampleadmin/js/jquery.PrintArea.js',

				$this->theme_folder.'ampleadmin/js/invoice.js'

			);



			$data = array(

				'page' => 'ecommerce_invoice',

				'root' => $this->theme,

				'order' => $order,

				'client' => $client,

				'orders_details' => $orders_details,

				'JavaScript' => $JavaScript,

				'StyleSheet' => $StyleSheet,

				'ExternalJS' => $ExternalJS

			);



			$this->load->view('admin', $data);

		}else{

			$this->load->view('error');

		}



	}



	public function ecommerce_settings(){

		$this->sess();

		$JavaScript = array(

			$this->theme_folder.'plugins/bower_components/custom-select/custom-select.min.js',

			$this->theme_folder.'plugins/bower_components/bootstrap-select/bootstrap-select.min.js',

			$this->theme_folder.'plugins/bower_components/icheck/icheck.min.js',

			$this->theme_folder.'plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js',

            $this->theme_folder.'plugins/bower_components/bootstrap-datepicker/boostrap-datepicker.pt-BR.js'

		);

		$StyleSheet = array(

			$this->theme_folder.'plugins/bower_components/custom-select/custom-select.css',

			$this->theme_folder.'plugins/bower_components/bootstrap-select/bootstrap-select.min.css',

			$this->theme_folder.'plugins/bower_components/icheck/skins/square/orange.css',

			$this->theme_folder.'plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css'

		);

		$ExternalJS = array(

			$this->theme_folder.'ampleadmin/js/settings.js',

			'https://maps.googleapis.com/maps/api/js?key=AIzaSyAViEK-3M8KaK08X3XNHqv3U_6K3Xm7kLM'

		);



		$data = array(

			'page' => 'ecommerce_settings',

			'root' => $this->theme,

			'JavaScript' => $JavaScript,

			'StyleSheet' => $StyleSheet,

			'ExternalJS' => $ExternalJS

		);


        if($this->session->admin){
            $this->load->view('admin', $data);
        }

	}



	public function ecommerce_reports(){

		$this->sess();
		
		$JavaScript = array(

			$this->theme_folder.'plugins/bower_components/bootstrap-select/bootstrap-select.min.js',

			$this->theme_folder.'plugins/bower_components/icheck/icheck.min.js',

			$this->theme_folder.'plugins/bower_components/raphael/raphael-min.js',

			$this->theme_folder.'plugins/bower_components/morrisjs/morris.js'

		);

		$StyleSheet = array(

			$this->theme_folder.'plugins/bower_components/bootstrap-select/bootstrap-select.min.css',

			$this->theme_folder.'plugins/bower_components/icheck/skins/square/orange.css',

			$this->theme_folder.'plugins/bower_components/morrisjs/morris.css'

		);

		$ExternalJS = array(

			$this->theme_folder.'ampleadmin/js/reports.js'

		);



		$data = array(

			'page' => 'ecommerce_reports',

			'root' => $this->theme,

			'base' => $this->base,

			'JavaScript' => $JavaScript,

			'StyleSheet' => $StyleSheet,

			'ExternalJS' => $ExternalJS

		);



		$this->load->view('admin', $data);

	}





}

