<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Ajax extends CI_Controller {



	var $CI;



	function __construct(){

		parent::__construct();

		$timezone = json_decode(file_get_contents('config/timezone.json'));

		date_default_timezone_set($timezone->timezone);

		$this->load->dbforge();

		$this->CI =& get_instance();

	}



	public function index()

	{

		echo 'Welcome to AjaxController';

	}



	public function installation(){



		$installer = $this->install->queries_execute();

		if($installer){



			$this->db->set('title', $_POST['title']);

			$this->db->set('slogan', $_POST['slogan']);

			$this->db->set('url', $_POST['url']);

			$this->db->set('email', $_POST['email']);

			$this->db->set('phone', $_POST['phone']);

			if(isset($_FILES['thumbnail'])):

				$thumbnail = $_FILES['thumbnail'];

				$tname = $thumbnail['name'];

				$tmp   = $thumbnail['tmp_name'];

				$dir   = 'themes/uploads/logo/'; 



				$ext = strtolower(substr($tname,-4));

				$hashname = sha1(md5(base64_encode(time()))) . $ext; 



				if(move_uploaded_file($tmp, $dir.$hashname)){

					$this->db->set('site_logo', $hashname);

				}

			endif;

			$this->db->insert('config');


			$address = explode(',', $_POST['address']);

			$this->db->set('address', $_POST['address']);

			$this->db->set('street', $address[0]);

			$this->db->set('number', $_POST['number']);

			$this->db->set('locality', $address[1]);

			$this->db->set('city', $address[2]);

			$this->db->set('complement', $_POST['complement']);

			$this->db->set('cep', $_POST['cep']);

			$this->db->insert('config_address');



			file_put_contents('config/INSTALL', 'TRUE');


			$this->db->set('name', $_POST['adminName']);

			$this->db->set('email', $_POST['adminEmail']);

			$this->db->set('password', sha1(md5(trim($_POST['adminPassword']))));

			$this->db->insert('admin');

			$id = $this->db->insert_id();

			if($id > 0){
				$this->session->set_userdata('admin', $id);
				echo json_encode(array('install' => true));
			}else{
				echo json_encode(array('install' => false));
			}

		}

	} 



	public function apis(){

		echo json_encode($this->db->get('oauth')->row());

	}



	public function node(){

		echo json_encode(

			array(

				'ipdomain' => $this->config_website()->ipdomain,

				'port' => $this->config_website()->port

			)

		);

	}



	public function config_website(){

		return $this->db->get('config')->row();

	}



	public function config_emails(){

		return $this->db->get('config_emails')->row();

	}



	public function panel_login_admin(){



		$email = trim($_POST['email']);

		$password = trim($_POST['password']);



		$this->db->where('email', $email);

		$this->db->where('password', sha1(md5($password)));

		$loggin = $this->db->get('admin');

		

		if($loggin->num_rows() > 0){

			$this->session->set_userdata('admin', $loggin->row()->id);

			echo json_encode(array('access' => true, 'uri' => base_url().'control'));

		}else{

			echo json_encode(array('access' => false));

		}



	}



	public function recovery_password_admin(){



		$email = trim($_POST['email']);

		$this->db->where('email', $email);

		$get = $this->db->get('admin');



		if ($get->num_rows() > 0) {

			$row = $get->row();



			$password = base64_encode(time());



			$this->db->where('id', $row->id);

			$this->db->set('password', sha1(md5(trim($password))));

			$this->db->update('admin');

			$recoveryAdmin = $this->emails->recoveryAdmin($row->email, $row->name, $password, base_url());

			echo json_encode(

				array(

					'uid' => $row->id, 

					'name' => $row->name, 

					'email' => $row->email, 

					'thumbnail' => $this->helps->verify_avatar($row->thumbnail, 'admin'),

					'send' => $recoveryAdmin

				)

			);

		}



	}



	public function sales_month(){

		$month = $_POST['month'];



		$this->db->order_by('date', 'ASC');

		$orders = $this->db->get('orders');

		if ($orders->num_rows() > 0) {

			$orders_array = array();

			$orders_total = 0;

			foreach ($orders->result() as $order) {

				$this->db->where("order_id", $order->id);

				if ($this->db->get('orders_details')->num_rows() > 0) {

					$m = date('m', strtotime($order->date));

					if ($m == $month) {

						$this->db->where('id', $order->clients_id); 

						$clients = $this->db->get('clients');

						if ($clients->num_rows() > 0) {

							$client = ($clients->num_rows() > 0) ? $clients->row() : false;

							$orders_total += $this->CI->invoice->invoice_order_total($order->id);

							$orders_array[] = 

							array(

								'client_id' => $client->id,

								'client_name' => $client->name,

								'order_id' => $order->id,

								'order_total' => $this->CI->invoice->invoice_order_total($order->id),

								'order_date' => $order->date,

								'order_status' => $order->status

							);

						}

					}

				}

			}

			$order_array = $this->helps->array_orderby($orders_array, 'order_date', SORT_ASC);

			echo json_encode(array('total_orders' => number_format($orders_total, 2, ",", "."), 'orders' => $order_array));

		}



	}



	public function trashing_invoice(){

		$order_id = $_POST['order'];

		$this->db->where('id', $order_id);

		$this->db->delete('orders');

		$this->db->where('order_id', $order_id);

		$this->db->delete('orders_details');



		$this->db->where('id', $order_id);

		$get = $this->db->get('orders');



		if ($get->num_rows() < 1) {



			$uri = 'control/orders';

			echo json_encode(

				array(

					'error' => false, 

					'uri_redirect' => $uri 

				)

			);



		}else{

			echo json_encode(

				array(

					'error' => true 

				)

			);

		}



	}



	public function trashing_client(){

		$client_id = $_POST['client'];



		$this->db->where('client_id', $client_id);

		$this->db->delete('chatting');



		$this->db->where('id', $client_id);

		$this->db->delete('clients');



		$this->db->where('clients_id', $client_id);

		$this->db->delete('clients_cc');



		$this->db->where('clients_id', $client_id);

		$this->db->delete('inbox');



		$this->db->where('clients_id', $client_id);

		$get = $this->db->get('orders');

		if ($get->num_rows() > 0) {

			foreach ($get->result() as $order) {

				$this->db->where('order_id', $order->id);

				$this->db->delete('orders_details');

				$this->db->where('id', $order->id);

				$this->db->delete('orders');

			}

		}



	}


	public function trashing_deliveryman(){
		$this->db->where('id', $_POST['id']);
		if ($this->db->delete('deliveryman')) {
			echo json_encode(array('error' => false));
		}else{
			echo json_encode(array('error' => true));
		}
	}

	public function change_deliveryman(){
		$id = $_POST['id'];
		$status = $_POST['status'];

		$this->db->where('id', $id);
		$this->db->set('status', $status);
		$this->db->update('deliveryman');
		if ($this->db->affected_rows() >= 0) {
			echo json_encode(array('error' => false, 'id' => $id, 'status' => $status));
		}else{
			echo json_encode(array('error' => true));
		}
	}

	public function create_deliveryman(){

		$name = isset($_POST['name']) ? $_POST['name'] : '';
		$cpf = isset($_POST['cpf']) ? $_POST['cpf'] : '';
		$board = isset($_POST['board']) ? $_POST['board'] : '';

		$this->db->where('cpf', $cpf);
		if ($this->db->get('deliveryman')->num_rows() == 0) {
			$this->db->set('name', $name);
			$this->db->set('cpf', $cpf);
			$this->db->set('board', $board);
			$this->db->insert('deliveryman');

			if ($this->db->insert_id() > 0) {
				echo json_encode(array(
					'id' => $this->db->insert_id(), 
					'name' => $name,
					'cpf' => $this->helps->cpf($cpf),
					'board' => $this->helps->board($board),
					'status' => 1
				));
			}
		}else{
			echo json_encode(array('error' => true));
		}
	}



	public function transaction_creditCard(){

		

		$this->pagseguro->transaction_creditCard();

	}

	



	public function change_minbuy(){

		$table = $_POST['table'];

		$row   = $_POST['row'];

		$value = ($row == 'minbuy') ? str_replace(",", ".", $_POST['value']) : $_POST['value'];



		$this->db->set($row, $value);

		$update = $this->db->update($table);

		if ($this->db->affected_rows() >= 0) {

			echo json_encode(array('error' => false));

		}else{

			echo json_encode(array('error' => true));

		}



	}



	public function change_payments(){



		if (!empty($_POST['payments'])) {

			$this->db->where('payments', strtolower($_POST['payments']));

			$check = $this->db->get('config_payments');



			if (isset($_POST['payments'])) {

				$this->db->set("payments", $_POST['payments']);

			}



			if (isset($_POST['payments_envelopment'])) {

				$this->db->set("payments_envelopment", '1');

			}else{

				$this->db->set("payments_envelopment", '0');

			}



			$this->db->set("payments_email", $_POST['payments_email']);

			$this->db->set("payments_token", $_POST['payments_token']);

			$this->db->set("payments_type", join(",", $_POST['payments_type']));

			$this->db->set("payments_x", $_POST['payments_x']);



			if ($check->num_rows() > 0) {

				$this->db->where('id', $check->row()->id);

				$this->db->update('config_payments');

				echo json_encode(array('error' => false));

			}else{

				$this->db->insert('config_payments');

				if($this->db->insert_id() > 0){

					echo json_encode(array('error' => false));

				}else{

					echo json_encode(array('error' => true));

				}

			}



		}



	}



	public function change_funcionally(){



		$d_array = array (

			1 => "Domingo",

			2 => "Segunda-feira",

			3 => "Terça-feira",

			4 => "Quarta-feira",

			5 => "Quinta-feira",

			6 => "Sexta-feira",

			7 => "Sábado"

		);



		if (isset($_POST['days'])) {

			$result = array();

			foreach ($_POST['days'] as $i => $v) {

				$days_array = array();



				foreach ($_POST['days'][$i] as $d) {

					$days_array[] = $d;

				};



				$days = join(",", $days_array);



				$hour = str_replace(" ", "", $_POST['hour'][$i][0]);

				$hour = trim($hour);

				$id = isset($_POST['id']) ? $_POST['id'][$i][0] : 0;



				$this->db->where('days', $days);

				$this->db->where('hours', $hour);

				$this->db->or_where('id', $id);

				$get = $this->db->get('funcionally');



				$this->db->set('days', $days);

				$this->db->set('hours', $hour);



				if($get->num_rows() < 1){

					

					$this->db->insert('funcionally');

					if ($this->db->insert_id() > 0) {

						$result[] = array('error' => false);

					}



				}else{

					$id = $get->row();

					$this->db->where('id', $id->id);

					$this->db->update('funcionally');

					if ($this->db->affected_rows() >= 0) {

						$result[] = array('error' => false);

					}



				}

			}



			echo json_encode($result);



		}



	}



	public function trash_funcionally(){



		$this->db->where('id', $_POST['id']);

		$this->db->delete('funcionally');



		$count = $this->db->get('funcionally')->num_rows();

		echo json_encode(array('count' => $count));

	}



	public function sales_products(){

		$product_name = $_POST['productname'];

		$this->db->like('name', $product_name);

		$get = $this->db->get('products');

		if($get->num_rows() > 0){



			echo json_encode(array($get->result_array()));

		}else{

			echo json_encode(array('error' => true));

		}

	} 



	public function ecommerce_products(){

		$inputJSON = file_get_contents('php://input');

		$input = json_decode( $inputJSON );



		switch ($input->req) {

			case 'addons':



			$this->db->where('status', '1');

			$get = $this->db->get('addons');



			if($get->num_rows() > 0):

				echo json_encode($get->result_array());

			else:

				echo json_encode($get->result_array());

			endif;



			break;



			case 'ingredients':



			$this->db->where('status', '1');

			$get = $this->db->get('ingredients');



			if($get->num_rows() > 0):

				echo json_encode($get->result_array());

			else:

				echo json_encode($get->result_array());

			endif;



			break;

			

			default:

				// code...

			break;

		}



	}



	public function ecommerce_products_add(){



		$name        = isset($_POST['name']) ? $_POST['name'] : null;

		$status      = isset($_POST['status']) ? $_POST['status'] : null;

		$category    = isset($_POST['category']) ? $_POST['category'] : null;

		$price       = isset($_POST['price']) ? str_replace(",", ".", $_POST['price']) : null;

		$description = isset($_POST['description']) ? $_POST['description'] : null;

		$addons      = isset($_POST['addons']) ? $_POST['addons'] : null;

		$ingredients = isset($_POST['ingredients']) ? $_POST['ingredients'] : null;

		$optionals   = isset($_POST['optionals']) ? $_POST['optionals'] : null;

		$pizza 	 	 = isset($_POST['pizza']) ? $_POST['pizza'] : 0;

		$sale 	 	 = isset($_POST['sale']) ? $_POST['sale'] : NULL;

		$stock 	 	 = isset($_POST['stock']) ? $_POST['stock'] : NULL;

		$flavors 	 = isset($_POST['flavors']) ? $_POST['flavors'] : null;



		$thumbnail   = isset($_FILES['thumbnail']) ? $_FILES['thumbnail'] : null;

		$thumbnails  = isset($_FILES['thumbnails']) ? $_FILES['thumbnails'] : null;



		$this->db->set('name', $name);

		$this->db->set('description', $description);

		$this->db->set('price', $price);

		$this->db->set('pizza', $pizza);

		$this->db->set('flavors', $flavors);

		$this->db->set('status', $status);

		$this->db->set('sale', $sale);

		$this->db->set('stock', $stock);

		$this->db->set('category_id', $category);



		if(!is_null($ingredients)){



			$ingredients_array = array();

			foreach ($ingredients as $value) {

				$ingredients_array[] = $value;

			}

			$this->db->set('ingredients', join("|", $ingredients_array));



		}



		if(!is_null($addons)){



			$addons_array = array();

			foreach ($addons as $value) {

				$addons_array[] = $value;

			}

			$this->db->set('addons', join("|", $addons_array));



		}



		if(!is_null($optionals)){



			$optionals_array = array();

			foreach ($optionals as $value) {

				$optionals_array[] = $value;

			}

			$this->db->set('optionals', join("|", $optionals_array));



		}



		if(!is_null($thumbnail)){



			$tname = $thumbnail['name'];

			$tmp   = $thumbnail['tmp_name'];

			$dir   = 'themes/uploads/thumbnail/'; 



			$ext = strtolower(substr($tname,-4));

			$hashname = sha1(md5(base64_encode(time()))) . $ext; 



			if(move_uploaded_file($tmp, $dir.$hashname)){

				$this->db->set('thumbnail', $hashname);

			}



		}



		$insert = $this->db->insert('products');

		$lastid = $this->db->insert_id();



		if(!is_null($thumbnails)){

			foreach ($_FILES['thumbnails']['name'] as $index => $file) {



				$tmp  = $thumbnails['tmp_name'][$index];

				$dir  = 'themes/uploads/gallery/'; 



				$ext = strtolower(substr($file,-4));

				$hashname = sha1(md5(base64_encode(time()))) . $ext; 

				

				if(move_uploaded_file($tmp, $dir.$hashname)){



					$this->db->set('alt', $file);

					$this->db->set('thumbnail', $hashname);

					$this->db->set('products_id', $lastid);

					$this->db->insert('gallery');



				}



			}

		}



		if($this->db->affected_rows() > 0){

			echo json_encode(array( 'error' => false, 'id' => $lastid, 'name' => $name ));

		}else{

			echo json_encode(array( 'error' => false ));

		}





	}



	public function ecommerce_products_edit(){



		$name        = isset($_POST['name']) ? $_POST['name'] : null;

		$status      = isset($_POST['status']) ? $_POST['status'] : null;

		$category    = isset($_POST['category']) ? $_POST['category'] : null;

		$price       = isset($_POST['price']) ? str_replace(",", ".", $_POST['price']) : null;

		$description = isset($_POST['description']) ? $_POST['description'] : null;

		$addons      = isset($_POST['addons']) ? $_POST['addons'] : null;

		$ingredients = isset($_POST['ingredients']) ? $_POST['ingredients'] : null;

		$optionals = isset($_POST['optionals']) ? $_POST['optionals'] : null;

		$pizza 	 	 = isset($_POST['pizza']) ? $_POST['pizza'] : 0;

		$flavors 	 = isset($_POST['flavors']) ? $_POST['flavors'] : null;

		$sale 	 	 = isset($_POST['sale']) ? $_POST['sale'] : NULL;

		$stock 	 	 = isset($_POST['stock']) ? $_POST['stock'] : NULL;	

		$thumbnail   = isset($_FILES['thumbnail']) ? $_FILES['thumbnail'] : null;

		$thumbnails  = isset($_FILES['thumbnails']) ? $_FILES['thumbnails'] : null;



		$lastid = $_POST['id'];



		$this->db->where('id', $lastid);

		$this->db->set('name', $name);

		$this->db->set('description', $description);

		$this->db->set('price', $price);

		$this->db->set('pizza', $pizza);

		$this->db->set('flavors', $flavors);

		$this->db->set('status', $status);

		$this->db->set('sale', $sale);

		$this->db->set('stock', $stock);

		$this->db->set('category_id', $category);



		if(!is_null($ingredients)){



			$ingredients_array = array();

			foreach ($ingredients as $value) {

				$ingredients_array[] = $value;

			}

			$this->db->set('ingredients', join("|", $ingredients_array));



		}



		if(!is_null($addons)){



			$addons_array = array();

			foreach ($addons as $value) {

				$addons_array[] = $value;

			}

			$this->db->set('addons', join("|", $addons_array));



		}



		if(!is_null($optionals)){



			$optionals_array = array();

			foreach ($optionals as $value) {

				$optionals_array[] = $value;

			}

			$this->db->set('optionals', join("|", $optionals_array));



		}



		if(!is_null($thumbnail)){



			$tname = $thumbnail['name'];

			$tmp   = $thumbnail['tmp_name'];

			$dir   = 'themes/uploads/thumbnail/'; 



			$ext = strtolower(substr($tname,-4));

			$hashname = sha1(md5(base64_encode(time()))) . $ext; 



			if(move_uploaded_file($tmp, $dir.$hashname)){

				$this->db->set('thumbnail', $hashname);

			}



		}



		$insert = $this->db->update('products');



		if(!is_null($thumbnails)){

			foreach ($_FILES['thumbnails']['name'] as $index => $file) {



				$tmp  = $thumbnails['tmp_name'][$index];

				$dir  = 'themes/uploads/gallery/'; 



				$ext = strtolower(substr($file,-4));

				$hashname = sha1(md5(base64_encode(time()))) . $ext; 

				

				if(move_uploaded_file($tmp, $dir.$hashname)){



					$this->db->set('alt', $file);

					$this->db->set('thumbnail', $hashname);

					$this->db->set('products_id', $lastid);

					$this->db->insert('gallery');



				}



			}

		}



		if($this->db->affected_rows() >= 0){

			echo json_encode(array( 'error' => false, 'id' => $lastid, 'name' => $name ));

		}else{

			echo json_encode(array( 'error' => false ));

		}





	}



	public function ecommerce_products_list(){



		if (isset($_GET['id'])):



			$this->db->where('id', $_GET['id']);

			$get_product = $this->db->get('products');

			$row = $get_product->row();



			$addons_array = array();

			$ingredients_array = array();



			$addons = explode("|", $row->addons);

			$ingredients = explode("|", $row->ingredients);



			for ($i = 0; $i < count($addons); $i++) {

				$this->db->where('id', $addons[$i]);

				$get_addons = $this->db->get('addons');



				foreach ($get_addons->result() as $row_addon) {

					$addons_array[$row->id] = $row_addon->name;

				}



			}



			for ($i = 0; $i < count($ingredients); $i++) {

				$this->db->where('id', $ingredients[$i]);

				$get_ingredients = $this->db->get('ingredients');



				foreach ($get_ingredients->result() as $row_ingredient) {

					$ingredients_array[$row->id] = $row_ingredient->name;

				}



			}



			$products_array = array('product' => $get_product->result(), 'addons' => $addons_array, 'ingredients' => $ingredients_array);

			echo json_encode($products_array);



		else:

			$get_products = $this->db->get('products');

			$addons_array = array();

			$ingredients_array = array();



			foreach ($get_products->result() as $row) {



				$addons = explode("|", $row->addons);

				$ingredients = explode("|", $row->ingredients);



				for ($i = 0; $i < count($addons); $i++) {

					$this->db->where('id', $addons[$i]);

					$get_addons = $this->db->get('addons');



					foreach ($get_addons->result() as $row_addon) {

						$addons_array[$row->id] = $row_addon->name;

					}



				}



				for ($i = 0; $i < count($ingredients); $i++) {

					$this->db->where('id', $ingredients[$i]);

					$get_ingredients = $this->db->get('ingredients');



					foreach ($get_ingredients->result() as $row_ingredient) {

						$ingredients_array[$row->id] = $row_ingredient->name;

					}



				}



			}



			$products_array = array('products' => $get_products->result(), 'addons' => $addons_array, 'ingredients' => $ingredients_array);

			echo json_encode($products_array);



		endif;

	}



	public function ecommerce_products_delete(){

		$id = $_POST['id'];

		$this->db->where('id', $id);

		if($this->db->delete('products')){

			echo json_encode(array('error' => false));

		}else{

			echo json_encode(array('error' => true));

		}

	}



	public function ecommerce_category_add(){





		foreach ($_POST as $name => $value) {

			

			$this->db->where('name', $value);

			$verify = $this->db->get('category');



			if($verify->num_rows() < 1){



				$this->db->set('name', $_POST['name']);

				if(isset($_POST['status'])):

					$this->db->set('status', $_POST['status']);

				endif;

				$this->db->insert('category');

				$lastid = $this->db->insert_id();



				if($lastid){

					$this->db->where('id', $lastid);

					echo json_encode($this->db->get('category')->result());

				}



			}else{

				echo json_encode(array('error' => true, 'message' => 'exists'));

			}



			break;

		}



	}



	public function ecommerce_category_list(){



		echo json_encode($this->db->get('category')->result());



	}



	public function ecommerce_category_remove(){



		$this->db->where('id', $_POST['id']);

		$this->db->delete($_POST['type']);



	}



	public function ecommerce_cupons_add(){



		$title  = $_POST['title'];

		$value  = $_POST['value'];

		$expire = $_POST['expire'];



		$this->db->where('title', $title);

		$verify = $this->db->get('cupons');



		if($verify->num_rows() < 1){



			$this->db->set('title', $title);

			$this->db->set('value', $value);

			$this->db->set('expire', $expire);

			$this->db->set('status', '1');

			$this->db->set('genered', substr(base64_encode($title.time()), 0, 6));

			$this->db->insert('cupons');

			$lastid = $this->db->insert_id();



			if($lastid){

				$this->db->where('id', $lastid);

				echo json_encode($this->db->get('cupons')->result());

			}



		}else{

			echo json_encode(array('error' => true, 'message' => 'exists'));

		}



	}



	public function ecommerce_cupons_list(){

		$this->db->order_by("id", "DESC");

		echo json_encode($this->db->get('cupons')->result());

	}



	public function ecommerce_addons_add(){





		foreach ($_POST as $name => $value) {

			

			$this->db->where('name', $value);

			$verify = $this->db->get('addons');



			if($verify->num_rows() < 1){



				$this->db->set('name', $_POST['name']);



				if(isset($_POST['price'])):

					$this->db->set('price', $_POST['price']);

				endif;

				

				if(isset($_POST['status'])):

					$this->db->set('status', $_POST['status']);

				endif;



				$this->db->insert('addons');

				$lastid = $this->db->insert_id();



				if($lastid){

					$this->db->where('id', $lastid);

					echo json_encode($this->db->get('addons')->result());

				}



			}else{

				echo json_encode(array('error' => true, 'message' => 'exists'));

			}



			break;

		}



	}



	public function ecommerce_addons_remove(){



		$this->db->where('id', $_POST['id']);

		$this->db->delete($_POST['type']);



	}



	public function ecommerce_addons_list(){



		echo json_encode($this->db->get('addons')->result());



	}



	public function ecommerce_options_add(){



		$this->db->set('name', $_POST['name']);

		if (!empty($_POST['price'])) {

			$this->db->set('price', number_format($_POST['price'], 2, ".", "."));

		}



		$obrigatory = isset($_POST['obrigatory']) ? $_POST['obrigatory'] : 0;

		$status = isset($_POST['status']) ? $_POST['status'] : 0;



		$this->db->set('obrigatory', $obrigatory);

		$this->db->set('status', $status);

		$this->db->insert('optionals');

		$last_id = $this->db->insert_id();



		if($last_id > 0){

			foreach ($_POST['optionals'] as $option) {



				$this->db->set('optionals_id', $last_id);	

				$this->db->set('name', $option);

				$this->db->insert('optionals_options');	

			}

			$this->db->where('id', $last_id);

			$get = $this->db->get('optionals')->row();

			echo json_encode($get);

		}else{

			echo json_encode(array('error' => true));

		}



	}



	public function ecommerce_options_remove(){



		$id = $_POST['id'];

		$this->db->where('optionals_id', $id);

		$delete_options = $this->db->delete($_POST['type'].'_options');



		if($delete_options){

			$this->db->where('id', $id);

			$this->db->delete($_POST['type']);

		}



	}



	public function ecommerce_options_list(){

		$this->db->order_by('id', 'DESC');

		echo json_encode($this->db->get('optionals')->result());



	}





	public function ecommerce_ingredients_add(){





		foreach ($_POST as $name => $value) {

			

			$this->db->where('name', $value);

			$verify = $this->db->get('ingredients');



			if($verify->num_rows() < 1){



				$this->db->set('name', $_POST['name']);



				if(isset($_POST['price'])):

					$this->db->set('price', $_POST['price']);

				endif;



				if(isset($_POST['about'])):

					$this->db->set('about', $_POST['about']);

				endif;



				if(isset($_POST['status'])):

					$this->db->set('status', $_POST['status']);

				endif;



				$this->db->insert('ingredients');

				$lastid = $this->db->insert_id();



				if($lastid){

					$this->db->where('id', $lastid);

					echo json_encode($this->db->get('ingredients')->result());

				}



			}else{

				echo json_encode(array('error' => true, 'message' => 'exists'));

			}



			break;

		}



	}



	public function ecommerce_ingredients_remove(){



		$this->db->where('id', $_POST['id']);

		$this->db->delete($_POST['type']);



	}



	public function ecommerce_ingredients_list(){



		echo json_encode($this->db->get('ingredients')->result());



	}



	public function ecommerce_ingredients_get(){



		if(!empty($_POST['value'])){

			$ingredients_array = array();

			foreach (explode("|", $_POST['value']) as $id) {

				$this->db->where('id', $id);

				$this->db->where('status', '1');

				$get = $this->db->get('ingredients');

				//$ingredients_array[] = $get->row();

				array_push($ingredients_array, $get->row());

			}

			echo json_encode($ingredients_array, JSON_FORCE_OBJECT);

		}



	}



	public function ecommerce_change_roles(){



		if (isset($_POST['role'])) {

			$id = $_POST['id'];

			$role = $_POST['role'];

			if ($role == 'optionals') {

				$this->db->where('id', $id);

				$optionals = $this->db->get($role);

				if ($optionals->num_rows() > 0) {

					$this->db->where('optionals_id', $id);

					$options = $this->db->get('optionals_options')->result();

					echo json_encode(array('optional' => $optionals->row(), 'options' => $options));

				}

			}else{

				$this->db->where('id', $id);

				$get = $this->db->get($role);

				if ($get->num_rows() > 0) {

					echo json_encode($get->row());

				}

			}

		}else if(isset($_POST['delete'])){

			$this->db->where('id', $_POST['id']);

			$this->db->delete($_POST['delete']);			

		}else{

			list($table, $id) = explode(":", $_GET['table']);

			if ($table == 'optionals') {



				if (isset($_POST['optionals'])) {

					foreach ($_POST['optionals'] as $i => $opt) {

						if (is_array($opt)) {

							foreach ($opt as $i_o => $oo) {

								$this->db->set('name', $oo);

							}

							$this->db->where('id', $i_o);

							$this->db->where('optionals_id', $id);

							$this->db->update('optionals_options');

						}else{

							$this->db->set('optionals_id', $id);	

							$this->db->set('name', $opt);

							$this->db->insert('optionals_options');

						}

					}

				}



				foreach ($_POST as $field => $input) {

					if ($field != 'obrigatory' && $field != 'optionals') {

						$this->db->set($field, $input);

					}

				}



				if (isset($_POST['obrigatory'])) {

					$this->db->set('obrigatory', '1');

				}else{

					$this->db->set('obrigatory', '0');

				}



				$this->db->where('id', $id);

				$this->db->update($table);



				if ($this->db->affected_rows() >= 0) {

					echo json_encode(array('update' => true));

				}else{

					echo json_encode(array('update' => false));

				}

			}else{

				foreach ($_POST as $field => $input) {

					$this->db->set($field, $input);

				}

				

				$this->db->where('id', $id);

				$this->db->update($table);



				if ($this->db->affected_rows() >= 0) {

					echo json_encode(array('update' => true));

				}else{

					echo json_encode(array('update' => false));

				}

			}

		}



	}



	public function ecommerce_change_tables(){



		if($_POST['value'] !== ''):

			$this->db->set($_POST['type'], $_POST['value']);

			$this->db->where('id', $_POST['id']);

			$this->db->update($_POST['table']);

		endif;

	}



	public function ecommerce_change_status(){

		

		if(isset($_POST['id'])){



			$this->db->set('status', $_POST['provider']);

			$this->db->where('id', $_POST['id']);

			$this->db->update($_POST['type']);



		}



	}



	public function ecommerce_login_client(){



		$this->db->where('email', trim($_POST['email']));

		$this->db->where('password', sha1(md5(trim($_POST['password']))));

		$select = $this->db->get('clients');



		if($select->num_rows() > 0):

			$this->session->set_userdata('user', $select->row()->id);

			echo json_encode(array('error' => false, 'message' => 'Account logged!', 'accountID' => $select->row()->id, 'base' => base_url(), 'request' => $_POST['request']));

		else:

			echo json_encode(array('error' => true, 'message' => 'Account invalid!'));

		endif;



	}



	public function ecommerce_login_recovery(){

		$email = trim($_POST['email']);

		$this->db->where('email', $email);

		$get = $this->db->get('clients');



		if ($get->num_rows() > 0) {

			$row = $get->row();



			$password = base64_encode(time());



			$this->db->where('id', $row->id);

			$this->db->set('password', sha1(md5(trim($password))));

			$this->db->update('clients');

			$recoveryPassword = $this->emails->recoveryAdmin($row->email, $row->name, $password, base_url());

			echo json_encode(

				array(

					'uid' => $row->id, 

					'name' => $row->name, 

					'email' => $row->email, 

					'thumbnail' => $this->helps->verify_client_thumbnail($row->thumbnail),

					'error' => false,

					'send' => $recoveryPassword

				)

			);	

		}



	}



	public function ecommerce_register_client(){



		$this->db->where('email', trim($_POST['email']));

		if($this->db->get('clients')->num_rows() < 1){



			$this->db->set('name', $_POST['name']);

			$this->db->set('email', trim($_POST['email']));

			$this->db->set('password', sha1(md5(trim($_POST['password']))));

			$this->db->set('phone', $_POST['phone']);

			$this->db->set('since', date('Y-m-d H:i:s'));



			if (isset($_POST['cep'])) {

				$cep        = $_POST['cep'];

				$address    = $_POST['address'];

				$number     = $_POST['number'];

				$complement = $_POST['complement'];



				$this->db->set('cep', $cep);

				$this->db->set('address', $address."|".$number);

				$this->db->set('refer', $complement);

			}



			$insert = $this->db->insert('clients');



			$last_id = $this->db->insert_id();

			if($last_id > 0){



				$this->db->where('id', $this->db->insert_id());

				$new = $this->db->get('clients')->row();

				$this->emails->welcome($new->name, $new->email);

				$this->session->set_userdata('user', $new->id);



				if (isset($_POST['cep'])) {



					$this->db->where("id", $last_id);

					$client = $this->db->get("clients")->row();

					$addr = ''; 

					if (isset($_POST['address']) && $_POST['address'] != '') {

						list($street, $district, $city) = explode(", ", $_POST['address']);

						$addr = $street.', '.$_POST['number'].', '.$district.', '.$city;

					}



					$thumbnail = $this->helps->verify_client_thumbnail($client->thumbnail);

					$phone 	   = $this->helps->formatPhone($client->phone);



					echo json_encode(

						array(

							'id' => $last_id, 

							'name' => $client->name, 

							'picture' => $thumbnail, 

							'address' => $addr,

							'phone' => $phone, 

							'error' => false,

							'message' => 'Account created!', 

							'base' => base_url(), 

							'request' => $_POST['request']

						)

					);

				}else{

					echo json_encode(

						array(

							'error' => false, 

							'message' => 

							'Account created!', 

							'base' => base_url(), 

							'request' => $_POST['request']

						)

					);

				}



			}



		}else{

			echo json_encode(array('error' => true, 'message' => 'Account already exists!'));

		}



	}



	public function save_ecommerce_settings_address(){



		$cep       = isset($_POST['cep']) ? $_POST['cep'] : null;

		$address   = explode(",", $_POST['address']);

		$number    = isset($_POST['number']) ? $_POST['number'] : null;

		$reff      = isset($_POST['reff']) ? $_POST['reff'] : null;

		$lat       = isset($_POST['lat']) ? $_POST['lat'] : null;

		$lng       = isset($_POST['lng']) ? $_POST['lng'] : null;

		$taxavalue = isset($_POST['taxavalue']) ? $_POST['taxavalue'] : 0;

		$districts = isset($_POST['districts']) ? join("|", $_POST['districts']) : null;

		$km_price  = isset($_POST['km_price']) ? $_POST['km_price'] : null;



		$records = $this->db->get('config_address');



		$this->db->set('address', $_POST['address']);

		$this->db->set('street', $address[0]);

		$this->db->set('number', $number);

		$this->db->set('locality', $address[1]);

		$this->db->set('city', $address[2]);

		$this->db->set('complement', $reff);

		$this->db->set('coord_y', $lat);

		$this->db->set('coord_x', $lng);

		$this->db->set('cep', $cep);

		$this->db->set('taxasvalue', $taxavalue);

		$this->db->set('districts', $districts);

		$this->db->set('delivery_price', $km_price);



		if ($records->num_rows() > 0) {

			$id = $records->row()->id;

			$this->db->where('id', $id);

			$this->db->update('config_address');

			if($this->db->affected_rows() >= 0){

				echo json_encode(array('error' => false));

			}else{

				echo json_encode(array('error' => true));

			}

		}else{

			$this->db->insert('config_address');

			if($this->db->insert_id() > 0){

				echo json_encode(array('error' => false));

			}else{

				echo json_encode(array('error' => true));

			}

		}



	}



	public function save_ecommerce_settings_general(){



		$title  = isset($_POST['title']) ? $_POST['title'] : null;

		$slogan = isset($_POST['slogan']) ? $_POST['slogan'] : null;

		$email  = isset($_POST['email']) ? $_POST['email'] : null;

		$url    = isset($_POST['url']) ? $_POST['url'] : null;

		$dirr   = isset($_POST['dirr']) ? $_POST['dirr'] : null;

		$android   = isset($_POST['android']) ? $_POST['android'] : null;

		$appstore   = isset($_POST['appstore']) ? $_POST['appstore'] : null;

		$windowsphone   = isset($_POST['windowsphone']) ? $_POST['windowsphone'] : null;

		$ipdomain   = isset($_POST['ipdomain']) ? $_POST['ipdomain'] : null;

		$port   = isset($_POST['port']) ? $_POST['port'] : null;


		$verify = $this->db->get('config');



		$this->db->set('title', $title);
		$this->db->set('slogan', $slogan);
		$this->db->set('email', $email);
		$this->db->set('url', $url);
		$this->db->set('dirr', $dirr);
		$this->db->set('android', $android);
		$this->db->set('ios', $appstore);
		$this->db->set('windowsphone', $windowsphone);
		$this->db->set('ipdomain', $ipdomain);
		$this->db->set('port', $port);

		if(isset($_FILES['logo'])):
			$logo = $_FILES['logo'];
			$tname = $logo['name'];
			$tmp   = $logo['tmp_name'];
			$dir   = 'themes/uploads/logo/'; 

			$ext = strtolower(substr($tname,-4));
			$hashname = sha1(md5(base64_encode(time()))) . $ext; 

			if(move_uploaded_file($tmp, $dir.$hashname)){
				$this->db->set('site_logo', $hashname);
			}

		endif;

		if ($verify->num_rows() > 0) {

			$id = $verify->row()->id;

			$this->db->where('id', $id);

			$this->db->update('config');

			if ($this->db->affected_rows() >= 0) {

				echo json_encode(array('error' => false));

			}else{

				echo json_encode(array('error' => true));

			}

		}else{

			$this->db->insert('config');

			if ($this->db->insert_id() > 0) {

				echo json_encode(array('error' => false));

			}else{

				echo json_encode(array('error' => true));

			}

		}



	}



	public function a088972718e5202c662b958ac389d6b8(){



		$url = 'http://falconapi.codefull.com.br/key';

		$fields = array(

			'key' => urlencode($_POST['key'])

		);



		$fields_string = '';



		foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }

		rtrim($fields_string, '&');



		$ch = curl_init();



		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		curl_setopt($ch, CURLOPT_URL, $url);

		curl_setopt($ch, CURLOPT_POST, count($fields));

		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

		curl_setopt($ch, CURLOPT_HEADER, false);



		$result = curl_exec($ch);



		curl_close($ch);

		echo $result;



	}



	public function list_menu(){

		$this->db->where('status', '1');

		$this->db->where('pizza', '1');

		$this->db->order_by('name');

		$get = $this->db->get('products');

		if($get->num_rows() > 0){

			$data = array();

			foreach ($get->result() as $row) {

				$addons = array();

				$ingredients = array();

				$ingredients_id = array();

				$ingredients_titles = array();

				$ingredients_options = array();



				if (isset($row->addons)) {

					$ing = explode("|", $row->addons);

					foreach ($ing as $v) {

						$this->db->where('id', $v);

						$g = $this->db->get('addons');

						if ($g->num_rows() > 0) {

							$addons[] = '<span class="label label-danger">'. $g->row()->name .'</span>';

						}

					}

				}



				if (isset($row->ingredients)) {

					$ing = explode("|", $row->ingredients);

					foreach ($ing as $v) {

						$this->db->where('id', $v);

						$g = $this->db->get('ingredients');

						if ($g->num_rows() > 0) {

							$ingredients[] = '<span class="label label-danger">'. $g->row()->name .'</span>';

							$ingredients_id[] = $v;

							$ingredients_titles[] = $g->row()->name;

							$ingredients_options[] = '<option value="'. $g->row()->id .'">'. $g->row()->name .'</option>';



						}

					}

				}



				$data[] = array(

					'id' => $row->id,

					'name' => ucwords($row->name),

					'thumbnail' => $this->helps->verify_thumbnail($row->thumbnail, 0),

					'price' => number_format($row->price, 2, ",", "."),

					'addons' => join(' ', $addons),

					'ingredients' => join(' ', $ingredients),

					'ingredients_id' => join('|', $ingredients_id),

					'ingredients_titles' => join(',', $ingredients_titles),

					'ingredients_options' => join(' ', $ingredients_options)

				);

			}



			echo json_encode($data);



		}else{

			echo json_encode(array('display' => false));

		}

	}



	public function promotions(){

		

	}



	public function promotions_add(){



		if (isset($_POST['active']) AND $_POST['active'] == 1) {

			$promotions = $this->db->get('promotions');

			if ($promotions->num_rows() > 0) {

				foreach ($promotions->result() as $row) {

					$this->db->where('id', $row->id);

					$this->db->set('active', 0);

					$this->db->update('promotions');

				}

			}

		}



		$price = str_replace(".", ",", $_POST['price']);



		$hour = isset($_POST['hour']) ? $_POST['hour'] : '';

		$date = isset($_POST['date']) ? $_POST['date'] : '';



		$expire = $date.' '.$hour;



		$active = ($_POST['active'] == 1) ? 1 : 0;



		if (isset($_POST['products'])) {

			$products = array();

			foreach ($_POST['products'] as $product) {

				$products[] = $product;

			}

			$this->db->set('products_id', join("|", $products));

		}

		$this->db->set('title', $_POST['title']);

		$this->db->set('promotional_price', $price);

		$this->db->set('date', date('Y-m-d H:i:s'));

		$this->db->set('expire', $expire);

		$this->db->set('active', $active);



		$insert = $this->db->insert('promotions');



		if ($this->db->insert_id()) {

			echo json_encode(array('error' => false));

		}else{

			echo json_encode(array('error' => true));

		}



	}



	public function promotions_choose(){

		$id = $_POST['id'];



		$get = $this->db->get('promotions');



		if ($_POST['type'] == 'unupdate') {

			foreach ($get->result() as $row) {

				$this->db->where('id', $row->id);

				$this->db->set('active', '0');

				$this->db->update('promotions');

			}

		}else{

			foreach ($get->result() as $row) {

				$this->db->where('id', $row->id);

				if ($row->id == $id) {

					$this->db->set('active', '1');

				}else{

					$this->db->set('active', '0');

				}

				$this->db->update('promotions');

			}

		}



		if($this->db->affected_rows() > 0){

			echo json_encode(array('error' => false));

		}



	}



	public function news_orders(){

		$id = $_POST['id'];



		$return = array();



		$this->db->where('id', $id);

		$get_order = $this->db->get('orders');



		if ($get_order->num_rows() > 0) {

			$row = $get_order->row();

			$return[]['order'] = $get_order->row();



			$this->db->where('id', $row->clients_id);

			$client = $this->db->get('clients');



			if ($client->num_rows() > 0) {

				$return[]['client'] = $client->row();

			}



		}



		echo json_encode($return);



	}



	public function oauth_facebook(){

		$id   	 = $_POST['id'];

		$email   = $_POST['email'];

		$picture = $_POST['picture'];

		$name    = $_POST['name'];



		$new_pic = sha1(md5($picture)).'.jpg';

		$get     = file_get_contents($picture);

		$drop    = file_put_contents('assets/clients/'.$new_pic, $get);



		$this->db->where('email', $email);

		$get = $this->db->get('clients');

		if ($get->num_rows() > 0) {

			$client = $get->row();

			$this->db->where('id', $client->id);

			$this->db->limit(1);

			$this->db->set('name', $name);

			$this->db->set('thumbnail', $new_pic);

			$this->db->update('clients');

			$this->session->set_userdata('user', $client->id);

			echo json_encode(array('autorize' => true, 'uid' => $client->id, 'uri' => base_url().'account'));

		}else{

			$this->db->set('name', $name);

			$this->db->set('email', $email);

			$this->db->set('thumbnail', $new_pic);

			$this->db->set('password', sha1(md5(trim($id))));

			$this->db->set('since', date('Y-m-d H:i:s'));

			$this->db->insert('clients');

			$insert_id = $this->db->insert_id();

			$this->session->set_userdata('user', $insert_id);

			echo json_encode(array('autorize' => true, 'uid' => $insert_id, 'uri' => base_url().'account'));

		}

	}



	public function oauth_google(){

		$id   	 = $_POST['id'];

		$email   = $_POST['email'];

		$picture = $_POST['picture'];

		$name    = $_POST['name'];



		$new_pic = sha1(md5($picture)).'.jpg';

		$get     = file_get_contents($picture);

		$drop    = file_put_contents('assets/clients/'.$new_pic, $get);



		$this->db->where('email', $email);

		$get = $this->db->get('clients');

		if ($get->num_rows() > 0) {

			$client = $get->row();

			$this->db->where('id', $client->id);

			$this->db->limit(1);

			$this->db->set('name', $name);

			$this->db->set('thumbnail', $new_pic);

			$this->db->update('clients');

			$this->session->set_userdata('user', $client->id);

			echo json_encode(array('autorize' => true, 'uid' => $client->id, 'uri' => base_url().'account'));

		}else{

			$this->db->set('name', $name);

			$this->db->set('email', $email);

			$this->db->set('thumbnail', $new_pic);

			$this->db->set('password', sha1(md5(trim($id))));

			$this->db->set('since', date('Y-m-d H:i:s'));

			$this->db->insert('clients');

			$insert_id = $this->db->insert_id();

			$this->session->set_userdata('user', $insert_id);

			echo json_encode(array('autorize' => true, 'uid' => $insert_id, 'uri' => base_url().'account'));

		}



	}	



	public function chatting_client__list(){



		$array = array();

		foreach ($_POST['connections'] as $index => $id) {

			$this->db->where('id', $id);

			$client = $this->db->get('clients');

			if ($client->num_rows() > 0) {

				$row = $client->row();

				$array[] =

				array(

					'id' => $row->id,

					'name' => $row->name,

					'thumbnail' => $this->helps->verify_client_thumbnail($row->thumbnail),

					'url' => 'control/chat/'.$row->id,

					'status' => true

				);

			}

		}



		echo json_encode($array);



	}



	public function chatting_client__notify(){

		$uid = $_POST['client'];

		$this->db->where('id', $uid);

		$get_client = $this->db->get('clients');



		if ($get_client->num_rows() > 0) {

			$client = $get_client->row();



			$this->db->where('client_id', $client->id);

			$this->db->limit(1);

			$this->db->order_by('id', 'DESC');

			$message = $this->db->get('chatting')->row();



			$this->db->where('id', $message->func_id);

			$admin = $this->db->get('admin')->row();



			echo json_encode(

				array(

					'id' => $client->id,

					'name' => $client->name,

					'thumbnail' => $this->helps->verify_client_thumbnail($client->thumbnail),

					'message_id' => $message->id,

					'message' => $message->message,

					'date' => $message->date,

					'send' => $message->send,

					'admin_id' => $message->func_id,

					'admin_name' => $admin->name,

					'admin_thumbnail' => $this->helps->verify_client_thumbnail($admin->thumbnail)

				)

			);

		}else{

			echo json_encode(array('error' => true));

		}



	}



	public function chatting_client__send(){

		$input = $_POST['input'];

		$client = $this->session->user;



		$this->db->set('client_id', $client);

		$this->db->set('message', htmlentities( $input ));

		$this->db->set('date', date('Y-m-d H:i:s'));

		$this->db->set('send', '1');

		$this->db->set('answer', '1');

		$this->db->insert('chatting');

		$last_id = $this->db->insert_id();



		if ($last_id > 0) {

			$this->db->where('id', $client);

			$get = $this->db->get('clients');

			if ($get->num_rows() > 0) {

				$row = $get->row();

				$thumbnail = $this->helps->verify_client_thumbnail($row->thumbnail);

				echo json_encode(

					array(

						'uid' => $row->id,

						'name' => $row->name,

						'thumbnail' => $thumbnail,

						'date' => date('Y-m-d H:i:s'),

						'input' => htmlentities( $input ),

						'message_id' => $last_id

					)

				);

			}else{

				echo json_encode(

					array(

						'error' => true

					)

				);

			}

		}



	}	



	public function chatting_client__details(){

		$id = $_POST['id'];

		$this->db->where('id', $id);

		$client = $this->db->get('clients');



		if ($client->num_rows() > 0) {

			echo json_encode(

				array(

					'id' => $client->row()->id,

					'name' => $client->row()->name,

					'thumbnail' => $this->helps->verify_client_thumbnail($client->row()->thumbnail)

				)

			);	

		}



	}



	public function chatting_client_message(){

		$id = $_POST['id'];



		$this->db->where('client_id', $id);

		$this->db->order_by('id', 'DESC');

		$this->db->limit(1);

		$messages = $this->db->get('chatting');



		if ($messages->num_rows() > 0) {

			$row = $messages->row();



			$this->db->where('id', $row->client_id);

			if ($this->db->get('clients')->num_rows() > 0) {



				$client = $this->db->get('clients')->row();

				$array = array(

					'message_id' => $row->id,

					'name' => $client->name,

					'thumbnail' => $this->helps->verify_client_thumbnail($client->thumbnail),

					'date' => $row->date,

					'uid' => $client->id,

					'message' => $row->message

				);

				echo json_encode($array);



			}



		}else{

			echo json_encode(

				array(

					'error' => true

				)

			);

		}



	}



	public function chatting_admin__send(){

		$input  = $_POST['input'];

		$client = $_POST['client'];

		$admin = $this->session->admin;



		$this->db->set('client_id', $client);

		$this->db->set('func_id', $admin);

		$this->db->set('message', htmlentities( $input ));

		$this->db->set('date', date('Y-m-d H:i:s'));

		$this->db->set('send', '2');

		$this->db->set('answer', '1');

		$this->db->insert('chatting');

		$last_id = $this->db->insert_id();



		if ($last_id > 0) {

			$this->db->where('id', $admin);

			$get = $this->db->get('admin');

			if ($get->num_rows() > 0) {

				$row = $get->row();

				$thumbnail = $this->helps->verify_client_thumbnail($row->thumbnail);

				echo json_encode(

					array(

						'uid' => $row->id,

						'name' => $row->name,

						'thumbnail' => $thumbnail,

						'date' => date('Y-m-d H:i:s'),

						'input' => htmlentities( $input ),

						'message_id' => $last_id

					)

				);

			}else{

				echo json_encode(

					array(

						'error' => true

					)

				);

			}

		}



	}



	public function chatting_admin__details(){

		$id = $_POST['id'];

		$this->db->where('id', $id);

		$client = $this->db->get('admin');



		if ($client->num_rows() > 0) {

			echo json_encode(

				array(

					'id' => $client->row()->id,

					'name' => $client->row()->name,

					'thumbnail' => $this->helps->verify_client_thumbnail($client->row()->thumbnail)

				)

			);	

		}



	}



	public function cupom_mailform(){

		$email = $_POST['email'];

		echo $email;

	}



	public function save_profile_admin_picture(){



		if(isset($_FILES['AdminThumbnail'])):

			$thumbnail = $_FILES['AdminThumbnail'];

			$tname = $thumbnail['name'];

			$tmp   = $thumbnail['tmp_name'];

			$dir   = 'assets/admin/img/avatar/'; 



			$ext = strtolower(substr($tname,-4));

			$hashname = sha1(md5(base64_encode($_POST['AdminEmail']))) . $ext; 



			if (file_exists($dir.$hashname)) {

				unlink($dir.$hashname);

			}



			if(move_uploaded_file($tmp, $dir.$hashname)){

				$this->db->where('email', $_POST['AdminEmail']);

				$this->db->set('thumbnail', $hashname);

				$this->db->update('admin');

				echo json_encode(array('error' => false));

			}else{

				echo json_encode(array('error' => true));

			}



		endif;

	}



	public function save_profile_clients_picture(){



		if(isset($_FILES['clientsThumbnail'])):

			$thumbnail = $_FILES['clientsThumbnail'];

			$tname = $thumbnail['name'];

			$tmp   = $thumbnail['tmp_name'];

			$dir   = 'assets/clients/'; 



			$ext = strtolower(substr($tname,-4));

			$hashname = sha1(md5(base64_encode($_POST['clientsEmail']))) . $ext; 



			if (file_exists($dir.$hashname)) {

				unlink($dir.$hashname);

			}



			if(move_uploaded_file($tmp, $dir.$hashname)){

				$this->db->where('email', $_POST['clientsEmail']);

				$this->db->set('thumbnail', $hashname);

				$this->db->update('clients');

				echo json_encode(array('error' => false));

			}else{

				echo json_encode(array('error' => true));

			}



		endif;

	}


    public function save_clients_edit(){

        if(isset($_POST['id'])){
            $id         = $_POST['id'];
            $cep        = $_POST['cep'];
            $name       = $_POST['name'];
            $address    = $_POST['address'];
            $number     = $_POST['number'];
            $complement = $_POST['complement'];

            $this->db->where('id', $id);
            $this->db->set('name', $name);
            $this->db->set('cep', $cep);
            $this->db->set('address', $address . '|' . $number . '|' . $complement);
            $this->db->update('clients_address');

            if ($this->db->affected_rows() > 0) {
                list($street, $district, $city) = explode(", ", $address);

                echo json_encode(
                    array(
                        'error' => false, 
                        'data' => array(
                            'id' => $id,
                            'cep' => $cep,
                            'name' => $name,
                            'address' => $address,
                            'street' => $street,
                            'district' => $district,
                            'city' => $city,
                            'number' => $number,
                            'complement' => $complement
                        )
                    )
                );
            } else {
                echo json_encode(array('error' => true));
            }

        }else{
            echo json_encode(array('error' => true));
        }

    }


    public function delete_clients_edit(){
        if(isset($_POST['id'])){
            $id = $_POST['id'];
            $this->db->where('id', $id);
            if($this->db->delete('clients_address')){
                echo json_encode(array('error' => false));
            }else{
                echo json_encode(array('error' => true));
            }
        }
    }


	public function admin_account()
    {

        $name = $_POST['name'];

        $email = trim($_POST['email']);

        $password = $_POST['password'];



        $this->db->set('name', $name);

        $this->db->set('email', $email);



        if (!empty($password)) {

            $this->db->set('password', sha1(md5(trim($_POST['password']))));

        }

        $this->db->where('id', $this->session->admin);

        $this->db->update('admin');



        if ($this->db->affected_rows() >= 0) {

            echo json_encode(array('email' => $email, 'name' => $name, 'error' => false));

        } else {

            echo json_encode(array('error' => true));

        }



    }



    public function client_account()
    {

        $id = $_POST['id'];
        $name = $_POST['name'];
        $phone = $_POST['phone'];
        $email = trim($_POST['email']);
        $password = $_POST['password'];



        $this->db->set('name', $name);
        $this->db->set('phone', $phone);
        $this->db->set('email', $email);



        if (!empty($password)) {

            $this->db->set('password', sha1(md5(trim($_POST['password']))));

        }

        $this->db->where('id', $id);

        $this->db->update('clients');



        if ($this->db->affected_rows() >= 0) {

            echo json_encode(array('email' => $email, 'name' => $name, 'error' => false));

        } else {

            echo json_encode(array('error' => true));

        }



    }



    public function save_email_logo()
    {



        if (isset($_FILES['EmailsThumbnail'])) :

            $thumbnail = $_FILES['EmailsThumbnail'];

        $tname = $thumbnail['name'];

        $tmp = $thumbnail['tmp_name'];

        $dir = 'themes/uploads/logo/emails/';



        $ext = strtolower(substr($tname, -4));

        $hashname = sha1(md5(base64_encode('EmailsThumbnail'))) . $ext;



        array_map('unlink', glob($dir . "*"));



        if (move_uploaded_file($tmp, $dir . $hashname)) {

            echo json_encode(array('error' => false));

        } else {

            echo json_encode(array('error' => true));

        }

        endif;

    }



    public function save_email_body()
    {



        $id = $_POST['id'];

        $body = $_POST['body'];

        $subject = $_POST['subject'];

        $save = file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/application/views/emails/' . $id . '.html', $body);

        if ($save) {

            $this->db->where('define', $id);

            if ($this->db->get('config_emails')->num_rows() > 0) {

                $this->db->set('subject', $subject);

                $this->db->set('body', $body);

                $this->db->where('define', $id);

                $this->db->update('config_emails');

            } else {

                $this->db->set('subject', $subject);

                $this->db->set('body', $body);

                $this->db->set('define', $id);

                $this->db->insert('config_emails');

            }

            echo json_encode(array('save' => true));

        } else {

            echo json_encode(array('save' => false));

        }

    }



    public function save_maintenance()
    {

        $active = isset($_POST['maintenance']) ? $_POST['maintenance'] : 0;

        $title = $_POST['title'];

        $subtitle = $_POST['subtitle'];

        $period = $_POST['period'];



        if (empty($_POST['maintenance'])) {

            $deletes = $this->db->get('maintenance')->result();

            foreach ($deletes as $rows) {

                $this->db->where('id', $rows->id);

                $this->db->delete('maintenance');

                echo json_encode(array('save' => true, 'message' => 'O sistema está funcionando normalmente!'));

            }

        } else {



            if ($this->db->get('maintenance')->num_rows() > 0) {

                $deletes = $this->db->get('maintenance')->result();

                foreach ($deletes as $rows) {

                    $this->db->where('id', $rows->id);

                    $this->db->delete('maintenance');

                }

            }



            if ($this->db->get('comingsoon')->num_rows() > 0) {

                $deletes = $this->db->get('comingsoon')->result();

                foreach ($deletes as $rows) {

                    $this->db->where('id', $rows->id);

                    $this->db->delete('comingsoon');

                }

            }



            $this->db->set('active', $active);

            $this->db->set('title', $title);

            $this->db->set('subtitle', $subtitle);

            $this->db->set('period', $period);

            $this->db->insert('maintenance');

            if ($this->db->insert_id() > 0) {

                echo json_encode(array('save' => true, 'message' => 'A loja está em manutenção!'));

            } else {

                echo json_encode(array('save' => false));

            }

        }



    }



    public function save_comingsoon()
    {

        $title = $_POST['title'];

        $period = $_POST['period'];



        if (empty($_POST['comingsoon'])) {

            $deletes = $this->db->get('comingsoon')->result();

            foreach ($deletes as $rows) {

                $this->db->where('id', $rows->id);

                $this->db->delete('comingsoon');

                echo json_encode(array('save' => true, 'message' => 'O sistema está funcionando normalmente!'));

            }

        } else {



            if ($this->db->get('comingsoon')->num_rows() > 0) {

                $deletes = $this->db->get('comingsoon')->result();

                foreach ($deletes as $rows) {

                    $this->db->where('id', $rows->id);

                    $this->db->delete('comingsoon');

                }

            }



            if ($this->db->get('maintenance')->num_rows() > 0) {

                $deletes = $this->db->get('maintenance')->result();

                foreach ($deletes as $rows) {

                    $this->db->where('id', $rows->id);

                    $this->db->delete('maintenance');

                }

            }



            $this->db->set('title', $title);

            $this->db->set('period', $period);

            $this->db->insert('comingsoon');

            if ($this->db->insert_id() > 0) {

                echo json_encode(array('save' => true, 'message' => 'A loja está em manutenção!'));

            } else {

                echo json_encode(array('save' => false));

            }

        }



    }



    public function oauthconfig()
    {



        foreach ($_POST as $input => $value) {

            $this->db->set($input, $value);

        }



        if ($this->db->get('oauth')->num_rows() > 0) {

            $this->db->update('oauth');

            if ($this->db->affected_rows() >= 0) {

                echo json_encode(array('save' => true, 'message' => 'As configurações de autenticação foram salvas.'));

            } else {

                echo json_encode(array('save' => false));

            }

        } else {

            $this->db->insert('oauth');

            if ($this->db->insert_id() > 0) {

                echo json_encode(array('save' => true, 'message' => 'As configurações de autenticação foram salvas.'));

            } else {

                echo json_encode(array('save' => false));

            }

        }

    }



    public function socialconfig()
    {



        foreach ($_POST as $input => $value) {

            $this->db->set($input, $value);

        }



        if ($this->db->get('networks')->num_rows() > 0) {

            $this->db->update('networks');

            if ($this->db->affected_rows() >= 0) {

                echo json_encode(array('save' => true, 'message' => 'As configurações das redes sociais foram salvas.'));

            } else {

                echo json_encode(array('save' => false));

            }

        } else {

            $this->db->insert('networks');

            if ($this->db->insert_id() > 0) {

                echo json_encode(array('save' => true, 'message' => 'As configurações das redes sociais foram salvas.'));

            } else {

                echo json_encode(array('save' => false));

            }

        }



    }

    public function get_districts()
    {
        $districts = $_POST['districts'];

        $dist = array();
        foreach ($districts as $id) {
            if (!is_null($id)) {
                $this->db->where('district_id', $id);
                $get = $this->db->get('districts');
                if ($get->num_rows() < 1) {
                    $this->db->where("id_bairro", $id);
                    $district = $this->db->get("cepbr_bairro");
                    if ($district->num_rows() > 0) {
                        $dist[] = array(
                            'id' => $id,
                            'name' => $district->row()->bairro
                        );
                    }
                }
            }
        }
        echo json_encode($dist);

    }

    public function add_districts()
    {
        $id = $_POST['id'];
        $price = str_replace(",", ".", $_POST['price']);

        $this->db->where('district_id', $id);
        if ($this->db->get('districts')->num_rows() < 1) {
            $this->db->set('district_id', $id);
            $this->db->set('price', $price);
            $this->db->insert('districts');
            $iid = $this->db->insert_id();
            if ($iid > 0) {
                $this->db->where('id', $iid);
                $get = $this->db->get('districts')->row();
                echo json_encode(array('id' => $iid, 'price' => $price));
            }
        } else {
            echo json_encode(array('error' => true, 'code' => 403));
        }
    }

    public function list_districts()
    {
        $get = $this->db->get('districts');
        if ($get->num_rows() > 0) {
            $results = $get->result();
            $dist = array();
            foreach ($results as $result) {
                $this->db->where("id_bairro", $result->district_id);
                $district = $this->db->get("cepbr_bairro");
                if ($district->num_rows() > 0) {
                    $dist[] = array(
                        'id' => $result->id,
                        'did' => $result->district_id,
                        'name' => $district->row()->bairro,
                        'price' => number_format($result->price, 2, ".", ".")
                    );
                }
            }
            echo json_encode($dist);
        } else {
            echo json_encode(array('error' => true, 'code' => 400));
        }
    }

    public function delete_districts()
    {
        $id = $_POST['id'];
        $this->db->where('id', $id);
        if ($this->db->delete('districts')) {
            echo json_encode(array('error' => false));
        } else {
            echo json_encode(array('error' => true));

        }
    }

    public function deliverymen()
    {
        $id = $_POST['id'];
        $order_id = $_POST['order'];

        $this->db->where('id', $order_id);
        $this->db->set('deliveryman_id', $id);
        $this->db->update('orders');
        if ($this->db->affected_rows() >= 0) {
            echo json_encode(array('error' => false));
        } else {
            echo json_encode(array('error' => true));
        }
    }

    public function cupomPrinter()
    {



        $id = isset($_POST['invoice']) ? $_POST['invoice'] : 0;

        $via = isset($_POST['via']) ? $_POST['via'] : 0;

        $where = isset($_POST['where']) ? $_POST['where'] : 0;

        $amount = isset($_POST['amount']) ? $_POST['amount'] : 0;



        $row = '<div class="drashed"></div>';

        $spaces = '&nbsp;&nbsp;';

        $total = 0;

        $torder = 0;



        $this->db->where('id', $id);

        $orders = $this->db->get('orders');

        if ($orders->num_rows() > 0) {

            $order = $orders->row();



            $this->db->where('order_id', $id);

            $orders_details = $this->db->get('orders_details');



            $config = $this->db->get('config')->row();

            $address = $this->db->get('config_address')->row();



            $this->db->where('id', $order->clients_id);

            $client = $this->db->get('clients')->row();



            $body['address'] = 'EMPRESA ' . $spaces . ': ' . $config->title . "\n\r";

            $body['address'] .= 'END. ' . $spaces . ': ' . $address->street . ', ' . $address->number . "\n\r";

            $body['address'] .= 'END. ' . $spaces . ': ' . $address->locality . ', ' . $address->city . "\n\r";

            $body['address'] .= 'TEL. ' . $spaces . ': ' . $this->helps->formatPhone($config->phone) . "\n\r";

            $shippingPrice = 0;

            if ($via == 'client') {

                if ($order->clients_address_id > 0) {

                    $this->db->where("id", $order->clients_address_id);

                    $address_client = $this->db->get('clients_address')->row();



                    $addr = explode("|", $address_client->address);

                    list($client_street, $client_district, $client_city) = explode(", ", $addr[0]);


                    $this->db->where('bairro', $client_district);
                    $cepbr_bairro = $this->db->get('cepbr_bairro');
                    if ($cepbr_bairro->num_rows() > 0) {
                        $rs = $cepbr_bairro->row();
                        $this->db->where('district_id', $rs->id_bairro);
                        $rso = $this->db->get('districts');
                        if ($rso->num_rows() > 0) {
                            $shippingPrice = $rso->row()->price;
                        } else {
                            $shippingPrice = 0;
                        }
                    }

                    $fulladdress = "$client_street, $addr[1], $client_district, $client_city";

                    $body['client'] = "<strong>E N T R E G A</strong> \n\r";

                    $body['client'] .= 'CLIENTE ' . $spaces . ': ' . $client->name . "\n\r";

                    $body['client'] .= 'END. ' . $spaces . ': ' . $fulladdress . "\n\r";
                    $body['client'] .= 'COMPLE. ' . $spaces . ': ' . $addr[2] . "\n\r";

                    $body['client'] .= 'CEP. ' . $spaces . ': ' . $address_client->cep . "\n\r";

                    $body['client'] .= 'TEL. ' . $spaces . ': ' . $this->helps->formatPhone($client->phone) . "\n\r";

                }

            } else {

                $body['client'] = '';

            }



            $body['product'] = '';

            foreach ($orders_details->result() as $detail) {

                $total += $detail->subtotal;

                $this->db->where('id', $detail->product_id);

                $product = $this->db->get('products')->row();



                $flavors = array();

                if ($detail->flavors != '') {

                    $explore_falvors = explode("|", $detail->flavors);

                    $price_flavor = array();

                    foreach ($explore_falvors as $findex => $flv) {

                        $this->db->where('id', $flv);

                        $get_flavor = $this->db->get('products');

                        if ($get_flavor->num_rows() > 0) {

                            $flavor = $get_flavor->row();

                            $price_flavor[] = $flavor->price;

                            if ($flavor->id !== $detail->product_id) {

                                $flavors_index = $findex + 1;

                                $flavors[$flavor->id][] = $flavor->name;

                            }

                        }

                    }



                }



                $addons = array();

                $addons_amount = array();

                if ($detail->addons != '') {

                    foreach (explode('|', $detail->addons) as $id) {

                        $this->db->where('id', $id);

                        $addon = $this->db->get('addons');

                        if ($addon->num_rows() > 0) {

                            $cost = ($addon->row()->price > 0) ? 'R$' . $this->helps->format_money($addon->row()->price) : '';

                            $addons[$id][] = ucfirst($addon->row()->name);

                            if ($addon->row()->price > 0) {

                                $addons_amount[$id][] = $addon->row()->price * $detail->quantity;

                            }

                        }

                    }

                }



                $optionals = array();

                $optionals_amount = array();

                if ($detail->optionals != '') :

                    $opts = explode("|", $detail->optionals);

                foreach ($opts as $opt) {

                    list($option_id, $odetail_id) = explode(":", $opt);

                    $this->db->where('id', $option_id);

                    $get_optional = $this->db->get('optionals');

                    if ($get_optional->num_rows() > 0) {

                        $optional = $get_optional->row();

                        $this->db->where('id', $odetail_id);

                        $optional_detail = $this->db->get('optionals_options')->row();

                        $cost = ($optional->price > 0) ? 'R$' . $this->helps->format_money($optional->price) : 'Grátis';

                        $optionals[$option_id][] = $optional->name . ': &nbsp;&nbsp;&nbsp;' . $optional_detail->name;

                        if ($optional->price > 0) {

                            $optionals_amount[$option_id][] = $optional->price * $detail->quantity;

                        }

                    }

                }

                endif;



                $takes = array();

                if ($detail->takes != '') {

                    foreach (explode(",", $detail->takes) as $ing) {

                        list($prod_ID, $ingredients_join) = explode("-", $ing);

                        $this->db->where_in("id", explode("|", $ingredients_join));

                        $ingredients = $this->db->get('ingredients');

                        if ($ingredients->num_rows() > 0) {

                            foreach ($ingredients->result() as $ingredient) {

                                $takes[$prod_ID][] = ucfirst($ingredient->name);

                            }

                        }

                    }

                }



                $flavors_array = array();

                if (count($flavors) > 0) {

                    foreach ($flavors as $id_f => $flav) {

                        $i = 0;

                        $takes_verify = isset($takes[$id_f]) ? '<br>Tirar: ' . join(", ", $takes[$id_f]) : '';

                        $flavors_array[] = $flav[$i] . $takes_verify;

                        $i++;

                    }

                }



                if (count($flavors) > 0) {

                    $take_group = '';

                    $take_first = (count($takes) > 0) ? "<br>" . 'Tirar: ' . join(", ", $takes[$product->id]) : '';

                    $product_complete = 'Meia: ' . $product->name . $take_first . '<br>Meia: ' . join(" ", $flavors_array);

                } else {

                    $take_group = (count($takes) > 0) ? "<br>" . 'Tirar: ' . join(", ", $takes[$product->id]) : '';

                    $product_complete = $product->name;

                }





                $body['product'] .= '<tr>

				<td width="50%"><strong>' . $product_complete . '</strong> ' . $take_group . '</td>

				<td style="text-align: right" valign="top">' . $detail->quantity . '</td>

				<td style="text-align: right" valign="top">R$' . $this->helps->format_money($product->price * $detail->quantity) . '</td>

				</tr>';



                $torder += $product->price * $detail->quantity;



                if (count($addons) > 0) {

                    $body['product'] .= '<tr>

					<td width="50%">Adicionais:</td>

					<td style="text-align: right" valign="top"></td>

					<td style="text-align: right" valign="top"></td>

					</tr>';

                    foreach ($addons as $addon_id => $addon) {

                        if (isset($addons_amount[$addon_id])) {

                            $addon_price = $this->helps->format_money($addons_amount[$addon_id][0]);

                            $torder += $addon_price;

                            $body['product'] .= '<tr>

							<td width="50%" height="15">&nbsp;&nbsp;&nbsp;' . $addons[$addon_id][0] . '</td>

							<td height="15" style="text-align: right" valign="top">1</td>

							<td height="15" style="text-align: right" valign="top">R$' . $this->helps->format_money($addon_price) . '</td>

							</tr>';

                        }

                    }

                }



                if (count($optionals) > 0) {

                    $body['product'] .= '<tr>

					<td width="50%">Opcionais:</td>

					<td style="text-align: right" valign="top"></td>

					<td style="text-align: right" valign="top"></td>

					</tr>';

                    foreach ($optionals as $optional_id => $optional) {

                        $torder += $optionals_amount[$optional_id][0];

                        $body['product'] .= '<tr>

						<td width="50%" height="15">' . $optionals[$optional_id][0] . '</td>

						<td height="15" style="text-align: right" valign="top">1</td>

						<td height="15" style="text-align: right" valign="top">R$' . $this->helps->format_money($optionals_amount[$optional_id][0]) . '</td>

						</tr>';

                    }



                }



            }



            if ($via == 'client') {



                $cupom = 0;



                if ($order->cupom != '' && $order->cupom != null) {

                    $this->db->where('genered', $order->cupom);

                    $cupons = $this->db->get('cupons');

                    if ($cupons->num_rows() > 0) {

                        $cupom = $cupons->row();

                        $descount = $cupom->value;

                        $cupom = $descount . '%';



                    }

                }



                $body['total'] = "<strong>T O T A L</strong> \n\r";

                $body['total'] .= 'VALOR DO PEDIDO ' . $spaces . ': R$' . $this->helps->format_money($torder) . "\n\r";



                if ($cupom > 0) {

                    $body['total'] .= 'DESCONTOS ' . $spaces . ': R$' . $this->helps->format_money($order->descount) . ' - ' . $cupom . "\n\r";

                    $body['total'] .= 'VALOR COM DESCONTO ' . $spaces . ': R$' . $this->helps->format_money(floatval($torder) - floatval($order->descount)) . "\n\r";

                }



                if ($order->shippingAddress == 1) {

                    if ($order->shippingData != null) {

                        list($distance, $duration) = explode("|", $order->shippingData); 

						//$total += $this->helps->shippingPrice($distance, $address->delivery_price);

                        $shipPrice = $this->helps->shippingPrice($distance, $address->delivery_price);

                    } else {
                        $shipPrice = $this->helps->format_money($shippingPrice);
                    }

                    $body['total'] .= 'TAXA DE ENTREGA ' . $spaces . ': R$' . $shipPrice . "\n\r";

                }

                $body['total'] .= 'TOTAL A PAGAR ' . $spaces . ': R$' . $this->helps->format_money($total) . "\n\r";



                $payment_type = ($order->payment == 'money') ? 'Dinheiro - R$' . $this->helps->format_money($amount) : 'Cartão de crédito';

                if ($where == 'sales') {

                    $payment_method = '';

                } else {

                    $payment_method = ($order->payment_shippingBrand == '') ? ' via PagSeguro Online' : ' via Maquineta';

                }



                $body['payment'] = "<strong>F O R M A S &nbsp; D E &nbsp; P A G A M E N T O</strong> \n\r";

                $body['payment'] .= 'PAGAMENTO ' . $spaces . ': ' . $payment_type . $payment_method . "\n\r";

                if ($order->payment == 'money') {

                    $back = $amount - $total;

                    $body['payment'] .= 'TROCO ' . $spaces . ': R$' . $this->helps->format_money(str_replace("-", "", $order->send)) . "\n\r";

                }

                if ($order->payment != 'money') {

                    $body['payment'] .= 'BANDEIRA. ' . $spaces . ': ' . $order->payment_shippingBrand . "\n\r";

                    $body['payment'] .= '<center>*** LEVAR MAQUINA PARA CARTAO ***</center>' . "\n\r";

                }

            } else {

                $body['total'] = '';

                $body['payment'] = '';

            }



            $html = '';

            $html .= '<style>

			body {

				width: 300px;

				font-size:12px;

				font-family: Verdana;

				margin: 0;

				padding: 0;

			}

			.drashed {

				width: 100%;

				border-bottom: 1.5px dashed #000;

			}

			.table {

				width: 100%;

				font-size: 12px;

				border-collapse: separate;

				border-spacing: 0 8px;

			}

			.table th {

				text-align: left;

				font-size: 11px;

			}

			.products td {

				height: 18px;

			}

			strong {

				font-size: 11px;

			}



			</style>';



            $html .= nl2br($body['address']);

            $html .= nl2br("\n");

            $html .= '<table class="table">

			<tr>

			<th width="100">P E D I D O</th>

			<th width="140">D A T A &nbsp; E M I S S Ã O</th>

			</tr>

			<tr>

			<td>';

            for ($i = 0; $i < 10 - strlen($id); $i++) {

                $html .= '0';

            }

            $html .= $id;

            $html .= '</td> 

			<td>' . date('d/m/Y H:i', strtotime($order->date)) . '</td>

			</tr>

			</table>';



            if ($order->shippingAddress == 1) {

                $html .= nl2br("\n");

                $html .= nl2br($body['client']);

            }

            if ($via == 'client') {

                $html .= nl2br("\n");

            }

            $html .= '<table class="table products" cellspacing="0" cellpadding="0">

			<tr>

			<th>P R O D U T O</th>

			<th style="text-align: right">Q T D.</th>

			<th style="text-align: right">P R E Ç O</th>

			</tr>

			<tr>

			<td></td>

			<td></td>

			<td></td>

			</tr>';

            $html .= $body['product'];

            $html .= '</table>';

            if ($via == 'client') {

                $html .= nl2br("\n");

                $html .= nl2br($body['total']);

                $html .= nl2br("\n");

                $html .= nl2br($body['payment']);

            }

            if ($via == 'client') {

                if ($order->shippingAddress == 1) {

                    $html .= nl2br("\n");

                    $html .= nl2br("<center><strong>ENTREGA NO ENDEREÇO</strong></center>\n\r");

                    if ($order->deliveryman_id > 0) {
                        $this->db->where('id', $order->deliveryman_id);
                        $del = $this->db->get('deliveryman');
                        if ($del->num_rows() > 0) {
                            $man = $del->row();
                            $html .= nl2br("<center><strong>ENTREGADOR: " . ucfirst($man->name) . "</strong></center>\n\r");
                        }
                    }


                } else {

                    $html .= nl2br("\n");

                    $html .= nl2br("<center><strong>RETIRADA NO BALCÃO</strong></center>\n\r");

                }

            }

            $html .= nl2br("\n");

            $html .= nl2br("<center><strong>ESTE CUPOM NAO TEM VALIDADE FISCAL</strong></center>\n\r");



            $invoice = file_put_contents("invoice/order_body.html", $html);

            if ($invoice) {



                echo $html;



                $ch = curl_init();

                $timeout = 0;

                curl_setopt($ch, CURLOPT_URL, base_url() . 'invoice/order_body.html');

                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);

                $content = curl_exec($ch);

                curl_close($ch);



				//file_put_contents("invoice/order_client.txt", $content);



            }



        }



    }



}