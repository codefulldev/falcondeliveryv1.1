<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Sales extends CI_Controller {



	public function __construct(){

		parent::__construct();

		$timezone = json_decode(file_get_contents('config/timezone.json'));

		date_default_timezone_set($timezone->timezone);

	}



	public function config_payments(){

		return $this->db->get('config_payments')->row();

	}



	public function config_emails(){

		return $this->db->get('config_emails')->row();

	}



	public function config_website(){

		return $this->db->get('config')->row();

	}



	public function get_product(){

		$id = $_POST['getID'];

		$this->db->where('id', $id);

		$get = $this->db->get('products');

		if($get->num_rows() > 0){

			$product = $get->row();

			$ingredients = array();

			$optionals = array();

			$addons = array();



			if(!empty($product->ingredients) && $product->ingredients != ''){

				foreach (explode("|", $product->ingredients) as $ingredient_id) {

					$this->db->where('id', $ingredient_id);

					$this->db->where('status', '1');

					$ingredient = $this->db->get('ingredients');

					if ($ingredient->num_rows() > 0) {

						$ingredients[] = $ingredient->row();

					}

				}

			}



			if(!empty($product->optionals) && $product->optionals != ''){

				foreach (explode("|", $product->optionals) as $optional_id) {

					$this->db->where('id', $optional_id);

					$this->db->where('status', '1');

					$optional = $this->db->get('optionals');

					if ($optional->num_rows() > 0) {

						$opt = $optional->row();

						$optionals_options = array();



						$this->db->where('optionals_id', $opt->id);

						$options = $this->db->get('optionals_options');



						if ($options->num_rows() > 0) {

							$optionals_options[] = $options->result();

						}



						$optionals[] = array(

							'id' => $opt->id,

							'name' => $opt->name,

							'obrigatory' => $opt->obrigatory,

							'price' => $this->helps->format_money($opt->price),

							'optionals_options' => $optionals_options

						);

					}

				}

			}





			if(!empty($product->addons) && $product->addons != ''){

				foreach (explode("|", $product->addons) as $addon_id) {

					$this->db->where('id', $addon_id);

					$this->db->where('status', '1');

					$addon = $this->db->get('addons');

					if ($addon->num_rows() > 0) {

						$addons[] = $addon->row();

					}

				}

			}



			$array = array(

				'id'          => $product->id,

				'name'        => $product->name,

				'price'       => $this->helps->format_money($product->price),

				'thumbnail'   => $this->helps->verify_thumbnail($product->thumbnail, 0),

				'ingredients' => $ingredients,

				'optionals'   => $optionals,

				'addons'      => $addons,

				'stock'       => $product->stock,

				'pizza'       => $product->pizza,

				'flavors'     => $product->flavors



			);



			echo json_encode($array);



		}else{

			echo json_encode(array('error' => true, 'message' => 'Não encontrado!'));

		}

	}



	public function cancel(){

		$this->session->unset_userdata('sales');

	}



	public function checkout_cupom_verify(){

		$uid = isset($_POST['uid']) ? $_POST['uid'] : 0;

		$cupom = isset($_POST['cupom']) ? trim($_POST['cupom']) : '';



		$this->db->where('genered', $cupom);

		$get = $this->db->get('cupons');

		if ($get->num_rows() > 0) {

			$cupom = $get->row();

			$date = date('Y-m-d', strtotime($cupom->expire));

			$now = date('Y-m-d');

			list($year,$month,$day) = explode("-", $date);

			list($nyear,$nmonth,$nday) = explode("-", $now);



			if ($nmonth <= $month && $nday <= $day) {

				$this->db->where("clients_id", $uid);

				$try = $this->db->get('orders');

				$var = '';

				$descount = $cupom->value;



				foreach ($try->result() as $row) {

					if ($row->cupom == $cupom) {

						$var = true;

					}

				}

				if ($var == '') {

					if ($descount > 0 && $descount < 10) {

						$porcent = "0.0".$descount;

					}else{

						$porcent = "0.".$descount;

					}

					echo json_encode(

						array(

							'status' => 'free',

							'expire' => date('d/m/Y', strtotime($cupom->expire)),

							'descount' => $cupom->value,

							'value' => $porcent

						)

					);

				}else{

					echo json_encode(

						array(

							'status' => 'used'

						)

					);

				}



			}else{

				echo json_encode(

					array(

						'status' => 'expired'

					)

				);

			}

		}else{

			echo json_encode(

				array(

					'status' => 'invalid'

				)

			);

		}

	}



	public function descount_cupom($descount){

		$porcent = 0;



		if ($descount > 0 && $descount < 10) {

			$porcent = "0.0.".$descount;

		}else{

			$porcent = "0.".$descount;

		}

		return intval($porcent);



	}



	public function add_to_cart(){



		$id = $_POST['productID'];

		if (is_numeric($id)) {

			$this->db->where('id', $id);

			$get = $this->db->get('products');

			if ($get->num_rows() > 0) {

				$product = $get->row();

				$params = array();



				if (!empty($_POST['ingredients']['pizza'])) {

					$ings_arr = array();

					foreach ($_POST['ingredients']['pizza'] as $pid => $ings) {

						$this->db->where('id', $pid);

						$product_ings = $this->db->get('products');

						if ($product_ings->num_rows() > 0) {

							$product_ing = $product_ings->row();



							$ingredients = explode('|', $product_ing->ingredients);

							$diff = $ings;

							$remove = array_diff($ingredients, $diff);



							if(count($remove) > 0){

								$ings_arr[$pid][] = $remove;

							}

						}

					}

					$params[] = array('ingredients' => $ings_arr);

				}





				if (!empty($_POST['ingredients']['default'])) {

					$ings_arr = array();



					$ingredients = explode('|', $product->ingredients);

					$diff = $_POST['ingredients']['default'];

					$remove = array_diff($ingredients, $diff);



					if(count($remove) > 0){

						$ings_arr[$id][] = $remove;

					}



					$params[] = array('ingredients' => $ings_arr);

				}



				if(isset($_POST['addons'])){

					$params[] = array('addons' => $_POST['addons']);

				}



				if(isset($_POST['optionals'])){

					$optionals_array = array();

					foreach ($_POST['optionals'] as $idoo => $options) {

						$options_array = array();

						foreach ($options as $opt) {

							$options_array[] = $opt;

						}

						$optionals_array[] = $idoo.':'.join(",", $options_array);

					}

					$params[] = array('optionals' => join("|", $optionals_array));

				}



				if(isset($_POST['flavors'])){

					$flavors_array = array();

					foreach ($_POST['flavors'] as $flavors) {

						$flavors_array[] = $flavors;

					}

					$params[] = array('flavors' => join("|", $flavors_array));

				}



				if(isset($_POST['schendule'])){

					$params[] = array('schendule' => $_POST['schendule']);

				}



				$this->add($product->id, (int)$_POST['quantity'], $params, base_url().'control/sales');



			}

		}

	}



	public function create_client(){



		$name        = $_POST['name'];

		$phone       = $_POST['phone'];

		$email       = url_title($name, 'dash', true).'@'.$_SERVER['SERVER_NAME'];

		$password    = str_replace(" ", "", $name).'@';

		$nameAddress = isset($_POST['nameAddress']) ? $_POST['nameAddress'] : null;

		$cep         = isset($_POST['cep']) ? $_POST['cep'] : null;

		$address     = isset($_POST['address']) ? $_POST['address'] : null;

		$number      = isset($_POST['number']) ? $_POST['number'] : null;

		$complement  = isset($_POST['complement']) ? $_POST['complement'] : null;



		$this->db->where('email', $email);

		$get = $this->db->get('clients');

		if ($get->num_rows() > 0) {

			echo json_encode(array('error' => true, 'message' => 'E-mail já cadastrado!'));

		}else{



			$this->db->set('name', $name);

			$this->db->set('email', trim($email));

			$this->db->set('password', sha1(md5(trim($password))));

			$this->db->set('phone', $phone);

			$this->db->set('since', date('Y-m-d H:i:s'));



			$insert = $this->db->insert('clients');



			$last_id = $this->db->insert_id();

			if($last_id > 0 && $nameAddress != null){

				$this->db->set('clients_id', $last_id);

				$this->db->set('name', $nameAddress);

				$this->db->set('cep', $cep);

				$this->db->set('address', $address.'|'.$number.'|'.$complement);

				$this->db->insert('clients_address');

			}



			if ($last_id > 0) {

				echo json_encode(array('error' => false, 'id' => $last_id, 'name' => $name, 'phone' => $phone));

			}else{

				echo json_encode(array('error' => true, 'message' => 'Ocorreu um erro!'));

			}

		}



	}

	public function create_newaddress_client(){



		$id    		 = isset($_POST['clientID']) ? $_POST['clientID'] : 0;

		$nameAddress = isset($_POST['nameAddress']) ? $_POST['nameAddress'] : null;

		$cep         = isset($_POST['cep']) ? $_POST['cep'] : null;

		$address     = isset($_POST['address']) ? $_POST['address'] : null;

		$number      = isset($_POST['number']) ? $_POST['number'] : null;

		$complement  = isset($_POST['complement']) ? $_POST['complement'] : 'S/C';



		if($id > 0){

			$this->db->set('clients_id', $id);

			$this->db->set('name', $nameAddress);

			$this->db->set('cep', $cep);

			$this->db->set('address', $address.'|'.$number.'|'.$complement);

			$this->db->insert('clients_address');



			if ($this->db->insert_id() > 0) {

				echo json_encode(array('error' => false, 'id' => $id));

			}else{

				echo json_encode(array('error' => true, 'message' => 'Ocorreu um erro!'));

			}

		}





	}



	public function add($id, $quantity = 1, $params, $uri){



		$session = count($this->session->sales) + 1;

		$index = sprintf('%s:%s', $id, $session);



		$cart_items = $this->session->userdata('sales');



		$array = array();



		if(count($params) > 0):

			foreach ($params as $param) {



				if(isset($param['addons'])){

					$array['addons'] = $param['addons'];

				}

				if(isset($param['ingredients'])){

					$array['ingredients'] = $param['ingredients'];

				}

				if(isset($param['optionals'])){

					$array['optionals'] = $param['optionals'];

				}

				if(isset($param['flavors'])){

					$array['flavors'] = $param['flavors'];

				}

				if(isset($param['schendule'])){

					$array['schendule'] = $param['schendule'];

				}



			}

		endif;



		$cart_items[$index] = array('quantity' => (int)$quantity, 'params' => $array);



		$storage = $this->session->set_userdata('sales', $cart_items);



		echo json_encode(array('added' => true, 'sales' => $uri.'sales', 'uri' => base_url()));

	}



	public function sales_list(){

		$sales = $this->session->sales;

		$total = 0;



		if (count($sales) > 0) {



			foreach ($sales as $i => $items):

				list($id, $index) = explode(":", $i);



				$this->db->where("id", $id);

				$product = $this->db->get('products')->row();

				$quantity = $items['quantity'];

				$thumbnail = $this->helps->verify_thumbnail($product->thumbnail, 0);



				$flavors = array();

				$flavors_price = array();

				if(isset($items['params']['flavors'])) {

					$explore_flavors = explode("|", $items['params']['flavors']);

					$price_flavor = array();

					$price_flavor[] = $product->price / 2;

					foreach ($explore_flavors as $findex => $flv) {

						$this->db->where('id', $flv);

						$get_flavor = $this->db->get('products');

						if ($get_flavor->num_rows() > 0) {

							$flavor = $get_flavor->row();

							$price_flavor[] = $flavor->price / 2;

							if ($flavor->id !== $id) {

								$flavors_index = $findex + 2;

								$flavors[$flavor->id][] = $flavor->name;

							}

						}

					}

					$flavors_price[$id][] = array_sum($price_flavor);

					$subtotal = array_sum($price_flavor) * $quantity;

				}else{

					$subtotal = $product->price * $quantity;

				}



				$this->db->where('id', $product->category_id);

				$category_get = $this->db->get('category');

				$category = ($category_get->num_rows() > 0) ? $category_get->row()->name : 'Sem categoria';



				$take = array();

				if(isset($items['params']['ingredients'])):



					foreach ($items['params']['ingredients'] as $p_id => $ingredients) {

						$ings_array = array();

						foreach ($ingredients[0] as $ing) {

							$this->db->where('id', $ing);

							$ingredient = $this->db->get('ingredients')->row();

							$take[$p_id][] = '<span class="label label-danger">'.$ingredient->name.'</span>';

						}

					}

				endif;



				$addons = array();

				if(isset($items['params']['addons'])):

					foreach ($items['params']['addons'] as $addon_id) {

						$this->db->where('id', $addon_id);

						$addon = $this->db->get('addons')->row();

						if($addon->price > 0){

							$subtotal += $addon->price * $quantity;

						}

						$cost = ($addon->price > 0) ? 'R$'.$this->helps->format_money($addon->price) : 'Grátis';

						$addons[] = '<span class="label label-info">'.$addon->name.' - '.$cost.'</span>';

					}

				endif;



				$optionals = array();

				if(isset($items['params']['optionals'])):

					$opts = explode("|", $items['params']['optionals']);

					foreach ($opts as $opt) {

						list($option_id, $odetail_id) = explode(":", $opt);

						$this->db->where('id', $option_id);

						$get_optional = $this->db->get('optionals');

						if ($get_optional->num_rows() > 0) {

							$optional = $get_optional->row();

							$this->db->where('id', $odetail_id);

							$optional_detail = $this->db->get('optionals_options')->row();

							if($optional->price > 0){

								$subtotal += $optional->price * $quantity;

							}

							$cost = ($optional->price > 0) ? 'R$'.$this->helps->format_money($optional->price) : 'Grátis';

							$optionals[] = '<span class="label label-info">'.$optional->name.': '.$optional_detail->name.' - '.$cost.'</span>';

						}

					}

				endif;



				$total += $subtotal;

				?>

				<tr id="product-<?= $index ?>" class="appearTr">

					<td width="150"><img src="<?= $thumbnail ?>" alt="<?= $product->name ?>" width="80"></td>

					<td width="550">

						<h5 class="font-500">

							<?php



							if (count($flavors) > 0) {

								echo '<strong>Meia:</strong> '.$product->name;

								if (count($take) > 0) {

									foreach ($take as $tidd => $value) {

										if ($id == $tidd) {

											echo '<br><br>Retirar: '.join(' ', $value);

											echo '<br>';

										}

									}

								}

								foreach ($flavors as $fidd => $value) {

									echo '<br>';

									echo '<strong>Meia:</strong> '.join(' ', $value);

									if (count($take) > 0) {

										foreach ($take as $tidd => $value) {

											if ($fidd == $tidd) {

												echo '<br><br>Retirar: '.join(' ', $value);

												echo '<br>';

											}

										}

									}

								}

							}else{

								echo $product->name;

							}

							?>

							<small>

								<a id="detail<?= $product->id ?>" class="show_details" href="javascript:void(0)"></a>

							</small>

						</h5>

						<p>

							<?php

							if (count($take) > 0 && count($flavors) == 0) {

								foreach ($take as $tidd => $value) {

									echo 'Retirar: '.join(' ', $value);

									echo '<br>';

								}

							}

							echo (isset($addons) && count($addons) > 0) ? 'Adicionais: '.join(' ', $addons).'<br>' : '';

							echo (isset($optionals) && count($optionals) > 0) ? 'Opcionais: '.join(' ', $optionals).'<br>' : '';

							?>

						</p>

					</td>

					<td>R$<?php echo (count($flavors) > 0) ? $this->helps->format_money($flavors_price[$id][0]): $this->helps->format_money($product->price); ?></td>

					<td width="70">

						<input type="number" min="1" class="quantity form-control" data-id="<?= $i ?>" placeholder="1" value="<?= $quantity ?>">

					</td>

					<td width="150" align="center" class="font-500">R$<span class="subtotal_<?= $index ?>"><?= $this->helps->format_money($subtotal); ?></span></td>

					<td align="center">

						<a href="javascript:void(0)" data-id="<?= $i ?>" data-index="<?= $index ?>" class="text-inverse removeSale" title="" data-toggle="tooltip" data-original-title="Remover">

							<i class="ti-trash text-dark"></i>

						</a>

					</td>

				</tr>

				<?php

			endforeach;

		}

	}



	public function sales_total($default = null){

		$sales = $this->session->sales;

		$subtotal = 0;

		$total = 0;



		if (count($sales) > 0) {



			foreach ($sales as $i => $items):

				list($id, $index) = explode(":", $i);



				$this->db->where("id", $id);

				$product = $this->db->get('products')->row();

				$quantity = $items['quantity'];

				$thumbnail = $this->helps->verify_thumbnail($product->thumbnail, 0);



				$flavors = array();

				if(isset($items['params']['flavors'])) {

					$explore_flavors = explode("|", $items['params']['flavors']);

					$price_flavor = array();

					$price_flavor[] = $product->price / 2;

					foreach ($explore_flavors as $findex => $flv) {

						$this->db->where('id', $flv);

						$get_flavor = $this->db->get('products');

						if ($get_flavor->num_rows() > 0) {

							$flavor = $get_flavor->row();

							$price_flavor[] = $flavor->price / 2;

						}

					}

					$subtotal += array_sum($price_flavor) * $quantity;

				}else{

					$subtotal += $product->price * $quantity;

				}



				$this->db->where('id', $product->category_id);

				$category_get = $this->db->get('category');

				$category = ($category_get->num_rows() > 0) ? $category_get->row()->name : 'Sem categoria';



				$take = array();

				if(isset($items['params']['ingredients'])):

					foreach ($items['params']['ingredients'] as $p_id => $ingredients) {

						$ings_array = array();

						foreach ($ingredients[0] as $ing) {

							$this->db->where('id', $ing);

							$ingredient = $this->db->get('ingredients')->row();

							if($ingredient->price > 0){

								//$subtotal += $ingredient->price * $quantity;

							}

						}

					}

				endif;



				$addons = array();

				if(isset($items['params']['addons'])):

					foreach ($items['params']['addons'] as $addon_id) {

						$this->db->where('id', $addon_id);

						$addon = $this->db->get('addons')->row();

						if($addon->price > 0){

							$subtotal += $addon->price * $quantity;

						}

					}

				endif;



				$optionals = array();

				if(isset($items['params']['optionals'])):

					$opts = explode("|", $items['params']['optionals']);

					foreach ($opts as $opt) {

						list($option_id, $odetail_id) = explode(":", $opt);

						$this->db->where('id', $option_id);

						$get_optional = $this->db->get('optionals');

						if ($get_optional->num_rows() > 0) {

							$optional = $get_optional->row();

							$this->db->where('id', $odetail_id);

							$optional_detail = $this->db->get('optionals_options')->row();

							if($optional->price > 0){

								$subtotal += $optional->price * $quantity;

							}

						}

					}

				endif;



			endforeach;



			$total += $subtotal;

			$obbj = json_encode(array('total' => $this->helps->format_money($total), 'subtotal' => $this->helps->format_money($subtotal)));

			if($default == null){

				echo $obbj;

			}else{

				return $obbj;

			}



		}else{

			$obbj = json_encode(array('total' => $this->helps->format_money(00.00), 'subtotal' => $this->helps->format_money(00.00)));

			if($default == null){

				echo $obbj;

			}else{

				return $obbj;

			}

		}

	}



	public function back_money(){



		$amount = $_POST['amount'];

		$shipping = $_POST['shipping'];

		$totalBuy = $_POST['total'];



		$total = 0;

		$total += $totalBuy;



		if($this->helps->format_money($amount) < $this->helps->format_money($total)){

			$valid = false;

		}else{

			$valid = true;

		}



		$back = floatval($total) - floatval($amount);
        $back = str_replace("-", "", $back);
        $back = floatval($back);


		echo json_encode(

			array(

				'total' => $this->helps->format_money($total),

				'back' => $this->helps->format_money($back),

				'shipping' => $this->helps->format_money($shipping),

				'valid' => $valid

			)

		);



	}



	public function remove_item_cart(){

		unset($_SESSION['sales'][$_POST['id']]);

		echo json_encode(array('count' => count($this->session->sales)));

	}



	public function change_quantity_item_cart(){



		if($_POST['quantity'] > 0){

			$indexid = $_POST['index'];

			$quantity = $_POST['quantity'];

			$session = $this->session->userdata('sales');

			$session[$indexid]['quantity'] = $quantity;

			$this->session->set_userdata('sales', $session);



			$total = 0;

			$subtotal = 0;

			$in = 0;



			foreach ($this->session->userdata('sales') as $i => $items):

				list($id, $index) = explode(":", $i);



				$this->db->where("id", $id);

				$product = $this->db->get('products')->row();



				if($i == $indexid):

					$in = $index;



					if(isset($items['params']['flavors'])) {

						$explore_flavors = explode("|", $items['params']['flavors']);

						$price_flavor = array();

						$price_flavor[] = $product->price / 2;

						foreach ($explore_flavors as $flv) {

							$this->db->where('id', $flv);

							$get_flavor = $this->db->get('products');

							if ($get_flavor->num_rows() > 0) {

								$flavor = $get_flavor->row();

								$price_flavor[] = $flavor->price / 2;

							}

						}

						$subtotal += array_sum($price_flavor) * $items['quantity'];

					}else{

						$subtotal = $product->price * $items['quantity'];

					}

				endif;



				$take = array();

				if(isset($items['params']['ingredients'])):

					foreach ($items['params']['ingredients'] as $p_id => $ingredients) {

						$ings_array = array();

						foreach ($ingredients[0] as $ing) {

							$this->db->where('id', $ing);

							$ingredient_q = $this->db->get('ingredients');

							if ($ingredient_q->num_rows() > 0) {

								$ingredient = $ingredient_q->row();

								if($ingredient->price > 0){

									$subtotal += $ingredient->price * $items['quantity'];

								}

							}

						}

					}

				endif;

				$optionals = array();

				if(isset($items['params']['optionals'])):

					$opts = explode("|", $items['params']['optionals']);

					foreach ($opts as $opt) {

						list($option_id, $odetail_id) = explode(":", $opt);

						$this->db->where('id', $option_id);

						$get_optional = $this->db->get('optionals');

						if ($get_optional->num_rows() > 0) {

							$optional = $get_optional->row();

							$this->db->where('id', $odetail_id);

							if($optional->price > 0){

								$subtotal += $optional->price * $items['quantity'];

							}

						}

					}

				endif;



				$addons = array();

				if(isset($items['params']['addons'])):

					foreach ($items['params']['addons'] as $addon_id) {

						$this->db->where('id', $addon_id);

						$addon_q = $this->db->get('addons');

						if ($addon_q->num_rows() > 0) {

							$addon = $addon_q->row();

							if($addon->price > 0){

								if($i == $indexid):

									$subtotal += $addon->price * $items['quantity'];

								endif;

							}

						}

					}

				endif;



				$total += $subtotal;



			endforeach;



			echo json_encode(array('total' => $this->helps->format_money($total), 'subtotal' => $this->helps->format_money($subtotal), 'index' => $in));



		}

	}



	public function address_calculate(){

		$chooseAddress = isset($_POST['id']) ? $_POST['id'] : 0;

		if ($chooseAddress > 0) {

			$address_q = $this->db->get('config_address');

			if ($address_q->num_rows() > 0) {

				$address = $address_q->row();

				$this->db->where("id", $chooseAddress);
				$address_client = $this->db->get('clients_address')->row();

                if ($addres->taxasvalue == 1) {
                    $origin = $address->cep;
                    $destination = $address_client->cep;
                    $distance = json_decode($this->helps->delivery_calculate($origin, $destination), true);

                    if (!empty($distance['duration']) && !empty($distance['distance'])) {

                        $shippingPrice = $this->helps->shippingPrice($distance['distance']['value'], $address->delivery_price);

                        $shippingDuration = $this->helps->shippingDuration($distance['duration']['value']);

                    }else{

                        $distance = null;

                        $shippingPrice = null;

                        $shippingDuration = null;

                    }
                }else{
                    list($add, $district, $cities) = explode(", ", $address_client->address);

                        $this->db->where('bairro', $district);
                        $cepbr_bairro = $this->db->get('cepbr_bairro');
                        if ($cepbr_bairro->num_rows() > 0) {
                            $rs = $cepbr_bairro->row();
                            $this->db->where('district_id', $rs->id_bairro);
                            $rso = $this->db->get('districts');
                            if ($rso->num_rows() > 0) {
                                $shippingPrice = $rso->row()->price;
                            } else {
                                $shippingPrice = null;
                            }
                        } else {
                            $shippingPrice = null;
                        }
                }


				if ($shippingPrice != null) {

					echo json_encode(

						array(

							'delivery' => $distance,

							'shipping' => $shippingPrice,

							'shippingDuration' => $shippingDuration,

							'price' => $address->delivery_price

						)

					);

				}else{

					echo json_encode(array('error' => true));

				}



			}



		}else{

			echo json_encode(array('error' => true));

		}

	}



	public function address_client(){

		$id = $_POST['id'];

		$this->db->where('clients_id', $id);

		$get_addr = $this->db->get('clients_address');



		if ($get_addr->num_rows() > 0):

			echo json_encode($get_addr->result());

		else:

			echo json_encode(array('error' => true, 'message' => 'Sem endereço!'));

		endif;



	}



	public function complete_sale(){



		$cart = $this->session->sales;

		$items_order = array();



		$uid   	     = isset($_POST['client']) ? $_POST['client'] : '';

		$payment     = $this->helps->payments($_POST['payment']);

		$delivery    = $_POST['deliveryaddress'];

		$deliveryman = isset($_POST['deliveryman']) ? $_POST['deliveryman'] : '';

		$amount      = isset($_POST['receiver']) ? $_POST['receiver'] : 0;

		$receiver    = $amount;

		$send        = isset($_POST['balance']) ? $_POST['balance'] : 0;

		$cupom 	     = isset($_POST['cupom']) ? $_POST['cupom'] : null;

		$desc_val    = 0;

		$descounted  = isset($_POST['descounted']) ? $_POST['descounted'] : 0;



		$deliveryaddress = ($_POST['deliveryaddress'] == 'delivery') ? true : false;



		if (count($cart) > 0):


			if (!empty($deliveryaddress) && $uid !== 'default') {

				$chooseAddress = isset($_POST['chooseAddress']) ? $_POST['chooseAddress'] : 0;
				$address_q = $this->db->get('config_address');

				if ($address_q->num_rows() > 0) {

                    $address = $address_q->row();
                    $this->db->where("id", $chooseAddress);
                    $address_client = $this->db->get('clients_address')->row();

                    if ($addres->taxasvalue == 1) {
						
                        $origin = $address->cep;
                        $destination = $address_client->cep;
                        $distance = json_decode($this->helps->delivery_calculate($origin, $destination), true);

                        if (!empty($distance['duration']) && !empty($distance['distance'])) {
                            $shippingPrice = $this->helps->shippingPrice($distance['distance']['value'], $address->delivery_price);
                            $shippingDuration = $this->helps->shippingDuration($distance['duration']['value']);
                            $this->db->set('shippingData', $distance['distance']['value'].'|'.$distance['duration']['value']);
                            $this->db->set('shippingAddress', '1');
                            $this->db->set('orderType', '0');

                        }else{
                            $distance = null;
                            $shippingPrice = null;
                            $shippingDuration = null;
                            $this->db->set('orderType', '0');
                            $this->db->set('shippingAddress', '1');
                        }

                    } else {

                        list($add, $district, $cities) = explode(", ", $address_client->address);

                        $this->db->where('bairro', $district);
                        $cepbr_bairro = $this->db->get('cepbr_bairro');
                        if ($cepbr_bairro->num_rows() > 0) {
                            $rs = $cepbr_bairro->row();
                            $this->db->where('district_id', $rs->id_bairro);
                            $rso = $this->db->get('districts');
                            if ($rso->num_rows() > 0) {
                                $shippingPrice = $rso->row()->price;
                            } else {
                                $shippingPrice = null;
                            }
                        } else {
                            $shippingPrice = null;
                        }

                        $distance = null;
                        $shippingDuration = null;
                        $this->db->set('orderType', '0');
                        $this->db->set('shippingAddress', '1');

                    }

					$this->db->set('clients_address_id', $chooseAddress);
				}

			}else{

				$distance = null;

				$shippingPrice = null;

				$shippingDuration = null;

                $this->db->set('shippingAddress', '0');

                $this->db->set('orderType', '1');

			}

			$this->db->set('clients_id', $uid);

			$this->db->set('payment', $payment);

			$this->db->set('date', date('Y-m-d H:i:s'));

			if ($receiver != 0) {

				$this->db->set('receiver', $this->helps->format_money($receiver));

			}

			$this->db->set('send', $send);

			if (!empty($_POST['deliveryman'])) {

				$this->db->set('deliveryman_id', $_POST['deliveryman']);

			}

			if (!empty($_POST['payment_shippingBrand'])) {

				$this->db->set('payment_shippingBrand', $_POST['payment_shippingBrand']);

			}

			if (isset( $_POST['cupom'] )) {

				$this->db->set('cupom', $_POST['cupom']);

			}

            $this->db->set('status', '1');
			$insert = $this->db->insert('orders');

			$insert_id = $this->db->insert_id();



			if($insert_id > 0):

				if ($deliveryman != '') {
					$this->db->where('id', $deliveryman);
					$this->db->set('status', '0');
					$deliveryman = $this->db->update('deliveryman');
				}

				foreach ($cart as $i => $items):



					list($id, $index) = explode(":", $i);



					$this->db->where("id", $id);

					$product = $this->db->get('products')->row();



					$flavors = array();

					if(isset($items['params']['flavors'])) {

						$explore_flavors = explode("|", $items['params']['flavors']);

						$price_flavor = array();

						$price_flavor[] = $product->price / 2;

						foreach ($explore_flavors as $findex => $flv) {

							$this->db->where('id', $flv);

							$get_flavor = $this->db->get('products');

							if ($get_flavor->num_rows() > 0) {

								$flavor = $get_flavor->row();

								$price_flavor[] = $flavor->price / 2;

								$flavors[$flavor->id][] = $flv;

							}

						}

						$subtotal = array_sum($price_flavor) * $items['quantity'];

						$desc_val = array_sum($price_flavor) * $items['quantity'];

					}else{

						$subtotal = $product->price * $items['quantity'];

						$desc_val = $product->price * $items['quantity'];

					}



					if (!is_null($shippingPrice)) {

						$subtotal += $shippingPrice / count($cart);

					}



					$this->db->set('quantity', $items['quantity']);



					$this->db->where('id', $product->category_id);

					$category_get = $this->db->get('category');

					$category = ($category_get->num_rows() > 0) ? $category_get->row()->name : 'Sem categoria';



					$take = array();

					if(isset($items['params']['ingredients'])):

						foreach ($items['params']['ingredients'] as $p_id => $ingredients) {

							foreach ($ingredients[0] as $ing) {

								$this->db->where('id', $ing);

								$ingredient_q = $this->db->get('ingredients');

								if ($ingredient_q->num_rows() > 0) {

									$ingredient = $ingredient_q->row();

								}

							}

							$take[] = $p_id.'-'.join("|", $ingredients[0]);

						}

					endif;



					$optionals = '';

					if(isset($items['params']['optionals'])):

						$opts = explode("|", $items['params']['optionals']);

						foreach ($opts as $opt) {

							list($option_id, $odetail_id) = explode(":", $opt);

							$this->db->where('id', $option_id);

							$get_optional = $this->db->get('optionals');

							if ($get_optional->num_rows() > 0) {

								$optional = $get_optional->row();

								$this->db->where('id', $odetail_id);

								if($optional->price > 0){

									$subtotal += $optional->price * $items['quantity'];

									$desc_val += $optional->price * $items['quantity'];

								}

								$optionals = $items['params']['optionals'];

							}

						}

					endif;



					$addons = array();

					if(isset($items['params']['addons'])):

						foreach ($items['params']['addons'] as $addon_id) {

							$this->db->where('id', $addon_id);

							$get_addon = $this->db->get('addons');

							if ($get_addon->num_rows() > 0) {

								$addon = $get_addon->row();

								if($addon->price > 0){

									$subtotal += $addon->price * $items['quantity'];

									$desc_val += $addon->price * $items['quantity'];

								}

							}

							$addons[] = $addon_id;

						}

					endif;



					$schendule = '';

					if(isset($items['params']['schendule'])):

						$schendule = $items['params']['schendule'];

					endif;



					if (!is_null($cupom)) {

						$this->db->where('genered', $cupom);

						$cp = $this->db->get('cupons');

						if ($cp->num_rows() > 0) {

							$descount = $cp->row()->value / 100;



							$descount_value = $descounted / count($cart);

							$final_value = $subtotal - $descount_value;

							$subtotal = $final_value;



						}

					}



					$this->db->set('order_id', $insert_id);

					$this->db->set('product_id', $id);

					$this->db->set('addons', join('|', $addons));

					$this->db->set('optionals', $optionals);



					if (count($flavors) > 0) {

						foreach ($flavors as $fidd => $value) {

							$this->db->set('flavors', join('|', $flavors[$fidd]));

						}

					}

					$this->db->set('takes', join(',', $take));

					$this->db->set('schendule', $schendule);

					$this->db->set('subtotal', number_format($subtotal, 2, ".", "."));

					$this->db->insert('orders_details');



					$this->session->unset_userdata('sales');



				endforeach;



				if ($descounted > 0) {

					$this->db->where("id", $insert_id);

					$this->db->set('descount', $this->helps->format_money($descounted));

					$this->db->update('orders');

				}



				echo json_encode(array('uid' => $uid, 'order' => $insert_id, 'checkout' => true, 'type' => $_POST['payment'], 'delivery' => $distance, 'shipping' => $shippingPrice, 'shippingDuration' => $shippingDuration));



				if ($uid !== 'default') {

					$this->db->where('id', $uid);

					$client_emails = $this->db->get('clients')->row();

					$this->emails->receipt($client_emails->name, $client_emails->email, $this->cupomPrinter($insert_id, 'client'));

				}



			endif;



		endif;



	}


	public function cupomPrinter($id, $via){



		$row    = '<div class="drashed"></div>';

		$spaces = '&nbsp;&nbsp;';

		$total  = 0;



		$this->db->where('id', $id);

		$orders = $this->db->get('orders');

		if ($orders->num_rows() > 0) {

			$order 	 = $orders->row();

			$this->db->where('order_id', $id);

			$orders_details = $this->db->get('orders_details');



			$config  = $this->db->get('config')->row();

			$address = $this->db->get('config_address')->row();



			$this->db->where('id', $order->clients_id);

			$client  = $this->db->get('clients')->row();



			$body['address']  = 'EMPRESA '.$spaces.': '.$config->title."\n\r";

			$body['address'] .= 'END. '.$spaces.': '.$address->street.', '.$address->number."\n\r";

			$body['address'] .= 'END. '.$spaces.': '.$address->locality.', '.$address->city."\n\r";

			$body['address'] .= 'TEL. '.$spaces.': '.$this->helps->formatPhone($config->phone)."\n\r";

            $shippingPrice = 0;

			if ($via == 'client') {

				if ($order->clients_address_id > 0) {

					$this->db->where("id", $order->clients_address_id);

					$address_client = $this->db->get('clients_address')->row();


					$addr = explode("|", $address_client->address);

					list($client_street, $client_district, $client_city) = explode(", ", $addr[0]);


					$body['client']   = "<strong>E N T R E G A</strong> \n\r";

					$body['client']  .= 'CLIENTE '.$spaces.': '.$client->name."\n\r";

					$body['client']  .= 'END. '.$spaces.': '.$addr[0]."\n\r";

					$body['client']  .= 'CEP. '.$spaces.': '.$client->cep."\n\r";

					$body['client']  .= 'TEL. '.$spaces.': '.$this->helps->formatPhone($client->phone)."\n\r";

				}

			}else{

				$body['client'] = '';

			}



			$body['product'] = '';

			foreach ($orders_details->result() as $detail){

				$total += $detail->subtotal;

				$this->db->where('id', $detail->product_id);

				$product = $this->db->get('products')->row();



				$flavors = array();

				if($detail->flavors != '') {

					$explore_falvors = explode("|", $detail->flavors);

					$price_flavor = array();

					$price_flavor[] = $product->price / 2;

					foreach ($explore_falvors as $findex => $flv) {

						$this->db->where('id', $flv);

						$get_flavor = $this->db->get('products');

						if ($get_flavor->num_rows() > 0) {

							$flavor = $get_flavor->row();

							$price_flavor[] = $flavor->price;

							if ($flavor->id !== $detail->product_id) {

								$flavors_index = $findex + 1;

								$flavors[$flavor->id][] = $flavor->name;

							}

						}

					}



				}



				$addons = array();

				$addons_amount = array();

				if ($detail->addons != '') {

					foreach (explode('|', $detail->addons) as $id) {

						$this->db->where('id', $id);

						$addon = $this->db->get('addons');

						if ($addon->num_rows() > 0) {

							$cost = ($addon->row()->price > 0) ? 'R$'.$this->helps->format_money($addon->row()->price) : '';

							$addons[$id][] = ucfirst($addon->row()->name);

							if ($addon->row()->price > 0) {

								$addons_amount[$id][] = $addon->row()->price * $detail->quantity;

							}

						}

					}

				}



				$optionals = array();

				$optionals_amount = array();

				if($detail->optionals != ''):

					$opts = explode("|", $detail->optionals);

					foreach ($opts as $opt) {

						list($option_id, $odetail_id) = explode(":", $opt);

						$this->db->where('id', $option_id);

						$get_optional = $this->db->get('optionals');

						if ($get_optional->num_rows() > 0) {

							$optional = $get_optional->row();

							$this->db->where('id', $odetail_id);

							$optional_detail = $this->db->get('optionals_options')->row();

							$cost = ($optional->price > 0) ? 'R$'.$this->helps->format_money($optional->price) : 'Grátis';

							$optionals[$option_id][] = $optional->name.': &nbsp;&nbsp;&nbsp;'.$optional_detail->name;

							if ($optional->price > 0) {

								$optionals_amount[$option_id][] = $optional->price * $detail->quantity;

							}

						}

					}

				endif;



				$takes = array();

				if ($detail->takes != '') {

					foreach (explode(",", $detail->takes) as $ing) {

						list($prod_ID, $ingredients_join) = explode("-", $ing);

						$this->db->where_in("id", explode("|", $ingredients_join));

						$ingredients = $this->db->get('ingredients');

						if ($ingredients->num_rows() > 0) {

							foreach ($ingredients->result() as $ingredient) {

								$takes[$prod_ID][] = ucfirst($ingredient->name);

							}

						}

					}

				}



				$flavors_array = array();

				if (count($flavors) > 0) {

					foreach ($flavors as $id_f => $flav) {

						$i=0;

						$takes_verify = isset($takes[$id_f]) ? '<br>Tirar: '. join(", ", $takes[$id_f]) : '';

						$flavors_array[] = $flav[$i] . $takes_verify;

						$i++;

					}

				}



				if (count($flavors) > 0) {

					$take_group = '';

					$take_first = (count($takes) > 0) ? "<br>".'Tirar: '.join(", ", $takes[$product->id]) : '';

					$product_complete = 'Meia: '.$product->name. $take_first .'<br>Meia: '.join(" ", $flavors_array);

				}else{

					$take_group = (count($takes) > 0) ? "<br>".'Tirar: '.join(", ", $takes[$product->id]) : '';

					$product_complete = $product->name;

				}





				$body['product'] .= '<tr>

				<td width="50%"><strong>'. $product_complete .'</strong> '. $take_group .'</td>

				<td style="text-align: right" valign="top">'. $detail->quantity .'</td>

				<td style="text-align: right" valign="top">R$'. $this->helps->format_money($product->price) .'</td>

				</tr>';



				if (count($addons) > 0) {

					$body['product'] .= '<tr>

					<td width="50%">Adicionais:</td>

					<td style="text-align: right" valign="top"></td>

					<td style="text-align: right" valign="top"></td>

					</tr>';

					foreach ($addons as $addon_id => $addon) {

						$body['product'] .= '<tr>

						<td width="50%" height="15">&nbsp;&nbsp;&nbsp;'. $addons[$addon_id][0] .'</td>

						<td height="15" style="text-align: right" valign="top">1</td>

						<td height="15" style="text-align: right" valign="top">R$'. $this->helps->format_money($addons_amount[$addon_id][0]) .'</td>

						</tr>';

					}



				}



				if (count($optionals) > 0) {

					$body['product'] .= '<tr>

					<td width="50%">Opcionais:</td>

					<td style="text-align: right" valign="top"></td>

					<td style="text-align: right" valign="top"></td>

					</tr>';

					foreach ($optionals as $optional_id => $optional) {

						$body['product'] .= '<tr>

						<td width="50%" height="15">'. $optionals[$optional_id][0] .'</td>

						<td height="15" style="text-align: right" valign="top">1</td>

						<td height="15" style="text-align: right" valign="top">R$'. $this->helps->format_money($optionals_amount[$optional_id][0]) .'</td>

						</tr>';

					}



				}

			}



			if ($via == 'client') {

                $body['total']   = "<strong>T O T A L</strong> \n\r";

				if ($order->shippingAddress == 1) {

                    if(!empty($order->shippingData) && $order->shippingData != ''){
                        list($distance, $duration) = explode("|", $order->shippingData);
                        $shipPrice = $this->helps->shippingPrice($distance, $address->delivery_price);
                        $body['total'] .= 'TAXA DE ENTREGA ' . $spaces . ': R$' . $shipPrice . "\n\r";
                    }
                    
                }
                
				$body['total']  .= 'TOTAL A PAGAR '.$spaces.': R$'.$total."\n\r";

				$payment_type = ($order->payment == 'delivery') ? 'Dinheiro' : 'Cartão de crédito';

				$body['payment']  = "<strong>F O R M A S &nbsp; D E &nbsp; P A G A M E N T O</strong> \n\r";

				$body['payment'] .= 'PAGAMENTO '.$spaces.': '.$payment_type."\n\r";

				if ($order->payment_shippingBrand != '') {

					$body['payment'] .= 'BANDEIRA. '.$spaces.': '.$order->payment_shippingBrand."\n\r";

					$body['payment'] .= '<center>*** LEVAR MAQUINA PARA CARTAO ***</center>'."\n\r";

				}

			}else{

				$body['total'] = '';

				$body['payment'] = '';

			}



			$html = '';

			$html .= '<style>

			body {

				width: 280px;

				font-size:12px;

				font-family: Verdana;

			}

			.drashed {

				width: 100%;

				border-bottom: 1.5px dashed #000;

			}

			.table {

				width: 100%;

				font-size: 12px;

				border-collapse: separate;

				border-spacing: 0 8px;

			}

			.table th {

				text-align: left;

				font-size: 11px;

			}

			strong {

				font-size: 11px;

			}

			</style>';



			$html .= nl2br($body['address']);



			$html .= nl2br("\n".$row."\n");



			$html .= '<table class="table">

			<tr>

			<th width="100">P E D I D O</th>

			<th width="140">D A T A &nbsp; E M I S S Ã O</th>

			</tr>

			<tr>

			<td>';

			for ($i = 0; $i < 10 - strlen($id); $i++) {

				$html .= '0';

			}

			$html .= $id;

			$html .= '</td>

			<td>'. date('d/m/Y H:i', strtotime($order->date)) .'</td>

			</tr>

			</table>';



			if ($order->shippingAddress == 1) {

				$html .= nl2br("\n".$row."\n");

				$html .= nl2br($body['client']);

			}



			$html .= nl2br("\n".$row."\n");

			$html .= '<table class="table">

			<tr>

			<th>P R O D U T O</th>

			<th style="text-align: right">Q T D.</th>

			<th style="text-align: right">P R E Ç O</th>

			</tr>

			<tr>

			<td></td>

			<td></td>

			<td></td>

			</tr>';

			$html .= $body['product'];

			$html .= '</table>';

			$html .= nl2br("\n".$row."\n");

			$html .= nl2br($body['total']);

			$html .= nl2br("\n".$row."\n");

			$html .= nl2br($body['payment']);

			if ($via == 'client') {

				if ($order->shippingAddress == 1) {

					$html .= nl2br("\n".$row."\n");

					$html .= nl2br("<center><strong>ENTREGA NO ENDEREÇO</strong></center>\n\r");

				}else{

					$html .= nl2br("\n".$row."\n");

					$html .= nl2br("<center><strong>RETIRADA NO BALCÃO</strong></center>\n\r");

				}

			}

			$html .= nl2br("\n".$row."\n");

			$html .= nl2br("<center><strong>ESTE CUPOM NAO TEM VALIDADE FISCAL</strong></center>\n\r");



			$invoice = file_put_contents("invoice/order_body.html", $html);

			if ($invoice) {

				return $html;

			}



		}



	}



}