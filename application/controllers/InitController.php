<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class InitController extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$timezone = json_decode(file_get_contents('config/timezone.json'));
		date_default_timezone_set($timezone->timezone);

		if(!file_exists('config/config.json') OR !file_exists('config/INSTALL')){
			redirect(base_url().'config/');
		}
	}

	public function index()
	{

		if ($this->db->get('maintenance')->num_rows() > 0) {
			redirect(base_url().'maintenance');
		}

		if ($this->db->get('comingsoon')->num_rows() > 0) {
			redirect(base_url().'comingsoon');
		}

		$stylesheets = array();
		$javascripts = array(
    		'themes/plugins/bower_components/jquery.countdown-2.2.0/jquery.countdown.min.js'
    	);
		$externalsjs = array(
			'assets/js/functions.js'
		);
		$data = array(
			'page' => 'main',
			'stylesheets' => $stylesheets,
			'javascripts' => $javascripts,
			'externalsjs' => $externalsjs
		);
		$this->load->view('init', $data);

	}

	public function datetimes(){
		echo date("d/m/Y H:i");
	}

	public function maintenance(){

		if ($this->db->get('maintenance')->num_rows() < 1) {
			//redirect('/');
		}

		$stylesheets = array();
		$javascripts = array();
		$externalsjs = array('assets/js/maintenance.js');
		$data = array(
			'page' => 'maintenance',
			'stylesheets' => $stylesheets,
			'javascripts' => $javascripts,
			'externalsjs' => $externalsjs
		);
		$this->load->view('init', $data);
	}

	public function comingsoon(){
		if ($this->db->get('comingsoon')->num_rows() < 1) {
			redirect('/');
		}
		$this->load->view('pages/coming-soon');
	}

	public function PagSeguroMethod(){
		$this->load->library('pagseguro');
		$this->pagseguro->payments_creditcard();
	}

	public function user(){
		$this->db->where('id', $this->session->user);
		return $this->db->get('clients')->row();
	}

	public function config_address(){
		return $this->db->get('config_address')->row();
	}

	public function config_payments(){
		return $this->db->get('config_payments')->row();
	}

	public function config_website(){
		return $this->db->get('config')->row();
	}

	public function logout(){
		$this->session->unset_userdata('shopcart');
		$this->session->unset_userdata('user');
		redirect(base_url(),'refresh');
	}

	public function get_chatting($id){
		
		$this->db->order_by('date', 'ASC');
		$this->db->where('client_id', $id);
		$get = $this->db->get('chatting');

		if ($get->num_rows() > 0) {
			return $get->result();
		}

	}

	public function continue(){
		$this->db->limit(6);
		$this->db->order_by('id', 'RAND');
		$products = $this->db->get('products');
		if ($products->num_rows() > 0) {
			$products_array = array();
			foreach ($products->result() as $row) {
				$products_array[] = array(
					'id' => $row->id,
					'category_id' => $row->category_id,
					'name' => $row->name,
					'description' => $row->description,
					'thumbnail' => $this->helps->verify_thumbnail($row->thumbnail, 0),
					'price' => 'R$'.$this->helps->format_money($row->price),
					'url' => base_url()."product/".$row->id."/"
				);
			}
			echo json_encode($products_array);
		}
	}

	public function device(){
		$device = $_POST['device'];
		foreach ($this->db->get('config')->row() as $index => $value) {
			if ($index == $device && $device != 'desktop') {
				echo json_encode(array('error' => false, 'store_url' => $value));
			}else{
				echo json_encode(array('error' => true));
			}
		}
	}

	public function funcionally(){

		$d_array = array (
			0 => "Domingo",
			1 => "Segunda-feira",
			2 => "Terça-feira",
			3 => "Quarta-feira",
			4 => "Quinta-feira",
			5 => "Sexta-feira",
			6 => "Sábado"
		);

		$funcionally = $this->db->get('funcionally');
		if ($funcionally->num_rows() > 0) {

			$today = (date('N') == 7) ? 0 : date('N');
			$array_days = array();

			foreach ($funcionally->result() as $row) {
				foreach (explode(",", $row->days) as $func_day) {
					if ($func_day == $today) {
						if(!in_array($func_day, $array_days)){
							array_push($array_days, array('day' => $func_day, 'hour' => $row->hours));
						}
					}
				}
			}

			if (count($array_days) > 0 && $array_days[0]['day'] == $today) {

				$day = $d_array[$today];
				$now = date('H:i:s');

				list($start, $end) = explode(",", $array_days[0]['hour']);
				$start = $start.":00";
				$end   = $end.":00";

				$ininio = new DateTime($start);
				$final  = new DateTime($end);
				$busca  = new DateTime($now);

				if ($final <= $ininio) {
					$final->add(new DateInterval("P1D"));
				}

				if ($busca <= $ininio) {
					$busca->add(new DateInterval("P1D"));
				}

				if ($busca >= $ininio && $busca <= $final) {
					if (isset($_POST['ajax'])) {
						echo json_encode(array('open' => true, 'today' => $day));
					}else{
						return json_encode(array('open' => true, 'today' => $day));
					}
				}else{
					if (isset($_POST['ajax'])) {
						echo json_encode(array('open' => false));
					}else{
						return json_encode(array('open' => false));
					}
				}

			}

		}
	
	}

	public function product(){

		$stylesheets = array(
			'themes/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
			'themes/plugins/bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css',
			'themes/plugins/bower_components/switchery/dist/switchery.min.css',
			'themes/plugins/bower_components/icheck/skins/square/orange.css',
			'https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css',
			'assets/css/custom.css',
			'assets/css/selectric.css',
			'assets/css/pizza/style.css'
		);
		$javascripts = array(
			'themes/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js',
			'themes/plugins/bower_components/moment/moment.js',
			'themes/plugins/bower_components/moment/pt-br.js',
			'themes/plugins/bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js',
			'themes/plugins/bower_components/switchery/dist/switchery.min.js',
			'themes/plugins/bower_components/styleswitcher/jQuery.style.switcher.js',
			'themes/plugins/bower_components/icheck/icheck.min.js',
			'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js',
			'https://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js',
			'assets/js/jquery.selectric.js'
		);
		$externalsjs = array(
			'assets/js/product.js',
			'assets/js/pizza.js',
			'assets/js/mount.js'
		);

		$uri = explode("/", $_SERVER['REQUEST_URI']);

		if (is_numeric(end($uri))) {
			$id = end($uri);
		}else{
			$id = $uri[ count($uri) - 2  ];
		}

		if(!is_null($id)):
			$this->db->where('id', $id);
			$product = $this->db->get('products')->row();
			$data = array(
				'page' => 'product',
				'product' => $product,
				'stylesheets' => $stylesheets,
				'javascripts' => $javascripts,
				'externalsjs' => $externalsjs
			);
			$this->load->view('init', $data);
		endif;
	}


	public function promotions(){

		$stylesheets = array();
		$javascripts = array();
		$externalsjs = array(
			'assets/js/promotions.js'
		);

		$uri = explode("/", $_SERVER['REQUEST_URI']);
		
		if (is_numeric(end($uri))) {
			$id = end($uri);
		}else{
			$id = $uri[ count($uri) - 2  ];
		}

		if(!is_null($id)):
			$this->db->where('id', $id);
			$promotion = $this->db->get('promotions')->row();
			$data = array(
				'page' => 'promotion',
				'promotion' => $promotion,
				'stylesheets' => $stylesheets,
				'javascripts' => $javascripts,
				'externalsjs' => $externalsjs
			);
			$this->load->view('init', $data);
		endif;
	}

	public function menu(){

		$stylesheets = array();
		$javascripts = array();
		$externalsjs = array();

		$this->db->where('status', '1');
		$products = $this->db->get('products')->result();
		$this->db->where('status', '1');
		$category = $this->db->get('category')->result();
		$data = array(
			'page' => 'menu',
			'products' => $products,
			'category' => $category,
			'stylesheets' => $stylesheets,
			'javascripts' => $javascripts,
			'externalsjs' => $externalsjs
		);
		$this->load->view('init', $data);

	}

	public function cart(){

		$stylesheets = array(
			'themes/plugins/bower_components/toast-master/css/jquery.toast.css'
		);
		$javascripts = array(
			'themes/plugins/bower_components/toast-master/js/jquery.toast.js'
		);
		$externalsjs = array(
			'assets/js/cart.js'
		);
		$data = array(
			'page' => 'cart',
			'cart' => $this->session->shopcart,
			'stylesheets' => $stylesheets,
			'javascripts' => $javascripts,
			'externalsjs' => $externalsjs
		);
		$this->load->view('init', $data);

	}

	public function total_verify(){

		$cart = $this->session->shopcart;
		$items_order = array();
		$uid = $this->session->user;
		$verify_total = 0;

		foreach ($cart as $i => $items):

			list($id, $index) = explode(":", $i);

			if($items['promotion'] == false){
				$this->db->where("id", $id);
				$product = $this->db->get('products')->row();

				if(isset($items['params']['flavors'])){
					$explore_falvors = explode("|", $items['params']['flavors']);
					$price_flavor = array();
					foreach ($explore_falvors as $fid) {
						$this->db->where('id', $fid);
						$get_flavor = $this->db->get('products');
						if ($get_flavor->num_rows() > 0) {
							$flavor = $get_flavor->row();
							$price_flavor[] = $flavor->price / 2;
						}
					}
					$subtotal = array_sum($price_flavor) * $items['quantity'];
				}else{
					$subtotal = $product->price * $items['quantity'];
				}

				if(isset($items['params']['ingredients'])):
					foreach ($items['params']['ingredients'] as $p_id => $ingredients) {
						foreach ($ingredients[0] as $ing) {
							$this->db->where('id', $ing);
							$ingredient = $this->db->get('ingredients')->row();
						}
					}
				endif;

				if(isset($items['params']['optionals'])):
					$opts = explode("|", $items['params']['optionals']);
					foreach ($opts as $opt) {
						list($option_id, $odetail_id) = explode(":", $opt);
						$this->db->where('id', $option_id);
						$get_optional = $this->db->get('optionals');
						if ($get_optional->num_rows() > 0) {
							$optional = $get_optional->row();
							if($optional->price > 0){
								$subtotal += $optional->price * $items['quantity'];
							}
						}
					}
				endif;

				if(isset($items['params']['addons'])):
					foreach ($items['params']['addons'] as $addon_id) {
						$this->db->where('id', $addon_id);
						$get_addon = $this->db->get('addons');
						if($get_addon->row()->price > 0 && $get_addon->num_rows() > 0){
							$addon = $get_addon->row();
							$subtotal += $addon->price * $items['quantity'];
						}
					}
				endif;
			}else{
				$this->db->where('id', $id);
				$promotions = $this->db->get('promotions');
				if ($promotions->num_rows() > 0) {
					$promotion = $promotions->row();
					$subtotal = $promotion->promotional_price * $items['quantity'];
				}
				
			}
			$verify_total += $subtotal;
		endforeach;

		return $this->helps->format_money($verify_total);
	}

	public function checkout(){

		$stylesheets = array(
			'themes/plugins/bower_components/toast-master/css/jquery.toast.css',
			'themes/plugins/bower_components/icheck/skins/square/orange.css',
			'themes/plugins/bower_components/card-plugin/dist/card.css',
			'themes/plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css',
			'assets/css/maps.css',
		);
		$javascripts = array(
			'themes/plugins/bower_components/toast-master/js/jquery.toast.js',
			'themes/plugins/bower_components/icheck/icheck.min.js',
			'themes/plugins/bower_components/card-plugin/dist/card.js',
			'assets/js/jquery.mask.min.js',
			'assets/js/masks.js',
			'assets/js/jquery-ui.custom.min.js',
    		'themes/plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js',
    		'themes/plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js',
    		'https://cdn.rawgit.com/kimmobrunfeldt/progressbar.js/0.5.6/dist/progressbar.js'
		);
		$externalsjs = array(
			'assets/js/oauth__socials.js',
			'assets/js/account.js',
			'https://maps.googleapis.com/maps/api/js?key=AIzaSyAilTnL72c0Kxg37mhVL_rfO_NTKS8i4zU',
			'assets/js/checkout.js'
		);

		$payments = $this->config_payments();

		if (!empty($this->session->user)) {
			if ($payments) {

				if ($payments->payments == 'pagseguro') {

					$url = ($payments->payments_envelopment == 1) ? 
					'https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js' : 
					'https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js';

					$paymentesMethods = array(
						'assets/js/payments_pagseguro_transparente.js',
						$url
					);

				}
			}else{
				$paymentesMethods = array();
			}
		}else{
			$paymentesMethods = array();
		}
		$data = array(
			'page'             => 'checkout',
			'cart'             => $this->session->shopcart,
			'stylesheets'      => $stylesheets,
			'javascripts'      => $javascripts,
			'externalsjs'      => $externalsjs,
			'paymentesMethods' => $paymentesMethods
		);

		$this->load->view('init', $data);

	}

	public function client_account(){

		$stylesheets = array(
			'themes/plugins/bower_components/toast-master/css/jquery.toast.css',
			'themes/plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css',
		);
		$javascripts = array(
			'themes/plugins/bower_components/toast-master/js/jquery.toast.js',
			'themes/plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js',
    		'themes/plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js'
		);
		$externalsjs = array(
			'assets/js/oauth__socials.js',
			'assets/js/account.js'
		);
		$data = array(
			'page'        => 'account',
			'tpl'         => 'account.tpl',
			'stylesheets' => $stylesheets,
			'javascripts' => $javascripts,
			'externalsjs' => $externalsjs
		);
		$this->load->view('init', $data);

	}

	public function client_address(){

		if(isset($_POST['cep'])){
			if (!empty($_POST['activerecord'])) {
				$name 	 = $_POST['name'];
				$cep     = $_POST['cep'];
				$address = $_POST['address'].'|'.$_POST['address_number'];
				$reff    = $_POST['reff'];
				$insert  = $this->insert_address($name, $cep, $address, $reff);
				if ($insert == true) {
					redirect(base_url().'address');
				}
			}else{
				$id 	 = $_POST['id'];
				$name 	 = $_POST['name'];
				$cep     = $_POST['cep'];
				$address = $_POST['address'].'|'.$_POST['address_number'];
				$reff    = $_POST['reff'];
				$update  = $this->save_address($id, $name, $cep, $address, $reff);
				if ($update == true) {
					redirect(base_url().'address');
				}
			}
		}


		$stylesheets = array(
			'themes/plugins/bower_components/toast-master/css/jquery.toast.css'
		);
		$javascripts = array(
			'themes/plugins/bower_components/toast-master/js/jquery.toast.js'
		);
		$externalsjs = array(
			'assets/js/oauth__socials.js',
			'assets/js/account.js'
		);
		$data = array(
			'page'        => 'account',
			'tpl'         => 'address.tpl',
			'stylesheets' => $stylesheets,
			'javascripts' => $javascripts,
			'externalsjs' => $externalsjs
		);
		$this->load->view('init', $data);

	}

	public function client_address_remove(){
		$id = $_POST['id'];
		if (!empty($id)) {
			$this->db->where('id', $id);
			$this->db->delete('clients_address');
			echo json_encode(array('url' => base_url().'address'));
		}
	}

	function insert_address($name, $cep, $address, $reff){

		$this->db->set('clients_id', $this->session->user);
		$this->db->set('name', $name);
		$this->db->set('cep', $cep);
		$this->db->set('address', $address.'|'.$reff);
		$this->db->insert('clients_address');

		if ($this->db->insert_id() > 0) {
			return true;
		}else{
			return false;
		}
	}

	function save_address($id, $name, $cep, $address, $reff){

		$this->db->where('id', $id);
		$this->db->set('name', $name);
		$this->db->set('cep', $cep);
		$this->db->set('address', $address.'|'.$reff);
		$this->db->update('clients_address');

		if ($this->db->affected_rows() > 0) {
			return true;
		}else{
			return false;
		}

	}

	public function client_settings(){

		$uri_get = parse_url($_SERVER['REQUEST_URI']);
		$delete = explode("/", $uri_get['path']);

		if (end($delete) == 'delete') {
			$uid = $this->session->user;

			$this->db->where('clients_id', $uid);
			$orders = $this->db->get('orders');
			if ($orders->num_rows() > 0) {
			  foreach ($orders->result() as $row) {
			  	$this->db->where('order_id', $row->id);
			  	$this->db->delete('orders_details');
			  	$this->db->where('id', $row->id);
			  	$this->db->delete('orders');
			  }
			}
			$this->db->where('client_id', $uid);
			$this->db->delete('chatting');

			$this->db->where('clients_id', $uid);
			$this->db->delete('inbox');

			$this->db->where("id", $uid);
			$this->db->delete("clients");

			$this->session->unset_userdata('user');
			redirect('account');
			
		}

		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$name = $_POST['name'];
			$email = $_POST['email'];
			$password = empty($_POST['password']) ? null : $_POST['password'];
			$phone = $_POST['phone'];
			$save = $this->save_settings($name,$email,$password,$phone);

			if ($save == true) {
				redirect(base_url().'settings');
			}
		}

		$stylesheets = array(
			'themes/plugins/bower_components/toast-master/css/jquery.toast.css'
		);
		$javascripts = array(
			'themes/plugins/bower_components/toast-master/js/jquery.toast.js'
		);
		$externalsjs = array(
			//'assets/js/account.js'
		);
		$data = array(
			'page'        => 'account',
			'tpl'         => 'settings.tpl',
			'stylesheets' => $stylesheets,
			'javascripts' => $javascripts,
			'externalsjs' => $externalsjs
		);
		$this->load->view('init', $data);

	}

	function save_settings($name,$email,$password,$phone){

		$this->db->where('id', $this->session->user);
		$this->db->set('name', $name);
		$this->db->set('email', $email);

		if (!is_null($password)) {
		  $this->db->set('password', sha1(md5(trim($password))));
		}

		$this->db->set('phone', $phone);
		$this->db->update('clients');

		if ($this->db->affected_rows() > 0) {
			return true;
		}else{
			return false;
		}
	}

	public function client_orders(){

		$stylesheets = array(
			'themes/plugins/bower_components/toast-master/css/jquery.toast.css'
		);
		$javascripts = array(
			'themes/plugins/bower_components/toast-master/js/jquery.toast.js'
		);
		$externalsjs = array(
			'assets/js/oauth__socials.js',
			'themes/ampleadmin/js/jquery.PrintArea.js',
			'assets/js/orders.js'
		);
		$data = array(
			'page'        => 'account',
			'tpl'         => 'orders.tpl',
			'stylesheets' => $stylesheets,
			'javascripts' => $javascripts,
			'externalsjs' => $externalsjs
		);
		$this->load->view('init', $data);

	}

	public function client_invoice(){

		$id = $_GET['order_id'];

		if($id !== null && $id > 0){

			$order = $this->invoice->invoice_order_load($id)->row();
			$this->db->where('id', $order->clients_id);
			$client = $this->db->get('clients')->row();
			$orders_details = $this->invoice->invoice_order_details($id);

			$data = array(
				'order' => $order,
				'client' => $client,
				'orders_details' => $orders_details
			);

			$this->load->view('tpl/client_invoice.tpl', $data);
		}

	}

	public function client_orders_table(){
		$this->db->where('clients_id', $this->session->user);
		$this->db->order_by('id', 'DESC');
		$get_orders = $this->db->get('orders');
		if ($get_orders->num_rows() > 0) {

		   foreach ($get_orders->result() as $orders) {
		     $this->db->select_sum('subtotal');
		   	 $this->db->where('order_id', $orders->id);
		   	 $get_details = $this->db->get('orders_details');
		   	 if ($get_details->num_rows() > 0) {
		   	 	$row_details = $get_details->row();
		   	 	switch ($orders->status) {
		   	 		case 0:
		   	 			$status = '<span class="label label-warning">Pendente</span>';
		   	 			break;
		   	 		case 1:
		   	 			$status = '<span class="label label-success">Confirmado</span>';
		   	 			break;
		   	 		default:
		   	 			$status = '<span class="label label-danger">Cancelado</span>';
		   	 			break;
		   	 	}
		   	 	echo '<tr>
            		<td>'. $orders->id .'</td>
            		<td>'. date('d/m/Y H:i', strtotime($orders->date)) .'</td>
            		<td>'. $status .'</td>
            		<td>R$'. number_format($row_details->subtotal, 2, ",", ".") .' <a href="javascript: void(0)" data-order="'. $orders->id .'" class="printer"><i class="fa fa-print pull-right"></i></a></td>
            	</tr>';
		   	 }
		   }

		}
	}

	public function client_orders_table_total(){
		$this->db->where('clients_id', $this->session->user);
		$get_orders = $this->db->get('orders');

		$total = 0;
		if ($get_orders->num_rows() > 0) {

			foreach ($get_orders->result() as $orders) {
				$this->db->select_sum('subtotal');
				$this->db->where('order_id', $orders->id);
				$get_details = $this->db->get('orders_details');
				if ($get_details->num_rows() > 0) {
					$row_details = $get_details->row();
					$total += $row_details->subtotal;
				}
			}

		}
		return 'R$'.number_format($total, 2, ",", ".");

	}

	public function emails(){
		$this->emails->document_root();
	}

	public function categories(){
		$this->db->where('status', '1');
		return $this->db->get('category')->result();
	}

	public function products($id){
		$this->db->where('category_id', $id);
		return $this->db->get('products')->result();
	}

	public function ingredients($ingredient_ID){
		$this->db->where('id', $ingredient_ID);
		$get = $this->db->get('ingredients');
		if ($get->num_rows() > 0) {
			return $get->row();
		}else{
			return null;
		}
	}

	public function addons($addon_ID){
		$this->db->where('id', $addon_ID);
        $get = $this->db->get('addons');
		if ($get->num_rows() > 0) {
			return $get->row();
		}
	}

	public function gallery($product_ID){
		$this->db->where('products_id', $product_ID);
        return $this->db->get('gallery')->result();
	}

}
