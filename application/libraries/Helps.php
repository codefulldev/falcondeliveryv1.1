<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Helps {



	var $ci; 



	function __construct(){

		$this->ci =& get_instance();

	}


	public function cpf($cpf){
		$nbr_cpf = trim($cpf);

		$parte_um     = substr($nbr_cpf, 0, 3);
		$parte_dois   = substr($nbr_cpf, 3, 3);
		$parte_tres   = substr($nbr_cpf, 6, 3);
		$parte_quatro = substr($nbr_cpf, 9, 2);

		$monta_cpf = "$parte_um.$parte_dois.$parte_tres-$parte_quatro";

		return $monta_cpf;
	}

	public function board($board){
		$letters     = substr($board, 0, 3);
		$numbers     = substr($board, 3);
		return strtoupper($letters).'-'.$numbers;
	}

	public function count_orders(){

		$count = $this->ci->db->get('orders')->num_rows();

		$processbar = (1000 * $count);

		return json_decode(json_encode(array('processbar' => $processbar, 'count' => $count )));

	}



	public function count_clients(){

		$count = $this->ci->db->get('clients')->num_rows();

		$processbar = (1000 * $count);

		return json_decode(json_encode(array('processbar' => $processbar, 'count' => $count )));

	}



	public function count_gainday(){

		$this->ci->db->where('status', '1');

		$count = $this->ci->db->get('orders');

		$processbar = (1000 * $count->num_rows());



		$total = 0;

		$day = date('d');

		foreach ($count->result() as $row) {

			$date = date('d', strtotime($row->date));

			if($day === $date){

				$this->ci->db->where('order_id', $row->id);

				$order_details = $this->ci->db->get('orders_details');

				if ($order_details->num_rows() > 0) {

					foreach ($order_details->result() as $ord) {

						$total += $ord->subtotal;

					}

				}

			}

		}

		return json_decode(json_encode(array('processbar' => $processbar, 'count' => $count->num_rows(), 'total' => number_format($total, 2, ",", ".") )));

	}



	public function buys(){

		$this->ci->db->where('clients_id', $id);

		$count = $this->ci->db->get('orders')->num_rows();

		return $count;

	}



	public function orders($id, $status){

		$this->ci->db->where('clients_id', $id);

		$this->ci->db->where('status', $status);

		$count = $this->ci->db->get('orders')->num_rows();

		return $count;

	}



	public function barcode($num){

		return hexdec( substr(sha1($num), 0, 7) );

	}



	public function payments($index){

		switch ($index) {

			case 0:

			$payment = 'money';

			break;

			

			case 1:

			$payment = 'creditcard';

			break;

			

			case 2:

			$payment = 'debitcard';

			break;

			

			default:

			$payment = 'ticket';

			break;

		}

		return $payment;

	}



	public function isMobile() {

		$uaFull = strtolower($_SERVER['HTTP_USER_AGENT']);

		$uaStart = substr($uaFull, 0, 4);



		$uaPhone = [

			'(android|bb\d+|meego).+mobile',

			'avantgo',

			'bada\/',

			'blackberry',

			'blazer',

			'compal',

			'elaine',

			'fennec',

			'hiptop',

			'iemobile',

			'ip(hone|od)',

			'iris',

			'kindle',

			'lge ',

			'maemo',

			'midp',

			'mmp',

			'mobile.+firefox',

			'netfront',

			'opera m(ob|in)i',

			'palm( os)?',

			'phone',

			'p(ixi|re)\/',

			'plucker',

			'pocket',

			'psp',

			'series(4|6)0',

			'symbian',

			'treo',

			'up\.(browser|link)',

			'vodafone',

			'wap',

			'windows ce',

			'xda',

			'xiino'

		];



		$uaMobile = [ 

			'1207', 

			'6310', 

			'6590', 

			'3gso', 

			'4thp', 

			'50[1-6]i', 

			'770s', 

			'802s', 

			'a wa', 

			'abac|ac(er|oo|s\-)', 

			'ai(ko|rn)', 

			'al(av|ca|co)', 

			'amoi', 

			'an(ex|ny|yw)', 

			'aptu', 

			'ar(ch|go)', 

			'as(te|us)', 

			'attw', 

			'au(di|\-m|r |s )', 

			'avan', 

			'be(ck|ll|nq)', 

			'bi(lb|rd)', 

			'bl(ac|az)', 

			'br(e|v)w', 

			'bumb', 

			'bw\-(n|u)', 

			'c55\/', 

			'capi', 

			'ccwa', 

			'cdm\-', 

			'cell', 

			'chtm', 

			'cldc', 

			'cmd\-', 

			'co(mp|nd)', 

			'craw', 

			'da(it|ll|ng)', 

			'dbte', 

			'dc\-s', 

			'devi', 

			'dica', 

			'dmob', 

			'do(c|p)o', 

			'ds(12|\-d)', 

			'el(49|ai)', 

			'em(l2|ul)', 

			'er(ic|k0)', 

			'esl8', 

			'ez([4-7]0|os|wa|ze)', 

			'fetc', 

			'fly(\-|_)', 

			'g1 u', 

			'g560', 

			'gene', 

			'gf\-5', 

			'g\-mo', 

			'go(\.w|od)', 

			'gr(ad|un)', 

			'haie', 

			'hcit', 

			'hd\-(m|p|t)', 

			'hei\-', 

			'hi(pt|ta)', 

			'hp( i|ip)', 

			'hs\-c', 

			'ht(c(\-| |_|a|g|p|s|t)|tp)', 

			'hu(aw|tc)', 

			'i\-(20|go|ma)', 

			'i230', 

			'iac( |\-|\/)', 

			'ibro', 

			'idea', 

			'ig01', 

			'ikom', 

			'im1k', 

			'inno', 

			'ipaq', 

			'iris', 

			'ja(t|v)a', 

			'jbro', 

			'jemu', 

			'jigs', 

			'kddi', 

			'keji', 

			'kgt( |\/)', 

			'klon', 

			'kpt ', 

			'kwc\-', 

			'kyo(c|k)', 

			'le(no|xi)', 

			'lg( g|\/(k|l|u)|50|54|\-[a-w])', 

			'libw', 

			'lynx', 

			'm1\-w', 

			'm3ga', 

			'm50\/', 

			'ma(te|ui|xo)', 

			'mc(01|21|ca)', 

			'm\-cr', 

			'me(rc|ri)', 

			'mi(o8|oa|ts)', 

			'mmef', 

			'mo(01|02|bi|de|do|t(\-| |o|v)|zz)', 

			'mt(50|p1|v )', 

			'mwbp', 

			'mywa', 

			'n10[0-2]', 

			'n20[2-3]', 

			'n30(0|2)', 

			'n50(0|2|5)', 

			'n7(0(0|1)|10)', 

			'ne((c|m)\-|on|tf|wf|wg|wt)', 

			'nok(6|i)', 

			'nzph', 

			'o2im', 

			'op(ti|wv)', 

			'oran', 

			'owg1', 

			'p800', 

			'pan(a|d|t)', 

			'pdxg', 

			'pg(13|\-([1-8]|c))', 

			'phil', 

			'pire', 

			'pl(ay|uc)', 

			'pn\-2', 

			'po(ck|rt|se)', 

			'prox', 

			'psio', 

			'pt\-g', 

			'qa\-a', 

			'qc(07|12|21|32|60|\-[2-7]|i\-)', 

			'qtek', 

			'r380', 

			'r600', 

			'raks', 

			'rim9', 

			'ro(ve|zo)', 

			's55\/', 

			'sa(ge|ma|mm|ms|ny|va)', 

			'sc(01|h\-|oo|p\-)', 

			'sdk\/', 

			'se(c(\-|0|1)|47|mc|nd|ri)', 

			'sgh\-', 

			'shar', 

			'sie(\-|m)', 

			'sk\-0', 

			'sl(45|id)', 

			'sm(al|ar|b3|it|t5)', 

			'so(ft|ny)', 

			'sp(01|h\-|v\-|v )', 

			'sy(01|mb)', 

			't2(18|50)', 

			't6(00|10|18)', 

			'ta(gt|lk)', 

			'tcl\-', 

			'tdg\-', 

			'tel(i|m)', 

			'tim\-', 

			't\-mo', 

			'to(pl|sh)', 

			'ts(70|m\-|m3|m5)', 

			'tx\-9', 

			'up(\.b|g1|si)', 

			'utst', 

			'v400', 

			'v750', 

			'veri', 

			'vi(rg|te)', 

			'vk(40|5[0-3]|\-v)', 

			'vm40', 

			'voda', 

			'vulc', 

			'vx(52|53|60|61|70|80|81|83|85|98)', 

			'w3c(\-| )', 

			'webc', 

			'whit', 

			'wi(g |nc|nw)', 

			'wmlb', 

			'wonu', 

			'x700', 

			'yas\-', 

			'your', 

			'zeto', 

			'zte\-'

		];



		$isPhone = preg_match('/' . implode($uaPhone, '|') . '/i', $uaFull);

		$isMobile = preg_match('/' . implode($uaMobile, '|') . '/i', $uaStart);



		if($isPhone || $isMobile) {

			return true;

		} else {

			return false;

		}

	}



	public function count_gainmonth(){

		$this->ci->db->where('status', '1');

		$count = $this->ci->db->get('orders');

		$processbar = (1000 * $count->num_rows());



		$total = 0;

		$day = date('m');

		foreach ($count->result() as $row) {

			$date = date('m', strtotime($row->date));

			if($day === $date){

				$this->ci->db->where('order_id', $row->id);

				$order_details = $this->ci->db->get('orders_details');

				if ($order_details->num_rows() > 0) {

					foreach ($order_details->result() as $ord) {

						$total += $ord->subtotal;

					}

				}

			}

		}

		return json_decode(json_encode(array('processbar' => $processbar, 'count' => $count->num_rows(), 'total' => number_format($total, 2, ",", ".") )));

	}



	public function date_converter($_date = null) {

		$format = '/^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/';

		if ($_date != null && preg_match($format, $_date, $partes)) {

			return $partes[3].'-'.$partes[2].'-'.$partes[1];

		}

		return false;

	}



	public function shippingPrice($distance, $price){

		$km = number_format($distance / 1000, 2);

		$shippingPrice = $km * floatval($price);

		$shippingPrice = number_format($shippingPrice, 2, ".", ".");

		return $shippingPrice;

	}



	public function shippingDuration($duration){

		return date('i:s',mktime(0,0, $duration + 600 ,15,03,2013));

	}



	public function funcionallyInterval($start, $end, $now) {

		$startTimestamp = strtotime($start);

		$endTimestamp = strtotime($end);

		$nowTimestamp = strtotime($now);

		return (($nowTimestamp >= $startTimestamp) && ($nowTimestamp <= $endTimestamp));

	}



	public function formatPhone($number){

		if(strlen($number) == 10){

			$new = substr_replace($number, '(', 0, 0);

			$new = substr_replace($new, ')', 3, 0);

			$new = substr_replace($new, '-', 8, 0);

		}else{

			$new = substr_replace($number, '(', 0, 0);

			$new = substr_replace($new, ')', 3, 0);

			$new = substr_replace($new, '-', 9, 0);

		}

		return $new;

	}



	public function array_orderby(){



		$args = func_get_args();

		$data = array_shift($args);

		foreach ($args as $n => $field) {

			if (is_string($field)) {

				$tmp = array();

				foreach ($data as $key => $row)

					$tmp[$key] = $row[$field];

				$args[$n] = $tmp;

			}

		}

		$args[] = &$data;

		call_user_func_array('array_multisort', $args);

		return array_pop($args);

	}



	public function verify_avatar($img, $type){

		switch ($type) {

			case 'admin':

			$dirr = 'assets/admin/img/avatar/';

			break;

			

			default:

			$dirr = 'assets/clients/';

			break;

		}

		if(isset($img) AND file_exists($dirr.$img)){

			return $dirr.$img;

		}else{

			return $dirr.'no.jpg';

		}



	}



	public function names_month($month){

		$array = array

		(

			1  => 'Janeiro',

			2  => 'Fevereiro',

			3  => 'Março',

			4  => 'Abril',

			5  => 'Maio',

			6  => 'Junho',

			7  => 'Julho',

			8  => 'Agosto',

			9  => 'Setembro',

			10 => 'Outubro',

			11 => 'Novembro',

			12 => 'Dezembro'

		);



		return $array[$month];

	}



	public function verify_thumbnail($img, $type){

		$dirr = 'themes/uploads/thumbnail/';

		if(isset($img) AND file_exists($dirr.$img)){

			return $dirr.$img;

		}else{

			return 'assets/images/no-hamburguer.jpg';

		}



	}



	public function verify_logo($img){

		$dirr = 'themes/uploads/logo/';

		if(isset($img) AND file_exists($dirr.$img)){

			return $dirr.$img;

		}else{

			return 'assets/favicon.png';

		}



	}



	public function verify_client_thumbnail($img){

		$dirr = 'assets/clients/';

		if(isset($img) AND file_exists($dirr.$img)){

			return $dirr.$img;

		}else{

			return $dirr.'no.jpg';

		}



	}



	public function first_name($fullname){

		$replace = str_replace(" ", "|", trim($fullname));

		if($replace){

			$explode = explode("|", $replace);

			return ucfirst($explode[0]);

		}else{

			return ucwords(trim($fullname));

		}

	}



	public function check_time($start, $end){

		$time = date('H:i');

	}



	public function format_money($money){

		return number_format(floatval($money), 2, '.', '.');

	}



	public function limit_text($message){



		$position = 100;



		$post = substr($message,$position,1);



		if($post != " "){

			for ($i = 0; $i < strlen($post); $i++) {

				$position = $position + $i; 

				$post = substr($message,$position,1); 

			}

		}



		$post = substr($message,0,$position); 



		return $post.'...';



	}

	public function delivery_calculate($origin, $destination){

		$origin = substr_replace($origin, '-', 5, 0);
		$destination = substr_replace($destination, '-', 5, 0);

		$object = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/distancematrix/json?origins='.$origin.'|&destinations='.$destination.'|&mode=CAR|&language=pt'));
		$data = $object->rows[0]->elements[0];

		echo json_encode(array(
			'distance' => array('text' => $data->distance->text, 'value' => $data->distance->value),
			'duration' => array('text' => $data->duration->text, 'value' => $data->duration->value)
		));

	}

	function formataReais($valor1, $valor2, $operacao){



		$valor1 = str_replace (",", "", $valor1);

		$valor1 = str_replace (".", "", $valor1);



		$valor2 = str_replace (",", "", $valor2);

		$valor2 = str_replace (".", "", $valor2);



		switch ($operacao) {

			case "+":

			$resultado = $valor1 + $valor2;

			break;



			case "-":

			$resultado = $valor1 - $valor2;

			break;



			case "*":

			$resultado = $valor1 * $valor2;

			break;



		}





		$len = strlen ($resultado);



		switch ($len) {



			case "2":

			$retorna = "0,$resultado";

			break;



			case "3":

			$d1 = substr("$resultado",0,1);

			$d2 = substr("$resultado",-2,2);

			$retorna = "$d1,$d2";

			break;





			case "4":

			$d1 = substr("$resultado",0,2);

			$d2 = substr("$resultado",-2,2);

			$retorna = "$d1,$d2";

			break;





			case "5":

			$d1 = substr("$resultado",0,3);

			$d2 = substr("$resultado",-2,2);

			$retorna = "$d1,$d2";

			break;





			case "6":

			$d1 = substr("$resultado",1,3);

			$d2 = substr("$resultado",-2,2);

			$d3 = substr("$resultado",0,1);

			$retorna = "$d3.$d1,$d2";

			break;





			case "7":

			$d1 = substr("$resultado",2,3);

			$d2 = substr("$resultado",-2,2);

			$d3 = substr("$resultado",0,2);

			$retorna = "$d3.$d1,$d2";

			break;





			case "8":

			$d1 = substr("$resultado",3,3);

			$d2 = substr("$resultado",-2,2);

			$d3 = substr("$resultado",0,3);

			$retorna = "$d3.$d1,$d2";

			break;



		}





		return $retorna;

	}



}

