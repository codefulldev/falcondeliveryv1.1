<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Emails {

	var $CI;

	function __construct(){
		$timezone = json_decode(file_get_contents('config/timezone.json'));
		date_default_timezone_set($timezone->timezone);
		$this->CI =& get_instance();
	}

	public function document_root(){
		$url = (dirname($_SERVER['PHP_SELF'])) ? $_SERVER['DOCUMENT_ROOT'].dirname($_SERVER['PHP_SELF']).'/' : '/';
		return $url;
	}

	public function config_website(){
		return $this->CI->config_website();
	}

	public function welcome($username, $email){

		$conn = $this->CI;
		$conn->db->where('define', 'welcome');
		$welcome = $conn->config_emails();
		$config  = $this->config_website();

		$to = $email;
		$subject = isset($welcome->subject) ? $welcome->subject : "Bem-vindo!";

		$systemEmail = (!empty($config->email)) ? $config->email : 'no-reply@falcondelivery.com.br';
		
		$body = file_get_contents($this->document_root().'application/views/emails/welcome.html');
		$body = str_replace("[{cliente_name}]", $username, $body);
		$body = str_replace("[{e_title}]", ucfirst($config->title), $body);
		$body = str_replace("[{e.phone}]", $conn->helps->formatPhone($config->phone), $body);
		$body = str_replace("[{e.email}]", $config->email, $body);
		$htmlContent = $this->email_template($subject, $body);

		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		$headers .= 'From: '.$config->title.' <'. $systemEmail . "> \r\n";
		$headers .= 'Cc: '.$systemEmail. "\r\n";

		if(mail($to, $subject, $htmlContent, $headers)):
			return json_encode(array('send' => true));
		else:
			return json_encode(array('send' => false));
		endif;

	}

	public function order_status($username, $email){
		$url = (dirname($_SERVER['PHP_SELF'])) ? dirname($_SERVER['PHP_SELF']) : '/';
		$conn = $this->CI;
		$conn->db->where('define', 'order_status');
		$order_status = $conn->config_emails();
		$config  = $this->config_website();

		$to = $email;
		$subject = isset($order_status->subject) ? $order_status->subject : "Status do pedido";

		$systemEmail = (!empty($config->email)) ? $config->email : 'no-reply@falcondelivery.com.br';
		
		$body = file_get_contents($this->document_root().'application/views/emails/order_status.html');
		$body = str_replace("[{cliente_name}]", $username, $body);
		$body = str_replace("[{e_title}]", ucfirst($config->title), $body);
		$body = str_replace("[{e.phone}]", $conn->helps->formatPhone($config->phone), $body);
		$body = str_replace("[{e.email}]", $config->email, $body);
		$htmlContent = $this->email_template($subject, $body);

		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		$headers .= 'From: '.$config->title.' <'. $systemEmail . "> \r\n";
		$headers .= 'Cc: '.$systemEmail. "\r\n";

		if(mail($to, $subject, $htmlContent, $headers)):
			return json_encode(array('send' => true));
		else:
			return json_encode(array('send' => false));
		endif;

	}

	public function receipt($username, $email, $cupom = null){

		$url = (dirname($_SERVER['PHP_SELF'])) ? dirname($_SERVER['PHP_SELF']) : '/';
		$conn = $this->CI;
		$conn->db->where('define', 'receipt');
		$receipt = $conn->config_emails();
		$config  = $this->config_website();

		$to = $email;
		$subject = isset($receipt->subject) ? $receipt->subject : "Recibo de compra";

		$systemEmail = (!empty($config->email)) ? $config->email : 'no-reply@falcondelivery.com.br';
		
		$body = file_get_contents($this->document_root().'application/views/emails/receipt.html');
		$body = str_replace("[{cliente_name}]", $username, $body);
		$body = str_replace("[{e_title}]", ucfirst($config->title), $body);
		$body = str_replace("[{cupom}]", $cupom, $body);
		$body = str_replace("[{e.phone}]", $conn->helps->formatPhone($config->phone), $body);
		$body = str_replace("[{e.email}]", $config->email, $body);
		$htmlContent = $this->email_template($subject, $body);

		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		$headers .= 'From: '.$config->title.' <'. $systemEmail . "> \r\n";
		$headers .= 'Cc: '.$systemEmail. "\r\n";

		if(mail($to, $subject, $htmlContent, $headers)):
			return json_encode(array('send' => true));
		else:
			return json_encode(array('send' => false));
		endif;

	}

	public function recoveryAdmin($email, $username, $password, $url){

		$dirr = (dirname($_SERVER['PHP_SELF'])) ? dirname($_SERVER['PHP_SELF']) : '/';
		$conn = $this->CI;

		$config = $this->config_website();

		$to = $email;
		$subject = "Redefinição de senha";

		$systemEmail = $config->email;
		
		$body = file_get_contents($this->document_root().'application/views/emails/recovery.html');
		$body = str_replace("[{username}]", $username, $body);
		$body = str_replace("[{password}]", $password, $body);
		$body = str_replace("[{year}]", date('Y'), $body);
		$body = str_replace("[{e.phone}]", $conn->helps->formatPhone($config->phone), $body);
		$body = str_replace("[{e.email}]", $config->email, $body);
		$htmlContent = $this->email_template($subject, $body);

		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

		$headers .= 'From: '.$config->title.' <'. $systemEmail . "> \r\n";
		$headers .= 'Cc: '.$systemEmail. "\r\n";
		$headers .= "Reply-To: ".$systemEmail. "\r\n";

		if(mail($to, $subject, $htmlContent, $headers)):
			return true;
		else:
			return false;
		endif;

	}

	public function data_subject(){

	}

	public function data_email(){
		$config = $this->config_website();
		$thumbnail = base_url().'themes/uploads/logo/emails/'.$config->site_logo;
		
		if (file_exists($thumbnail) AND $thumbnail != '') {
			return $thumbnail;
		}else{
			return base_url().'themes/uploads/logo/no.png';
		}
	}

	public function email_template($title, $body){
		$config = $this->config_website();
		$picture = $this->data_email();
		return '<html>
		<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>'.$title.'</title>
		</head>
		<body style="margin:0px; background: #fee253; ">
		<div width="100%" style="background: #fee253; padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #ffffff;">
		<div style="max-width: 700px; padding:50px 0;  margin: 0px auto; font-size: 14px">
		<table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom: 20px">
		<tbody>
		<tr>
		<td style="vertical-align: top; padding-bottom:30px;" align="center">
		<a href="'. base_url() .'" target="_blank">
		<img src="'.$picture.'" width="45" height="45" alt="'.$title.'" style="border:none">
		</a>
		</td>
		</tr>
		</tbody>
		</table>
		<div style="padding: 40px; background: #fff;">
		<table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
		<tbody>
		<tr>
		<td style="text-align: center;color: #212121;">'.$body.'</td>
		</tr>
		</tbody>
		</table>
		</div>
		<div style="text-align: center; font-size: 12px; color: #666; margin-top: 20px">
		<p> Powered by <b>'. $config->title .'</b></p>
		</div>
		</div>
		</div>
		</body>
		</html>';

	}

}
