<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice {

	var $ci; 

	function __construct(){
		$this->ci =& get_instance();
	}

	function orders_list(){
		$this->ci->db->order_by('date', 'DESC');
		return $this->ci->db->get('orders');
	}

	function invoice_order_load($id){
		$this->ci->db->where('id', $id);
		return $this->ci->db->get('orders');
	}

	function invoice_order_client($id){
		$this->ci->db->where('id', $id);
		return $this->ci->db->get('clients');
	}

	function invoice_order_details($id){
		$this->ci->db->where('order_id', $id);
		return $this->ci->db->get('orders_details');
	}

	function invoice_order_total($id){
		$this->ci->db->where('order_id', $id);
		$get = $this->ci->db->get('orders_details');
		if ($get->num_rows() > 0) {
			$total = 0;
			foreach ($get->result() as $order) {
				$total += $order->subtotal;
			}
			return number_format($total, 2, ".", ".");
		}
	}

}