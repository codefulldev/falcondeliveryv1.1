<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PagSeguro {

	private $CI;
	var $url;
	var $params = array(
		'sessions' => 'sessions',

	);

	public function __construct(){
		$this->CI =& get_instance();
	}

	function get_credentials(){
		$this->CI->db->where('payments', 'pagseguro');
		$payments = $this->CI->db->get('config_payments');
		if ($payments->num_rows() > 0) {
			return $payments->row();
		}
	}

	function get_url(){
		$cd = $this->get_credentials();
		if ($cd->payments_envelopment == 0) {
			return 'https://ws.pagseguro.uol.com.br/v2/';
		}else{
			return 'https://ws.sandbox.pagseguro.uol.com.br/v2/';
		}
	}

	public function payments_creditcard(){
		$Session_ID = $this->get_session();
		echo json_encode(
			array(
				'session'      => $Session_ID,
				'cartTotal'    => $this->get_cart_total(),
				'installments' => $this->CI->config_payments()->payments_x
			)
		);
	}

	public function get_session(){
		$params['email'] = $this->get_credentials()->payments_email;
		$params['token'] = $this->get_credentials()->payments_token;
		$return = $this->call_curl('sessions', $params);
		if (count($return->id)) {
			return $return->id;
		}else{
			return false;
		}
	}

	public function get_cart_total(){
		$total = 0;

		foreach ($this->CI->session->userdata('shopcart') as $i => $items):
			list($id, $index) = explode(":", $i);

			$this->CI->db->where("id", $id);
			$product = $this->CI->db->get('products')->row();

			$subtotal = $product->price * $items['quantity'];

			$take = array();
			if(isset($items['params']['ingredients'])):
				foreach ($items['params']['ingredients'] as $ingredients) {
					$this->CI->db->where('id', $ingredients);
					$ingredient = $this->CI->db->get('ingredients')->row();
					if($ingredient->price > 0){
						$subtotal += $ingredient->price * $items['quantity'];
					}
				}
			endif;

			$addons = array();
			if(isset($items['params']['addons'])):
				foreach ($items['params']['addons'] as $addon_id) {
					$this->CI->db->where('id', $addon_id);
					$addon = $this->CI->db->get('addons')->row();
					if($addon->price > 0){
						$subtotal += $addon->price * $items['quantity'];
					}
				}
			endif;
			$total += $subtotal;
		endforeach;

		//$total += DELIVERY_COST;

		return $total;
	}

	public function transaction_creditCard(){
		$xml = getCreditCardXML();
	}

	public function getCreditCardXML() {
		return "<payment>
		<mode>default</mode>
		<currency>BRL</currency>
		<notificationURL>" . $notificationURL . "</notificationURL>
		<receiverEmail>" . $emailPagseguro . "</receiverEmail>
		<sender>
		<hash>". $senderHash . "</hash>
		<ip>" . $_SERVER['REMOTE_ADDR'] . "</ip>
		<email>". $email . "</email>
		<documents>
		<document>
		<type>CPF</type>
		<value>" . $cpf . "</value>
		</document>
		</documents>
		<phone>
		<areaCode>" . $ddd . "</areaCode>
		<number>" . $telefone . "</number>
		</phone>
		<name>" . $nome . "</name>
		</sender>
		<creditCard>
		<token>". $cardToken ."</token>
		<holder>
		<name>" . $holdCardNome . "</name>
		<birthDate>" . $holdCardNasc ."</birthDate>
		<documents>
		<document>
		<type>CPF</type>
		<value>" . $holdCardCPF . "</value>
		</document>
		</documents>
		<phone>
		<areaCode>" . $holdCardArea . "</areaCode>
		<number>" . $holdCardFone . "</number>
		</phone>
		</holder>
		<billingAddress>
		<street>" . $enderecoPagamento . "</street>
		<number>" . $numeroPagamento . "</number>
		<complement>" . $complementoPagamento . "</complement>
		<district>" . $bairroPagamento . "</district>
		<city>" . $cidadePagamento . "</city>
		<state>" . $estadoPagamento . "</state>
		<postalCode>" . $cepPagamento . "</postalCode>
		<country>BRA</country>
		</billingAddress>
		<installment>
		<quantity>" . $parcelas . "</quantity>
		<value>" . $valorParcelas . "</value>
		<noInterestInstallmentQuantity>2</noInterestInstallmentQuantity>
		</installment>
		</creditCard>
		<items>
		<item>
		<id>" . $id . "</id>
		<description>" . $produto . "</description>
		<amount>" . $valor . "</amount>
		<quantity>1</quantity>
		</item>
		</items>
		<reference>" . $id . "</reference>
		<shipping>
		<address>
		<street>" . $endereco . "</street>
		<number>" . $numero . "</number>
		<complement>" . $complemento . "</complement>
		<district>" . $bairro . "</district>
		<city>" . $cidade . "</city>
		<state>" . $estado . "</state>
		<country>BRA</country>
		<postalCode>" . $cep . "</postalCode>
		</address>
		<type>1</type>
		<cost>0.00</cost>
		<addressRequired>true</addressRequired>
		</shipping>
		<extraAmount>0.00</extraAmount>
		<method>creditCard</method>
		<dynamicPaymentMethodMessage>
		<creditCard>infoEnem</creditCard>
		<boleto>infoEnem</boleto>
		</dynamicPaymentMethodMessage>
		</payment>";
	}

	public function call_curl($param, $params){

		$data = http_build_query($params);
		$curl = curl_init($this->get_url().$param);

		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
		$xml= curl_exec($curl);
		curl_close($curl);

		$xml = simplexml_load_string($xml);
		if (count($xml) > 0) {
			return $xml;
		}else{
			return false;
		}

	}

}