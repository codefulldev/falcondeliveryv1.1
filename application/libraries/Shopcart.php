<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$CI =& get_instance();

final class Shopcart {

	public function __construct(){
		$timezone = json_decode(file_get_contents('config/timezone.json'));
		date_default_timezone_set($timezone->timezone);
		if(!$CI->session->shopcart){
			$_SESSION['shopcart'] = array();
		}

	}

	public function add($id, $quantity = 1, $params, $uri){

		$index = sprintf('%s:%s', $id, count($_SESSION['cart'])+1);
		print_r($index);
		$array = array();

		if(count($params) > 0):
			foreach ($params as $param) {

			  if(isset($param['addons'])){
			  	$array['addons'] = $param['addons'];
			  }
			  if(isset($param['ingredients'])){
			  	$array['ingredients'] = $param['ingredients'];	
			  }

			}
		endif;

		if(empty($_SESSION['shopcart'][$index])){
			if($_SESSION['shopcart'][$index] = array('quantity' => (int)$quantity, 'params' => $array)){
				echo json_encode(array('added' => true, 'uri' => $uri));
			}else{
				echo json_encode(array('added' => false));
			}
		}

	}

}
