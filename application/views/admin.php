<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$CI =& get_instance();
$config = $CI->ConfigController();
$base = $CI->base;
$logo = $CI->helps->verify_logo($config->site_logo);
?>
<!DOCTYPE html>

<html lang="en" ng-app="FoodApplication">
<head>
	<base href="<?php echo base_url(); ?>">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?= $logo ?>">
    <title>Painel de Controle</title>
    <!-- Bootstrap Core CSS -->
    <link href="themes/ampleadmin/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- This is Sidebar menu CSS -->
    <link href="themes/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- This is a Animation CSS -->
    <link href="themes/ampleadmin/css/animate.css" rel="stylesheet">
    <!-- Sweetalert CSS -->
    <link href="themes/plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet">
    <!-- dataTables CSS -->
    <link href="themes/plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet">
    <link href="themes/plugins/bower_components/datatables/buttons.dataTables.min.css" rel="stylesheet">
    <!-- This is a Custom CSS -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Changa+One:400,400i%7CGrand+Hotel%7CLato:300,400,400italic,700">
    <link href="themes/ampleadmin/css/style.css" rel="stylesheet">
    <link href="themes/ampleadmin/css/colors/blue-dark.css" id="theme" rel="stylesheet">

    <?php foreach ($StyleSheet as $files): ?>
<link href="<?= $files ?>" rel="stylesheet">
    <?php endforeach ?>

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
</head>

<body class="fix-sidebar" ng-controller="GeneralController">
    <div id="pn" data-address="<?= $config->ipdomain ?>" data-port="<?= $config->port ?>"></div>
	<!-- Preloader -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/>
        </svg>
    </div>
    <?php
        $this->load->view($root.'navbar');
        $this->load->view($root.'sidebar');
    ?>
    <div id="page-wrapper">
    <?php 
        $this->load->view($root.'pages/'.$page);
    ?>


    <!-- .right-sidebar -->
    <div class="right-sidebar">
        <div class="slimscrollright">
            <div class="rpanel-title"> Atendimento Online <span><i class="ti-close right-side-toggle"></i></span> </div>
            <div class="r-panel-body">
                <ul id="sidebar_chatonline" class="m-t-20 chatonline">
                    <li><b>Clientes Online</b></li>
                    
                </ul>
            </div>
        </div>
    </div>
    <!-- /.right-sidebar -->
    
       <footer class="footer text-center">
        <?php echo date('Y'); ?> &copy; FalconDelivery
       </footer>
    </div>

    <!-- jQuery -->
    <script src="themes/plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!--  AngularJS  -->
    <script src="themes/plugins/bower_components/angular/angular.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="themes/ampleadmin/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Moment JavaScript -->
    <script src="themes/ampleadmin/js/moment.min.js"></script>
    <script src="themes/ampleadmin/js/moment.locale.pt-br.js"></script>
    <!-- Sidebar menu plugin JavaScript -->
    <script src="themes/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!-- Buttons generates Files -->
    <script src="themes/plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
    <!-- start - This is for export functionality only -->
    <script src="themes/plugins/bower_components/datatables/dataTables.buttons.min.js"></script>
    <script src="themes/plugins/bower_components/datatables/buttons.flash.min.js"></script>
    <script src="themes/plugins/bower_components/datatables/jszip.min.js"></script>
    <script src="themes/plugins/bower_components/datatables/pdfmake.min.js"></script>
    <script src="themes/plugins/bower_components/datatables/vfs_fonts.js"></script>
    <script src="themes/plugins/bower_components/datatables/buttons.html5.min.js"></script>
    <script src="themes/plugins/bower_components/datatables/buttons.print.min.js"></script>

    <!--Slimscroll JavaScript For custom scroll-->
    <script src="themes/ampleadmin/js/jquery.slimscroll.js"></script>
    <script src="themes/ampleadmin/js/jquery.snake.min.js"></script>
    <script src="themes/plugins/bower_components/sweetalert/sweetalert.min.js"></script>
    <?php foreach ($JavaScript as $files): ?>
<script src="<?= $files ?>"></script>  
    <?php endforeach ?>
    <!--Wave Effects -->
    <script src="themes/ampleadmin/js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="themes/ampleadmin/js/custom.js"></script>

    <?php foreach ($ExternalJS as $files): ?>
<script src="<?= $files ?>"></script>  
    <?php endforeach ?>
    
    <script src="themes/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>

    <script src="node_modules/socket.io-client/dist/socket.io.js"></script>
    <script src="node_modules/angular-socket-io/socket.js"></script>
    <script src="assets/js/broadcast.js"></script>
    <script src="app.js"></script>

    <div id="chat-open-sidebar" style="position: fixed;left:65px;bottom: 40px;width:70px;z-index:1000">
     <button class="right-side-toggle-chat btn btn-primary btn-circle btn-xl"><i class="ti-comments"></i></button>
    </div>

    <script src="assets/plugins/timeago/jquery.livequery.js"></script>
    <script src="assets/plugins/timeago/jquery.timeago.js"></script>
    <script>
        jQuery(document).ready(function($) {
            $(".time").timeago();
        });
    </script>

</body>
</html>