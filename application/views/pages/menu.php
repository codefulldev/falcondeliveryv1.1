<?php
  $CI =& get_instance();
  $agent = $CI->helps->isMobile();
?>

<?php if ($agent): ?>
  <section>
<?php else: ?>
  <section class="section-50 section-sm-100">
<?php endif ?>
  <div class="shell">
    <div class="range range-xs-center">
      <h3>Cardápio <?php  ?></h3>
      <?php foreach ($category as $i => $cat):
      $class = ($i > 0) ? 'offset-top-50 offset-sm-top-0' : '';

      $CI->db->where('category_id', $cat->id);
      $get = $CI->db->get('products');
      if ($get->num_rows() > 0) {
      ?>
        <div class="cell-sm-6 <?= $class ?>">
          <div class="menu-classic" style="background: url('assets/images/background-for-food-3.jpg');">
            <h4 class="title"><a href="javascript:void(0)" class="link-white"><?= $cat->name ?></a></h4>
            <ul class="list-menu">
              <?php 
                foreach ($get->result() as $product): 
                  $ingredients_array = array();
                  if(isset($product->ingredients)):
                    foreach (explode("|", $product->ingredients) as $ingredient_ID) {
                      $ing = $CI->ingredients($ingredient_ID);
                      $ingredients_array[] = $ing->name;
                    }
                  endif;
                ?>
                <li>
                  <div class="menu-item h6"><span><span><a href="<?= base_url().'product/'.$product->id ?>"><?= $product->name ?></a></span></span><span class="price"><?= $CI->helps->format_money($product->price) ?></span></div>
                  <div class="menu-item-desc"><span><?= join(" / ", $ingredients_array) ?></span></div>
                </li>
              <?php endforeach; ?>
            </ul>
          </div>
        </div>
      <?php 
        } 
      endforeach; 
      ?>

    </div>

  </div>
</section>