<?php
$CI =& get_instance();
$helps = $CI->helps;
$thumbnail = $helps->verify_thumbnail($product->thumbnail, 0);
$classSection = (!$helps->isMobile()) ? "section-50 section-sm-100" : "";
?>
<div>
  <form method="post" role="add_to_cart" data-id="<?php echo $product->id; ?>">
    <div id="product_ID" data-id="<?= $product->id ?>"></div>
    <section class="<?= $classSection ?>">
      <div class="shell">
        <div class="range range-xs-center"> 
          <div class="cell-sm-6 text-lg-left">
            <?php if ($product->pizza == 1): ?>
              <div id="pizza" style="z-index: 55;">
                <?php $CI->load->view('tpl/make.tpl', array('product' => $product, 'thumbnail' => $thumbnail)); ?>
              </div>
            <?php else: ?>
            <div data-arrows="false" data-loop="false" data-dots="false" data-swipe="false" data-items="1" data-child="#child-carousel" data-for="#child-carousel" class="slick-slider carousel-parent">
              <div class="item">
                <img src="<?php echo $thumbnail ?>" alt="" width="542" height="448" class="img-responsive reveal-inline-block"/>
              </div>
            </div>
            <?php endif ?>
            
            <?php if ($CI->gallery($product->id)): ?>
            <style type="text/css">
              .MultiCarousel { float: left; overflow: hidden; padding: 15px; width: 100%; position:relative; }
              .MultiCarousel .MultiCarousel-inner { transition: 1s ease all; float: left; }
              .MultiCarousel .MultiCarousel-inner .item { float: left;}
              .MultiCarousel .MultiCarousel-inner .item > div { text-align: center; padding:10px; margin:10px; background:#f1f1f1; color:#666;}
              .MultiCarousel .leftLst, .MultiCarousel .rightLst { position:absolute; border-radius:50%;top:calc(50% - 20px); }
              .MultiCarousel .leftLst { left:0; }
              .MultiCarousel .rightLst { right:0; }
              .MultiCarousel .leftLst.over, .MultiCarousel .rightLst.over { pointer-events: none; background:#ccc; }
            </style>

            <div class="MultiCarousel hidden" data-items="1,3,5,6" data-slide="1" id="MultiCarousel"  data-interval="1000">
              <div class="MultiCarousel-inner">
               <?php foreach ($CI->gallery($product->id) as $gallery): ?>        
                 <div class="item">
                   <div>
                    <a class="fancybox" rel="ligthbox" href="<?= base_url(); ?>themes/uploads/gallery/<?php echo $gallery->thumbnail ?>">
                     <img src="<?= base_url(); ?>themes/uploads/gallery/<?php echo $gallery->thumbnail ?>" alt="" width="84" height="69"/>
                    </a>
                   </div>
                 </div>
               <?php endforeach; ?>
              </div>
              <a href="javascript: void(0)" class="btn btn-xs btn-primary text-middle btn-shape-circle leftLst" style="padding:5px 10px;">
                <span><</span>
              </a>
              <a href="javascript: void(0)" class="btn btn-xs btn-primary text-middle btn-shape-circle rightLst" style="padding:5px 10px;">
                <span>></span>
              </a>
            </div>

            <?php endif ?>
          </div>

        <div class="cell-sm-6 cell-lg-5 text-sm-left offset-top-60 offset-sm-top-0">
          <div class="reveal-xs-flex range-xs-middle range-xs-center range-sm-left">
            <h4 class="font-default"><?php echo ucwords($product->name) ?></h4>
            <input type="hidden" name="productID" value="<?php echo $product->id; ?>">
            <div class="inset-xs-left-50 offset-top-0">
              <div class="team-member-position team-member-position-burnt-sienna">
                <?php
                 $this->db->where('id', $product->category_id);
                 $category = $CI->db->get('category')->row();
                 echo '<span class="middle text-italic text-middle">'. $category->name .'</span>';
                ?>
              </div>
            </div>
          </div>
          <div class="offset-top-15">
            <?php
             $facebookGet = $CI->db->get('networks'); 
            ?>
            <?php if ($facebookGet->num_rows() > 0): 
              $facebook = $facebookGet->row(); 
              $oauth = $CI->db->get('oauth')->row();
            ?>
                
              <div id="fb-root"></div>
              <script>(function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=<?= $oauth->version ?>";
                fjs.parentNode.insertBefore(js, fjs);
              }(document, 'script', 'facebook-jssdk'));</script>

              <div class="fb-share-button" 
                data-href="<?= base_url().'product/'.$product->id ?>/" 
                data-layout="button">
              </div>

            <?php endif ?>
          
            <div class="group-sm hidden">
              <a href="#" class="link-zoom"><img src="assets/images/shop-single-01-57x21.png" alt="" width="57" height="21"/></a>
              <a href="#" class="link-zoom"><img src="assets/images/shop-single-02-52x21.png" alt="" width="52" height="21"/></a>
              <a href="#" class="link-zoom"><img src="assets/images/shop-single-03-54x21.png" alt="" width="54" height="21"/></a>
            </div>

          </div>
          <div class="offset-top-15">

              <?php
              $ingredients_array = array();
              if(!empty($product->ingredients) && $product->pizza < 1):
                echo "<p>Ingredientes: <i class='fa fa-info-circle' data-toggle=\"tooltip\" title=\"Você pode retirar os ingredientes que você quiser.\"></i></p>";
                foreach (explode("|", $product->ingredients) as $ingredient_ID) {
                  $ing = $CI->ingredients($ingredient_ID);
                  $ingredients_array[] = '<span class="label label-danger">'.$ing->name.'</span>';
                }
                echo '<input id="ingredients" name="ingredients[default][]" type="text" data-ingredients="'.$product->ingredients.'" readyonly />';
              endif;
              ?>
              <?php if ($product->pizza == 1): ?>
                <div id="new_flavors">
                  
                </div>
              <?php endif ?>
              <?php 
              if (isset($product->addons)): 
                echo "<p>Adicionais: <i class='fa fa-info-circle' data-toggle=\"tooltip\" title=\"Você pode escolher alguns produtos adicionais, de acordo com o valor.\"></i></p>";
                $addons_separe = (explode("|", $product->addons)) ? explode("|", $product->addons) : $product->addons;
                foreach ($addons_separe as $index => $addon_ID) {
                  $CI->db->where('id', $addon_ID);
                  $query = $CI->db->get('addons');
                  $addon = $query->row();
                  if($query->num_rows() > 0):
                  $cost = ($addon->price > 0) ? 'R$'.$CI->helps->format_money($addon->price) : 'Grátis';
                  echo'<div class="form-group text-left">
                    <input name="addons[]" value="'.$addon_ID.'" id="addon'.$index.'" type="checkbox">
                    <label for="addon'.$index.'">
                      '.$addon->name.'
                    </label>
                    <span class="pull-right"><b>'.$cost.'</b></span>
                  </div>';
                  endif;
                }
              endif;
              ?>

              <?php 
              if (isset($product->optionals)): 

                echo "<p>Opcionais: <i class='fa fa-info-circle' data-toggle=\"tooltip\" title=\"Você pode/deve escolher algumas opções do produto. Algumas, a seleção será obrigatória.\"></i></p> <div class='optionals'>";
                $optionals_separe = (explode("|", $product->optionals)) ? explode("|", $product->optionals) : $product->optionals;
                foreach ($optionals_separe as $index => $optional_ID) {
                  $CI->db->where('id', $optional_ID);
                  $query = $CI->db->get('optionals');
                  $optional = $query->row();
                  if($query->num_rows() > 0):
                  $cost = ($optional->price > 0) ? 'R$'.$CI->helps->format_money($optional->price) : 'Grátis';
                  echo '<div class="col-xs-6">
                  <p>'.$optional->name.' - <span><b>'.$cost.'</b></span></p>
                  <ul>';   
                    $CI->db->where('optionals_id', $optional->id);
                    $options = $CI->db->get('optionals_options'); 
                    foreach ($options->result() as $opt) {
                       echo '<li class="form-group">
                      <input name="optionals['.$optional->id.'][]" value="'.$opt->id.'" id="optional'.$opt->id.'" type="radio">
                      <label for="optional'.$opt->id.'">'.$opt->name.'</label>
                    </li>';            
                    }             
                  echo '</ul>
                </div>';
                  endif;
                }
                echo '<div class="clearfix"></div></div>';
              endif;
              ?>
            </div>
            <hr class="offset-top-30 veil reveal-sm-block">
            <div class="offset-top-30">
              <div class="responsive-tabs responsive-tabs-shop responsive-tabs-horizontal">
                <ul class="resp-tabs-list">
                  <li><span class="price">R$<?php echo $helps->format_money($product->price) ?></span><span class="icon icon-circle hidden">Big</span></li>
                  <li class="hidden"><span class="price">$13.90</span><span class="icon icon-circle">Mini</span></li>
                </ul>
              </div>
            </div>
            <div class="offset-top-45">
              <div class="group-sm">
                <div class="stepper-type-1">
                  <input type="number" data-zeros="true" name="quantity" value="1" min="1" max="<?= $product->stock - $product->sells ?>" readonly class="form-control">
                </div>
                <?
                if (file_exists("open")):
                    switch ($product->stock - $product->sells) {
                    case 0:
                        echo '<button type="button" class="text-top btn btn-burnt-sienna btn-shape-circle"><span>ESGOTADO!</span></button>';
                        break;

                    case 1:
                        echo '<br><p>Apenas 1 disponível!</p><br><button type="submit" class="text-top btn btn-burnt-sienna btn-shape-circle"><span>'.ADD_TO_CART.'</span></button>';
                        break;
                    
                    default:
                        echo '<button type="submit" class="text-top btn btn-burnt-sienna btn-shape-circle"><span>'.ADD_TO_CART.'</span></button>';
                        break;
                    }
                else:
                    echo '<button type="button" class="text-top btn btn-burnt-sienna btn-shape-circle"><span>FORA DO HORÁRIO!</span></button>';
                endif;
                ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="text-left">
      <div class="responsive-tabs responsive-tabs-horizontal responsive-tabs-horizontal-background">
        <ul class="resp-tabs-list shell">
          <li>Descrição</li>
          <?php if ($CI->config_address()->schendule == 1): ?>
          <li>Entrega agendada</li>
          <?php endif ?>
        </ul>
        <div class="resp-tabs-container">
          <div>
            <p class="text-base"><?php echo $product->description ?></p>

            <?php if (count($ingredients_array) > 0): ?>
              <div class="unit unit-horizontal unit-spacing-xs offset-top-20">
                <div class="unit-left">
                  <p class="h6">Ingredientes:</p>
                </div>
                <div class="unit-body">
                  <?php echo join(" ", $ingredients_array); ?>
                </div>
              </div>
            <?php endif ?>

          </div>
          <div>
            <p class="text-base">
              Selecione o horário:
            </p>

            <div class="col-lg-6">
              <div class="form-group">
                <div class='input-group date' id='datetimepicker2'>
                  <input type='text' name="schendule" value="" class="form-control" />
                  <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                  </span>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </section>
  </form>
</div>