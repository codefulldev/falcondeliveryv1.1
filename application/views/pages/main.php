<?php
  $CI =& get_instance();
  $helps = $CI->helps;
  $address = $CI->config_address();
  $oauth = $this->db->get('oauth')->row();
?>
<script>
  if(screen.width > 760){
    document.write('<section id="menu-section" class="section-50 section-sm-top-30 section-sm-bottom-100"> <h4>Cardápio</h4> <div id="menu-section-item-one" class="responsive-tabs responsive-tabs-button responsive-tabs-horizontal responsive-tabs-carousel offset-top-40">');
  }else{
    document.write('<section id="menu-section"> <h4>Cardápio</h4> <div id="menu-section-item-one" class="responsive-tabs responsive-tabs-button responsive-tabs-horizontal responsive-tabs-carousel">');
  }
</script>
    <ul class="resp-tabs-list">
      <?php foreach ($CI->categories() as $category): 
        $CI->db->where('category_id', $category->id);
        $verify = $CI->db->get('products');
      ?>
       <?php if ($verify->num_rows() > 0): ?>
        <li><?= $category->name ?></li>
       <?php endif ?>
      <?php endforeach ?>
    </ul>
    <div class="resp-tabs-container text-left">
      <?php foreach ($CI->categories() as $category):  
        $CI->db->where('category_id', $category->id);
        $verify = $CI->db->get('products');
      ?>
      <?php if ($verify->num_rows() > 0): ?>
      <div>
        <!-- Slick Carousel-->
        <div data-arrows="true" data-loop="true" data-dots="false" data-swipe="true" data-items="1" data-xs-items="1" data-sm-items="2" data-md-items="3" data-lg-items="3" data-xl-items="5" data-center-mode="true" data-center-padding="10" class="slick-slider slick-tab-centered">

          <?php foreach ($CI->products($category->id) as $product): 
          $thumbnail = $helps->verify_thumbnail($product->thumbnail, 0);
          ?>
          <div class="item">
            <div class="thumbnail-menu-modern">
              <?php if ($product->pizza == 1): ?>
                <figure class="table-pizza">
                  <img src="<?php echo $thumbnail ?>" class="pizza-thumbnail"/>
                  <div class="table-background"></div>
                </figure>
              <?php else: ?>
                <figure>
                  <img src="<?php echo $thumbnail ?>" alt="" width="310" height="260" class="img-responsive"/>
                </figure>
              <?php endif ?>
              <div class="caption">
                <h5><a href="<?php echo PRODUCT_PAGE.'/'.(int)$product->id.'/' ?>" class="link link-default"><?php echo ucwords($product->name) ?></a></h5>
                <?php
                $ingredients_array = array();
                if(isset($product->ingredients)):
                  foreach (explode("|", $product->ingredients) as $ingredient_ID) {
                    $ing = $CI->ingredients($ingredient_ID);
                    $ingredients_array[] = '<span class="label label-danger">'.$ing->name.'</span>';
                  }
                  echo join(" ", $ingredients_array);
                endif;
                ?>
                <p class="price"><?php echo $helps->format_money($product->price) ?></p><a href="<?php echo PRODUCT_PAGE.'/'.(int)$product->id.'/' ?>" class="btn btn-shape-circle btn-burnt-sienna offset-top-15"><?php echo ORDER_ONLINE ?></a>
              </div>
            </div>
          </div>
          <?php endforeach ?>

        </div>
      </div>
     <?php endif ?>
    <?php endforeach ?>
     <p class="text-center">
       <a href="<?= base_url().'menu' ?>" class="btn btn-primary">Ver cardápio</a>
     </p>
    </div>
  </div>
</section>
<!--banner-->
<?php 
$CI->db->where('active', '1');
$CI->db->limit('1');
$promotions = $CI->db->get('promotions');
if ($promotions->num_rows() > 0): 
  $promo = $promotions->row();
  $price_format = number_format($promo->promotional_price, 2, ",", ".");
  $price = explode(",", $price_format);

  $dateExpire = date('d/m/Y', strtotime($promo->date.' +1 days'));

  list($d,$m,$y) = explode("/", $dateExpire);
  list($nd,$nm,$ny) = explode("/", date('d/m/Y'));

  $expi = trim($d).'/'.trim($m).'/'.trim($y);
  $nows = $nd.'/'.$nm.'/'.$ny;

  $inactive = '';
  if ($nows > $expi) {
    $CI->db->where('id', $promo->id);
    $CI->db->delete('promotions');
    $inactive = 'hidden';
  }
?>
<section class="bg-image-5 <?= $inactive ?>">
  <!-- RD Parallax-->
  <div data-on="false" data-md-on="true" class="rd-parallax rd-parallax-light">
    <div data-speed="0" data-type="html" class="rd-parallax-layer">
      <div class="shell section-80 section-sm-top-140 section-sm-bottom-150 text-center">
        <div class="range range-xs-center">
          <div class="cell-sm-10 cell-lg-6">
            <h4 class="text-italic divider-custom-small-primary">Promoção do dia</h4>
            <p class="label label-danger">Expira em: <span class="timerCount" data-date="<?= date('Y-m-d H:i:s', strtotime($promo->date.' +1 days')) ?>"></span></p>
            <h2 class="text-uppercase text-italic offset-top-10 offset-sm-top-0"><?= $promo->title ?></h2>
            <div class="label-price offset-top-10">
              <div class="unit unit-horizontal unit-middle unit-spacing-xs">
                <div class="unit-left">
                  <h1 class="text-primary"><?= $price[0] ?><sup class="h3 text-primary">,<?= $price[1] ?></sup></h1>
                </div>
                <div class="unit-body">
                  <ul class="big text-left">
                    <?php
                      $explode_items = explode('|', $promo->products_id);
                      foreach ($explode_items as $ids) {
                        $CI->db->where('id', $ids);
                        $product_title = $CI->db->get('products')->row()->name; 
                        echo '<li>'. $product_title .'</li><br>';
                      }
                    ?>
                  </ul>
                </div>
              </div>
            </div><a href="<?= base_url()."promotions/".$promo->id ?>" class="btn btn-burnt-sienna btn-md btn-shape-circle offset-top-15">PEDIR AGORA</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php endif; ?>
<!--services-->
<section class="section-50 section-sm-130">
  <div class="shell">
    <div class="range range-xs-center">
      <div class="cell-sm-6 cell-md-3 view-animate fadeInUpBigger delay-04">
        <!-- Box icon-->
        <article class="box-icon box-icon-variant-1">
          <div class="icon-wrap"><span class="icon icon-lg text-base thin-icon-time icon-xl"></span></div>
          <div class="box-icon-header">
            <h5>ENTREGA RÁPIDA</h5>
          </div>
          <hr class="divider-xs bg-primary"/>
          <p>Tudo o que encomendar será rapidamente entregue à sua porta, o tempo será calculado de acordo com sua localização.</p>
        </article>
      </div>
      <div class="cell-sm-6 cell-md-3 offset-top-50 offset-sm-top-0 view-animate fadeInUpBigger delay-08">
        <!-- Box icon-->
        <article class="box-icon box-icon-variant-1">
          <div class="icon-wrap"><span class="icon icon-lg text-base restaurant-icon-17 icon-xxl"></span></div>
          <div class="box-icon-header">
            <h5>COMIDA FRESCA</h5>
          </div>
          <hr class="divider-xs bg-primary"/>
          <p>Utilizamos apenas os melhores ingredientes para cozinhar o saboroso alimento fresco para você.</p>
        </article>
      </div>
      <div class="cell-sm-6 cell-md-3 offset-top-50 offset-md-top-0 view-animate fadeInUpBigger delay-04">
        <!-- Box icon-->
        <article class="box-icon box-icon-variant-1">
          <div class="icon-wrap"><span class="icon icon-lg text-base restaurant-icon-23 icon-xxl"></span></div>
          <div class="box-icon-header">
            <h5>CHEFS EXPERIENTES</h5>
          </div>
          <hr class="divider-xs bg-primary"/>
          <p>Nossa equipe é composta de chefs e cozinheiros com muita experiência e dedicação.</p>
        </article>
      </div>
      <div class="cell-sm-6 cell-md-3 offset-top-50 offset-md-top-0 view-animate fadeInUpBigger delay-06">
        <!-- Box icon-->
        <article class="box-icon box-icon-variant-1">
          <div class="icon-wrap"><span class="icon icon-lg text-base restaurant-icon-22 icon-xxl"></span></div>
          <div class="box-icon-header">
            <h5>UMA VARIEDADE DE PRATOS</h5>
          </div>
          <hr class="divider-xs bg-primary"/>
          <p>No nosso cardápio, você encontrará uma grande variedade de pratos, sobremesas e bebidas.</p>
        </article>
      </div>
    </div>
  </div>
</section>

<?php if ($address->coord_y != NULL AND $address->coord_x != NULL && $oauth->mapskey != '' ): ?>
<section>
  <div id="map"></div>
  <div id="mapsParams" data-zoom="18" data-y="<?= $address->coord_y ?>" data-x="<?= $address->coord_x ?>" data-key="<?= $oauth->mapskey ?>" data-styles="" class="rd-google-map rd-google-map__model">
    <ul class="map_locations">
      <li data-y="<?= $address->coord_y ?>" data-x="<?= $address->coord_x ?>">
        <p><span class="icon"><img src="assets/images/gmap-24x34.png" alt="" width="24" height="34"/></span><?= $address->street.', '.$address->number ?><br><?= $address->locality.', '.$address->city ?></p>
      </li>
    </ul>
  </div>
</section>
<?php endif ?>