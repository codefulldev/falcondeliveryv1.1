<?php
  $CI =& get_instance();
  $user = $CI->user();
  $website = $CI->config_website();
  $address = $CI->config_address();
  $title_header = current( str_word_count( $website->title , 2 ) );
  $networks = $CI->db->get('networks');
  $comingsoon = $CI->db->get('comingsoon')->row();
?>
<!DOCTYPE html>
<html lang="en" class="wide wow-animation smoothscroll">
  <head>
    <!-- Site Title-->
    <title><?= $website->title ?></title>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <link rel="icon" href="http://static.livedemo00.template-help.com/wt_61177/images/favicon.ico" type="image/x-icon">
    <!-- Stylesheets-->
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Changa+One:400,400i%7CGrand+Hotel%7CLato:300,400,400italic,700">
    <link rel="stylesheet" href="assets/css/style.css">
		<!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
		<![endif]-->
  </head>
  <body class="one-screen-page bg-image-2">
    <div class="page">
      <div class="page-loader">
        <div class="page-loader-body">
          <div class="cssload-container">
            <div class="cssload-speeding-wheel"></div>
          </div>
          <p>Carregando...</p>
        </div>
      </div>
    </div>
    <!-- Page-->
    <div class="page text-center">
      <!-- Page Header-->
      <header class="page-head"><a href="<?= base_url() ?>" class="rd-navbar-brand brand">
          <div class="brand-logo">
            <svg x="0px" y="0px" width="auto" height="50px" viewbox="0 0 -1 50">
              <text transform="matrix(1 0 0 1 1.144409e-004 32)" fill="#2C2D2F" font-family="'Grand Hotel'" font-size="45.22"><?= $website->title ?></text>
            </svg>
          </div></a></header>
      <!-- Page Content-->
      <main class="page-content">
        <section>
          <div class="shell">
            <div class="range range-condensed range-xs-center">
              <div class="cell-md-8">
                <h4 class="font-default"><?= $comingsoon->title ?></h4>
                <hr class="hr divider-sm bg-primary-lighter offset-top-25">
                <div class="countdown-wrap offset-top-25">
                  <!-- Countdown-->
                  <div data-type="until" data-date="<?= $CI->helps->date_converter($comingsoon->period) ?> 00:00:00" data-format="wdhms" data-color="#fccb56" data-bg="rgba(255, 255, 255, 1)" data-bg-width="0.8" data-width="0.025" class="DateCountdown"></div>
                </div>
                <div class="offset-top-40 offset-sm-top-60">
                  <p>Nosso site está em construção, estamos trabalhando muito<br class="visible-lg-inline">para lhe dar a melhor experiência em nosso novo site.</p>
                </div>
              </div>
            </div>
          </div>
        </section>

      </main>
      <!-- Page Footer-->
      <footer class="page-foot text-center">
        <p class="copyright">
          <?= $website->title ?>
          &nbsp;&#169;&nbsp;<span id="copyright-year"></span>&nbsp;<br class="veil-sm"><a href="privacy.html">Termos</a>
        </p>
      </footer>
    </div>
    <!-- Global Mailform Output-->
    <div id="form-output-global" class="snackbars"></div>
    <!-- PhotoSwipe Gallery-->
    <div tabindex="-1" role="dialog" aria-hidden="true" class="pswp">
      <div class="pswp__bg"></div>
      <div class="pswp__scroll-wrap">
        <div class="pswp__container">
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
          <div class="pswp__top-bar">
            <div class="pswp__counter"></div>
            <button title="Close (Esc)" class="pswp__button pswp__button--close"></button>
            <button title="Share" class="pswp__button pswp__button--share"></button>
            <button title="Toggle fullscreen" class="pswp__button pswp__button--fs"></button>
            <button title="Zoom in/out" class="pswp__button pswp__button--zoom"></button>
            <div class="pswp__preloader">
              <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                  <div class="pswp__preloader__donut"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip"></div>
          </div>
          <button title="Previous (arrow left)" class="pswp__button pswp__button--arrow--left"></button>
          <button title="Next (arrow right)" class="pswp__button pswp__button--arrow--right"></button>
          <div class="pswp__caption">
            <div class="pswp__caption__cent"></div>
          </div>
        </div>
      </div>
    </div>
    <!-- Java script-->
    <script src="assets/js/core.min.js"></script>
    <script src="assets/js/script.js"></script>
  </body>
</html>