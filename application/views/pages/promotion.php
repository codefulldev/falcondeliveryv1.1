<?php
$CI =& get_instance();
?>
<section class="section-50 section-sm-100">
	<div class="shell">
		<div class="range range-xs-center">
			<h4>Veja o que preparamos para você hoje:</h4>
			<div class="cell-sm-6">
				<div class="menu-classic bg-menu-6">
					<h4 class="title"><a href="javascript: void(0)" class="link-white"><?= $promotion->title ?></a></h4>
					<ul class="list-menu">
						<?php
						$explode_items = explode('|', $promotion->products_id);
						foreach ($explode_items as $ids) {
							$CI->db->where('id', $ids);
							$product = $CI->db->get('products')->row();
							if (!empty($product->id)) {
								$ingredients_array = array();
								if(isset($product->ingredients)):
									foreach (explode("|", $product->ingredients) as $ingredient_ID) {
										$ing = $CI->ingredients($ingredient_ID);
										$ingredients_array[] = $ing->name;
									}
								endif;
								?>
								<li>
									<div class="menu-item h6"><span><span><?= $product->name ?></span></span><strike><span class="price"><?= $CI->helps->format_money($product->price) ?></span></strike></div>
									<div class="menu-item-desc"><span><?= join($ingredients_array, '&nbsp;/&nbsp;') ?></span></div>
								</li>
								<?php
							}
						}
						?>
					</ul>
					<span>Tudo por:</span>
					<h3 class="text-primary text-right">R$<?= $CI->helps->format_money($promotion->promotional_price) ?></h3>
					<form method="post" role="add_to_cart">
						<input type="hidden" name="promotionID" value="<?= $promotion->id ?>">
						<div class="offset-top-45">
							<div class="group-sm">
								<div class="stepper-type-1">
									<label for="quantity">Quantidade</label><br>
									<input id="quantity" type="number" data-zeros="true" name="quantity" value="1" min="1" max="10" readonly class="form-control btn btn-primary" style="background: #fff;">
								</div>
							</div>
						</div>
						<div class="offset-top-25">
							<button type="submit" class="btn btn-md btn-danger">Comprar agora!</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>