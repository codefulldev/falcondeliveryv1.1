<?php
$CI =& get_instance();

if(empty($CI->session->user)){
	$oauth = $this->db->get('oauth')->row();
	if ($oauth) {
		$this->load->view('tpl/login.tpl', 
			array(
				'appid' => $oauth->appid,
				'version' => $oauth->version,
				'clientid' => $oauth->clientid
			)
		);
	}else{
		$this->load->view('tpl/login.tpl', 
			array(
				'appid' => $oauth->appid,
				'version' => $oauth->version,
				'clientid' => $oauth->clientid
			)
		);
	}

}else{ 

?>

<section class="text-center text-sm-left section-50">
	<div class="shell">

		<div class="range range-sm-center">

			<div class="cell-md-3 offset-top-60 offset-md-top-0">
				<div class="">
					<!-- Section Blog Modern-->
					<aside class="text-left">

						<div class="range ">
							<div class="cell-xs-6 cell-md-12">

								<div class="h6 text-uppercase">Minha conta</div>
								<ul class="list list-marked list-marked-burnt-sienna list-bordered offset-top-10">
									<li><a href="orders" class="link-default">Meus pedidos </a></li>
									<li><a href="address" class="link-default">Meus endereços</a></li>
									<li><a href="settings" class="link-default">Meus dados</a></li>
								</ul>
							</div>
						</div>

					</aside>
				</div>
			</div>

			<div class="cell-sm-10 cell-md-8 cell-lg-7">
				<div class="inset-left-0">

					<section>
						<?php $this->load->view('tpl/'.$tpl); ?>
					</section>
				</div>
			</div>

		</div>


	</div>
</section>
<?php } ?>