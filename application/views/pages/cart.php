<?php
 $CI =& get_instance();
 $DELIVERY_COST = ($CI->db->get('config_address')->row()->delivery_price) ? number_format($CI->db->get('config_address')->row()->delivery_price / 100, 2, '.', '.') : 0;
?>
<section class="section-sm-100">
  <div class="shell">
    <div class="range">
      <div id="shopcart" class="cell-xs-12">
        <?php if (count($CI->session->shopcart) > 0): ?>
        <h4 class="text-left font-default">
          <?php switch (count($CI->session->shopcart)) {
            case '1':
              echo '<span class="countcart">1 item adicionado</span>';
              break;
            
            default:
              echo '<span class="countcart">'. count($CI->session->shopcart) .' itens adicionados</span>';
              break;
          } ?>
          
        </h4>
        <div class="offset-top-10">
          <table class="table table-shopping-cart">
            <tbody>
              <?php 
              $total = 0;
              foreach ($cart as $i => $items):
                
              list($id, $index) = explode(":", $i);

              if($items['promotion'] == false){
              $this->db->where("id", $id);
              $product = $this->db->get('products')->row();

              $flavors = array();
              if(isset($items['params']['flavors'])) {
                $explore_falvors = explode("|", $items['params']['flavors']);
                $price_flavor = array();
                $price_flavor[] = $product->price / 2;
                foreach ($explore_falvors as $findex => $flv) {
                  $this->db->where('id', $flv);
                  $get_flavor = $this->db->get('products');
                  if ($get_flavor->num_rows() > 0) {
                     $flavor = $get_flavor->row();
                     $price_flavor[] = $flavor->price / 2;
                     if ($flavor->id !== $id) {
                        $flavors_index = $findex + 1;
                        $flavors[$flavor->id][] = $flavor->name;
                     }
                  }
                }
                $subtotal = array_sum($price_flavor) * $items['quantity'];
              }else{
                $subtotal = $product->price * $items['quantity'];
              }

              $this->db->where('id', $product->category_id);
              $category_get = $this->db->get('category');
              $category = ($category_get->num_rows() > 0) ? $category_get->row()->name : 'Sem categoria';

              $take = array();
              if(isset($items['params']['ingredients'])):
                if($product->pizza > 0):
                  foreach ($items['params']['ingredients'] as $p_id => $ingredients) {
                    $ings_array = array();
                    foreach ($ingredients[0] as $ing) {
                      $this->db->where('id', $ing);
                      $ingredient = $this->db->get('ingredients')->row();
                      $take[$p_id][] = '<span class="label label-danger">' . $ingredient->name . '</span>';
                    }
                  }
                else:
                  foreach ($items['params']['ingredients'][0] as $ing) {
                      $ings_array = array();
                      $this->db->where('id', $ing);
                      $ingredient = $this->db->get('ingredients')->row();
                      $take[$product->id][] = '<span class="label label-danger">'.$ingredient->name.'</span>';
                  }
                endif;
              endif;
              
              $addons = array();
              if(isset($items['params']['addons'])):
                foreach ($items['params']['addons'] as $addon_id) {
                  $this->db->where('id', $addon_id);
                  $addon = $this->db->get('addons')->row();
                  if($addon->price > 0){
                    $subtotal += $addon->price * $items['quantity'];
                  }
                  $cost = ($addon->price > 0) ? 'R$'.$CI->helps->format_money($addon->price) : 'Grátis';
                  $addons[] = '<span class="label label-info">'.$addon->name.' - '.$cost.'</span>';
                }
              endif;

              $optionals = array();
              if(isset($items['params']['optionals'])):
                $opts = explode("|", $items['params']['optionals']);
                foreach ($opts as $opt) {
                  list($option_id, $odetail_id) = explode(":", $opt);
                  $this->db->where('id', $option_id);
                  $get_optional = $this->db->get('optionals');
                  if ($get_optional->num_rows() > 0) {
                    $optional = $get_optional->row();
                    $this->db->where('id', $odetail_id);
                    $optional_detail = $this->db->get('optionals_options')->row();
                    if($optional->price > 0){
                      $subtotal += $optional->price * $items['quantity'];
                    }
                    $cost = ($optional->price > 0) ? 'R$'.$CI->helps->format_money($optional->price) : 'Grátis';
                    $optionals[] = '<span class="label label-info">'.$optional->name.': '.$optional_detail->name.' - '.$cost.'</span>';
                  }
                }
              endif;

              $total += $subtotal;
              ?>
              <div class="row item_long_press" id="item<?=$index?>" data-tr="<?=$index?>" data-id="<?=$i?>">
                <div class="col-xs-1 hidden-xs item_long_press">
                  <div class="form-group stepper-type-2">
                    <input type="number" data-id="<?php echo $i ?>" data-zeros="true" data-flavors="<?= isset($items['params']['flavors']) ? $items['params']['flavors'] : '' ?>" value="<?php echo $items['quantity'] ?>" min="1" max="20" readonly class="form-control text-bold change_quantity">
                  </div>
                </div>
                <div class="col-xs-3 col-sm-1 item_long_press">
                  <div>
                    <div class="product-image"><img src="themes/uploads/thumbnail/<?php echo $product->thumbnail ?>" width="70" height="70" alt=""></div>
                  </div>
                </div>
                <div class="col-xs-9 col-sm-8 item_long_press" style="position: relative;">
                  <div class="text-left">
                    <span class="product-brand text-italic label-success label"><?php echo $category ?></span>
                      <?php
                      if (count($flavors) > 0) {
                        echo '<div class="text-sbold offset-top-0">';
                        echo '<strong>Meia:</strong> <a href="'.base_url().'product/'.$id.'" class="link-default">'.$product->name.'</a>';
                        if (count($take) > 0) {
                          foreach ($take as $tidd => $value) {
                            if ($id == $tidd) {
                              echo '<br>Retirar: '.join(' ', $value); 
                            }
                          }
                        }
                        foreach ($flavors as $fidd => $value) {
                          echo '<br>';
                          echo '<strong>Meia:</strong> <a href="'.base_url().'product/'.$id.'" class="link-default">'.join(' ', $value).'</a>';
                          if (count($take) > 0) {
                            foreach ($take as $tidd => $value) {
                              if ($fidd == $tidd) {
                                echo '<br>Retirar: '.join(' ', $value); 
                                echo '<br>';
                              }
                            }
                          }
                        }
                        echo '</div>';
                      }else{
                        echo '<div class="h5 text-sbold offset-top-0">';
                        echo $product->name;
                        echo '</div>';
                      }
                      ?>
                    <?php if (count($take) > 0 && !empty($take[$product->id]) && count($flavors) == 0): ?>
                      <p style="margin:0;padding:0">Tirar ingredientes: <?php echo join(" ", $take[$product->id]) ?></p>
                    <?php endif ?>

                    <?php if (count($optionals) > 0): ?>
                      <p style="margin:0;padding:0">Opcionais: <?php echo join(" ", $optionals) ?></p>
                    <?php endif ?>

                    <?php if (count($addons) > 0): ?>
                      <p style="margin:0;padding:0">Adicionais: <br> <?php echo join(" ", $addons) ?></p>
                    <?php endif ?>
                  </div>
                  <div class="visible-xs item_long_press" style="position: absolute;top:0;right:0;">
                    <div class="col-sm-1" style="margin:0 3px 0 0;padding:0;">
                      <span class="h5 text-sbold subtotal_shopcart_<?php echo $index ?>">R$<?php echo $CI->helps->format_money($subtotal); ?></span>
                    </div>
                  </div>
                  
                </div>

                <div class="col-sm-2 hidden-xs item_long_press">
                  <span class="h5 text-sbold subtotal_shopcart_<?php echo $index ?>">R$<?php echo $CI->helps->format_money($subtotal); ?></span>
                </div>

                <div class="col-sm-2 hidden-xs item_long_press">
                  <div class="inset-left-20"><a href="#" data-tr="<?=$index?>" data-id="<?=$i?>" class="icon icon-sm mdi mdi-window-close link-gray-lightest remove_item_cart"></a></div>
                </div>

              </div>
              <?php 
              }else{
                $CI->db->where('id', $id);
                $promotions = $CI->db->get('promotions');
                if ($promotions->num_rows() > 0) {
                  $promotion = $promotions->row();
                  $subtotal = $promotion->promotional_price * $items['quantity'];
              ?>
              <div class="row item_long_press" id="item<?=$index?>" data-tr="<?=$index?>" data-id="<?=$i?>">
                <div class="col-xs-1 hidden-xs item_long_press">
                  <div class="form-group stepper-type-2">
                    <input type="number" data-id="<?php echo $i ?>" data-zeros="true" value="<?php echo $items['quantity'] ?>" min="1" max="20" readonly class="form-control text-bold change_quantity">
                  </div>
                </div>
                <div class="col-xs-3 col-sm-1 item_long_press">
                  <div>
                    <div class="product-image"><img src="assets/images/promo.png" width="70" height="70" alt=""></div>
                  </div>
                </div>
                <div class="col-xs-9 col-sm-8 item_long_press" style="position: relative;">
                  <div class="text-left">
                    <span class="product-brand text-italic label-success label">Promoção do dia</span>
                    <div class="h5 text-sbold offset-top-0">
                      <a href="<?php echo base_url().'promotions/'.$id; ?>" class="link-default"><?php echo $promotion->title ?> <small>R$<?php echo $CI->helps->format_money($promotion->promotional_price) ?></small></a>
                    </div>
                </div>
                <div class="visible-xs item_long_press" style="position: absolute;top:0;right:0;">
                  <div class="col-sm-1" style="margin:0 3px 0 0;padding:0;">
                    <span class="h5 text-sbold subtotal_shopcart_<?php echo $index ?>">R$<?php echo $CI->helps->format_money($subtotal); ?></span>
                  </div>
                </div>

              </div>

              <div class="col-sm-2 hidden-xs item_long_press">
                <span class="h5 text-sbold subtotal_shopcart_<?php echo $index ?>">R$<?php echo $CI->helps->format_money($subtotal); ?></span>
              </div>

              <div class="col-sm-2 hidden-xs item_long_press">
                <div class="inset-left-20"><a href="#" data-tr="<?=$index?>" data-id="<?=$i?>" class="icon icon-sm mdi mdi-window-close link-gray-lightest remove_item_cart"></a></div>
              </div>

            </div>
              <?php
                  $total += $subtotal;
                }
              }
              endforeach;
              ?>

            </tbody>
          </table>
        </div>
        <div class="offset-top-35 text-right">
          <div class="h4 font-default text-bold">
            <small class="inset-right-5 text-gray-light">Total: </small>
            <span class="total_shopcart"><?php echo 'R$'.$CI->helps->format_money($total) ?></span>
          </div>
          <a href="javascript: void(0);" data-url="<?php echo base_url().'checkout' ?>" class="btn btn-icon btn-icon-left goto_checkout btn-burnt-sienna btn-shape-circle offset-top-35">
            <span class="icon icon-xs mdi mdi-cart-outline"></span>
            <span>Concluir</span>
          </a>
        </div>
        <?php else: ?>
           <h3>O carrinho está vázio!</h3>
           <a href="<?= base_url(); ?>" class="btn btn-icon btn-icon-left btn-burnt-sienna btn-shape-circle offset-top-35"><span class="icon icon-xs mdi mdi-cart-outline"></span><span>Adicionar produto</span></a>
        <?php endif ?>

      </div>
    </div>
  </div>
</section>