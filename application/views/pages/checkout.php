<?php
$CI =& get_instance();
if(empty($CI->session->user)){
  echo '<div id="ischeckout"></div>';
  $oauth = $this->db->get('oauth')->row();
  $this->load->view('tpl/login.tpl', 
    array(
      'appid' => $oauth->appid,
      'version' => $oauth->version,
      'clientid' => $oauth->clientid
    )
  );
}else{
  if (empty($CI->session->shopcart)) {
    redirect('cart');
  }
  $user = $CI->user();
  $config_address = $CI->config_address();
  $city = explode(" - ", $config_address->city);

  $address = explode("|", $user->address);
  $CI->db->where('clients_id', $user->id);
  $get_addr = $CI->db->get('clients_address');

?>
<section class="bg-white text-center section-sm-100" ng-controller="CheckoutController">
  <div class="shell">
    <div class="range range-xs-center">
    <?php if(file_exists("open")): ?>
      <div class="col-lg-12">
        <h3 class="title_page">{{checkoutPageTitle}}</h3>
        <?php if($get_addr->num_rows() > 0): ?>
       <form id="checkout" method="post">
         <button type="submit" data-loading-text="Finalizando..." class="btn btn-burnt-sienna btn-loader btn-xs text-bottom pull-right hidden-sm hidden-xs">Finalizar pedido</button>
         <div class="clearfix"></div>
         <h5 class="text-left">Total do pedido: <strong>R$<span class="totalverify" data-pricetotal="<?= $CI->total_verify(); ?>"><?= $CI->total_verify(); ?></span></strong></h5>
        <div class="col-lg-6 text-left">
          <h5>Formas de pagamento</h5>
          <div class="form-group">
            <input id="delivery" type="radio" name="payment" value="money" checked="" required="">
            <label for="delivery">
              Dinheiro
            </label>
            &nbsp;
            <input id="creditcard" type="radio" name="payment" value="creditcard" required="">
            <label for="creditcard">
              Cartão de crédito
            </label>
          </div>
          
          <div id="tab-delivery" class="tabs-payment">

            <div class="form-group">
              <label>Valor</label>
              <input type="text" class="form-control" name="receiver" name="Quantia" required>
              <button type="button" class="btn btn-burnt-sienna btn-xs text-bottom calculate" style="margin:8px 0 0 0;">Calcular</button> 
            </div>

            <div class="form-group">
              <label>Troco</label>
              <input type="text" class="form-control" name="send" name="Troco" readonly="" required>
            </div>

          </div>

          <div id="tab-creditcard" class="tabs-payment collapse">
            
            <div class="form-group">
              <input id="pagseguro" type="radio" name="creditcardType" value="pagseguro" checked="" required="">
              <label for="pagseguro">
                PagSeguro
              </label>
              &nbsp;
              <input id="shipping" type="radio" name="creditcardType" value="shipping" required="">
              <label for="shipping">
                Maquineta
              </label>
            </div>

            <div id="tab-pagseguro" class="creditcard_type">
              <p>As transações realizadas atráves de nossa plataforma, são completamente seguras. Todo processo de pagamento realizado via cartão de crédito/débito, é processado pelo <a href="https://pagseguro.uol.com.br">PagSeguro</a> Checkout. <br>Os dados fornecidos não ficarão armazenados em nenhuma hípotese.</p>

              <div class="form-group">
                <div class="creditcard" style="margin-bottom:20px;"></div>

                <div class="form-group">
                  <input placeholder="Número do cartão" type="text" name="number" class="form-control">
                </div>

                <div class="form-group">
                  <input placeholder="Nome titular" type="text" name="name" class="form-control">
                </div class="form-group">

                <div class="form-group">
                  <input placeholder="Válidade do cartão" type="text" name="expiry" class="form-control">
                  <small>Formato de data: 02/2025</small>
                </div>

                <div class="form-group">
                  <input placeholder="Código de Segurança" type="text" name="cvc" class="form-control">
                </div>

                <div id="installments" class="form-group" style="display: none;">
                  <select name="installments" class="form-control">
                    <option value="" selected disabled>Parcelas</option>
                  </select>
                </div>

                <div class="form-group">
                  <input placeholder="Data de Nascimento" type="text" data-date-format="dd/mm/YYYY" onfocus="(this.type='date')" onchange="this.blur();" name="birthday" class="form-control">
                </div>

                <div class="form-group">
                  <input placeholder="CPF do titular" type="text" name="cpf" class="form-control">
                </div>

                <div class="form-group">
                  <input type="button" id="load" data-loading-text="Verificando..." name="verify" class="verify-card btn btn-success" value="Verificar cartão">
                </div>

                <div class="form-group">
                  <input type="hidden" name="brand" class="creditcard_brand">
                  <input type="hidden" name="hash" class="creditcard_hash">
                  <input type="hidden" name="token" class="creditcard_token">
                  <input type="hidden" name="session" class="creditcard_session">
                  <input type="hidden" name="total" class="creditcard_total">
                </div>

              </div>
            </div>

            <div id="tab-shipping" class="creditcard_type collapse">
              <div class="form-group">
                <label>Bandeira do cartão</label>
                <select name="payment_shippingBrand" id="select_brands" class="form-control">
                  <option value="Visa" selected>Visa</option>
                  <option value="Mastercard">Mastercard</option>
                  <option value="American Express">American Express</option>
                  <option value="Elo">Elo</option>
                  <option value="Discover Network">Discover Network</option>
                </select>
              </div>
            </div>

          </div>

        </div>

        <div class="col-lg-6 text-left">
          <h5>Entrega</h5>
          
          <?php
          if ($get_addr->num_rows() > 0): 
          ?>
          <?php if ($get_addr->num_rows() > 0): ?>
            <div id="address_types" class="form-group">
              <span><strong>Endereço</strong></span><br>
              <?php foreach ($get_addr->result() as $index => $row_addr): ?>
                <?php if ($index == 0): ?>
                  <input id="this" type="radio" name="chooseAddress" value="<?= $row_addr->id ?>" checked="" required="">
                  <label for="this">
                    <?= $row_addr->name ?> &nbsp;&nbsp;|&nbsp;&nbsp;<span id="taxa_<?= $row_addr->id ?>"></span>
                  </label>
                <?php else: ?>
                  <input id="this" type="radio" name="chooseAddress" value="<?= $row_addr->id ?>">
                  <label for="this">
                    <?= $row_addr->name ?> &nbsp;&nbsp;|&nbsp;&nbsp;<span id="taxa_<?= $row_addr->id ?>"></span>
                  </label>
                <?php endif ?>
              <?php endforeach ?>
              <p>
                <a href="<?= base_url() ?>address">Editar endereço</a>
              </p>
            </div>
          <?php else: ?>
           <div id="address_types">
              <span class="text-danger">Você precisa de um endereço</span>
              <a href="<?= base_url().'address' ?>">Adicionar endereço</a>
           </div>
          <?php 
            endif;
            endif;
          ?>

          <br>
          <div class="form-group">
            <p><strong>Entrega</strong></p>
            <input id="this" type="radio" name="deliveryaddress" value="true" checked="" required="">
            <label for="this">
              Entrega
            </label>
            &nbsp;
            <input id="another" type="radio" name="deliveryaddress" value="false" required="">
            <label for="another">
              Retirada
            </label>
          </div>
          
          <div class="form-group">
            <p><strong>Cupom de desconto</strong></p>
            <input id="cupom" name="descount" type="text" class="form-control" placeholder="Cupom de desconto">
          </div>

        </div>  
        
        <div class="col-lg-12 text-center visible-sm visible-xs" style="margin-top: 30px;">
          <center>
            <button type="submit" data-loading-text="Finalizando..." class="btn btn-burnt-sienna btn-xs btn-loader">Finalizar pedido</button>
          <center>
          <div class="clearfix"></div>
        </div>

       </form>
        <?php else: ?>
            <div class="text-center">
                <p class="text-center">Você precisa adicionar um endereço.</p>
                <a href="<?= base_url() . 'address' ?>">Adicionar endereço</a>
            </div>
        <?php endif; ?>

       <a href="#lightbox--create_promotion" class="popup-with-form hidden"></a>
       <div id="lightbox--process_order" class="white-popup-block mfp-hide text-center">
         <p>Estamos processando o seu pedido...</p>
         <div id="processOrder"></div>
       </div>
      </div>
    <?php else: ?>
        <div class="col-lg-12">
            <h3>Estamos fora do horário de atendimento</h3>
        </div>
    <?php endif ?>

    </div>
  </div>
  <div id="continue-promotions" class="shell isotope-wrap">
    <div class="range range-sm-center">
      <div class="cell-xs-12 offset-top-40">
        <!-- Isotope Content-->
        <div id="isotope-container" data-isotope-layout="fitRows" data-isotope-group="gallery" data-photo-swipe-gallery="gallery" class="row isotope isotope--loaded" style="position: relative; height: 928.164px;">
        </div>
      </div>
    </div>
  </div>
</section>
<?php } ?>