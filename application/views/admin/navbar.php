<?php
    $CI =& get_instance();
    $admin = $CI->UserController();
    $config = $CI->config_general();
    $first_name = $CI->helps->first_name($admin->name);
    $thumbnail = $CI->helps->verify_avatar($admin->thumbnail, 'admin');
    $base = base_url().$CI->base;
?>
<!-- Top Navigation -->
<nav class="navbar navbar-default navbar-static-top m-b-0">
    <div class="navbar-header">
        <!-- Toggle icon for mobile view -->
        <div class="top-left-part">
            <!-- Logo -->
             <a class="logo" href="<?= $base ?>">
                <!-- Logo icon image, you can use font-icon also -->
                <b>
                    <!--This is dark logo icon-->
                    <img src="assets/falcon-logo.png" height="40" alt="home" class="Falcon Application" />
                    <!--This is light logo icon-->
                </b>
             </a>
            </div>
            <!-- /Logo -->
            <!-- Search input and Toggle icon -->
            <ul class="nav navbar-top-links navbar-left">
                <li><a href="javascript:void(0)" class="open-close waves-effect waves-light visible-xs"><i class="ti-close ti-menu"></i></a></li>
                
                <li class="dropdown">
                    <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"> 
                        <i class="mdi mdi-bell"></i>
                        <div class="notify hidden"><span class="heartbit"></span><span class="point"></span></div>
                    </a>
                    <ul class="dropdown-menu mailbox animated bounceInDown">
                        <li>
                            <div class="drop-title"><span class="count--orders">0</span> novos pedidos</div>
                        </li>
                        <li id="list--news_orders"></li>
                        <li>
                           <a class="text-center" href="javascript:void(0);"> <strong>Todos os pedidos</strong> <i class="fa fa-angle-right"></i> </a>
                        </li>
                    </ul>
                </li>

                <li>
                    <?php 
                        if(isset($_GET['open'])){
                            file_put_contents('open', 'open');
                        } else if(isset($_GET['close'])){
                            unlink("open");
                        }
                    ?>
                    <?php if(!file_exists("open")): ?>
                        <a href="<?= $base ?>?open">
                            Abrir loja
                        </a>
                    <?php else: ?>
                        <a href="<?= $base ?>?close">
                            Fechar loja
                        </a>
                    <?php endif ?>

                </li>

                </ul>
                <div class="hidden-xs">
                    <?php
                        $current_uri = "//$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                        if($_SERVER['REQUEST_METHOD'] == 'POST'){
                            $deliverytime = $_POST['deliveryestimated'];
                            $CI->db->set('deliverytime', $deliverytime);
                            $CI->db->where('id >', 0);
                            $setup = $CI->db->update('config');
                            if($setup){
                                redirect($current_uri);
                            }
                        }
                    ?>
                    <form action="<?php echo $current_uri ?>" method="post">
                        <div class="col-xs-1" style="line-height: 61px">
                            <label>Tempo de entrega</label>
                        </div>
                        <div class="col-xs-1" style="padding-top: 13px">
                            <input type="number" min="1" max="60" value="<?php echo $config->deliverytime ?>" maxlength="3" name="deliveryestimated" class="form-control" />
                        </div>
                        <div class="col-xs-1" style="padding-top: 22px">
                            <button type="submit" class="btn btn-xs btn-success">Ok</button>
                        </div>
                    </form>
                </div>
                <!-- This is the message dropdown -->
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <!-- /.Task dropdown -->
                    <!-- /.dropdown -->
                    <li>
                        <li class="dropdown">
                            <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="<?= $thumbnail ?>" alt="user-img" width="36" class="img-circle"><b class="hidden-xs"><?= $first_name ?></b> <span class="caret"></span> </a>
                            <ul class="dropdown-menu dropdown-user animated flipInY">
                                <li>
                                    <div class="dw-user-box">
                                        <div class="u-img"><img src="<?= $thumbnail ?>" alt="user" /></div>
                                        <div class="u-text"><h4><?= ucwords($admin->name) ?></h4><p class="text-muted"><?= strtolower($admin->email) ?></p></div>
                                    </div>
                                </li>
                                <li role="separator" class="divider"></li>
                                <li><a href="<?php echo $base.$CI->account ?>"><i class="ti-user"></i> Minha conta</a></li>
                                <li class="hidden"><a href="<?php echo $base.$CI->finances ?>"><i class="ti-wallet"></i> Minhas finanças</a></li>
                                <li class="hidden"><a href="<?php echo $base.$CI->inbox ?>"><i class="ti-email"></i> E-mails</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="<?php echo $base.$CI->configURI ?>"><i class="ti-settings"></i> Configurações</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="<?php echo $base.'logout' ?>"><i class="fa fa-power-off"></i> Sair</a></li>
                            </ul>
                            <!-- /.dropdown-user -->
                        </li>

                        <!-- /.dropdown -->
                    </ul>
                </div>
                <!-- /.navbar-header -->
                <!-- /.navbar-top-links -->
                <!-- /.navbar-static-side -->
            </nav>