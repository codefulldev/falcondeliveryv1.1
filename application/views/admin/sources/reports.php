<?php $CI =& get_instance(); if ($reports != ''): ?>
<?php
$total = 0;
$quantity = 0;
foreach ($reports as $report):
	if (date('Y-d-m', strtotime($report->date)) == date('Y-d-m')) {
		$subtotal = array();
		$CI->db->where('order_id', $report->id);
		$orders_details = $CI->db->get('orders_details');
		if ($orders_details->num_rows() > 0) {
			foreach ($orders_details->result() as $row) {
				$subtotal[] = $row->subtotal;
            }
            $quantity++;
		}
		$total += array_sum($subtotal);
	}
endforeach;

$verify = 0;
$this->db->where('deliveryman_id', $id);
$get = $this->db->get('deliveryman_historic');
if ($get->num_rows() > 0) {
	foreach ($get->result() as $row) {
		if (date('Y-m-d', strtotime($row->period)) == date('Y-m-d')) {
			$verify += 1;
		}
	}
}
?>
<?php if ($verify > 0): ?>
	<h3 class="text-center">O dia já foi quitado!</h3>
<?php else: ?>
	<form method="post" role="close_day">
		<div class="form-group">
			Hoje, <?php echo date('d/m/Y'); ?>
		</div>
		<div class="form-group">
			<label for="inputQuantity">Quantidade de entregas</label>
			<input name="id" type="hidden" value="<?= $id ?>">
			<input id="inputQuantity" name="quantity" type="text" value="<?= $quantity ?>" class="form-control" readonly="true">
		</div>
		<div class="form-group">
			<label for="inputEntry">Total de entregas</label>
			<input id="inputEntry" name="total" type="text" value="R$<?= $CI->helps->format_money($total) ?>" class="form-control" readonly="true">
		</div>
		<div class="form-group">
			<label for="inputCoast">Taxa por entrega</label>
			<input id="inputCoast" name="coast" type="text" class="form-control">
		</div>
		<div id="totalComission" class="hidden">
			<div class="form-group">
				<label for="inputReceiver">A receber</label>
				<input id="inputReceiver" name="receiver" type="text" class="form-control" readonly="true">
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-xs btn-success">Concluir</button>
			</div>
		</div>
	</form>
<?php endif ?>

<?php else: ?>
	<h2>Nenhuma entrega hoje!</h2>
<?php endif ?>