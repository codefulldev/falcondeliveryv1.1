<!DOCTYPE html>  
<html lang="en">

<head>
  <base href="<?php echo base_url(); ?>">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" type="image/png" sizes="16x16" href="assets/favicon.png">
  <title>Login</title>
  <!-- Bootstrap Core CSS -->
  <link href="themes/ampleadmin/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
  <!-- animation CSS -->
  <link href="themes/ampleadmin/css/animate.css" rel="stylesheet">
  <!-- Custom CSS -->
  <link href="themes/ampleadmin/css/style.css" rel="stylesheet">
  <!-- color CSS -->
  <link href="themes/ampleadmin/css/colors/blue.css" id="theme"  rel="stylesheet">
  <link href="themes/plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body>
  <!-- Preloader -->
  <div class="preloader">
    <div class="cssload-speeding-wheel"></div>
  </div>
  <section id="wrapper" class="login-register">
    <div class="login-box login-sidebar">
      <div class="white-box">
        <form method="post" class="form-horizontal form-material" id="loginform">
          <a href="javascript:void(0)" class="text-center db">
            <img src="assets/falcon-logo-vertical.png" height="100" alt="Falcon" />
          </a>  

          <div class="form-group m-t-40">
            <div class="col-xs-12">
              <input class="form-control" type="text" name="email" required="" placeholder="E-mail">
            </div>
          </div>
          <div class="form-group">
            <div class="col-xs-12">
              <input class="form-control" type="password" name="password" required="" placeholder="Senha">
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-12">
              <div class="checkbox checkbox-primary pull-left p-t-0">
                <input id="checkbox-signup" type="checkbox">
                <label for="checkbox-signup"> Lembrar </label>
              </div>
              <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> Recuperar senha!</a> </div>
            </div>
            <div class="form-group text-center m-t-20">
              <div class="col-xs-12">
                <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Entrar</button>
              </div>
            </div>
          </form>
          <form class="form-horizontal" id="recoverform">
            <div class="pull-right"><a href="javascript: void(0)" id="loginformReturn"><i class="fa fa-times"></i></a></div>
            <div class="form-group ">
              <div class="col-xs-12">
                <h3>Recuperar senha</h3>
                <p class="text-muted">Digite seu email, enviaremos uma mensagem de confirmação. </p>
              </div>
            </div>
            <div id="recoveryDetails" class="form-group text-center"></div>
            <div class="form-group">
              <div class="col-xs-12">
                <input class="form-control" type="text" name="email" required="" placeholder="E-mail">
              </div>
            </div>
            <div class="form-group text-center m-t-20">
              <div class="col-xs-12">
                <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Recuperar</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </section>
    <!-- jQuery -->
    <script src="themes/plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="themes/ampleadmin/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="themes/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>

    <!--slimscroll JavaScript -->
    <script src="themes/ampleadmin/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="themes/ampleadmin/js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="themes/ampleadmin/js/custom.min.js"></script>
    <!--Style Switcher -->
    <script src="themes/plugins/bower_components/sweetalert/sweetalert.min.js"></script>
    <script src="themes/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    <script src="themes/ampleadmin/js/login.js"></script>
  </body>
  </html>
