<?php 

    $CI =& get_instance(); 

    $base = base_url().$CI->base;

?>

<!-- Left navbar-header -->

<div class="navbar-default sidebar" role="navigation">

    <div class="sidebar-nav slimscrollsidebar">

        <div class="sidebar-head">

            <h3><span class="fa-fw open-close"><i class="ti-menu hidden-xs"></i><i class="ti-close visible-xs"></i></span> 

                <span class="hide-menu">Menu</span></h3> 

        </div>

        <ul class="nav" id="side-menu">

                <li>

                    <a href="javascript:void(0)" onclick="window.location.href='<?php echo $base.$CI->salesURI ?>'" class="waves-effect active">

                        <i data-icon="Z" class="icon-screen-desktop fa-fw"></i>

                        <span class="hide-menu">Ponto de vendas</span>

                    </a> 

                </li>

                <li>

                    <a href="javascript:void(0)" onclick="window.location.href='<?= $base ?>'" class="waves-effect">

                        <i data-icon="Z" class="linea-icon linea-basic fa-fw"></i>

                        <span class="hide-menu">Página Inicial</span>

                    </a> 

                </li>

                <li> 

                    <a href="javascript:void(0)" class="waves-effect">

                        <i class="ti-bag fa-fw"></i>

                        <span class="hide-menu">E-commerce <span class="fa arrow"></span></span>

                    </a>

                    <ul class="nav nav-second-level">

                        <li>

                            <a href="javascript:void(0)" onclick="window.location.href='<?php echo $base.$CI->productURI ?>'">

                                <i class="fa fa-shopping-cart fa-fw"></i>

                                <span class="hide-menu"> Produtos</span>

                            </a>

                        </li>

                        

                        <li>

                            <a href="javascript:void(0)" onclick="window.location.href='<?php echo $base.$CI->categoryURI ?>'">

                                <i class="ti-tag fa-fw"></i>

                                <span class="hide-menu"> Categorias</span>

                            </a>

                        </li>

                        

                        <li> <a href="javascript:void(0)" class="waves-effect"><i data-icon="&#xe008;" class="ti-shopping-cart-full fa-fw"></i><span class="hide-menu">Opções do produto </span><span class="fa arrow"></span></a>

                            <ul class="nav nav-third-level">

                                <li> <a href="javascript:void(0)" onclick="window.location.href='<?php echo $base.$CI->addonsURI ?>'"> <i class="ti-plus fa-fw"></i> <span class="hide-menu"> Adicionais</span> </a> </li>



                                <li> <a href="javascript:void(0)" onclick="window.location.href='<?php echo $base.$CI->optionURI ?>'"> <i class="ti-layers-alt fa-fw"></i> <span class="hide-menu"> Opcionais</span> </a> </li>



                                <li> <a href="javascript:void(0)" onclick="window.location.href='<?php echo $base.$CI->ingredURI ?>'"> <i class="ti-receipt fa-fw"></i> <span class="hide-menu"> Ingredientes</span> </a> </li>

                            </ul>

                        </li>



                        <li>

                            <a href="javascript:void(0)" onclick="window.location.href='<?php echo $base.$CI->ordersURI ?>'">

                                <i class="ti-list fa-fw"></i>

                                <span class="hide-menu"> Pedidos</span>

                            </a>

                        </li>



                        <li>

                            <a href="javascript:void(0)" onclick="window.location.href='<?php echo $base.$CI->clientsURI ?>'">

                                <i class="ti-user fa-fw"></i>

                                <span class="hide-menu"> Clientes</span>

                            </a>

                        </li>



                        <li>

                            <a href="javascript:void(0)" onclick="window.location.href='<?php echo $base.$CI->shippingURI ?>'">

                                <i class="ti-truck fa-fw"></i>

                                <span class="hide-menu"> Entregadores</span>

                            </a>

                        </li>



                        <li>

                            <a href="javascript:void(0)" onclick="window.location.href='<?php echo $base.$CI->configURI ?>'">

                                <i class="ti-settings fa-fw"></i>

                                <span class="hide-menu"> Configurações</span>

                            </a>

                        </li>

                    </ul>

                </li>

                <li> 

                    <a href="javascript:void(0)" class="waves-effect">

                        <i data-icon="&#xe013;" class="linea-icon linea-basic fa-fw"></i>

                        <span class="hide-menu">

                            Promoções<span class="fa arrow"></span>

                        </span>

                    </a>

                    <ul class="nav nav-second-level">

                        <li> 

                            <a href="javascript:void(0)" onclick="window.location.href='<?php echo $base.$CI->promoCupom ?>'">

                                <i data-icon="/" class="linea-icon linea-basic fa-fw"></i>

                                <span class="hide-menu">Cupons</span>

                            </a>

                        </li>

                        <li class="hidden"> 

                            <a href="javascript:void(0)" onclick="window.location.href='<?php echo $base.$CI->promoPromotion ?>'">

                                <i data-icon="/" class="linea-icon linea-basic fa-fw"></i>

                                <span class="hide-menu">Promoções</span>

                            </a>

                        </li>

                    </ul>

                </li>

                <li class="hidden">

                    <a href="javascript:void(0)" onclick="window.location.href='<?= $base.$CI->reports ?>'" class="waves-effect">

                        <i data-icon="n" class="linea-icon linea-basic fa-fw"></i>

                        <span class="hide-menu">Relatórios</span>

                    </a> 

                </li>

                

                <li class="hidden"> 

                    <a href="javascript:void(0)" class="waves-effect"><i data-icon="" class="linea-icon linea-basic fa-fw"></i><span class="hide-menu">Multi Dropdown<span class="fa arrow"></span></span></a>

                    <ul class="nav nav-second-level">

                        <li> <a href="javascript:void(0)"><i data-icon="/" class="linea-icon linea-basic fa-fw"></i><span class="hide-menu">Second Level Item</span></a> </li>

                        <li> <a href="javascript:void(0)"><i data-icon="7" class="linea-icon linea-basic fa-fw"></i><span class="hide-menu">Second Level Item</span></a> </li>

                        <li> <a href="javascript:void(0)" class="waves-effect"><i data-icon="&#xe008;" class="linea-icon linea-basic fa-fw"></i><span class="hide-menu">Third Level </span><span class="fa arrow"></span></a>

                            <ul class="nav nav-third-level">

                                <li> <a href="javascript:void(0)"><i class=" fa-fw">T</i><span class="hide-menu">Third Level Item</span></a> </li>

                                <li> <a href="javascript:void(0)"><i class=" fa-fw">M</i><span class="hide-menu">Third Level Item</span></a> </li>

                                <li> <a href="javascript:void(0)"><i class=" fa-fw">R</i><span class="hide-menu">Third Level Item</span></a> </li>

                                <li> <a href="javascript:void(0)"><i class=" fa-fw">G</i><span class="hide-menu">Third Level Item</span></a> </li>

                            </ul>

                        </li>

                    </ul>

                </li>

        </ul>



    </div>

</div>