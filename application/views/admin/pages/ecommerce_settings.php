<?php 
$CI =& get_instance(); 
$base = base_url().$CI->base;
$config['default'] = $CI->config_general();
$config['address'] = $CI->config_address();

$oauth = $CI->db->get('oauth')->row();
$config['oauth']['appID']    = isset($oauth->appid) ? $oauth->appid : '';
$config['oauth']['version']  = isset($oauth->version) ? $oauth->version : '';
$config['oauth']['clientID'] = isset($oauth->clientid) ? $oauth->clientid : '';
$config['oauth']['mapsKEY'] = isset($oauth->mapskey) ? $oauth->mapskey : '';

$networks = $CI->db->get('networks')->row();
$config['networks']['facebook']  = isset($networks->facebook) ? $networks->facebook : '';
$config['networks']['twitter']   = isset($networks->twitter) ? $networks->twitter : '';
$config['networks']['google']    = isset($networks->google) ? $networks->google : '';
$config['networks']['instagram'] = isset($networks->instagram) ? $networks->instagram : '';
?>
<div class="container-fluid">
	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title">Configurações da loja</h4> </div>
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
				<ol class="breadcrumb">
					<li><a href="<?php echo $base ?>">Página inicial</a></li>
					<li class="active">Configurações da loja</li>
				</ol>
			</div>
			<!-- /.col-lg-12 -->
		</div>

		<div class="col-lg-12 col-xs-12">
			<div class="white-box">
				<h3 class="box-title">Configurações</h3>

				<div style="width: 100%;">
					<ul class="nav customtab nav-tabs">
						<li class="tab active">
							<a data-toggle="tab" href="#tab1" aria-expanded="true"> <span class="visible-xs"><i class="ti-home"></i></span> <span class="hidden-xs">Geral</span> </a>
						</li>
						<li class="tab">
							<a data-toggle="tab" href="#tab2" aria-expanded="false"> <span class="visible-xs"><i class="ti-truck"></i></span> <span class="hidden-xs">Endereço e entregas</span> </a>
						</li>
						<li class="tab">
							<a aria-expanded="false" data-toggle="tab" href="#tab3"> <span class="visible-xs"><i class="ti-money"></i></span> <span class="hidden-xs">Pagamentos</span> </a>
						</li>
						<li class="tab">
							<a aria-expanded="false" data-toggle="tab" href="#tab4"> <span class="visible-xs"><i class="ti-clock"></i></span> <span class="hidden-xs">Horário de funcionamento</span> </a>
						</li>
						<li class="tab">
							<a aria-expanded="false" data-toggle="tab" href="#tab5"> <span class="visible-xs"><i class="ti-email"></i></span> <span class="hidden-xs">Emails</span> </a>
						</li>
						<li class="tab">
							<a aria-expanded="false" data-toggle="tab" href="#tab6"> <span class="visible-xs"><i class="ti-email"></i></span> <span class="hidden-xs">Manutenção</span> </a>
						</li>
						<li class="tab">
							<a aria-expanded="false" data-toggle="tab" href="#tab7"> <span class="visible-xs"><i class="ti-email"></i></span> <span class="hidden-xs">OAuth e Redes Sociais</span> </a>
						</li>
					</ul>
					<div class="tab-content">

						<div id="tab1" class="tab-pane active">
							<h4 class="m-t-0 m-b-20">Configuração geral</h4>
							<form method="post" role="generalSettings">
							<div class="col-lg-6 col-xs-12 m-b-20">
							  
								<div class="form-group">
									<label>Titulo da loja</label>
									<input type="text" name="title" class="form-control" placeholder="Titulo da Loja" value="<?= $config['default']->title ?>">
								</div>

								<div class="form-group">
									<label>Descrição</label>
									<input type="text" name="slogan" class="form-control" placeholder="Descrição da Loja" value="<?= $config['default']->slogan ?>">
								</div>

								<div class="form-group">
									<label>E-mail</label>
									<input type="text" name="email" class="form-control" placeholder="E-mail" value="<?= $config['default']->email ?>">
								</div>

								<div class="form-group">
									<label>URL do site</label>
									<input type="text" name="url" class="form-control" placeholder="URL do site" value="<?= $config['default']->url ?>">
								</div>

								<div class="form-group">
									<label>Diretório raiz</label>
									<input type="text" name="dirr" class="form-control" placeholder="Diretório raiz" value="<?= $config['default']->dirr ?>">
								</div>

								<div class="form-group">
									<label>Logo</label>
									<input type="file" name="logo" class="form-control">
								</div>
							  
							</div>

							<div class="col-lg-6 col-xs-12 m-b-20">
								<h4 class="m-t-0 m-b-20">App URL</h4>
								<div class="form-group">
									<label>PlayStore URL</label>
									<input type="text" name="android" class="form-control" value="<?= $config['default']->android ?>">
								</div>
								<div class="form-group">
									<label>AppStore URL</label>
									<input type="text" name="appstore" class="form-control" value="<?= $config['default']->ios ?>">
								</div>
								<div class="form-group">
									<label>WindowPhone URL</label>
									<input type="text" name="windowsphone" class="form-control" value="<?= $config['default']->windowsphone ?>">
								</div>
								<h4 class="m-t-0 m-b-20">Servidor Node</h4>
								<div class="form-group">
									<label>IP/Dominio</label>
									<input type="text" name="ipdomain" class="form-control" value="falcondelivery.com.br" readonly="true">
								</div>
								<div class="form-group">
									<label>Porta</label>
									<input type="text" name="port" class="form-control" value="<?= $config['default']->port ?>">
								</div>
							</div>

							<div class="col-lg-12 text-right">
								<button type="submit" class="btn btn-info">Salvar</button>
							</div>

							</form>

							<div class="clearfix"></div>
						</div>
						<div id="tab2" class="tab-pane">
						  <form method="post" role="save_ecommerce_settings">

							<div class="col-lg-6 m-b-20">
							  <h4 class="m-t-0 m-b-20">Endereço</h4>
								<div class="form-group">
									<label>CEP</label>
									<input type="text" name="cep" class="form-control" value="<?= $config['address']->cep ?>">
								</div>

								<div class="form-group">
									<label>Endereço</label>
									<input type="text" name="address" readonly="true" class="form-control" value="<?= $config['address']->address ?>">
								</div>

								<div class="form-group">
									<label>Número</label>
									<input type="text" name="number" class="form-control" value="<?= $config['address']->number ?>">
								</div>

								<div class="form-group">
									<label>Referência</label>
									<input type="text" name="reff" class="form-control" value="<?= $config['address']->complement ?>">
								</div>

								<input type="hidden" id="lat" name="lat" value="<?= $config['address']->coord_y ?>">
								<input type="hidden" id="lng" name="lng" value="<?= $config['address']->coord_x ?>">

							</div>

							<div class="col-lg-6 m-b-20">
							  <h4 class="m-t-0 m-b-20">Entregas</h4>
								
								<div class="form-group">
								 <?php
									if (!empty($config['address']->city)):
									list($city_name, $state) = explode(" - ", $config['address']->city);

									$districts_array = isset($config['address']->districts) ? explode("|", $config['address']->districts) : array();

									$CI->db->where("cidade", trim($city_name));					
									$get = $CI->db->get("cepbr_cidade");
									if ($get->num_rows() > 0):
										$city = $get->row();
										$CI->db->where("id_cidade", $city->id_cidade);
										$districts = $CI->db->get("cepbr_bairro");
								 ?>
									<select name="districts[]" class="select2 select2-multiple districts" title="Bairros de entrega" multiple>
										<?php if ($districts->num_rows() > 0):?>
											<?php foreach ($districts->result() as $row): ?>
												<?php if (in_array($row->id_bairro, $districts_array)): ?>
													<option value="<?= $row->id_bairro ?>" selected><?= $row->bairro ?></option>
												<?php else: ?>
													<option value="<?= $row->id_bairro ?>"><?= $row->bairro ?></option>
												<?php endif ?>
											<?php endforeach ?>
										<?php else: ?>
											<option value="">Nenhum bairro encontrado!</option>
										<?php endif ?>
									</select>
								  <?php endif; ?>
								 <?php endif; ?>
								</div>

							  	<div class="form-group">
                                    <div class="col-lg-6">
                                        <label for="perKm">
                                        <input id="perKm" class="choose-taxas" type="radio" name="taxavalue" value="1" <?php echo ($config['address']->taxasvalue == 1) ? 'checked' : '' ?>>
                                            Preço por KM
                                        </label>
                                    </div>
                                    <div class="col-lg-6">
							  		    <label for="perDistrict">
							  		        <input id="perDistrict" class="choose-taxas" type="radio" name="taxavalue" value="2" <?php echo ($config['address']->taxasvalue == 2) ? 'checked' : '' ?>>
							  			    Preço por bairro
                                        </label>
                                    </div>
                                    <div class="col-lg-12">
                                        <div id="kms" style="display:none;">
                                            <div class="form-group">
                                                <label>Preço por KM (centavos)</label>
                                                <input type="text" name="km_price" class="form-control" value="<?= $config['address']->delivery_price ?>">
                                            </div>
                                        </div>
                                        <div id="districtsC" style="display:none;padding:15px;">
                                            <ul id="list-districts" class="list-group"></ul>
                                            <ul id="add-districts" class="list-group"></ul>
                                            <div class="clearfix"></div> 
                                            <div class="text-center">
                                                <button type="button" class="btn btn-primary add-district" style="margin-top:15px;">Adicionar bairro</button>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
								
								<div class="form-group">
								  <div class="embed-responsive embed-responsive-16by9">
									<div id="findCEP" class="embed-responsive-item" style="width:100%;height: 500px;">
									  <?php if ($config['address']->coord_y != '' && $config['address']->coord_x != ''): ?>
										 <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed/v1/place?key=<?= $config['oauth']['mapsKEY'] ?>&q=<?= $config['address']->coord_y ?>,<?= $config['address']->coord_x ?>&zoom=18" frameborder="0"></iframe>
									  <?php endif ?>
									</div>
								  </div>
								</div>
							</div>
							<div class="clearfix"></div>
							  <div class="col-xs-12">
							  	<div class="form-group text-right">
									<button type="submit" class="btn btn-info">Salvar</button>
								</div>
							  </div>
							</form>
						  <div class="clearfix"></div>
						</div>
						<div id="tab3" class="tab-pane">

							<div class="vtabs customvtab">
								<ul class="nav tabs-vertical">
									<li class="tab active">
										<a data-toggle="tab" href="#tabs1" aria-expanded="true"> <span class="visible-xs"><i class="ti-home"></i></span> <span class="hidden-xs">PagSeguro</span> </a>
									</li>
									<li class="tab hidden">
										<a data-toggle="tab" href="#tabs2" aria-expanded="false"> <span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">MercadoPago</span> </a>
									</li>
								</ul>
								<div class="tab-content" style="width: 100%;">
									<div id="tabs1" class="tab-pane active">
										<div class="form-group">
											<h3>Valor mínimo de compra</h3>
											<input id="minBuy" type="number" min="0.00" max="10000.00" step="0.01" class="form-control" placeholder="00.00" value="<?= ($config['default']->minbuy > 0) ? number_format($config['default']->minbuy, 2, ".", "") : "" ?>">
											<div class="form-group p-t-10">
											  <button type="button" data-target="#minBuy" data-table="config" data-row="minbuy" class="define btn btn-success btn-xs">Definir</button>
											</div>
										</div>
									  <form method="post" role="pagseguro-payment">
										<h3>PagSeguro Checkout Transparente</h3>
										<?php
										$CI->db->where('payments', 'pagseguro');
										$pagseguro = $CI->db->get('config_payments')->row();
										$config['pagseguro'] = array(
											'payments' => isset($pagseguro->payments) ? 'checked' : '',
											'payments_envelopment' => isset($pagseguro->payments_envelopment) ? 'value="0" checked' : 'value="1"',
											'payments_email' => isset($pagseguro->payments_email) ? $pagseguro->payments_email  : '',
											'payments_token' => isset($pagseguro->payments_token) ? $pagseguro->payments_token : '',
											'payments_type' => isset($pagseguro->payments_type) ? $pagseguro->payments_type : '',
											'payments_x' => isset($pagseguro->payments_x) ? $pagseguro->payments_x : '',
											'payments_url' => isset($pagseguro->payments_url) ? $pagseguro->payments_url : ''
										);
										$types = '';
										if (isset($config['pagseguro']['payments_type'])) {
											$type = str_replace(" ", "", $config['pagseguro']['payments_type']);
											$types = explode(",", $type);
										}

										?>
										<div class="form-group">
											<label for="payment">
												<input id="payment" type="checkbox" name="payments" value="pagseguro" <?= $config['pagseguro']['payments'] ?>>
												Ativar PagSeguro
											</label>
											<label for="envelopment">
												<input id="envelopment" type="checkbox" name="payments_envelopment" <?= $config['pagseguro']['payments_envelopment'] ?>>
												Testes
											</label>
										</div>
										
										<div class="form-group">
											<label>E-mail PagSeguro</label>
											<input type="text" class="form-control" name="payments_email" value="<?= $config['pagseguro']['payments_email'] ?>" placeholder="E-mail PagSeguro" required>
										</div>
										<div class="form-group">
											<label>Token de Segurança</label>
											<input type="text" class="form-control" name="payments_token" value="<?= $config['pagseguro']['payments_token'] ?>" placeholder="Token de Segurança" required>
										</div>
										<p><strong>Opções de compra</strong></p>
										<div class="form-group">
											<div class="col-xs-12 col-sm-3 col-lg-2">
												<label for="creditcard">
													<input id="creditcard" type="checkbox" name="payments_type[]" value="1" <?php if (isset($types[0])) {echo 'checked';} ?> required>
													Cartão de crédito
												</label>
											</div>
											<div class="col-xs-12 col-sm-3 col-lg-2 hidden">
												<label for="debit">
													<input id="debit" type="checkbox" name="payments_type[]" value="2" <?php if (isset($types[1])) {echo 'checked';} ?>>
													Débito Online
												</label>
											</div>
											<div class="col-xs-12 col-sm-3 col-lg-2 hidden">
												<label for="bolet">
													<input id="bolet" type="checkbox" name="payments_type[]" value="3" <?php if (isset($types[2])) {echo 'checked';} ?>>
													Boleto
												</label>
											</div>
											<div class="clearfix"></div>
										</div>

										<div class="col-lg-6">
											<div class="form-group">
												<label>Número máximo de parcelas</label>
												<code>O padrão é 1x para cartões de crédito.</code>
												<select name="payments_x" class="selectpicker form-control">
													<?php for ($i = 0; $i < 12; $i++) {
														$index = $i+1;
														if (isset($config['pagseguro']['payments_x']) && $config['pagseguro']['payments_x'] != '') {
															echo '<option value="'.$config['pagseguro']['payments_x'].'">'.$config['pagseguro']['payments_x'].' vez</option>';
														}else{
															if($index == 1){
																echo '<option value="'.$index.'">'.$index.' vez</option>';
															}else{
																echo '<option value="'.$index.'">'.$index.' vezes</option>';
															}
														}

													} ?>
												</select>
											</div>
										</div>

										<div class="col-lg-6 hidden">
											<div class="form-group">
												<label>Página de sucesso da transação.</label>
												<input type="text" class="form-control" name="payments_url" <?= $config['pagseguro']['payments_url'] ?> placeholder="URL Página personalizada">
											</div>
										</div>
										
										<div class="form-group text-right">
											<button type="submit" class="fcbtn btn btn-xs btn-info">Salvar</button>
										</div>

										<div class="clearfix"></div>
									  </form>
									</div>
									<div id="tabs2" class="tab-pane">
										<p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt.Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.</p>
									</div>
								</div>
							</div>
						</div>
						<div id="tab4" class="tab-pane">
							<h3>Configurações de funcionamento</h3>
							<form method="post" role="funcionally">
								<div class="col-lg-6">
									<code>OBS: Você pode adicionar até 5 campos.</code>
									<div class="form-group text-right">
										<button type="button" class="fcbtn btn btn-xs btn-info more-fields" data-toggle="tooltip" title="Adicionar mais campos de horários."><i class="fa fa-plus"></i></button>
									</div>
									<div id="hours-container">
										<?php 
										$get_func = $CI->db->get('funcionally');
										if($get_func->num_rows() > 0):
											$d_array = array (
												0 => "Domingo",
												1 => "Segunda-feira",
												2 => "Terça-feira",
												3 => "Quarta-feira",
												4 => "Quinta-feira",
												5 => "Sexta-feira",
												6 => "Sábado"
											);
											foreach ($get_func->result() as $i => $func): 
												$is = array();
												$ex = explode(",", $func->days);
												foreach ($ex as $in) {
													$is[] = $in;
												}
												?>
												<div id="field-<?= $func->id ?>" class="form-group p-t-10 hour-fields">
													<a href="#" class="pull-right delete" data-id="<?= $func->id ?>">apagar</a>
													<label>Dias de funcionamento. <i class="fa fa-info-circle" data-toggle="tooltip" title="Você pode selecionar de 1 à 7 dias, de acordo com o funcionamento do restaurante."></i></label>
													<select name="days[<?= $i ?>][]" class="selectpicker form-control" title="Selecione os dias" multiple>
														<?php foreach ($d_array as $index => $dd): ?>
															<?php if (in_array($index, $is)): ?>
																<option value="<?= $index ?>" selected><?= $dd ?></option>
															<?php else: ?>
																<option value="<?= $index ?>"><?= $dd ?></option>
															<?php endif ?>
														<?php endforeach ?>
													</select>
													<label>Hora de funcionamento</label>
													<input type="text" name="hour[<?= $i ?>][]" class="form-control hour-changed" value="<?= str_replace(',', ', ', $func->hours); ?>" placeholder="00:00, 00:00" maxlength="12" required="">
													<input type="hidden" name="id[<?= $i ?>][]" value="<?= $func->id ?>">
												</div>
												<?php 
											endforeach;
										else:
											?>
											<div class="form-group p-t-10 hour-fields">
												<label>Dias de funcionamento. <i class="fa fa-info-circle" data-toggle="tooltip" title="Você pode selecionar de 1 à 7 dias, de acordo com o funcionamento do restaurante."></i></label>
												<select name="days[0][]" class="selectpicker form-control" title="Selecione os dias" multiple>
													<option value="0">Domingo</option>
													<option value="1">Segunda-feira</option>
													<option value="2">Terça-feira</option>
													<option value="3">Quarta-feira</option>
													<option value="4">Quinta-feira</option>
													<option value="5">Sexta-feira</option>
													<option value="6">Sábado</option>
												</select>
												<label>Hora de funcionamento</label>
												<input type="text" name="hour[0][]" class="form-control hour-changed" placeholder="00:00, 00:00" maxlength="12" required="">
											</div>
											<?php  	
										endif;
										?>
									</div>

									<div class="form-group text-right">
										<button type="submit" class="fcbtn btn btn-xs btn-info submit-block" disabled="">Salvar</button>
									</div>
								</div>
							</form>

							<div class="clearfix"></div>
						</div>
						<div id="tab5" class="tab-pane">
							<h3>Configurações de Emails</h3>
							<div class="row text-center">
								<form method="post" enctype="multipart/form-data" id="logo_emails">
									<div id="logo_load">
										<?php
										$url = (dirname($_SERVER['PHP_SELF'])) ? $_SERVER['DOCUMENT_ROOT'].dirname($_SERVER['PHP_SELF']).'/' : '/';

											$thumbnail = 'themes/uploads/logo/emails/';
											$files = glob($thumbnail.'*');
											if (count($files) > 0) {
												echo '<img src="'. $files[0] .'" height="120">';
											}
										?>
									</div>
									<label for="change_logo" class="btn btn-primary">Alterar logotipo dos emails</label>
									<input type="file" id="change_logo" style="display: none;">
								</form>
							</div>
							
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="panel panel-default">
									<div class="panel-heading">Boas-vindas</div>
									<div class="panel-wrapper collapse in">
										<div class="panel-body m-0 p-l-0 p-r-0 p-t-10 p-b-10">
											<p>
											  <?php 
												$receipt = trim(file_get_contents($url.'application/views/emails/welcome.html'));
												$body_receipt = str_replace("[{", "<code>[{", $receipt);
												$body_receipt = str_replace("}]", "}]</code>", $body_receipt);
												echo $body_receipt;
												$email_welcome = $CI->config_emails('welcome');
											  ?>
											</p>
											<a class="btn btn-custom m-t-10 collapseble" data-target="welcome">Editar</a>
											<div id="box-welcome" class="m-t-15 collapseblebox dn">
												<div class="well">
													<p><code>Não alterar os valores entre: [{valor}]</code></p>
													<label><strong><small>Assunto do e-mail</small></strong></label>
													<input id="welcome-subject" type="text" name="subject" class="form-control m-b-5" placeholder="Boas-vindas" value="<?= isset($email_welcome->subject) ? $email_welcome->subject : '' ?>">
													<label><strong><small>Corpo do e-mail</small></strong></label>
													<textarea id="welcome" rows="10" class="form-control textarea-emails"><?php echo trim(file_get_contents($url.'application/views/emails/welcome.html')); ?></textarea>
													<button type="button" data-id="welcome" class="submit btn btn-success m-t-10">Salvar</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="panel panel-default">
									<div class="panel-heading">Status do pedido</div>
									<div class="panel-wrapper collapse in">
										<div class="panel-body m-0 p-l-0 p-r-0 p-t-10 p-b-10">
											<p>
											  <?php 
												$receipt = trim(file_get_contents($url.'application/views/emails/order_status.html'));
												$body_receipt = str_replace("[{", "<code>[{", $receipt);
												$body_receipt = str_replace("}]", "}]</code>", $body_receipt);
												echo $body_receipt;
												$email_order_status = $CI->config_emails('order_status');
											  ?>
											</p> 
											<a class="btn btn-custom m-t-10 collapseble" data-target="order_status">Editar</a>
											<div id="box-order_status" class="m-t-15 collapseblebox dn">
												<div class="well">
													<p><code>Não alterar os valores entre: [{valor}]</code></p>
													<label><strong><small>Assunto do e-mail</small></strong></label>
													<input id="order_status-subject" type="text" name="subject" class="form-control m-b-5" placeholder="Status do pedido" value="<?= isset($email_order_status->subject) ? $email_order_status->subject : '' ?>">
													<label><strong><small>Corpo do e-mail</small></strong></label>
													<textarea id="order_status" rows="10" class="form-control textarea-emails"><?php echo trim(file_get_contents($url.'application/views/emails/order_status.html')); ?></textarea>
													<button type="button" data-id="order_status" class="submit btn btn-success m-t-10">Salvar</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
								<div class="panel panel-default">
									<div class="panel-heading">Recibo de compra</div>
									<div class="panel-wrapper collapse in">
										<div class="panel-body m-0 p-l-0 p-r-0 p-t-10 p-b-10">
											<p>
												<?php 
													$receipt = trim(file_get_contents($url.'application/views/emails/receipt.html')); 
													$body_receipt = str_replace("[{", "<code>[{", $receipt);
													$body_receipt = str_replace("}]", "}]</code>", $body_receipt);
													echo $body_receipt;
													$email_receipt = $CI->config_emails('receipt');
												?>
											</p> 
												<a class="btn btn-custom m-t-10 collapseble" data-target="receipt">Editar</a>
											<div id="box-receipt" class="m-t-15 collapseblebox dn">
												<div class="well">
													<p><code>Não alterar os valores entre: [{valor}]</code></p>
													<label><strong><small>Assunto do e-mail</small></strong></label>
													<input id="receipt-subject" type="text" name="subject" class="form-control m-b-5" placeholder="Recibo de compra" value="<?= isset($email_receipt->subject) ? $email_receipt->subject : '' ?>">
													<label><strong><small>Corpo do e-mail</small></strong></label>
													<textarea id="receipt" rows="10" class="form-control textarea-emails"><?php echo trim(file_get_contents($url.'application/views/emails/receipt.html')); ?></textarea>
													<button type="button" data-id="receipt" class="submit btn btn-success m-t-10">Salvar</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="clearfix"></div>
						</div>
						<div id="tab6" class="tab-pane">
							<h3>Manutenção e Inauguração</h3>
							<?php
							$maintenance = $CI->db->get('maintenance');
							$active_maintenance = ($maintenance->num_rows() > 0) ? 'value="1" checked="checked"' : '';
							$period_maintenance = ($maintenance->num_rows() > 0) ? $maintenance->row()->period : ''; 
							$titles_maintenance = ($maintenance->num_rows() > 0) ? $maintenance->row()->title : ''; 
							$subtit_maintenance = ($maintenance->num_rows() > 0) ? $maintenance->row()->subtitle : ''; 
							?>
							<div class="col-lg-6 col-xs-12">
							  <form role="active_maintenance" method="post">
								<div class="form-group">
									<label for="maintenance">
										<input id="maintenance" type="checkbox" name="maintenance" <?= $active_maintenance ?>>
										Ativar Modo Manutenção
									</label>
								</div>

								<div class="form-group">
									<label>Titulo da manutenção</label>
									<input type="text" name="title" value="<?= $titles_maintenance ?>" class="form-control" required>
								</div>

								<div class="form-group">
									<label>Descrição da manutenção</label>
									<input type="text" name="subtitle" value="<?= $subtit_maintenance ?>" class="form-control">
								</div>

								<div id="maintenance_period" class="form-group">
									<label>Data prevista para término</label>
									<div class="input-group">
										<input type="text" class="form-control datepicker-autoclose" name="period" placeholder="dd/mm/yyyy" data-date-format="dd/mm/yyyy" value="<?= $period_maintenance ?>" required> <span class="input-group-addon"><i class="icon-calender"></i></span>
									</div>
								</div>

								<div class="form-group text-right">
									<button type="submit" class="fcbtn btn btn-xs btn-info">Salvar</button>
								</div>
							  </form>
							</div>

							<div class="col-lg-6 col-xs-12">
							  <form role="active_comingsoon" method="post">
							  	<?php
							  	$comingsoon = $CI->db->get('comingsoon');
							  	$active_comingsoon = ($comingsoon->num_rows() > 0) ? 'value="1" checked="checked"' : '';
							  	$period_comingsoon = ($comingsoon->num_rows() > 0) ? $comingsoon->row()->period : ''; 
							  	$titles_comingsoon = ($comingsoon->num_rows() > 0) ? $comingsoon->row()->title : ''; 
							  	?>
								<div class="form-group">
									<label for="comingsoon">
										<input id="comingsoon" type="checkbox" name="comingsoon" <?= $active_comingsoon ?>>
										Ativar Modo Inauguração
									</label>
								</div>

								<div class="form-group">
									<label>Titulo da destaque</label>
									<input type="text" name="title" value="<?= $titles_comingsoon ?>" class="form-control" required>
								</div>

								<div id="maintenance_period" class="form-group">
									<label>Data prevista para término</label>
									<div class="input-group">
										<input type="text" class="form-control datepicker-autoclose" name="period" placeholder="dd/mm/yyyy" data-date-format="dd/mm/yyyy" value="<?= $period_comingsoon ?>" required> <span class="input-group-addon"><i class="icon-calender"></i></span>
									</div>
								</div>

								<div class="form-group text-right">
									<button type="submit" class="fcbtn btn btn-xs btn-info">Salvar</button>
								</div>
							  </form>
							</div>
							<div class="clearfix"></div>
						</div>
						<div id="tab7" class="tab-pane">

							<h3>oAuth e Redes Sociais</h3>
							<div class="col-lg-6 col-xs-12">
							  <form role="oauthconfig" method="post">
							  	<h4>Facebook API</h4>
							  	<div class="form-group">
							  		<label>Login API AppID:</label>
							  		<input type="text" name="appid" value="<?= $config['oauth']['appID'] ?>" placeholder="145563396087169" class="form-control">
							  	</div>
							  	<div class="form-group">
							  		<label>Login API Versão:</label>
							  		<input type="text" name="version" value="<?= $config['oauth']['version'] ?>" placeholder="v2.11" class="form-control">
							  	</div>
							  	<h4>Google OAuth API</h4>
							  	<div class="form-group">
							  		<label>Google oAuth Cliente ID:</label>
							  		<input type="text" name="clientid" value="<?= $config['oauth']['clientID'] ?>" placeholder="153533789642-suah3cjp2vo13p24883bt6d5e6btmfo7.apps.googleusercontent.com" class="form-control">
							  	</div>
							  	<h4>Google Maps API</h4>
							  	<div class="form-group">
							  		<label>Google Maps API KEY:</label>
							  		<input type="text" name="mapskey" value="<?= $config['oauth']['mapsKEY'] ?>" placeholder="AIzaSyAhmvgclv5un23pDB0oyuofJzpzcoZJ24c" class="form-control">
							  	</div>
							  	<div class="fomr-group text-right">
							  		<button type="submit" class="fcbtn btn btn-xs btn-info">Salvar</button>
							  	</div>
							  </form>
							</div>

							<div class="col-lg-6 col-xs-12">
							  <form role="socialconfig" method="post">

							  	<h4>Redes sociais</h4>
							  	<div class="form-group">
							  		<label>Facebook:</label>
							  		<input type="text" name="facebook" value="<?= $config['networks']['facebook'] ?>" class="form-control">
							  	</div>
							  	<div class="form-group">
							  		<label>Google+:</label>
							  		<input type="text" name="google" value="<?= $config['networks']['twitter'] ?>" class="form-control">
							  	</div>
							  	<div class="form-group">
							  		<label>Twitter:</label>
							  		<input type="text" name="twitter" value="<?= $config['networks']['google'] ?>" class="form-control">
							  	</div>
							  	<div class="form-group">
							  		<label>Instagram:</label>
							  		<input type="text" name="instagram" value="<?= $config['networks']['instagram'] ?>" class="form-control">
							  	</div>
							  	<div class="fomr-group text-right">
							  		<button type="submit" class="fcbtn btn btn-xs btn-info">Salvar</button>
							  	</div>
							  </form>
							</div>

						</div>
						<div class="clearfix"></div>

					</div>
				</div>
			</div>
		</div>


	</div>