<?php 
	$CI =& get_instance(); 
    $base = $CI->base;
    $address = $CI->config_address();
    $city = explode(" - ", $address->city);
    $districts = isset($address->districts) ? explode("|", $address->districts) : null;
    $districts_array = array();
    if (!is_null($districts)) {
        foreach ($districts as $id_bairro) {
            $CI->db->where("id_bairro", $id_bairro);
            $get = $CI->db->get("cepbr_bairro");
            if ($get->num_rows() > 0) {
                array_push($districts_array, $get->row()->bairro);
            }
        }
    }

?>

<div class="container-fluid">
	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title">Ponto de venda</h4> </div>
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
				<ol class="breadcrumb">
					<li><a href="<?php echo $base ?>">Página Inicial</a></li>
					<li class="active">Ponto de venda</li>
				</ol>
			</div>
	</div>

	<div class="row">
        <div class="col-sm-12">
            <div class="white-box">
            	<div class="col-md-8">
              		<div class="form-group">
              			<div id="product-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">Adicionar produto</h4> 
                                        </div>

                                        <form id="product-sales" method="post">
                                        <div class="modal-body">
                                                <div class="form-group" style="position: relative;">
                                                    <label for="product-name" class="control-label">Nome do produto</label>
                                                    <select id="product-name" class="select2 m-b-10 form-control" data-placeholder="Selecionar produto">
                                                        <?php
                                                            $CI->db->where("status", "1");
                                                            $categories = $CI->db->get("category");

                                                            if ($categories->num_rows() > 0) {

                                                                foreach ($categories->result() as $category) {
                                                                 echo '<optgroup label="'.strtoupper($category->name).'">';

                                                                 $CI->db->where("category_id", $category->id);
                                                                 $products = $CI->db->get("products");
                                                                 if ($products->num_rows() > 0) {
                                                                    foreach ($products->result() as $product): 

                                                                        $ing = array();
                                                                        $add = array();
                                                                        $opt = array();

                                                                
                                                                        $CI->db->where_in('id', explode("|", $product->ingredients));
                                                                        $ingredients = $CI->db->get('ingredients');

                                                                        if ($ingredients->num_rows() > 0) {
                                                                            foreach ($ingredients->result() as $ingredient) {
                                                                                $ing[] = $ingredient->name;
                                                                            }
                                                                        }

                                                                        $add_find = (strpos($product->addons, "|")) ? explode("|", $product->addons) : $product->addons;
                                                                        if ($product->addons != null && $product->addons != '') {
                                                                            $CI->db->where_in('id', $add_find);

                                                                            $addons = $CI->db->get('addons');
                                                                            if ($addons->num_rows() > 0) {
                                                                                foreach ($addons->result() as $addon) {
                                                                                    $add[] = array(
                                                                                        "id" => $addon->id, 
                                                                                        "name" => $addon->name, 
                                                                                        "price" => ($addon->price > 0) ? $CI->helps->format_money($addon->price) : 'Grátis'
                                                                                    );
                                                                                }
                                                                            }
                                                                        }

                                                                        $opt_find = (strpos($product->optionals, "|")) ? explode("|", $product->optionals) : $product->optionals;
                                                                        if ($product->optionals != null && $product->optionals != '') {
                                                                            $CI->db->where_in('id', $opt_find);
                                                                            $optionals = $CI->db->get('optionals');
                                                                            if ($optionals->num_rows() > 0) {
                                                                                foreach ($optionals->result() as $optional) {
                                                                                    $CI->db->where("status", "1");
                                                                                    $CI->db->where("optionals_id", $optional->id);
                                                                                    $optional_details = $CI->db->get("optionals_options");
                                                                                    if ($optional_details->num_rows() > 0) {
                                                                                     $opt_options = array();
                                                                                     foreach ($optional_details->result() as $option) {
                                                                                        $opt_options[] = array(
                                                                                            'id' => $option->id,
                                                                                            'name' => $option->name
                                                                                        );
                                                                                     }
                                                                                     $opt[] = array(
                                                                                        'id' => $optional->id,
                                                                                        'name' => $optional->name,
                                                                                        'price' => ($optional->price > 0) ? $CI->helps->format_money($optional->price) : 'Grátis',
                                                                                        'obrigatory' => $optional->obrigatory,
                                                                                        'options' => $opt_options
                                                                                    );
                                                                                 }

                                                                             }
                                                                         }
                                                                     }
                                                        ?>
                                                                        <option value="<?= $product->name ?>" data-id="<?= $product->id ?>" data-price="<?= $CI->helps->format_money($product->price) ?>" data-ingredients="<?= $product->ingredients ?>" data-addons='<?= json_encode($add) ?>' data-optionals='<?= json_encode($opt) ?>' data-falvors="<?= $product->flavors ?>" data-pizza="<?= $product->pizza ?>"><?= $product->name ?> - R$<?= $CI->helps->format_money($product->price) ?></option>   
                                                        <?php 
                                                                    endforeach;
                                                                 }
                                                                 echo '</optgroup>'; 
                                                                }
                                                            }
                                                        ?>
                                                    </select>

                                                    <input id="ingredients" name="ingredients" type="text" style="background:none;border:none;" />

                                                    <div class="row">
                                                        <div id="flavors" style="display:none;">
                                                          <div class="col-lg-6 col-xs-12 m-t-10">
                                                            <select name="flavors[]" id="flavors_select" class="select2 m-b-10 select2-multiple" multiple="multiple" data-placeholder="Sabores"></select>
                                                          </div>
                                                          <div id="selected_flavors" class="col-lg-6 col-xs-12 m-t-10">
                                                            <ul class="list-group"></ul>
                                                          </div>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>

                                                    <div class="row">
                                                        <div id="addons" class="col-lg-6 col-xs-12 m-t-10"></div>
                                                        <div id="options" class="col-lg-6 col-xs-12 m-t-10"></div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="product-quantity" class="control-label">Quantidade</label>
                                                    <input type="number" class="form-control" id="product-quantity" min="1" value="1" required> 
                                                </div>

                                                <div class="form-group">
                                                    <label>Observação:</label>
                                                    <textarea name="observation" class="form-control"></textarea>
                                                </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="reset" class="btn btn-default waves-effect" data-dismiss="modal">Fechar</button>
                                            <button type="submit" class="btn btn-danger waves-effect waves-light">Adicionar</button>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                        </div>

                        <div id="client-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">Adicionar cliente</h4> 
                                        </div>
                                        <form id="client-sales" method="post">
                                            <div class="modal-body">
                                                <p>
                                                    <strong>Dados do cliente</strong>
                                                </p>
                                                <div class="form-group">
                                                    <input type="text" name="name" class="form-control" placeholder="Nome">
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" name="phone" class="form-control" placeholder="Telefone">
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" name="email" class="form-control" placeholder="E-mail">
                                                </div>
                                                <div class="form-group">
                                                    <input type="password" name="password" class="form-control" placeholder="Senha">
                                                    <input type="hidden" name="request" value="">
                                                </div>
                                                <hr>
                                                <p>
                                                    <strong>Endereço</strong>
                                                </p>
                                                <div class="form-group">
                                                    <input type="text" name="cep" data-location="<?= trim($city[0]) ?>" data-districts="<?= join(",", $districts_array) ?>" class="form-control" placeholder="CEP" required>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" name="address" class="form-control" placeholder="Endereço" readonly="true">
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" name="number" class="form-control" placeholder="Número" required>
                                                </div>
                                                <div class="form-group">
                                                    <input type="text" name="complement" class="form-control" placeholder="Complemento">
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="reset" class="btn btn-default waves-effect" data-dismiss="modal">Cancelar</button>
                                                <button type="submit" class="btn btn-danger waves-effect waves-light">+ Criar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                        </div>
              			<div class="pull-right">
              				<button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#product-modal">Adicionar produtos</button>
              			</div>
                        <form id="complete-order" method="post">
            			<label class="control-label">Lista de produtos</label>
            			<div class="table-responsive">
                            <table class="table table-hover manage-u-table">
                                <thead>
                                    <tr>
                                        <th width="70" class="text-center">#</th>
                                        <th>PRODUTO</th>
                                        <th>QUANTIDADE</th>
                                        <th>VALOR UNI.</th>
                                        <th>TOTAL</th>
                                    </tr>
                                </thead>
                                <tbody id="products-list">
                                <?php
                                    $sales = $CI->session->sales;
                                    if (count($sales) > 0):
                                    $total = 0;

                                    foreach ($sales as $i => $items):
                                      list($id, $index) = explode(":", $i);

                                      $CI->db->where("id", $id);
                                      $product = $CI->db->get('products')->row();
                                      $name = $product->name;
                                      $quantity = $items['quantity'];
                                      $price = $CI->helps->format_money($product->price);

                                      $flavors = array();
                                      if(isset($items['params']['flavors'])) {
                                        $explore_falvors = explode("|", $items['params']['flavors']);
                                        $price_flavor = array();
                                        foreach ($explore_falvors as $findex => $flv) {
                                          $CI->db->where('id', $flv);
                                          $get_flavor = $CI->db->get('products');
                                          if ($get_flavor->num_rows() > 0) {
                                             $flavor = $get_flavor->row();
                                             $price_flavor[] = $flavor->price;
                                             if ($flavor->id !== $id) {
                                                $flavors_index = $findex + 2;
                                                $flavors[] = '<span class="label label-warning label-lg">'.$flavors_index.'º Sabor: <strong>'. $flavor->name .'</strong></span>';
                                            }
                                        }
                                    }
                                    $subtotal = array_sum($price_flavor) / count($price_flavor) * $items['quantity'];
                                }else{
                                    $subtotal = $product->price * $items['quantity'];
                                }

                                $CI->db->where('id', $product->category_id);
                                $category_get = $CI->db->get('category');
                                $category = ($category_get->num_rows() > 0) ? $category_get->row()->name : 'Sem categoria';

                                $take = array();
                                if(isset($items['params']['ingredients'])):
                                    foreach ($items['params']['ingredients'] as $ingredients) {
                                      $CI->db->where('id', $ingredients);
                                      $ingredient = $CI->db->get('ingredients')->row();
                                      if($ingredient->price > 0){
                                        $subtotal += $ingredient->price * $items['quantity'];
                                    }
                                    $take[] = '<span class="label label-danger">'.$ingredient->name.'</span>';
                                }
                            endif;

                            $addons = array();
                            if(isset($items['params']['addons'])):
                                foreach ($items['params']['addons'] as $addon_id) {
                                  $CI->db->where('id', $addon_id);
                                  $addon = $CI->db->get('addons')->row();
                                  if($addon->price > 0){
                                    $subtotal += $addon->price * $items['quantity'];
                                }
                                $cost = ($addon->price > 0) ? 'R$'.$CI->helps->format_money($addon->price) : 'Grátis';
                                $addons[] = '<span class="label label-info">'.$addon->name.' - '.$cost.'</span>';
                            }
                        endif;
                        $optionals = array();
                        if(isset($items['params']['optionals'])):
                            $opts = explode("|", $items['params']['optionals']);
                            foreach ($opts as $opt) {
                              list($option_id, $odetail_id) = explode(":", $opt);
                              $CI->db->where('id', $option_id);
                              $get_optional = $CI->db->get('optionals');
                              if ($get_optional->num_rows() > 0) {
                                $optional = $get_optional->row();
                                $CI->db->where('id', $odetail_id);
                                $optional_detail = $CI->db->get('optionals_options')->row();
                                if($optional->price > 0){
                                  $subtotal += $optional->price * $items['quantity'];
                                }
                                $cost = ($optional->price > 0) ? 'R$'.$CI->helps->format_money($optional->price) : 'Grátis';
                                $optionals[] = '<span class="label label-info">'.$optional->name.': '.$optional_detail->name.' - '.$cost.'</span>';
                              }
                            }
                        endif;
                         $total += $subtotal;
                         $total = number_format($total, 2, ".", ".");
                         $fl = (count($flavors) > 0) ? '<br>'.join(" ", $flavors) : '';
                         $tk = (count($take) > 0) ? '<br>Tirar: '.join(" ", $take) : '';
                         $ad = (count($addons) > 0) ? '<br>Adicionais: '.join(" ", $addons) : '';
                         $op = (count($optionals) > 0) ? '<br>Opcionais: '.join(" ", $optionals) : '';
                         echo '<tr id="product-'.$index.'">
                         <td class="text-center">'.$id.'</td>
                         <td> 
                            <span class="font-medium">'.$name.'</span> 
                            '.$fl.' 
                            '.$tk.' 
                            '.$ad.'
                            '.$op.'
                         </td>
                         <td> '.$quantity.'</td>
                         <td> R$'.$price.'</td>
                         <td> R$'.$total.'</td>
                         <td> <button type="button" id="'.$index.'" class="btn btn-info btn-outline btn-circle btn-remove btn-xs m-r-5" data-id="'.$id.':'.$index.'" data-total="'.$total.'"><i class="ti-trash"></i></button> </td>
                         </tr>';
                        endforeach;
                    endif;
                    ?>
                                </tbody>
                            </table>
                        </div>
            		</div>
            	</div>

            	<div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">Cliente</label>

                        <select id="sales-client" name="client" class="select2 m-b-10 form-control" data-placeholder="Selecionar cliente">
                          <option selected disabled>Selecione um cliente</option>
                          <?php 
                            $clients = $CI->db->get("clients");

                            if($clients->num_rows() > 0):
                                foreach ($clients->result() as $client):
                                    $address = ''; 

                                    if (isset($client->address) && $client->address != '') {
                                        list($addr, $number) = explode("|", $client->address);
                                        list($street, $district, $city) = explode(", ", $addr);
                                        $address = $street.', '.$number.', '.$district.', '.$city;
                                    }
                          ?>
                                <option value="<?= $client->id ?>" data-picture="<?= $CI->helps->verify_client_thumbnail($client->thumbnail) ?>" data-address="<?= $address ?>" data-phone="<?= $CI->helps->formatPhone($client->phone) ?>">
                                    <?= ucwords($client->name) ?> - <?= $CI->helps->formatPhone($client->phone) ?>
                                </option>
                          <?php
                                endforeach;
                            endif;
                          ?>
                        </select>

                        <a data-toggle="modal" data-target="#client-modal">Cadastrar cliente</a>
                        <div id="client_data" class="el-element-overlay text-center" style="display: none;">
                            <div style="margin:0 auto;">
                                <div class="el-card-item">
                                    <div style="width:120px; margin:0 auto;" class="el-card-avatar el-overlay-1"> 
                                        <img id="client_thumb" width="100" class="img-circle" src="">
                                        <div class="el-overlay">
                                            <ul class="el-info">
                                                <li><a id="client_tel" href="tel:" class="btn default btn-outline image-popup-vertical-fit"><i class="icon-phone"></i></a></li>
                                                <li><a id="client_id" href="<?= base_url() ?>control/inbox/compose" data-id="" class="btn default btn-outline" href="javascript:void(0);"><i class="icon-envelope"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="el-card-content">
                                        <h3 id="client_address" class="box-title"></h3> <small id="client_outers"></small>
                                        <br> 
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>

            		<div class="form-group">
            			<label class="control-label">Forma de pagamento</label>
            			<select name="payment" class="form-control" data-placeholder="Forma de pagamento" tabindex="1">
            				<option value="delivery">Dinheiro</option>
            				<option value="creditcard">Cartão de crédito</option>
            				<option value="debitcard">Cartão de débito</option>
            				<option value="ticket">Ticket alimentação</option>
            			</select>
            		</div>

                    <div class="form-group">
                        <div class="radio radio-success radio-inline">
                            <input id="optionRadio0" name="deliveryaddress" value="mesa" type="radio" checked="">
                            <label for="optionRadio0"> Mesa </label>
                        </div>
                        <div class="radio radio-success radio-inline">
                            <input id="optionRadio1" name="deliveryaddress" value="delivery" type="radio">
                            <label for="optionRadio1"> Entrega </label>
                        </div>
                        <div class="radio radio-success radio-inline">
                            <input id="optionRadio2" name="deliveryaddress" value="withdraw" type="radio">
                            <label for="optionRadio2"> Retirada </label>
                        </div>
                    </div>

            		<div class="form-group">
            			<label class="control-label">Valor recebido</label>
            			<input type="text" id="amount" name="amount" class="form-control" placeholder="00,00">
            			<div class="checkbox checkbox-success">
            				<input id="complete_amount" type="checkbox">
            				<label for="complete_amount"> Completar</label>
            			</div>
            		</div>

					<div id="balance" class="form-group collapse">
						<label class="control-label">Troco</label>
            			<input type="text" name="balance" class="form-control" readonly>
					</div>

					<h1 id="total-sell" class="collapse">Total: <strong>R$<span class="total-buy"></span></strong></h1>

                    <div class="form-group">
                        <button type="submit" name="complete" class="btn btn-primary">Concluir pedido</button>
                    </div>
            	</div>
            	<div class="clearfix"></div>
                </form>
            </div>
        </div>
	</div>
</div>