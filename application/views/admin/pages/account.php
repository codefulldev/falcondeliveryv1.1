<?php
$CI =& get_instance();
$admin = $CI->UserController();
$first_name = $CI->helps->first_name($admin->name);
$thumbnail = $CI->helps->verify_avatar($admin->thumbnail, 'admin');
$base = base_url().$CI->base;
?>
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Minha conta</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="<?php echo $base ?>">Página inicial</a></li>
                    <li class="active">Minha conta</li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4 col-xs-12">
                <div class="white-box">
                    <div class="user-bg"> <img width="100%" id="background-pic" alt="user" src="<?= $thumbnail ?>">
                        <div class="overlay-box">
                            <label id="change_profile__picture" class="fcbtn btn btn-xs btn-info" style="position: absolute;right: 10px;top:10px;">
                                <i class="fa fa-camera"></i>
                                <input type="file" name="profile" id="change_profile__picture" style="display: none;">
                                <input type="hidden" name="thumbnail" value="<?= $admin->email ?>">
                            </label>
                            <div class="user-content">
                                <a href="javascript:void(0)"><img id="profile-pic" src="<?= $thumbnail ?>" class="thumb-lg img-circle" alt="img"></a>
                                <h4 class="text-white json-name"><?= $admin->name ?></h4>
                                <h5 class="text-white json-email"><?= $admin->email ?></h5> 
                            </div>
                        </div>
                    </div>
                    <div class="user-btm-box hidden">
                        <div class="col-md-4 col-sm-4 text-center">
                            <p class="text-purple"><i class="ti-facebook"></i></p>
                            <h1>258</h1> 
                        </div>
                        <div class="col-md-4 col-sm-4 text-center">
                            <p class="text-blue"><i class="ti-twitter"></i></p>
                            <h1>125</h1> 
                        </div>
                        <div class="col-md-4 col-sm-4 text-center">
                            <p class="text-danger"><i class="ti-dribbble"></i></p>
                            <h1>556</h1> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-xs-12">
                <div class="white-box">
                    <ul class="nav nav-tabs tabs customtab">
                        <li class="tab active">
                            <a href="#settings" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Minha conta</span> </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="settings">
                            <form role="admin_account" class="form-horizontal form-material">
                                <div class="form-group">
                                    <label id="admin-name" class="col-md-12">Nome</label>
                                    <div class="col-md-12">
                                        <input type="text" placeholder="<?= $admin->name ?>" value="<?= $admin->name ?>" class="form-control form-control-line" name="name" id="admin-name"> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="admin-email" class="col-md-12">Email</label>
                                    <div class="col-md-12">
                                        <input type="email" placeholder="<?= $admin->email ?>" value="<?= $admin->email ?>" class="form-control form-control-line" name="email" id="admin-email"> 
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label id="admin-password" class="col-md-12">Senha</label>
                                    <div class="col-md-12">
                                        <input type="password" class="form-control form-control-line" name="password" value="" id="admin-password"> 
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button class="btn btn-success">Salvar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>