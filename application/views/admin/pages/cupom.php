<?php 
    $CI =& get_instance(); 
    $base = base_url().$CI->base;
?>
<div class="container-fluid" ng-controller="CupomController">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">{{cupomPageTitle}}</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="<?php echo $base ?>">Página inicial</a></li>
                    <li class="active">{{cupomPageTitle}}</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <h3 class="box-title">{{cupomPageTitle}} adicionadas</h3>
                <div class="scrollable">
                    <div class="table-responsive">
                        <table class="table m-t-30 table-hover contact-list" data-page-size="10">
                            <button type="button" class="btn btn-primary btn-rounded actions-btn" data-toggle="modal" data-target="#addCupom">Adicionar cupom</button>
                            <thead>
                                <tr>
                                    <th>Cod. Cupom</th>
                                    <th>Nome</th>
                                    <th>Desconto</th>
                                    <th>Validade</th>
                                    <th>Status</th>
                                    <th>Ações</th>
                                </tr>
                            </thead>
                            <tbody id="cupom">
                                <tr ng-repeat="cup in cupons" id="{{cup.id}}">
                                    <td data-id="{{cup.id}}" data-type="name">{{cup.genered}}</td>
                                    <td data-id="{{cup.id}}" data-type="name">{{cup.title}}</td>
                                    <td data-id="{{cup.id}}" data-type="value">{{cup.value}}%</td>
                                    <td data-id="{{cup.id}}" data-type="expire">{{ cup.expire | formatDate : "dd/MM/yyyy" }}</td>
                                    <td>
                                    <div ng-if="cup.status == 0">
                                        <span class="label label-danger change_status" data-toggle="tooltip" title="Mudar status" data-id="{{cup.id}}" data-type="cupons" data-provider="{{cup.status}}" style="cursor:pointer;">Indisponível</span>
                                    </div>

                                    <div ng-if="cup.status == 1">
                                        <span class="label label-success change_status" data-toggle="tooltip" title="Mudar status" data-id="{{cup.id}}" data-type="cupons" data-provider="{{cup.status}}" style="cursor:pointer;">Disponível</span>
                                    </div>

                                    </td>
                                    <td>
                                     <button type="button" data-id="{{cup.id}}" data-role="cupons" data-toggle="modal" data-target="#editItem" class="btn btn-info btn-outline btn-circle btn-xs m-r-5 edit">
                                        <i class="ti-pencil"></i>
                                     </button>
                                     <button type="button" data-id="{{cup.id}}" class="btn btn-info btn-outline btn-circle btn-xs m-r-5 remove">
                                        <i class="ti-trash"></i>
                                     </button>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="7">
                                        <div class="text-right">
                                            <ul class="pagination"> </ul>
                                        </div>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>           
        </div>
    </div>

    
    <div id="addCupom" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Adicionar {{cupomPageTitle}}</h4>
                </div>

              <form method="post" role="cupom">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-sm-12">Titulo do cupom</label>
                        <div class="col-sm-12">
                            <input type="text" name="title" class="form-control" placeholder="Titulo" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-12">Desconto</label>
                        <div class="col-sm-12">
                            <input type="text" name="value" class="form-control" placeholder="Valor do desconto" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-12">Validade</label>
                        <div class="col-sm-12">
                            <input type="date" name="expire" class="form-control" placeholder="25/05/2018">
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success waves-effect">Adicionar</button>
                    <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Fechar</button>
                </div>
              </form>

            </div>

        </div>

    </div>
    
    <div id="editItem" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel">Editar {{cupomPageTitle}}</h4>
                </div>

              <form method="post" role="editItems" data-table="cupons" id="">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-sm-12">Titulo do cupom</label>
                        <div class="col-sm-12">
                            <input type="text" name="title" class="title form-control" placeholder="Titulo" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-12">Desconto</label>
                        <div class="col-sm-12">
                            <input type="text" name="value" class="value form-control" placeholder="Valor do desconto" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-12">Validade</label>
                        <div class="col-sm-12">
                            <input type="date" name="expire" class="expire form-control" placeholder="25/05/2018">
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success waves-effect">Editar</button>
                    <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Fechar</button>
                </div>
              </form>

            </div>

        </div>

    </div>

</div>

