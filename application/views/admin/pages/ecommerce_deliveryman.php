<?php 
	$CI =& get_instance(); 
	$base = base_url().$CI->base;
?>
<div class="container-fluid" ng-controller="ClientsController">
	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title">Entregadores</h4> </div>
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
				<ol class="breadcrumb">
					<li><a href="<?php echo $base ?>">Página inicial</a></li>
					<li class="active">Entregadores</li>
				</ol>
			</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="white-box">
				<div id="reports" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
								<h4 class="modal-title" id="myModalLabel">Fechamento</h4> 
							</div>
							<div id="reports_load" class="modal-body"></div>
							<div class="modal-footer">
								<button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Fechar</button>
							</div>
						</div>
					</div>
				</div>
				<h3 class="box-title">Todos os entregadores</h3>
				<div id="orders" class="scrollable">
					<div id="orders_table" class="table-responsive">
						<table id="demo-foo-addrow" class="table m-t-30 table-hover contact-list" data-page-size="10">
							<button type="button" class="btn btn-primary btn-rounded actions-btn" data-toggle="modal" data-target="#addDeliveryman">Adicionar entregador</button>
							<thead>
								<tr>
									<th>ID</th>
									<th>Nome</th>
									<th>CPF</th>
									<th>Placa</th>
									<th>Status</th>
									<th>Ação</th>
								</tr>
							</thead>
							<tbody id="deliveryman">
							<?php 
							 $deliverymans = $CI->ecommerce_deliveryman_list();
							 foreach ($deliverymans as $deliveryman): 
							 if ($deliveryman->id > 0) {
							?>
							<tr id="deliveryman_<?= $deliveryman->id ?>">
								<td><a href="javascript:void(0)"><?= $deliveryman->id ?></a></td>
								<td><?= ucwords($deliveryman->name) ?></td>
								<td><?= $CI->helps->cpf($deliveryman->cpf) ?></td>
								<td><?= $CI->helps->board($deliveryman->board) ?></td>
								<td class="status_<?= $deliveryman->id ?>">
									<?php if ($deliveryman->status == 1): ?>
										<span class="label label-success">
											Livre
										</span>
									<?php else: ?>
										<span class="label label-warning">
											Em entrega
										</span>
									<?php endif ?>
								</td>
								<td>
								   <a href="javascript: void(0)" data-id="<?= $deliveryman->id ?>" rel="<?= $deliveryman->status ?>" class="btn btn-info btn-outline btn-circle btn-xs m-r-5 change-status" data-toggle="tooltip" title="Mudar status">
								      <i class="ti-arrows-horizontal"></i>
								   </a>
								   <a href="javascript: void(0)" data-id="<?= $deliveryman->id ?>" class="btn btn-info btn-outline btn-circle btn-xs m-r-5 reports" data-toggle="modal" data-target="#reports" title="Relatórios">
								      <i class="ti-stats-up"></i>
								   </a>
								   <button type="button" data-id="<?= $deliveryman->id ?>" class="btn btn-info btn-outline btn-circle btn-xs m-r-5 remove" data-toggle="tooltip" title="Apagar entregador">
								      <i class="ti-trash"></i>
								   </button>
								</td>
							</tr>		
							<?php 
							 }
							 endforeach;
							?>	
							</tbody>
							<tfoot>
								<tr>
									<td colspan="7">
										<div class="text-right">
											<ul class="pagination"> </ul>
										</div>
									</td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>

			</div>           
		</div>
	</div>

	
	<div id="addDeliveryman" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myModalLabel">Adicionar entregador</h4>
				</div>

			  <form method="post" role="deliveryman">
				<div class="modal-body">
					<div class="form-group">
						<label class="col-sm-12">Nome do entregador</label>
						<div class="col-sm-12">
							<input type="text" name="name" class="form-control" placeholder="Nome" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-12">CPF</label>
						<div class="col-sm-12">
							<input type="text" name="cpf" class="form-control" placeholder="CPF" maxlength="11" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-12">Placa da(o) motocicleta/veiculo</label>
						<div class="col-sm-12">
							<input type="text" name="board" class="form-control" placeholder="Placa" maxlength="8" required>
						</div>
					</div>

					<div class="clearfix"></div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success waves-effect">Adicionar</button>
					<button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Fechar</button>
				</div>
			  </form>

			</div>
		</div>
	</div>

</div>