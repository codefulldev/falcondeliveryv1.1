<?php 
$CI =& get_instance(); 
$base = base_url().$CI->base;
$explode = explode("/", $_SERVER['REQUEST_URI']);
$get_id = end($explode);
$base = base_url().$CI->base;

if (!is_numeric($get_id)) {
	redirect($base.'clients');
}

$CI->db->where('id', $get_id);
$client_get = $CI->db->get('clients');

$address = $CI->config_address();
$city = explode(" - ", $address->city);
$districts = isset($address->districts) ? explode("|", $address->districts) : null;
$districts_array = array();
if (!is_null($districts)) {
    foreach ($districts as $id_bairro) {
        $CI->db->where("id_bairro", $id_bairro);
        $get = $CI->db->get("cepbr_bairro");
        if ($get->num_rows() > 0) {
            array_push($districts_array, $get->row()->bairro);
        }
    }
}

?>
<div class="container-fluid">
	<?php if ($client_get->num_rows() < 1): ?>
	<section id="wrapper" class="error-page">
        <div class="error-box">
            <div class="error-body text-center">
                <h1 class="text-danger">:(</h1>
                <h3 class="text-uppercase">Cliente não encontrado!</h3>
                <p class="text-muted m-t-30 m-b-30">Verifique o número do ID e tente novamente.</p>
                <a href="<?= $base.'clients' ?>" class="btn btn-danger btn-rounded waves-effect waves-light m-b-40">Todos os clientes</a> </div>
        </div>
    </section>
	<?php else: 
	$client = $client_get->row();
	$first_name = $CI->helps->first_name($client->name);
	$thumbnail = $CI->helps->verify_avatar($client->thumbnail, 'client');
	?>
	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title">Visualizando: <?= $client->name ?></h4> </div>
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
				<ol class="breadcrumb">
					<li><a href="<?php echo $base ?>">Página inicial</a></li>
					<li class="active">Editar cliente</li>
				</ol>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4 col-xs-12">
				<div class="white-box">
					<div class="user-bg"> <img width="100%" id="background-pic" alt="user" src="<?= $thumbnail ?>">
						<div class="overlay-box">
							<label id="change_profile__picture" class="fcbtn btn btn-xs btn-info" style="position: absolute;right: 10px;top:10px;">
								<i class="fa fa-camera"></i>
								<input type="file" name="profile" id="change_profile__picture" style="display: none;">
								<input type="hidden" name="thumbnail" value="<?= $client->email ?>">
							</label>
							<div class="user-content">
								<a href="javascript:void(0)"><img id="profile-pic" src="<?= $thumbnail ?>" class="thumb-lg img-circle" alt="img"></a>
								<h4 class="text-white json-name"><?= $client->name ?></h4>
								<h5 class="text-white json-email"><?= $client->email ?></h5> 
							</div>
						</div>
					</div>
					<div class="user-btm-box">
						<div class="col-md-4 col-sm-4 text-center">
							
						</div>
						<div class="col-md-4 col-sm-4 text-center">
							<p class="text-blue" data-toggle="tooltip" title="Compras realizadas"><i class="ti-shopping-cart"></i></p>
							<h1><?php $CI->db->where('clients_id', $get_id); echo $CI->db->get('orders')->num_rows(); ?></h1> 
						</div>
						<div class="col-md-4 col-sm-4 text-center">
							
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-8 col-xs-12">
				<div class="white-box">
					<ul class="nav nav-tabs tabs customtab">
						<li class="tab active">
							<a href="#settings" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Dados da usuário</span> </a>
						</li>
						<li class="tab">
							<a href="#address" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">Endereços</span> </a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="settings">
							<form role="clients_account" class="form-horizontal form-material">
								<div class="form-group">
									<label id="client-name" class="col-md-12">Nome</label>
									<div class="col-md-12">
										<input type="text" placeholder="<?= $client->name ?>" value="<?= $client->name ?>" class="form-control form-control-line" name="name" id="client-name"> 
										<input type="hidden" value="<?= $client->id ?>" name="id"> 
									</div>
								</div>
								<div class="form-group">
									<label for="client-email" class="col-md-12">Telefone</label>
									<div class="col-md-12">
										<input type="text" placeholder="<?= $client->phone ?>" value="<?= $client->phone ?>" class="form-control form-control-line" name="phone" id="client-phone"> 
									</div>
								</div>
								<div class="form-group">
									<label for="client-email" class="col-md-12">Email</label>
									<div class="col-md-12">
										<input type="email" placeholder="<?= $client->email ?>" value="<?= $client->email ?>" class="form-control form-control-line" name="email" id="client-email"> 
									</div>
								</div>
								<div class="form-group">
									<label id="client-password" class="col-md-12">Senha</label>
									<div class="col-md-12">
										<input type="password" class="form-control form-control-line" name="password" value="" id="client-password"> 
									</div>
								</div>

								<div class="form-group">
									<div class="col-sm-12">
										<button class="btn btn-success">Salvar</button>
										<p class="m-t-20"><a href="javascript:void(0)" class="text-danger remove" data-id="<?= $client->id ?>">Excluir cliente</a></p>
									</div>
								</div>
							</form>
						</div>
						<div class="tab-pane" id="address">
							<div class="table-responsive manage-table">
								<?php
									$CI->db->where('clients_id', $get_id);
									$addressr = $CI->db->get('clients_address');
								?>
								<?php if ($addressr->num_rows() > 0): ?>
                                
                                <div id="editline" style="display:none">
                                    <form role="editclientaddress" method="post">
                                        <div class="form-group">
                                            <label for="nameinput">Nome do endereço</label>
                                            <input type="hidden" id="idinput" name="id" />
                                            <input type="text" id="nameinput" name="name" class="form-control" />
                                        </div>
                                        <div class="form-group">
                                            <label for="cepinput">CEP</label>
                                            <input type="text" id="cepinput" name="cep" data-location="<?= trim($city[0]) ?>" data-districts="<?= join(",", $districts_array) ?>" class="form-control" />
                                        </div>
                                        <div class="form-group">
                                            <label for="addressinput">Endereço</label>
                                            <input type="text" id="addressinput" name="address" class="form-control" readonly="true" />
                                        </div>
                                        <div class="form-group">
                                            <label for="numberinput">Número</label>
                                            <input type="text" id="numberinput" name="number" class="form-control" />
                                        </div>
                                        <div class="form-group">
                                            <label for="complementinput">Complemento</label>
                                            <input type="text" id="complementinput" name="complement" class="form-control" />
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-sm btn-info save-submit">Salvar</button>
                                        </div>
                                    </form>
                                </div>
								<table class="table" cellspacing="14">
									<thead>
										<tr>
											<th>NOME</th>
											<th>ENDEREÇO</th>
											<th>CIDADE</th>
											<th>CEP</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($addressr->result() as $row): 
										list($addressClient, $numberClient, $referenceClient) = explode("|", $row->address);
										list($street, $district, $city) = explode(", ", $addressClient);
										?>
											<tr id="row_client--<?= $row->id ?>" class="advance-table-row active">
												<td class="name<?= $row->id ?>"><?= $row->name ?></td>
                                                <td class="address<?= $row->id ?>"><?= $street.', '.$district.', Nº '.$numberClient ?></td>
												<td class="city<?= $row->id ?>"><?= $city ?></td>
												<td class="cep<?= $row->id ?>"><?= $row->cep ?></td>
												<td>
                                                    <button data-id="<?= $row->id ?>" class="btn btn-xs btn-warning deleteclient">Apagar</button>
                                                    <button data-id="<?= $row->id ?>" data-name="<?= $row->name ?>" data-address="<?= $addressClient ?>" data-number="<?= $numberClient ?>" data-complement="<?= $referenceClient ?>" data-cep="<?= $row->cep ?>" class="btn btn-xs btn-success editclient editable<?= $row->id ?>">Editar</button>
                                                </td>
											</tr>
											<tr>
												<td colspan="7" class="sm-pd"></td>
											</tr>
										<?php endforeach ?>
									</tbody>
								</table>
								<?php else: ?>
									<h3><?= $first_name ?> não registrou nenhum endereço!</h3>
								<?php endif ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif ?>
	</div>