<?php 
	$CI =& get_instance(); 
	$base = base_url().$CI->base;
?>
<div class="container-fluid" ng-controller="IngredientsController">
	<div class="row bg-title">

		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title">{{ingredientsPageTitle}}</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
               <li><a href="<?php echo $base ?>">Página inicial</a></li>
               <li class="active">{{ingredientsPageTitle}}</li>
           </ol>
       </div>

   </div>

    
   <div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <h3 class="box-title">{{ingredientsPageTitle}} adicionados</h3>
            <div class="scrollable">
                <div class="table-responsive">
                    <table class="table m-t-30 table-hover contact-list" data-page-size="10">
                        <button type="button" class="btn btn-primary btn-rounded actions-btn" data-toggle="modal" data-target="#addIngredients">Adicionar ingrediente</button>
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Sobre</th>
                                <th>Preço</th>
                                <th>Status</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody id="ingredients">
                            <tr ng-repeat="ingredient in ingredients" id="{{ingredient.id}}">
                                <td data-id="{{ingredient.id}}" data-type="name">{{ingredient.name}}</td>
                                <td data-id="{{ingredient.id}}" data-type="about">{{ingredient.about}}</td>
                                <td data-id="{{ingredient.id}}" data-type="price">{{ingredient.price | currency:'R$'}}</td>
                                <td>
                                    <div ng-if="ingredient.status == 0">
                                        <span class="label label-danger change_status" data-toggle="tooltip" title="Mudar status" data-id="{{ingredient.id}}" data-type="addons" data-provider="{{ingredient.status}}" style="cursor:pointer;">Indisponível</span>
                                    </div>
                                    <div ng-if="ingredient.status == 1">
                                        <span class="label label-success change_status" data-toggle="tooltip" title="Mudar status" data-id="{{ingredient.id}}" data-type="addons" data-provider="{{ingredient.status}}" style="cursor:pointer;">Disponível</span>
                                    </div>
                                </td>
                                <td>
                                    <button type="button" data-id="{{ingredient.id}}" data-role="ingredients" data-toggle="modal" data-target="#editItem" class="btn btn-info btn-outline btn-circle btn-xs m-r-5 edit">
                                        <i class="ti-pencil"></i>
                                    </button>
                                    <button type="button" data-id="{{ingredient.id}}" class="btn btn-info btn-outline btn-circle btn-xs m-r-5 remove">
                                        <i class="ti-trash"></i>
                                    </button>
                            </td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="7">
                                <div class="text-right">
                                    <ul class="pagination"> </ul>
                                </div>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>           
</div>
</div>


<div id="addIngredients" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Adicionar {{ingredientsPageTitle}}</h4>
            </div>

            <form method="post" role="ingredients">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-sm-12">Nome do ingrediente</label>
                        <div class="col-sm-12">
                            <input type="text" name="name" class="form-control" placeholder="Ingrediente" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-12">Preço</label>
                        <div class="col-sm-12">
                            <input type="text" name="price" class="form-control money" placeholder="00,00">
                            <span class="help-block"><small>Se for grátis, deixe em branco.</small></span>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-12">Sobre o ingrediente</label>
                        <div class="col-sm-12">
                            <textarea name="about" class="form-control" placeholder="Descrição" rows="3" style="resize: vertical;min-height:90px;max-height:180px;"></textarea>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-lg-12">
                          <div class="checkbox checkbox-success checkbox-circle">
                           <input id="checkbox-10" name="status" type="checkbox" value="1" checked="">
                           <label for="checkbox-10"> Disponível </label>
                       </div>
                   </div>
               </div>

               <div class="clearfix"></div>
           </div>
           <div class="modal-footer">
            <button type="submit" class="btn btn-success waves-effect">Adicionar</button>
            <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Fechar</button>
        </div>
    </form>

</div>

</div>

</div>


<div id="editItem" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Editar {{ingredientsPageTitle}}</h4>
            </div>
            <form method="post" role="editItems" data-table="ingredients" id="">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-sm-12">Nome do ingrediente</label>
                        <div class="col-sm-12">
                            <input type="text" name="name" class="form-control name" placeholder="Ingrediente" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-12">Preço</label>
                        <div class="col-sm-12">
                            <input type="text" name="price" class="form-control price money" placeholder="00,00">
                            <span class="help-block"><small>Se for grátis, deixe em branco.</small></span>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-12">Sobre o ingrediente</label>
                        <div class="col-sm-12">
                            <textarea name="about" class="form-control about" placeholder="Descrição" rows="3" style="resize: vertical;min-height:90px;max-height:180px;"></textarea>
                        </div>
                    </div>
                    

                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success waves-effect">Editar</button>
                    <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Fechar</button>
                </div>
            </form>
        </div>
    </div>
</div>

</div>
