<?php 
$CI =& get_instance(); 
$base = $CI->base;
?>
<div class="container-fluid">
	
	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title">Atendimento Online</h4> </div>
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
				<ol class="breadcrumb">
					<li><a href="<?php echo $base ?>">Página Inicial</a></li>
					<li class="active">Chat Online</li>
				</ol>
			</div>
			<!-- /.col-lg-12 -->
		</div>

		<!-- .chat-row -->
		<div class="chat-main-box">
			<!-- .chat-left-panel -->
			<div class="chat-left-aside">
				<div class="open-panel"><i class="ti-angle-right"></i></div>
				<div class="chat-left-inner">
					<div class="form-material">
						<input class="form-control p-20" type="text" placeholder="Procurar cliente">
					</div>
					<ul id="listOnlineUsers" class="chatonline style-none ">
						<?php foreach ($CI->get_chatting_users() as $client): ?>
						<li id="li--client__list_<?= $client->id ?>">
							<a href="<?php echo $base ?>chat/<?= $client->id ?>">
								<img src="<?= $CI->helps->verify_client_thumbnail($client->thumbnail) ?> " alt="<?= $client->name ?> " class="img-circle"> 
								<span>
									<?= $client->name ?> 
									<small id="status_client__<?= $client->id ?>" class="text-muted">offline</small>
								</span>
							</a>
						</li>
					<?php endforeach ?>
					<li class="p-20"></li>
				</ul>
			</div>
		</div>
		<!-- .chat-left-panel -->
		<!-- .chat-right-panel -->
		<div class="chat-right-aside">
			<?php 
			$request = explode("/", $_SERVER['REQUEST_URI']);
			$id = end($request);

			if ($id < 1): ?>

		<?php else: 
			$CI->db->where('id', $id);
			$get_client = $CI->db->get('clients');
			$client = $get_client->row();
		?>
			<div id="window__starting__<?= $id ?>" class="window-chatting_is__opening chat-main-header">
				<div class="p-20 b-b">
					<h3 class="box-title">Chat Message</h3> </div>
				</div>
                <style type="text/css">
                    .slimScrollDiv {
                        min-height: 700px !important;
                    }
                </style>
				<div id="conversation-box" class="chat-box" style="height: 700px">
					<ul id="content__chatting__<?= $id ?>" class="chat-list slimscroll-chat p-t-30" style="min-height: 700px">
						<?php if($CI->get_chatting($id)): foreach ($CI->get_chatting($id) as $row): ?>
							<?php if ($row->send == 1): 

							if ($get_client->num_rows() > 0) {
								?>
								<li id="box__<?= $row->id ?>" data-chat="<?= $id ?>">
									<div class="chat-image"> <img alt="<?= $client->name ?>" src="<?= $CI->helps->verify_client_thumbnail($client->thumbnail) ?>"> </div>
									<div class="chat-body">
										<div class="chat-text">
											<h4><strong><?= $client->name ?></strong></h4>
											<p> <?= $row->message ?> </p> <b><span class="time" title="<?= $row->date ?>"></span></b> 
										</div>
									</div>
								</li>
								<?php } ?>

							<?php else: 

							$CI->db->where('id', $row->func_id);
							$get_admin = $CI->db->get('admin');

							if ($get_admin->num_rows() > 0) {
								$admin = $get_admin->row();
								?>
								<li id="box__<?= $row->id ?>" class="odd">
									<div class="chat-image"> <img alt="<?= $admin->name ?>" src="<?= $CI->helps->verify_client_thumbnail($admin->thumbnail) ?>"> </div>
									<div class="chat-body">
										<div class="chat-text">
											<h4><strong><?= $admin->name ?></strong></h4>
											<p> <?= $row->message ?> </p> <b><span class="time" title="<?= $row->date ?>"></span></b> 
										</div>
									</div>
								</li>
								<?php } endif ?>
							<?php endforeach; endif;?>
						</ul>

						<div class="row send-chat-box">
							<div class="col-sm-12">
								<textarea id="sendtextarea" class="form-control" placeholder="Escreva uma resposta..."></textarea>
								<div class="custom-send">
									<button id="sendbutton" data-client="<?= $id ?>" class="btn btn-danger btn-rounded" type="button">Enviar</button>
								</div>
							</div>
						</div>
					</div>
				<?php endif ?>
			</div>

		</div>

	</div>