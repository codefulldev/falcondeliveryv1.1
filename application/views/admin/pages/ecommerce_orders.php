<?php 
	$CI =& get_instance(); 
	$base = base_url().$CI->base;
?>
<div class="container-fluid" ng-controller="OrdersController">
	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title">{{ordersPageTitle}}</h4> </div>
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
				<ol class="breadcrumb">
					<li><a href="<?php echo $base ?>">Página inicial</a></li>
					<li class="active">{{ordersPageTitle}}</li>
				</ol>
			</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="white-box">
				<h3 class="box-title">Todos os {{ordersPageTitle}}</h3>
				<div id="orders" class="scrollable">
					<div id="orders_table" class="table-responsive">
						<table id="demo-foo-addrow" class="table m-t-30 table-hover contact-list" data-page-size="10">
							<thead>
								<tr>
									<th>ID</th>
									<th>Cliente</th>
									<th>Data</th>
									<th>Status</th>
									<th>Total</th>
									<th>Ações</th>
								</tr>
							</thead>
							<tbody>
							<?php 
							 $orders_list = $CI->invoice->orders_list();
							 foreach ($orders_list->result() as $order): 
							 if ($order->clients_id > 0) {
							 	$CI->db->where('id', $order->clients_id);
							 	$client = $CI->db->get('clients')->row();
							 	$client_name = $client->name;
							 }else{
							 	$client_name = 'Balcão';
							 }
							 $details = $CI->db->get('orders_details', array('order_id' => $order->id));
							?>
							<tr id="order_<?= $order->id ?>">
								<td><a href="javascript:void(0)"><?= $order->id ?></a></td>
								<td><?= $client_name ?></td>
								<td><?= date('d/m/Y H:i', strtotime($order->date)) ?></td>
								<td>
								  <?php
								  switch ($order->status) {
								  	case 0:
								  		echo '<span id="order_status_html_'. $order->id .'" class="label label-warning" data-toggle="collapse" data-target="#order_status_'. $order->id .'" title="Clique para mudar o status" style="cursor:pointer" data-toggle="tooltip">Pendente</span>';
								  		break;
								  	case 1:
								  		echo '<span id="order_status_html_'. $order->id .'" class="label label-success" data-toggle="collapse" data-target="#order_status_'. $order->id .'" title="Clique para mudar o status" style="cursor:pointer" data-toggle="tooltip">Completo</span>';
								  		break;
								  	default:
								  		echo '<span id="order_status_html_'. $order->id .'" class="label label-danger" data-toggle="collapse" data-target="#order_status_'. $order->id .'" title="Clique para mudar o status" style="cursor:pointer" data-toggle="tooltip">Cancelado</span>';
								  		break;
								  }
								  ?>
								  <div class="form-group m-t-10">
								  	<select id="order_status_<?= $order->id ?>" data-id="<?= $order->id ?>" class="form-control change_order_status collapse">
								  		<option value="0">Pendente</option>
								  		<option value="1">Completo</option>
								  		<option value="2">Cancelado</option>
								  	</select>
								  </div>
								</td>
								<td>
									R$<?php echo $CI->invoice->invoice_order_total($order->id); ?>
								</td>
								<td>
								   <button type="button" onclick="window.location.href='<?= $base.'invoice/'.$order->id ?>'" class="btn btn-info btn-outline btn-circle btn-xs m-r-5" data-toggle="tooltip" title="Ver pedido">
								      <i class="ti-eye"></i>
								   </button>
								   <button type="button" data-id="<?= $order->id ?>" class="btn btn-info btn-outline btn-circle btn-xs m-r-5 remove" data-toggle="tooltip" title="Excluir pedido">
								      <i class="ti-trash"></i>
								   </button>
								</td>
							</tr>		
							<?php 
							 endforeach 
							?>	
							</tbody>
							<tfoot>
								<tr>
									<td colspan="7">
										<div class="text-right">
											<ul class="pagination"> </ul>
										</div>
									</td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>           
		</div>
	</div>

	
	<div id="addCategory" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myModalLabel">Adicionar {{ordersPageTitle}}</h4>
				</div>

			  <form method="post" role="category">
				<div class="modal-body">
					<div class="form-group">
						<label class="col-sm-12">Nome da categoria</label>
						<div class="col-sm-12">
							<input type="text" name="name" class="form-control" placeholder="Categoria" required>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-lg-12">
						  <div class="checkbox checkbox-success checkbox-circle">
                             <input id="checkbox-10" name="status" type="checkbox" value="1" checked="">
                           	 <label for="checkbox-10"> Disponível </label>
                          </div>
						</div>
					</div>

					<div class="clearfix"></div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success waves-effect">Adicionar</button>
					<button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Fechar</button>
				</div>
			  </form>

			</div>

		</div>

		</div>

</div>

