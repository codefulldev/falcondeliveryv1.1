<?php 
	$CI =& get_instance(); 
	$base = base_url().$CI->base;
?>
<div class="container-fluid" ng-controller="ProductsController">
	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title">{{productPageTitle}}</h4> </div>
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
				<ol class="breadcrumb">
					<li><a href="<?php echo $base ?>">Página inicial</a></li>
					<li class="active">{{productPageTitle}}</li>
				</ol>
			</div>
			<!-- /.col-lg-12 -->
	</div>

	<div class="row tabber list_product">
		<div class="col-md-12">
			<div class="white-box">
				<h3 class="box-title">Produtos adicionados</h3>
				<div class="scrollable">
					<div class="table-responsive">
						<table id="demo-foo-addrow" class="table m-t-30 table-hover contact-list" data-page-size="10">
							<button type="button" class="btn btn-primary btn-rounded actions-btn" data-target="add_product">Adicionar produto</button>
							<thead>
								<tr>
									<th>ID</th>
									<th>Nome</th>
									<th>Preço</th>
									<th>Status</th>
									<th>Estoque/Compras</th>
									<th>Ações</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="product in products" data-row="table-row-{{product.id}}">
									<td>{{product.id}}</td>
									<td>
										<a href="#">
											<img ng-src="themes/uploads/thumbnail/{{product.thumbnail}}" alt="user" class="img-circle" /> 
											{{product.name}}
										</a>
									</td>
									<td>{{product.price | currency:'R$'}}</td>
									<td>
										<div ng-if="product.status == 0">
											<span class="label label-danger change_status" data-toggle="tooltip" title="Mudar status" data-id="{{product.id}}" data-type="products" data-provider="{{product.status}}" style="cursor:pointer;">Indisponível</span>
										</div>

										<div ng-if="product.status == 1">
											<span class="label label-success change_status" data-toggle="tooltip" title="Mudar status" data-id="{{product.id}}" data-type="products" data-provider="{{product.status}}" style="cursor:pointer;">Disponível</span>
										</div>
									</td>
									<td>{{product.stock}}/{{product.sells}}</td>
									<td>
										<button type="button" data-id="{{product.id}}" class="btn btn-sm btn-icon btn-pure btn-outline delete-row" data-toggle="tooltip" data-original-title="Remover"><i class="ti-trash" aria-hidden="true"></i></button>

										<a href="<?= $base.$CI->productURI ?>/edit/{{product.id}}" class="btn btn-sm btn-icon btn-pure btn-outline actions-btn" data-toggle="tooltip" data-original-title="Editar"><i class="ti-settings" aria-hidden="true"></i></a>
									</td>
								</tr>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="7">
										<div class="text-right">
											<ul class="pagination"> </ul>
										</div>
									</td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>           
		</div>
	</div>

	<form id="056cf7d70d3010713f4384149c6efd1f078f90f9" method="post" role="056cf7d70d3010713f4384149c6efd1f078f90f9" class="tabber add_product collapse">

		<div class="row">
			<div class="col-lg-12 white-box">

				<h1 class="box-title">Adicionar produto</h1>
				<button type="button" class="btn btn-primary btn-rounded actions-btn" data-target="list_product">Lista de produtos</button>
				<hr>

				<div class="row">

					<div class="col-md-6">
						<div class="form-group">
							<label class="box-title">Nome do produto</label>
							<input type="text" name="name" id="firstName" class="form-control" placeholder="Nome do produto"> 
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label class="box-title">Status</label>
							<div class="radio-list">
								<label class="radio-inline p-0">
									<div class="radio radio-info">
										<input type="radio" name="status" id="radio1" value="1" checked>
										<label for="radio1">Disponível</label>
									</div>
								</label>
								<label class="radio-inline">
									<div class="radio radio-info">
										<input type="radio" name="status" id="radio2" value="0">
										<label for="radio2">Indisponível</label>
									</div>
								</label>
							</div>
						</div>

					</div>

				</div>

				<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
							<label class="box-title">Venda</label>
							<div class="radio-list">
								<label class="radio-inline p-0">
									<div class="radio radio-info">
										<input type="radio" name="sale" id="sales1" value="0" checked>
										<label for="sales1">Ativada</label>
									</div>
								</label>
								<label class="radio-inline">
									<div class="radio radio-info">
										<input type="radio" name="sale" id="sales2" value="1">
										<label for="sales2">Desativada</label>
									</div>
								</label>
							</div>
						</div>
					</div>

					<div class="col-lg-6">
						<div class="form-group">
							<label class="box-title">Estoque</label>
							<input name="stock" type="number" min="1" value="1" class="form-control">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="box-title">Categoria</label>
							<select class="selectpicker form-control" name="category" title="Escolha uma categoria" data-style="form-control">
								<?php if (count($CI->model_foreach_select('category', '1')) < 1): ?>
									<option value="" disabled>Nenhuma categoria</option>
								<?php else: ?>

									<?php foreach ($CI->model_foreach_select('category', '1') as $category): ?>
										<option value="<?= $category->id ?>"><?= $category->name ?></option>
									<?php endforeach ?>

								<?php endif ?>
							</select>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label class="box-title">Preço</label>
							<div class="input-group">
								<div class="input-group-addon">R$</div>
								<input type="text" name="price" class="form-control money" placeholder="00,00"> 
							</div>
						</div>
					</div>

					</div>

					<div class="row">

						<div class="col-lg-6 col-sm-6 col-xs-12 m-t-20">
							<h6 class="box-title">Adicionais</h6>
							<select class="selectpicker form-control" name="addons[]" multiple title="Escolha os adicionais" data-style="form-control">
								<?php if (count($CI->model_foreach_select('addons', '1')) < 1): ?>
									<option value="" selected disabled>Nenhum adicional</option>
								<?php else: ?>

									<?php foreach ($CI->model_foreach_select('addons', '1') as $addons): ?>
										<option value="<?= $addons->id ?>"><?= $addons->name ?></option>
									<?php endforeach ?>

								<?php endif ?>
							</select>
						</div>

						<div class="col-lg-6 col-sm-6 col-xs-12 m-t-20">
							<h6 class="box-title">Ingredientes</h6>
							<select class="selectpicker form-control" name="ingredients[]" multiple title="Escolha os ingredientes" data-style="form-control">
								<?php if (count($CI->model_foreach_select('ingredients', '1')) < 1): ?>
									<option value="" disabled>Nenhum ingrediente</option>
								<?php else: ?>

									<?php foreach ($CI->model_foreach_select('ingredients', '1') as $ingredients): ?>
										<option value="<?= $ingredients->id ?>"><?= $ingredients->name ?></option>
									<?php endforeach ?>

								<?php endif ?>
							</select>
						</div>

						<div class="col-lg-6 col-sm-6 col-xs-12 m-t-20">
							<h6 class="box-title">Opcionais</h6>
							<select class="selectpicker form-control" name="optionals[]" multiple title="Escolha os opcionais" data-style="form-control">
								<?php if (count($CI->model_foreach_select('optionals', '1')) < 1): ?>
									<option value="" disabled>Nenhum ingrediente</option>
								<?php else: ?>

									<?php foreach ($CI->model_foreach_select('optionals', '1') as $optionals): ?>
										<option value="<?= $optionals->id ?>"><?= $optionals->name ?></option>
									<?php endforeach ?>

								<?php endif ?>
							</select>
						</div>

					</div>


					<div class="row m-t-40">
						<div class="col-lg-6">
							<h5 class="box-title">Pizza</h5>
							<div class="checkbox checkbox-info">
								<input id="pizza" name="pizza" type="checkbox" value="1">
								<label for="pizza"> <strong>Produto Pizza</strong> </label>
							</div>
							<div class="clearfix"></div>
							<div id="pizzaID" class="form-group collapse">
								<label>Quantidade de sabores</label>
								<input type="number" id="flavors" name="flavors" min="1" max="8" placeholder="Quantidade" class="form-control">
							</div>
						</div>

						<div class="col-md-6">
							<h3 class="box-title">Descrição do produto</h3>
							<div class="form-group">
								<textarea class="form-control" name="description" rows="4"></textarea>
							</div>
						</div>

					</div>

					<div class="clearfix"></div>

					<div class="row m-t-40">

						<div class="col-lg-6">
							<h5 class="box-title">Imagem do produto</h5>
							<div>
								<div class="text-center">
									<div id="resultsThumbnail"></div>
									<label for="thumbnailUpload" class="btn btn-info m-t-30">Selecionar imagem</label>
									<input id="thumbnailUpload" name="thumbnail" type="file" style="display: none" />
								</div>
							</div>
						</div>

						<div class="col-lg-6 hidden">
							<h5 class="box-title">Galeria de imagens</h5>
							<div class="text-center">
								<label for="thumbnailUploads" class="btn btn-info m-b-30 m-t-30">Selecionar imagens</label>
								<input id="thumbnailUploads" name="thumbnails[]" type="file" multiple style="display: none" />
							</div>

							<div class="clearfix"></div>

							<style type="text/css">
							#gallery-content-center img {
								height: 180px;
							}
						</style>
						<div id="gallery">
							<div id="gallery-content">
								<div id="gallery-content-center"></div>       				 		
							</div>
						</div>
					</div>

				</div>

				<div class="col-lg-12 text-right">
					<button type="submit" class="btn btn-info waves-effect waves-light">Adicionar produto</button>
				</div>

				<div class="clearfix"></div>
			</div>
			
		</div>

	</form>

</div>