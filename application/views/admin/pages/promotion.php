<?php 
	$CI =& get_instance(); 
	$base = base_url().$CI->base;
?>
<div class="container-fluid" ng-controller="PromotionController">
	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title">{{PromotionPageTitle}}</h4> </div>
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
				<ol class="breadcrumb">
					<li><a href="<?php echo $base ?>">Página inicial</a></li>
					<li class="active">{{PromotionPageTitle}}</li>
				</ol>
			</div>
			<!-- /.col-lg-12 -->
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="white-box">
				<h3 class="box-title">{{PromotionPageTitle}} adicionadas</h3>
				<div class="scrollable">
					<div class="table-responsive">
						<table class="table m-t-30 table-hover contact-list" data-page-size="10">
							<button type="button" class="btn btn-primary btn-rounded actions-btn" data-toggle="modal" data-target="#addPromotion">Adicionar categoria</button>
							<thead>
								<tr>
									<th>Nome</th>
									<th>Status</th>
									<th>Ações</th>
								</tr>
							</thead>
							<tbody id="Promotion">
								<tr ng-repeat="cat in categories" id="{{cat.id}}">
				   					<td data-id="{{cat.id}}" data-type="name">{{cat.name}}</td>
				   					<td>
				   					<div ng-if="cat.status == 0">
				   						<span class="label label-danger change_status" data-toggle="tooltip" title="Mudar status" data-id="{{cat.id}}" data-type="Promotion" data-provider="{{cat.status}}" style="cursor:pointer;">Indisponível</span>
				   					</div>

				   					<div ng-if="cat.status == 1">
				   						<span class="label label-success change_status" data-toggle="tooltip" title="Mudar status" data-id="{{cat.id}}" data-type="Promotion" data-provider="{{cat.status}}" style="cursor:pointer;">Disponível</span>
				   					</div>

				   					</td>
				   					<td>
				   					 <button type="button" data-id="{{cat.id}}" data-role="Promotion" data-toggle="modal" data-target="#editItem" class="btn btn-info btn-outline btn-circle btn-xs m-r-5 edit">
				   						<i class="ti-pencil"></i>
				   					 </button>
				   					 <button type="button" data-id="{{cat.id}}" class="btn btn-info btn-outline btn-circle btn-xs m-r-5 remove">
				   						<i class="ti-trash"></i>
				   					 </button>
				   					</td>
				   				</tr>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="7">
										<div class="text-right">
											<ul class="pagination"> </ul>
										</div>
									</td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>           
		</div>
	</div>

	
	<div id="addPromotion" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myModalLabel">Adicionar {{PromotionPageTitle}}</h4>
				</div>

			  <form method="post" role="Promotion">
				<div class="modal-body">
					<div class="form-group">
						<label class="col-sm-12">Nome da categoria</label>
						<div class="col-sm-12">
							<input type="text" name="name" class="form-control" placeholder="Categoria" required>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-lg-12">
						  <div class="checkbox checkbox-success checkbox-circle">
                             <input id="checkbox-10" name="status" type="checkbox" value="1" checked="">
                           	 <label for="checkbox-10"> Disponível </label>
                          </div>
						</div>
					</div>

					<div class="clearfix"></div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success waves-effect">Adicionar</button>
					<button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Fechar</button>
				</div>
			  </form>

			</div>

		</div>

	</div>
	
	<div id="editItem" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myModalLabel">Editar {{PromotionPageTitle}}</h4>
				</div>

			  <form method="post" role="editItems" data-table="Promotion" id="">
				<div class="modal-body">
					<div class="form-group">
						<label class="col-sm-12">Nome da categoria</label>
						<div class="col-sm-12">
							<input type="text" name="name" class="form-control name" placeholder="Categoria" required>
						</div>
					</div>

					<div class="clearfix"></div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success waves-effect">Editar</button>
					<button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Fechar</button>
				</div>
			  </form>

			</div>

		</div>

	</div>

</div>

