<?php 

$CI =& get_instance(); 

$base = $CI->base;

$address = $CI->config_address();
$city = explode(" - ", $address->city);
$districts = isset($address->districts) ? explode("|", $address->districts) : null;
$districts_array = array();
if (!is_null($districts)) {
    foreach ($districts as $id_bairro) {
        $CI->db->where("id_bairro", $id_bairro);
        $get = $CI->db->get("cepbr_bairro");
        if ($get->num_rows() > 0) {
            array_push($districts_array, $get->row()->bairro);
        }
    }
}

?>



<div class="container-fluid">



	<div class="row bg-title">

		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">

			<h4 class="page-title">Ponto de venda</h4> 

		</div>

		<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

			<ol class="breadcrumb">

				<li><a href="<?php echo $base ?>">Página Inicial</a></li>

				<li class="active">Ponto de venda</li>

			</ol>

		</div>

	</div>

	

	<!-- Modals -->

	<div id="client-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">



		<div class="modal-dialog">

			<div class="modal-content">



				<div class="modal-header">

					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

					<h4 class="modal-title">Adicionar cliente</h4> 

				</div>



				<form id="sales-client_create" method="post">

					<div class="modal-body">



						<p> <strong>Dados do cliente</strong> </p>



						<div class="form-group">

							<input type="text" name="name" class="form-control" placeholder="Nome" required>

						</div>



						<div class="form-group">

							<input type="text" name="phone" class="form-control" placeholder="Telefone" required>

						</div>



						<input type="hidden" name="request" value="">



						<hr>



						<p> <strong>Endereço</strong> </p>



						<div class="form-group">

							<input type="text" name="nameAddress" class="form-control" placeholder="Nome do endereço">

						</div>



						<div class="form-group">

                            <input type="text" name="cep" data-location="<?= trim($city[0]) ?>" data-districts="<?= join(",", $districts_array) ?>" class="form-control" placeholder="CEP">

						</div>



						<div class="form-group">

							<input type="text" name="address" class="form-control" placeholder="Endereço" readonly="true">

						</div>



						<div class="form-group">

							<input type="text" name="number" class="form-control" placeholder="Número">

						</div>



						<div class="form-group">

							<input type="text" name="complement" class="form-control" placeholder="Complemento">

						</div>



					</div>



					<div class="modal-footer">

						<button type="reset" class="btn btn-default waves-effect" data-dismiss="modal">Cancelar</button>

						<button type="submit" class="btn btn-danger waves-effect waves-light">+ Criar</button>

					</div>



				</form>

			</div>

		</div>

	</div>



	<div id="client-newaddress-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">



		<div class="modal-dialog">

			<div class="modal-content">



				<div class="modal-header">

					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

					<h4 class="modal-title">Adicionar endereço</h4> 

				</div>



				<form id="sales-client_newaddress" method="post">

					<div class="modal-body">



						<p> <strong>Endereço</strong> </p>



						<div class="form-group">

							<input type="hidden" class="clientidaddress" name="clientID">

							<input type="text" name="nameAddress" class="form-control" placeholder="Nome do endereço">

						</div>



						<div class="form-group">

							<input type="text" name="cep" class="form-control" placeholder="CEP">

						</div>



						<div class="form-group">

							<input type="text" name="address" class="form-control" placeholder="Endereço" readonly="true">

						</div>



						<div class="form-group">

							<input type="text" name="number" class="form-control" placeholder="Número">

						</div>



						<div class="form-group">

							<input type="text" name="complement" class="form-control" placeholder="Complemento">

						</div>



					</div>



					<div class="modal-footer">

						<button type="reset" class="btn btn-default waves-effect" data-dismiss="modal">Cancelar</button>

						<button type="submit" class="btn btn-danger waves-effect waves-light">+ Criar</button>

					</div>



				</form>

			</div>

		</div>

	</div>



	<div class="modal fade add--product" tabindex="-1" role="dialog">

		<div class="modal-dialog modal-lg">

			<div class="modal-content">

				<form id="add_product" method="post">

					<div class="modal-header">

						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

						<h4 class="modal-title" id="LargeModalLabel">Adicionar produto</h4> 

					</div>

					<div id="slimtest1" class="modal-body">

						<div class="form-group">

							<div id="product_ID" data-id=""></div>

							<label for="inputProductName">Produto</label>

							<select id="inputProductName" name="productID" class="form-control select2">

								<option>Digite o código ou nome do produto</option>

								<?php

								$CI->db->where('status', '1');

								$category = $CI->db->get('category');

								foreach ($category->result() as $cat):

									$CI->db->where('status', '1');

									$CI->db->where('category_id', $cat->id);

									$products = $CI->db->get('products');

									if ($products->num_rows() > 0):

										?>

										<optgroup label="<?= $cat->name ?>">

											<?php foreach ($products->result() as $product): ?>

												<option value="<?= $product->id ?>"><?= $CI->helps->barcode($product->id).' - '.$product->name ?></option>

											<?php endforeach ?>

										</optgroup>

										<?php 

									endif;

								endforeach;

								?>

							</select>

						</div>

						<div id="flavors" class="col-lg-12 col-xs-12 m-t-20 m-b-30" style="display: none;position: relative;">

							<div class="first-flavor" data-thumbnail=""></div>

							<div class="form-group text-center">

								<div class="radio radio-danger radio-inline">

									<input id="choose-1" name="choose-qtd" value="1" type="radio" checked="">

									<label for="choose-1"> Inteira </label>

								</div>

								<div class="radio radio-danger radio-inline">

									<input id="choose-2" name="choose-qtd" value="2" type="radio">

									<label for="choose-2"> Meia </label>

								</div>

							</div>

							<div id="mount" class="pieContainer">

								<div id="1" class="pie big" data-start="1" data-value="360"></div>

								<div id="pieBackground" class="pieBackground-1"></div>

							</div>

							<div class="sidepizza-left col-lg-4 col-md-4 col-sm-12 col-xs-12" style="display:none;">

								<div class="white-box">

									<div id="ingredients-left" class="box-title">

										Ingredientes

									</div>

									<ul class="ingredientsStyles"></ul>

								</div>

								<div class="clearfix"></div>

							</div>

							<div class="sidepizza-right col-lg-4 col-md-4 col-sm-12 col-xs-12" style="display:none;">

								<div class="white-box">

									<div id="ingredients-right" class="box-title">

										Ingredientes

									</div>

									<ul class="ingredientsStyles"></ul>

								</div>

								<div class="clearfix"></div>

							</div>

							<div class="clearfix"></div>

						</div>

						<hr class="m-t-20">

						<div class="row">

							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

								<label for="inputQuantity" class="control-label">Quantidade </label>

								<input id="inputQuantity" type="text" value="1" min="1" max="" name="quantity" class="touchSpin" data-bts-button-down-class="btn btn-default btn-outline" data-bts-button-up-class="btn btn-default btn-outline"> 

							</div>

							<div class="ingredients_div col-lg-6 col-md-6 col-sm-12 col-xs-12" style="display: none;">

								<label>Ingredientes:</label><br>

								<select id="ingredients" name="ingredients[default][]" class="ingredients selectpicker" multiple data-style="form-control" data-width="100%"></select>

							</div>

							<div class="clearfix"></div>

						</div>



						<div class="row">

							<div id="addons" class="col-lg-6 col-xs-12 m-t-10"></div>

							<div id="options" class="col-lg-6 col-xs-12 m-t-10"></div>

							<div class="clearfix"></div>

						</div>



						<div class="clearfix"></div>

					</div>

					<div class="modal-footer">

						<button type="submit" data-loading-text="Adicionando..." class="btn btn-info btn-loader waves-effect text-left">Adicionar</button>

						<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Fechar</button>

					</div>

				</form>

			</div>



		</div>

	</div>

	



	<div id="choose-flavor" class="modal fade" style="margin-top:5%;" role="dialog" data-keyboard="false" data-backdrop="static">

		<div class="modal-dialog">



			<div class="modal-content">

				<div class="modal-body">

					<div id="choose-control"></div>

					<div class="search-list">

						<input id="search-list" type="text" class="form-control" placeholder="Encontrar sabor">

						<div class="search-list-icon">

							<i class="fa fa-search"></i>

						</div>

					</div>

					<ul id="list-menu"></ul>

				</div>

				<div class="modal-footer">

					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>

				</div>

			</div>



		</div>

	</div>

	

	<div class="row">

		<form id="sales_submit" method="post">



			<div id="lightbox--details" class="white-popup-block mfp-hide text-center">

				<p class="text-center">Carregando...</p>

			</div>



			<div id="lightbox--add_product" class="white-popup-block mfp-hide text-center">

				<p class="text-center">Carregando...</p>

			</div>



			<div id="lightbox--complete_sales" class="white-popup-block mfp-hide text-center">

				<p class="text-center">Carregando...</p>

			</div>









			<div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">



				<div class="sidebar-right-box">

					<div class="white-box">

						<h3 class="box-title">Cliente</h3>

						<hr>

						<select id="sales-client" name="client" class="form-control select2">

							<?php 

							$clients = $CI->db->get("clients");

							if($clients->num_rows() > 0):

								echo '<option value="default">Balcão</option>';

								echo '<option disabled>--- Clientes cadastrados ---</option>';

								foreach ($clients->result() as $client):

									$address = ''; 

									if (isset($client->address) && $client->address != '') {

										list($addr, $number) = explode("|", $client->address);

										list($street, $district, $city) = explode(", ", $addr);

										$address = $street.', '.$number.', '.$district.', '.$city;

									}



									?>

									<option value="<?= $client->id ?>">

										<?= ucwords($client->name) ?> - <?= $CI->helps->formatPhone($client->phone) ?>

									</option>

									<?php

								endforeach;

							endif;

							?>

						</select>

						<div id="client-address"></div>

						<a href="javascript:void(0)" class="btn btn-xs btn-info m-t-10" data-toggle="modal" data-target="#client-modal">Cadastrar cliente</a>

						<a href="javascript:void(0)" class="newaddress btn btn-xs btn-success m-t-10" data-toggle="modal" data-target="#client-newaddress-modal" style="display: none">Novo endereço</a>

					</div>

				</div>


                <div class="sidebar-right-box">

<div class="white-box">

    <h3 class="box-title">Forma de pagamento</h3>

    <hr>

    <select name="payment" class="payments selectpicker" data-style="form-control" data-width="100%">

        <option value="0">Dinheiro</option>

        <option value="1">Cartão de crédito</option>

        <option value="2">Cartão de débito</option>

        <option value="3">Ticket alimentação</option>

    </select>

    <div id="payment_brand" style="display:none;">

        <label>Bandeira do cartão</label>

        <select name="payment_shippingBrand" class="form-control">

            <option value="Visa">Visa</option>

            <option value="Mastercard">Mastercard</option>

            <option value="American Express">American Express</option>

            <option value="Elo">Elo</option>

            <option value="Discover Network">Discover Network</option>

        </select>

    </div>

    <div id="payment_insert">

        <label for="moneycash">Valor</label>

        <input id="moneycash" name="receiver" type="text" class="form-control" placeholder="00.00" required>

    </div>



</div>

</div>



			</div>



			<div class="col-md-9 col-lg-9 col-sm-12 col-xs-12">
				<div class="panel panel-info">
					<div class="panel-heading"> Produtos (<span class="session_quantity"><?= count($CI->session->sales) ?></span>) 
						<a href="javascript:void(0)" class="btn btn-md btn-success pull-right text-center" data-toggle="modal" data-target=".add--product">
							<i title="" data-toggle="tooltip" data-original-title="Adicionar produtos" class="ti-plus"></i>
							<span class="hidden-xs">Adicionar</span>
						</a>
					</div>

					<div class="panel-wrapper collapse in" aria-expanded="true">
						<div class="panel-body">

							<div class="table-responsive">

								<table class="table product-overview">

									<thead>

										<tr>

											<th></th>

											<th>Produto</th>

											<th>Preço</th>

											<th>Quantidade</th>

											<th style="text-align:center">Total</th>

											<th style="text-align:center">Ação</th>

										</tr>

									</thead>

									<tbody id="salesList"></tbody>

								</table>

							</div>

						</div>

					</div>

				</div>



				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m0 sidebar-right-box appearDiv">

					<div class="sidebar-right-box">

<div class="white-box">

    <h3 class="box-title">Pedido</h3>

    <hr>

    <select name="deliveryaddress" class="methods selectpicker" data-style="form-control" data-width="100%">

        <option value="delivery">Entrega</option>

        <option value="withdraw">Retirada</option>

    </select>

    <div id="deliveryman">
        <hr>
        <select name="deliveryman" class="selectpicker" data-style="form-control" data-width="100%">
            <option value="" selected="" disabled="">Entregador</option>
            <?php
            $deliverymans = $CI->db->get('deliveryman');
            if ($deliverymans->num_rows() > 0) {
            foreach ($deliverymans->result() as $deliveryman): ?>
                <option value="<?= $deliveryman->id ?>"><?= $deliveryman->name ?></option>
            <?php 
            endforeach; 
            }
            ?>
        </select>
    

        <div id="reports" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h4 class="modal-title" id="myModalLabel">Fechamento</h4> 
					</div>
					<div id="reports_load" class="modal-body"></div>
					<div class="modal-footer">
						<button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Fechar</button>
					</div>
				</div>
			</div>
		</div>
        <div id="orders" class="scrollable">
			<div id="orders_table" class="table-responsive">
				<table id="demo-foo-addrow" class="table m-t-30 table-hover contact-list" data-page-size="10">
					<thead>
						<tr>
							<th>Nome</th>
							<th>Placa</th>
							<th> </th>
						</tr>
					</thead>
					<tbody id="deliveryman">
                    <?php
                        $deliverymans = $CI->db->get('deliveryman');
                        if ($deliverymans->num_rows() > 0) {
                        foreach ($deliverymans->result() as $deliveryman): ?>
                            <tr id="deliveryman_<?= $deliveryman->id ?>">
                                <td><?= $deliveryman->id ?></td>
                                <td><?= $deliveryman->name ?></td>
                                <td class="status_<?= $deliveryman->id ?>">
									<?php if ($deliveryman->status == 1): ?>
										<span class="label label-success">
											Livre
										</span>
									<?php else: ?>
										<span class="label label-warning">
											Em entrega
										</span>
									<?php endif ?>
								</td>
                                <td>
								   <a href="javascript: void(0)" data-id="<?= $deliveryman->id ?>" rel="<?= $deliveryman->status ?>" class="btn btn-info btn-outline btn-circle btn-xs m-r-5 change-status" data-toggle="tooltip" title="Mudar status">
								      <i class="ti-arrows-horizontal"></i>
								   </a>
								   <a href="javascript: void(0)" data-id="<?= $deliveryman->id ?>" class="btn btn-info btn-outline btn-circle btn-xs m-r-5 reports" data-toggle="modal" data-target="#reports" title="Relatórios">
								      <i class="ti-stats-up"></i>
								   </a>
								</td>
                            </tr>
                        <?php 
                        endforeach; 
                        }
                    ?>  
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>

</div>

				</div>



				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 m0 sidebar-right-box appearDiv">

					<div class="white-box">

						<div class="form-group text-center">

							<button type="button" data-loading-text="Finalizando..." class="btn btn-loader doneBuy btn-sm btn-success">Finalizar compra</button>

						</div>

						<div class="table-responsive appearDone" style="display: none;">

							<table class="table">

								<tbody>

									<tr>

										<td>Subtotal:</td>

										<td>R$<span class="subtotal">00.00</span></td>

									</tr>

									<tr>

										<td>

											<div class="form-group">

												<label for="inputCupom">Cupom de desconto</label>

												<input id="inputCupom" name="cupom" type="text" class="form-control" placeholder="Cupom de desconto">

											</div>

										</td>

										<td width="1"></td>

									</tr>

									<tr>

										<td>Descontos:</td>

										<td>

											R$<span class="descounts">00.00</span>

											<input type="hidden" name="descounted" class="descounted">

										</td>

									</tr>

									<tr>

										<td>Total com descontos:</td>

										<td>

											R$<span class="descountsTotal">00.00</span>

										</td>

									</tr>

									<tr id="shipping">

										<td>Taxa de entrega:</td>

										<td>R$<span class="shipping">00.00</span></td>

									</tr>

									<tr id="money_back" style="display:none;">

										<td>Trocos:</td>

										<td>R$<span class="money_back">00.00</span></td>

									</tr>

									<tr>

										<td>Total a pagar:</td>

										<td></td>

									</tr>

								</tbody>

							</table>

						</div>

						<h1 id="total_price" class="text-right appearDone" style="display: none;">

							R$

							<span class="total-buy" data-total="00.00">00.00</span>

							<input type="hidden" name="amount" id="amount" value="00.00">

							<input type="hidden" name="balance" id="balance" value="00.00">

						</h1>

						<hr>

						<div class="text-right appearDone" style="display: none;">

							<button type="button" class="cancel-sale btn btn-danger"> Cancelar</button>

							<button type="submit" data-loading-text="Processando..." class="done-sale btn btn-info">Concluir</button>

						</div>

					</div>

				</div>



			</div>



		</form>

	</div>