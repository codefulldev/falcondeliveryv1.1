<?php 
	$CI =& get_instance(); 
	$base = base_url().$CI->base;
	$get_id = explode("/",$_SERVER['REQUEST_URI']);
?>
<div class="container-fluid" ng-controller="ClientsController">
	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title">{{clientsPageTitle}}</h4> </div>
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
				<ol class="breadcrumb">
					<li><a href="<?php echo $base ?>">Página inicial</a></li>
					<li class="active">{{clientsPageTitle}}</li>
				</ol>
			</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="white-box">
				<?php if (!empty($get_id[4]) && is_numeric($get_id[4])): ?>

				<?php else: ?>
				<h3 class="box-title">Todos os {{clientsPageTitle}}</h3>
				<div id="orders" class="scrollable">
					<div id="orders_table" class="table-responsive">
						<table id="demo-foo-addrow" class="table m-t-30 table-hover contact-list" data-page-size="10">
							<thead>
								<tr>
									<th>ID</th>
									<th>Cliente</th>
									<th>Registro</th>
									<th>Status</th>
									<th>Total</th>
									<th>Ações</th>
								</tr>
							</thead>
							<tbody>
							<?php 
							 $clients = $CI->ecommerce_clients_list();
							 foreach ($clients as $client): 
							 if ($client->id > 0) {
							?>
							<tr id="client_<?= $client->id ?>">
								<td><a href="javascript:void(0)"><?= $client->id ?></a></td>
								<td>
									<img src="<?= $CI->helps->verify_client_thumbnail($client->thumbnail); ?>" class="img-circle" width="35" height="30">
									<?= $client->name ?>
								</td>
								<td><?= date('d/m/Y', strtotime($client->since)) ?></td>
								<td>
									<?php if ($client->status == 1): ?>
										<span class="label label-success">
											Verificado
										</span>
									<?php else: ?>
										<span class="label label-warning">
											Não verificado
										</span>
									<?php endif ?>
								</td>
								<td></td>
								<td>
								   <button type="button" onclick="window.location.href='<?= $base.'invoice/'.$client->id ?>'" class="btn btn-info btn-outline btn-circle btn-xs m-r-5" data-toggle="tooltip" title="Enviar e-mail">
								      <i class="ti-email"></i>
								   </button>
								   <button type="button" onclick="window.location.href='<?= $base.$CI->clientsURI.'/edit/'.$client->id ?>'" class="btn btn-info btn-outline btn-circle btn-xs m-r-5" data-toggle="tooltip" title="Ver cliente">
								      <i class="ti-eye"></i>
								   </button>
								   <button type="button" data-id="<?= $client->id ?>" class="btn btn-info btn-outline btn-circle btn-xs m-r-5 remove" data-toggle="tooltip" title="Apagar cliente">
								      <i class="ti-trash"></i>
								   </button>
								</td>
							</tr>		
							<?php 
							 }
							 endforeach;
							?>	
							</tbody>
							<tfoot>
								<tr>
									<td colspan="7">
										<div class="text-right">
											<ul class="pagination"> </ul>
										</div>
									</td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
				<?php endif ?>
			</div>           
		</div>
	</div>

	
	<div id="addCategory" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
					<h4 class="modal-title" id="myModalLabel">Adicionar {{clientsPageTitle}}</h4>
				</div>

			  <form method="post" role="category">
				<div class="modal-body">
					<div class="form-group">
						<label class="col-sm-12">Nome da categoria</label>
						<div class="col-sm-12">
							<input type="text" name="name" class="form-control" placeholder="Categoria" required>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-lg-12">
						  <div class="checkbox checkbox-success checkbox-circle">
                             <input id="checkbox-10" name="status" type="checkbox" value="1" checked="">
                           	 <label for="checkbox-10"> Disponível </label>
                          </div>
						</div>
					</div>

					<div class="clearfix"></div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success waves-effect">Adicionar</button>
					<button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Fechar</button>
				</div>
			  </form>

			</div>
		</div>
	</div>

</div>