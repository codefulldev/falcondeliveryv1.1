<?php 
	$CI =& get_instance(); 
	$base = base_url().$CI->base;
	$request = explode("/", $_SERVER['REQUEST_URI']);
	$id = end($request);
	if (!is_numeric($id)) {
		redirect($base.$productURI);
	}
	$product = $CI->ecommerce_products_get($id);
?>
<div class="container-fluid" ng-controller="ProductsController">
	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title">{{productPageTitle}}</h4> </div>
			<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
				<ol class="breadcrumb">
					<li><a href="<?php echo $base ?>">Página inicial</a></li>
					<li class="active">{{productPageTitle}}</li>
				</ol>
			</div>
			<!-- /.col-lg-12 -->
	</div>

	<div class="row tabber view_product collapse">
		<div class="col-md-12">
			<div class="white-box">
				<h3 class="box-title">{{productName}}</h3>
				<button type="button" class="btn btn-primary btn-rounded pull-right actions-btn" data-target="list_product">Lista de produtos</button>
				<div class="clearfix"></div>
				<hr>
			</div>
		</div>
	</div>

	<form id="13f5a48ed1d5991d5d0bd3c2cf598f1ff1ff4520" method="post" role="13f5a48ed1d5991d5d0bd3c2cf598f1ff1ff4520" class="tabber">

		<div class="row">
			<div class="col-lg-12 white-box">

				<h1 class="box-title">Adicionar produto</h1>
				<button type="button" onclick="window.location.href='<?= $base.$CI->productURI ?>'" class="btn btn-primary btn-rounded actions-btn">Lista de produtos</button>
				<hr>

				<div class="row">
					<input type="hidden" name="id" value="<?= $product->id ?>">
					<div class="col-md-6">
						<div class="form-group">
							<label class="box-title">Nome do produto</label>
							<input type="text" name="name" id="firstName" class="form-control" placeholder="Nome do produto" value="<?= $product->name ?>"> 
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label class="box-title">Status</label>
							<div class="radio-list">
								<label class="radio-inline p-0">
									<div class="radio radio-info">
										<input type="radio" name="status" id="radio1" value="1" <?php if($product->status == 1){echo'checked';} ?>>
										<label for="radio1">Disponível</label>
									</div>
								</label>
								<label class="radio-inline">
									<div class="radio radio-info">
										<input type="radio" name="status" id="radio2" value="0" <?php if($product->status == 0){echo'checked';} ?>>
										<label for="radio2">Indisponível</label>
									</div>
								</label>
							</div>
						</div>
					</div>

				</div>

				<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
							<label class="box-title">Venda</label>
							<div class="radio-list">
								<label class="radio-inline p-0">
									<div class="radio radio-info">
										<input type="radio" name="sale" id="sales1" value="0" <?php if($product->sale == 0){echo'checked';} ?>>
										<label for="sales1">Ativada</label>
									</div>
								</label>
								<label class="radio-inline">
									<div class="radio radio-info">
										<input type="radio" name="sale" id="sales2" value="1" <?php if($product->sale == 1){echo'checked';} ?>>
										<label for="sales2">Desativada</label>
									</div>
								</label>
							</div>
						</div>
					</div>

					<div class="col-lg-6">
						<div class="form-group">
							<label class="box-title">Estoque</label>
							<input name="stock" type="number" min="1" value="<?= $product->stock ?>" class="form-control">
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label class="box-title">Categoria</label>
							<select class="selectpicker form-control" name="category" title="Escolha uma categoria" data-style="form-control">
								<?php if (count($CI->model_foreach_select('category', '1')) < 1): ?>
									<option value="" disabled>Nenhuma categoria</option>
								<?php else: ?>

									<?php foreach ($CI->model_foreach_select('category', '1') as $category): ?>
									  <?php if ($category->id == $product->category_id): ?>
									  	<option value="<?= $category->id ?>" selected><?= $category->name ?></option>
									  <?php else: ?>
									  	<option value="<?= $category->id ?>"><?= $category->name ?></option>
									  <?php endif ?>
										
									<?php endforeach ?>

								<?php endif ?>
							</select>
						</div>
					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label class="box-title">Preço</label>
							<div class="input-group">
								<div class="input-group-addon">R$</div>
								<input type="text" name="price" class="form-control money" placeholder="00,00" value="<?= number_format($product->price, 2, ",", "."); ?>"> </div>
							</div>
						</div>

					</div>

					<div class="row">

						<div class="col-lg-6 col-sm-6 col-xs-12 m-t-20">
							<h6 class="box-title">Adicionais</h6>
							<select class="selectpicker form-control" name="addons[]" multiple title="Escolha os adicionais" data-style="form-control">
								<?php if (count($CI->model_foreach_select('addons', '1')) < 1): ?>
									<option value="" selected disabled>Nenhum adicional</option>
								<?php else: ?>

									<?php 
									$addons_array = explode("|", $product->addons);
									foreach ($CI->model_foreach_select('addons', '1') as $addons): ?>
									 <?php if (in_array($addons->id, $addons_array)): ?>
									 	<option value="<?= $addons->id ?>" selected><?= $addons->name ?></option>
									 <?php else: ?>
									 	<option value="<?= $addons->id ?>"><?= $addons->name ?></option>
									 <?php endif ?>
									<?php endforeach ?>

								<?php endif ?>
							</select>
						</div>

						<div class="col-lg-6 col-sm-6 col-xs-12 m-t-20">
							<h6 class="box-title">Ingredientes</h6>
							<select class="selectpicker form-control" name="ingredients[]" multiple title="Escolha os ingredientes" data-style="form-control">
								<?php if (count($CI->model_foreach_select('ingredients', '1')) < 1): ?>
									<option value="" disabled>Nenhum ingrediente</option>
								<?php else: ?>

									<?php 
									$ingredients_array = explode("|", $product->ingredients);
									foreach ($CI->model_foreach_select('ingredients', '1') as $ingredients): ?>
									 <?php if (in_array($ingredients->id, $ingredients_array)): ?>
									 	<option value="<?= $ingredients->id ?>" selected><?= $ingredients->name ?></option>
									 <?php else: ?>
									 	<option value="<?= $ingredients->id ?>"><?= $ingredients->name ?></option>
									 <?php endif ?>
									<?php endforeach ?>

								<?php endif ?>
							</select>
						</div>

						<div class="col-lg-6 col-sm-6 col-xs-12 m-t-20">
							<h6 class="box-title">Opcionais</h6>
							<select class="selectpicker form-control" name="optionals[]" multiple title="Escolha os opcionais" data-style="form-control">
								<?php if (count($CI->model_foreach_select('optionals', '1')) < 1): ?>
									<option value="" disabled>Nenhum ingrediente</option>
								<?php else: ?>

									<?php 
									$optionals_array = explode("|", $product->optionals);
									foreach ($CI->model_foreach_select('optionals', '1') as $optionals): ?>
									 <?php if (in_array($optionals->id, $optionals_array)): ?>
									 	<option value="<?= $optionals->id ?>" selected><?= $optionals->name ?></option>
									 <?php else: ?>
									 	<option value="<?= $optionals->id ?>"><?= $optionals->name ?></option>
									 <?php endif ?>
									<?php endforeach ?>

								<?php endif ?>
							</select>
						</div>

					</div>


					<div class="row m-t-40">
						<div class="col-lg-6">
							<h5 class="box-title">Pizza</h5>
							<div class="checkbox checkbox-info">
								<input id="pizza" name="pizza" type="checkbox" value="1" <?php if($product->pizza == 1){echo 'checked';} ?>>
								<label for="pizza"> <strong>Produto Pizza</strong> </label>
							</div>
							<div class="clearfix"></div>
							<div id="pizzaID" class="form-group <?php if($product->pizza < 1){echo 'collapse';} ?>">
								<label>Quantidade de sabores</label>
								<input type="number" id="flavors" name="flavors" min="1" max="8" placeholder="Quantidade" class="form-control" value="<?= $product->flavors ?>">
							</div>
						</div>

						<div class="col-md-6">
							<h3 class="box-title">Descrição do produto</h3>
							<div class="form-group">
								<textarea class="form-control" name="description" rows="4"><?= $product->description ?></textarea>
							</div>
						</div>

					</div>

					<div class="clearfix"></div>

					<div class="row m-t-40">

						<div class="col-lg-6">
							<h5 class="box-title">Imagem do produto</h5>
							<div>
								<div class="text-center">
									<div id="resultsThumbnail">
										<img src="themes/uploads/thumbnail/<?= $product->thumbnail ?>" <?php if($product->pizza == 1){echo 'width="180" height="180"';}else{echo 'width="310" height="260"';}  ?>>
									</div>
									<label for="thumbnailUpload" class="btn btn-info m-t-30">Selecionar imagem</label>
									<input id="thumbnailUpload" name="thumbnail" type="file" style="display: none" />
								</div>
							</div>
						</div>

						<div class="col-lg-6 hidden">
							<h5 class="box-title">Galeria de imagens</h5>
							<div class="text-center">
								<label for="thumbnailUploads" class="btn btn-info m-b-30 m-t-30">Selecionar imagens</label>
								<input id="thumbnailUploads" name="thumbnails[]" type="file" multiple style="display: none" />
							</div>

							<div class="clearfix"></div>

							<style type="text/css">
							#gallery-content-center img {
								height: 180px;
							}
						</style>
						<div id="gallery">
							<div id="gallery-content">
								<div id="gallery-content-center">
									<?php
									 $CI->db->where('products_id', $product->id);
									 $gallery_get = $CI->db->get('gallery');
									 foreach ($gallery_get->result() as $gallery) {
									 	echo '<img src="themes/uploads/gallery/'.$gallery->thumbnail.'" alt="'.$gallery->alt.'">';
									 }
									?>
								</div>       				 		
							</div>
						</div>
					</div>

				</div>

				<div class="col-lg-12 text-right">
					<button type="submit" class="btn btn-info waves-effect waves-light">Salvar</button>
				</div>

				<div class="clearfix"></div>
			</div>
			
		</div>

	</form>

</div>