<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Relatórios</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="<?php echo $base ?>">Página inicial</a></li>
                    <li class="active">Relatórios</li>
                </ol>
            </div>

        </div>

        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="white-box">
                    <h3 class="box-title">Ganhos mensais</h3>
                    <div id="morris-area-chart2"></div>
                </div>
            </div>


            <div class="col-md-6 col-lg-6 col-xs-12">
                <div class="white-box">
                    <h3 class="box-title">Vendas</h3>
                    <div id="morris-line-chart"></div>
                </div>
            </div>

        </div>
    </div>