<?php 
	$CI =& get_instance(); 
	$base = base_url().$CI->base;
?>
<div class="container-fluid" ng-controller="OptionalsController">
	<div class="row bg-title">

		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title">{{optionalsPageTitle}}</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
               <li><a href="<?php echo $base ?>">Página inicial</a></li>
               <li class="active">{{optionalsPageTitle}}</li>
           </ol>
       </div>

   </div>

    
   <div class="row">
    <div class="col-md-12">
        <div class="white-box">
            <h3 class="box-title">{{optionalsPageTitle}} adicionados</h3>
            <div class="scrollable">
                <div class="table-responsive">
                    <table class="table m-t-30 table-hover contact-list" data-page-size="10">
                        <button type="button" class="btn btn-primary btn-rounded actions-btn" data-toggle="modal" data-target="#addOptionals">Adicionar opcional</button>
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Preço</th>
                                <th>Status</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody id="optionals">
                          <tr ng-repeat="optional in optionals" id="{{optional.id}}">
                            <td data-id="{{optional.id}}" data-type="name" class="editableRow">{{optional.name}}</td>
                            <td data-id="{{optional.id}}" data-type="price" class="editableRow">{{optional.price | currency:'R$'}}</td>
                            <td>
                                <div ng-if="optional.status == 0">
                                <span class="label label-danger change_status" data-toggle="tooltip" title="Mudar status" data-id="{{optional.id}}" data-type="optionals" data-provider="{{optional.status}}" style="cursor:pointer;">Indisponível</span>
                                </div>

                                <div ng-if="optional.status == 1">
                                <span class="label label-success change_status" data-toggle="tooltip" title="Mudar status" data-id="{{optional.id}}" data-type="optionals" data-provider="{{optional.status}}" style="cursor:pointer;">Disponível</span>
                                </div>
                            </td>
                            <td>
                                <button type="button" data-id="{{optional.id}}" data-role="optionals" data-toggle="modal" data-target="#editItem" class="btn btn-info btn-outline btn-circle btn-xs m-r-5 edit">
                                    <i class="ti-pencil"></i>
                                </button>
                                <button type="button" data-id="{{optional.id}}" class="btn btn-info btn-outline btn-circle btn-xs m-r-5 remove">
                                   <i class="ti-trash"></i>
                                </button>
                            </td>
                          </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="7">
                                <div class="text-right">
                                    <ul class="pagination"> </ul>
                                </div>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>           
</div>
</div>


<div id="addOptionals" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Adicionar {{optionalsPageTitle}}</h4>
            </div>

            <form method="post" role="optionals">
                <div class="modal-body">
                  <div class="row">
                    <div class="form-group">
                        <div class="col-xs-7 col-lg-7">
                            <label>Nome do opcional</label>
                            <input type="text" name="name" class="form-control" placeholder="Opcional" required>
                        </div>
                        <div class="col-xs-5 col-lg-5">
                            <label>Preço</label>
                            <input type="text" name="price" class="form-control money" placeholder="00,00">
                        </div>
                    </div>
                  </div>
                    
                  <div class="row">
                    <div class="col-lg-12 p-t-10">
                     <ul id="options_list" class="list-group">
                        <div class="text-right">
                            <button type="button" class="btn btn-info btn-xs m-b-10 add-field">Adicionar</button>
                        </div>
                        <li class="list-group-item">
                          <label>Nome opção</label>
                          <input type="text" name="optionals[]" class="form-control input-sm optional" required>
                        </li>
                     </ul>  
                    </div>  
                  </div>
                    
                  <div class="row">
                    <div class="form-group">
                        <div class="col-xs-6 col-lg-6 text-center">
                          <div class="checkbox checkbox-success checkbox-circle">
                           <input id="checkbox-10" name="obrigatory" type="checkbox" value="1" checked="">
                           <label for="checkbox-10"> Obrigatório </label>
                          </div>
                       </div>
                        <div class="col-xs-6 col-lg-6 text-center">
                          <div class="checkbox checkbox-success checkbox-circle">
                           <input id="checkbox-11" name="status" type="checkbox" value="1" checked="">
                           <label for="checkbox-11"> Disponível </label>
                          </div>
                       </div>
                    </div>
                  </div>

               <div class="clearfix"></div>
           </div>
           <div class="modal-footer">
            <button type="submit" class="btn btn-success waves-effect">Adicionar</button>
            <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Fechar</button>
        </div>
    </form>

</div>

</div>

</div>


<div id="editItem" data-backdrop="static" data-keyboard="false" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Editar {{optionalsPageTitle}}</h4>
            </div>

            <form method="post" role="editItems" data-table="optionals" id="">
                <div class="modal-body">
                  <div class="row">
                    <div class="form-group">
                        <div class="col-xs-7 col-lg-7">
                            <label>Nome do opcional</label>
                            <input type="text" name="name" class="form-control name" placeholder="Opcional" required>
                        </div>
                        <div class="col-xs-5 col-lg-5">
                            <label>Preço</label>
                            <input type="text" name="price" class="form-control price money" placeholder="00,00">
                        </div>
                    </div>
                  </div>
                    
                  <div class="row">
                    <div class="col-lg-12 p-t-10">
                     <ul id="options_list_edit" class="list-group">
                        <div class="text-right">
                            <button type="button" class="btn btn-info btn-xs m-b-10 add-field" data-function="edit">Adicionar</button>
                        </div>
                     </ul>  
                    </div>  
                  </div>
                    
                  <div class="row">
                    <div class="form-group">
                        <div class="col-xs-6 col-lg-6 text-center">
                          <div class="obrigatory-inner checkbox checkbox-success checkbox-circle"></div>
                        </div>
                    </div>
                  </div>

               <div class="clearfix"></div>
           </div>
           <div class="modal-footer">
            <button type="submit" class="btn btn-success waves-effect">Editar</button>
            <button type="button" class="btn btn-info waves-effect" data-dismiss="modal">Fechar</button>
        </div>
    </form>

</div>

</div>

</div>

</div>
