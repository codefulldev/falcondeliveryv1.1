<?php 
$CI      = & get_instance();
$base    = base_url().$CI->base;
$config  = $CI->ConfigController();
$address = $CI->config_address();

if ($order->shippingData) {
  list($distance, $duration) = explode("|", $order->shippingData);
}else{
  list($distance, $duration) = array('', '');
}

switch ($order->orderType) {
  case '0':
  $deliveryType = '<span class="label label-success">ENTREGA</span>';
  break;

  case '1':
  $deliveryType = '<span class="label label-info">RETIRADA</span>';
  break;

  default:
  $deliveryType = '<span class="label label-danger">MESA</span>';
  break;
}

switch ($order->payment) {
  case 'money':
  $paymentType = 'Dinheiro - R$'.$order->receiver;
  break;

  default:
  if ($order->payment_shippingBrand != '' && $order->payment_shippingBrand != null) {
    $paymentType = 'Maquineta - '.$order->payment_shippingBrand;
  }else{
    $paymentType = 'Cartão de crédito';
  }
  break;
}

if ($addres->taxasvalue == 1) {
    $shippingPrice = (!empty($distance) && $distance != '') ? $CI->helps->shippingPrice($distance, $address->delivery_price) : 0;
}else{

    $CI->db->where('id', $order->clients_address_id);
    $CI->db->where('clients_id', $client->id);
    $addrClient = $CI->db->get('clients_address');

    if ($addrClient->num_rows() > 0) {
        $address_client = $addrClient->row();

        if ($address_client->address) : 
            list($add, $district, $cities) = explode(", ", $address_client->address);

            $this->db->where('bairro', $district);
            $cepbr_bairro = $this->db->get('cepbr_bairro');
            if ($cepbr_bairro->num_rows() > 0) {
                $rs = $cepbr_bairro->row();
                $this->db->where('district_id', $rs->id_bairro);
                $rso = $this->db->get('districts');
                if ($rso->num_rows() > 0) {
                    $shippingPrice = $rso->row()->price;
                } else {
                    $shippingPrice = 0;
                }
            } else {
                $shippingPrice = 0;
            }
        endif;
    }else{
        $shippingPrice = 0;
    }

}

?>
<div class="container-fluid">
  <div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title">Detalhes do pedido</h4> </div>
      <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
        <ol class="breadcrumb">
          <li><a href="<?php echo $base ?>">Página inicial</a></li>
          <li class="active">Pedidos</li>
        </ol>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="white-box printableArea">
          <h3><b>PEDIDO Nº<?= $order->id ?> </b> <span class="pull-right"><?= $deliveryType ?></span></h3>
          <hr>
          <div class="row">
            <div class="col-xs-12">
              <div class="text-right">
                <button id="trash" data-id="<?= $order->id ?>" class="btn btn-danger" type="button"> Apagar pedido </button>
                <button id="cupom" class="btn btn-default btn-outline cupomPrinter" data-id="<?= $order->id ?>" data-via="client" data-amount="<?= $order->receiver ?>" type="button"> <span><i class="fa fa-print"></i> Imprimir via cliente</span></button>
                <button id="cupomkath" class="btn btn-default btn-outline cupomPrinter" data-id="<?= $order->id ?>" data-via="kitchen" data-amount="<?= $order->receiver ?>" type="button"> <span><i class="fa fa-print"></i> Imprimir via cozinha</span></button>
              </div>
            </div>
            <div class="col-md-12">
                <div class="text-left"> 
                    <?php if ($order->clients_id): ?>
                        <address>
                            <h3>Cliente</h3>
                            <h4 class="font-bold"><?= ucwords($client->name) ?>, <?= $CI->helps->formatPhone($client->phone) ?></h4>
                            <p class="text-muted m-l-30"><?php
                            $CI->db->where('id', $order->clients_address_id);
                            $CI->db->where('clients_id', $client->id);
                            $addrGet = $CI->db->get('clients_address');
                            if ($addrGet->num_rows() > 0) {
                            $rowAddress = $addrGet->row();

                            if ($rowAddress->address): 
                                list($addressClient, $numberClient, $referenceClient) = explode("|", $rowAddress->address);
                                list($street, $district, $city) = explode(", ", $addressClient);

                                ?>
                                <?= $street ?>, <?= $numberClient ?>.<br/>
                                <?= $district ?>, <?= $city ?>.<br/>
                                CEP: <?= $rowAddress->cep ?> <br/>
                                Complemento: <?= $referenceClient ?></p>
                            <?php endif; 
                            }else {

                            }
                            if (!empty($order->date)): ?>
                            <p class="m-t-30"><b>Data do pedido:</b> <i class="fa fa-calendar"></i> <?php echo date('d/m/Y H:i', strtotime($order->date)) ?></p>
                            <?php endif;
                            if (!empty($order->date_agend)): ?>
                                <p><b>Data da entrega:</b> <i class="fa fa-calendar"></i> <?php echo date('d/m/Y H:i', strtotime($order->date_agend)) ?></p>
                            <?php endif ?>
                        </address> 
                    <?php else: ?>
                        <h3>Venda via Balcão</h3>
                    <?php endif ?>
                </div>
                <?php if($order->orderType == 0): ?>
                    <div class="col-lg-3">
                        <h5>Entregador</h5>
                        <select id="deliverymen" data-order="<?php echo $order->id ?>" class="form-control">
                            <?php
                                $del = $this->db->get('deliveryman');
                                if($del->num_rows() > 0){
                                    if ($order->deliveryman_id < 1) {
                                        echo "<option selected disabled>Selecione um entregador.</option>";
                                    }
                                    foreach($del->result() as $man):
                                        if($order->deliveryman_id > 0 && $order->deliveryman_id == $man->id){
                                            echo "<option value='$man->id' selected>$man->name</option>";
                                        }else{
                                            echo "<option value='$man->id'>$man->name</option>";
                                        }
                                    endforeach;
                                }else{
                                    echo "<option selected disabled>Nenhum entregador cadastrado.</option>";
                                }
                            ?>
                        </select>
                    </div>
                <?php endif; ?>
            </div>
        <div class="col-md-12">
          <div class="table-responsive m-t-40" style="clear: both;">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th class="text-center">#</th>
                  <th>Produto</th>
                  <th class="text-center">Preço</th>
                  <th class="text-center">Quantidade</th>
                  <th class="text-center">Descontos</th>
                  <th class="text-center">Subtotal</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                $total = 0;
                $totalSubtotal = 0;
                $totalDescount = 0;
                if ($orders_details->num_rows() < 1) {
                 redirect($base.$CI->ordersURI);
               }
               foreach ($orders_details->result() as $detail): 
                $total += $detail->subtotal;
                $subtotal = $shippingPrice / $orders_details->num_rows();
                $subtotal = floatval($detail->subtotal) - floatval($subtotal);
                $totalSubtotal += $subtotal;

                $CI->db->where('id', $detail->product_id);
                $product = $CI->db->get('products')->row();
                
                if (!is_null($order->cupom)):
                  $descount = floatval($product->price) - floatval($subtotal);
                  $descount = str_replace("-", "", $descount);
                  $totalDescount += $descount;
                endif;
                
                $flavors = array();
                if($detail->flavors != '') {
                  $explore_falvors = explode("|", $detail->flavors);
                  $price_flavor = array();
                  foreach ($explore_falvors as $findex => $flv) {
                    $this->db->where('id', $flv);
                    $get_flavor = $this->db->get('products');
                    if ($get_flavor->num_rows() > 0) {
                     $flavor = $get_flavor->row();
                     $price_flavor[] = $flavor->price;
                     if ($flavor->id !== $detail->product_id) {
                      $flavors_index = $findex + 1;
                      $flavors[$flavor->id][] = $flavor->name;
                    }
                  }
                }

              }

              $addons = array();
              $addons_amount = 0;
              if ($detail->addons != '') {
                foreach (explode('|', $detail->addons) as $id) {
                  $this->db->where('id', $id);
                  $addon = $CI->db->get('addons');
                  if ($addon->num_rows() > 0) {
                    $addons_amount += $addon->row()->price * $detail->quantity;
                    $addons[] = '<span class="label label-warning">'. ucfirst($addon->row()->name) .'</span>';
                  }
                }
              }

              $optionals = array();
              $optionals_amount = 0;
              if($detail->optionals != ''):
                $opts = explode("|", $detail->optionals);
                foreach ($opts as $opt) {
                  list($option_id, $odetail_id) = explode(":", $opt);
                  $this->db->where('id', $option_id);
                  $get_optional = $this->db->get('optionals');
                  if ($get_optional->num_rows() > 0) {
                    $optional = $get_optional->row();
                    $this->db->where('id', $odetail_id);
                    $optional_detail = $this->db->get('optionals_options')->row();
                    $cost = ($optional->price > 0) ? 'R$'.$CI->helps->format_money($optional->price) : 'Grátis';
                    $optionals[] = '<span class="label label-info">'.$optional->name.': '.$optional_detail->name.' - '.$cost.'</span>';
                  }
                }
              endif;

              $takes = array();
              if($detail->takes != ''):

                foreach (explode(",", $detail->takes) as $ing) {
                  list($prod_ID, $ingredients_join) = explode("-", $ing);
                   $this->db->where_in("id", explode("|", $ingredients_join));
                   $ingredients = $this->db->get('ingredients');
                   if ($ingredients->num_rows() > 0) {
                     foreach ($ingredients->result() as $ingredient) {
                       $takes[$prod_ID][] = '<span class="label label-danger">'. ucfirst($ingredient->name) .'</span>';
                     }
                   }
               }

             endif;

             ?>
             <tr>
              <td class="text-center"><?= $order->id ?></td>
              <td>
                <?php
                if (count($flavors) > 0) {
                  echo '<div class="text-sbold offset-top-0">';
                  echo '<strong>Meia:</strong> '.$product->name;
                  if (count($takes) > 0) {
                    foreach ($takes as $tidd => $value) {
                      if ($product->id == $tidd) {
                        echo '<br>Retirar: '.join(' ', $value); 
                      }
                    }
                  }
                  foreach ($flavors as $fidd => $value) {
                    echo '<br>';
                    echo '<strong>Meia:</strong> '.join(' ', $value);
                    if (count($takes) > 0) {
                      foreach ($takes as $tidd => $value) {
                        if ($fidd == $tidd) {
                          echo '<br>Retirar: '.join(' ', $value); 
                          echo '<br>';
                        }
                      }
                    }
                  }
                  echo '</div>';
                }
                ?>
                <?= ucfirst($product->name) ?>
                <?php if (!empty($detail->schendule) && $detail->schendule != ''): ?>
                  <span class="btn btn-inverse btn-rounded btn-xs" data-toggle="tooltip" title="" data-original-title="Entrega agendada">
                    <i class="icon-clock"></i>
                    <?= $detail->schendule ?>
                  </span>
                <?php endif ?>

                <?php if (count($takes) > 0 && !empty($takes[$product->id]) && count($flavors) == 0 ): ?>
                 <div>
                   <p><strong>Tirar ingredientes</strong> <br> <?= join(' ', $takes[$product->id]) ?></p>
                 </div>
               <?php endif ?>

               <?php if (count($optionals) > 0): ?>
                 <div>
                   <p><strong>Opcionais</strong><br> <?= join(' ', $optionals) ?></p>
                 </div>
               <?php endif ?>

               <?php if (count($addons) > 0): ?>
                 <div>
                   <p><strong>Adicionais</strong> <small>R$<?php echo $CI->helps->format_money($addons_amount) ?></small> <br> <?= join(' ', $addons) ?></p>
                 </div>
               <?php endif ?>
             </td>
             <td class="text-center"> R$<?= number_format($product->price, 2, ",", ".") ?> </td>
             <td class="text-center"> <?= $detail->quantity ?> </td>
             <td class="text-center"> R$<?= number_format($descount, 2, ",", ".") ?> </td>
             <td class="text-center"> R$<?= number_format($subtotal, 2, ",", ".") ?> </td>
           </tr>
           <?php 
         endforeach 
         ?>
       </tbody>
     </table>
   </div>
 </div>
 <div class="col-md-12">
  <div class="pull-right m-t-30 text-right">
    <p class="text-left"><strong>Subtotal:</strong> R$<?php echo number_format($totalSubtotal, 2, ".", "."); ?></p>
    <p class="text-left"><strong>Valor com descontos:</strong> R$<?php echo number_format($totalDescount, 2, ".", "."); ?></p>
    <p class="text-left"><strong>Taxa de entrega:</strong> R$<?php echo number_format($shippingPrice, 2, ".", "."); ?></p>
    <p class="text-left"><strong>Forma de pagamento: </strong><?= $paymentType ?></p>
    <?php if ($order->send > 0): ?>
      <p class="text-left"><strong>Troco:</strong> R$<?php echo number_format($order->send, 2, ".", ".") ?></p>
    <?php endif ?>
    <h3 class="text-left"><strong>Total :</strong> R$<?php echo number_format($total, 2, ".", ".") ?></h3> </div>
    <div class="clearfix"></div>
    <hr>
  </div>
</div>
</div>
</div>
</div>

</div>