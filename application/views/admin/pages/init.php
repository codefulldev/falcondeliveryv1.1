<?php 
$CI        = & get_instance();
$base      = $CI->base;
// helps statistics
$orders    = $CI->helps->count_orders();
$clients   = $CI->helps->count_clients();
$gainday   = $CI->helps->count_gainday();
$gainmonth = $CI->helps->count_gainmonth();
// recent orders
$recent_orders = $CI->recent_orders();

?>
<div class="container-fluid">
    <div class="row bg-title">

        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Página Inicial</h4> </div>

            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="<?php echo $base ?>">Página Inicial</a></li>
                </ol>
            </div>

        </div>

        <div class="row">
          <div class="col-sm-12">
            <div class="white-box">
             <div class="row row-in">
              <div class="col-lg-3 col-sm-6 row-in-br">
                <ul class="col-in">
                  <li>
                    <span class="circle circle-md bg-danger"><i class="ti-wallet"></i></span>
                  </li>
                  <li class="col-last"><h3 class="counter text-right m-t-15"><?= $orders->count ?></h3></li>
                  <li class="col-middle">
                    <h4>Pedidos</h4>
                    <div class="progress">
                      <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="<?= $orders->processbar ?>" aria-valuemin="0" aria-valuemax="1000" style="width: <?= $orders->processbar ?>%"> 
                        <span class="sr-only"><?= $orders->processbar ?>%</span> 
                      </div>
                    </div>
                  </li>

                </ul>
              </div>
              <div class="col-lg-3 col-sm-6 row-in-br  b-r-none">
                <ul class="col-in">
                  <li>
                    <span class="circle circle-md bg-info"><i class="ti-user"></i></span>
                  </li>
                  <li class="col-last"><h3 class="counter text-right m-t-15"><?= $clients->count ?></h3></li>
                  <li class="col-middle">
                    <h4>Clientes</h4>
                    <div class="progress">
                      <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="<?= $clients->processbar ?>" aria-valuemin="0" aria-valuemax="1000" style="width: <?= $clients->processbar ?>%"> 
                        <span class="sr-only"><?= $clients->processbar ?>%</span> 
                      </div>
                    </div>
                  </li>

                </ul>
              </div>
              <div class="col-lg-3 col-sm-6 row-in-br">
                <ul class="col-in">
                  <li>
                    <span class="circle circle-md bg-success"><i class=" ti-shopping-cart"></i></span>
                  </li>
                  <li class="col-last"><h4 class="text-right m-t-15">R$<?= $gainday->total ?></h4></li>
                  <li class="col-middle">
                    <h4>Ganhos diário</h4>
                    <div class="progress">
                      <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?= $gainday->processbar ?>" aria-valuemin="0" aria-valuemax="1000" style="width: <?= $gainday->processbar ?>%"> 
                        <span class="sr-only"><?= $gainday->processbar ?></span> 
                      </div>
                    </div>
                  </li>

                </ul>
              </div>
              <div class="col-lg-3 col-sm-6  b-0">
                <ul class="col-in">
                  <li>
                    <span class="circle circle-md bg-warning"><i class="fa fa-dollar"></i></span>
                  </li>
                  <li class="col-last"><h4 class="text-right m-t-15">R$<?= $gainmonth->total ?></h4></li>
                  <li class="col-middle">
                    <h4>Ganhos mensal</h4>
                    <div class="progress">
                      <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="<?= $gainmonth->processbar ?>" aria-valuemin="0" aria-valuemax="1000" style="width: <?= $gainmonth->processbar ?>%"> 
                        <span class="sr-only"><?= $gainmonth->processbar ?>%</span> 
                      </div>
                    </div>
                  </li>

                </ul>
              </div>

              <div class="col-xs-12 text-right">
                <a href="#" class="btn btn-primary btn-xs">Ver relatórios</a>
              </div>  
              
            </div> 
          </div>
        </div>
      </div>

      <div class="col-md-12 col-lg-6 col-sm-12">
        <div class="white-box">
          <h3 class="box-title">Vendas mensais
            <div class="col-md-6 col-sm-6 col-xs-6 pull-right">
              <select id="choose__month" class="form-control pull-right row b-none">
                <?php
                  $CI->db->order_by('date', 'DESC');
                  $orders = $CI->db->get('orders');
                ?>
                <?php 
                if ($orders->num_rows() > 0): $months = array(); 
                    foreach ($orders->result() as $order):
                      $this->db->where("order_id", $order->id);
                      if ($this->db->get('orders_details')->num_rows() > 0) {
                        $month = date('m',strtotime($order->date));
                        $month_name = date('n',strtotime($order->date));
                        $year  = date('Y',strtotime($order->date));
                        if (!in_array(array('month' => $month, 'year' => $year, 'month_name' => $month_name), $months)) {
                          array_push($months, array('month' => $month, 'year' => $year, 'month_name' => $month_name));
                        }
                      }
                    endforeach;

                    foreach ($months as $month) {
                      echo '<option value="'. $month['month'] .'">'.$CI->helps->names_month($month['month_name']).', '.$month['year'].'</option>';
                    }
                  endif;
                ?>
              </select>
            </div>
          </h3>
          <div class="row sales-report">
            <div class="col-md-6 col-sm-6 col-xs-6">
              <h2 id="month_text" class="text-info"></h2>
              <p>Relatório mensal</p>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6 ">
              <h1 id="total_orders" class="text-right text-info m-t-20"></h1> </div>
            </div>
            <div class="table-responsive">
              <table id="tableMonths" class="table">
                <thead>
                  <tr>
                    <th>#ID</th>
                    <th>Cliente</th>
                    <th>Status</th>
                    <th>Data</th>
                    <th>Total</th>
                  </tr>
                </thead>
                <tbody id="orders_list"></tbody>
              </table>
             </div>
            </div>
          </div>


<div class="col-md-12 col-lg-6 col-sm-12">
    <div class="white-box">
        <h3 class="box-title">Pedidos recentes</h3>
        <div class="comment-center p-t-10">
         <?php if (!is_null($recent_orders)): ?>
            <?php foreach ($recent_orders as $row):
              $now = date('d');
              $date = date('d', strtotime($row->date));
              if ($date === $now) {
              $payment = ($row->payment == 'delivery') ? 'À vista' : 'Cartão de crédito';
              $this->db->where('id', $row->clients_id);
              $gg = $this->db->get('clients');
              if ($gg->num_rows() > 0) {               

              $client = $gg->row();
              $thumbnail = $CI->helps->verify_avatar($client->thumbnail, 0);
              $this->db->where('order_id', $row->id);
              $this->db->select_sum('subtotal');
              $total_order = $CI->db->get('orders_details')->row();
            ?>
                <div id="recent_order__<?= $row->id ?>" class="comment-body">
                    <div class="user-img"> <img src="<?= $thumbnail ?>" alt="user" class="img-circle"></div>
                    <div class="mail-contnet">
                        <h5><?= $client->name ?> <span class="time" title="<?= $row->date ?>"></span> <span class="label label-rouded label-warning">PENDENTE</span></h5>
                        <span class="mail-desc">
                          <p class="m-0">Forma de pagamento: <strong><?= $payment ?></strong></p>
                          <p class="m-0">Total do pedido: <strong>R$<?= number_format($total_order->subtotal, 2, ",", ".") ?></strong></p>
                        </span> 
                        <a href="javacript:void(0)" class="action__order btn btn btn-rounded btn-default btn-outline m-r-5 recent-action" data-types="accept" data-action="accept" data-order="<?= $row->id ?>" data-id="<?= $row->id ?>" data-content="#recent_order__<?= $row->id ?>">
                            <i class="ti-check text-success m-r-5"></i>Confirmar
                        </a>
                        <a href="javacript:void(0)" class="action__order btn-rounded btn btn-default btn-outline recent-action" data-types="decline" data-action="decline" data-order="<?= $row->id ?>" data-id="<?= $row->id ?>" data-content="#recent_order__<?= $row->id ?>">
                            <i class="ti-close text-danger m-r-5"></i> Recusar
                        </a> 
                    </div>
                </div>
            <?php } } endforeach; ?>
         <?php else: ?>
            <h3 class="text-center">Nenhum pedido recente</h3>
         <?php endif ?>
        </div>
    </div>

    <div class="white-box">
        <h3 class="box-title">Promoção do dia</h3>
        <div class="comment-center">
          
          <?php 
          $CI->db->where('active', '1');
          $CI->db->limit('1');
          $promotions = $CI->db->get('promotions');
          if ($promotions->num_rows() > 0): 
            $promo = $promotions->row();
            $price_format = number_format($promo->promotional_price, 2, ".", ".");
            $price = explode(",", number_format($promo->promotional_price, 2, ",", "."));

            $dateExpire = date('d/m/Y', strtotime($promo->date.' +1 days'));

            list($d,$m,$y) = explode("/", $dateExpire);
            list($nd,$nm,$ny) = explode("/", date('d/m/Y'));

            $expi = trim($d).'/'.trim($m).'/'.trim($y);
            $nows = $nd.'/'.$nm.'/'.$ny;

            if ($nows > $expi) {
              $CI->db->where('id', $promo->id);
              $CI->db->delete('promotions');
              redirect($base);
            }
            ?>

            <div class="col-xs-12 text-right m-b-15">
                <a href="#lightbox--create_promotion" class="popup-with-form btn btn-success btn-sm">Criar promoção</a>
                <a href="#lightbox--edit_promotion" class="popup-with-form btn btn-info btn-sm">Editar promoção</a>
            </div>
            <div class="clearfix"></div>
            <div class="show-active_promotions text-center" style="background-image: url(assets/images/home-slide-1-1920x800.jpg);">
                <span class="pull-right m-t-10 m-r-10" style="position: absolute;right:10px;" data-toggle="tooltip" title="Desativar promoção">
                  <input type="checkbox" data-id="<?= $promo->id ?>" checked class="js-switch" data-color="#ff7676" data-size="small" />
                </span>
                <div class="range range-xs-center text-center p-t-20 p-b-20">
                  <div class="cell-sm-10 cell-lg-6">
                    <h4 class="text-italic divider-custom-small-primary">Promoção do dia</h4>
                    <p class="label label-danger">Expira em: <span class="timerCount" data-date="<?= date('Y-m-d H:i:s', strtotime($promo->date.' +1 days')) ?>"></span></p>
                    <h2 class="text-uppercase text-italic offset-top-10 offset-sm-top-0"><?= $promo->title ?></h2>
                    <div class="label-price offset-top-10">
                      <div class="unit unit-horizontal unit-middle unit-spacing-xs">
                        <div class="unit-left">
                          <h1 class="price-promotions"><?= $price[0] ?><sup class="h3 price-promotions">,<?= $price[1] ?></sup></h1>
                        </div>
                        <div class="unit-body">
                           <ul class="big text-left">
                            <?php
                            $explode_items = explode('|', $promo->products_id);
                            foreach ($explode_items as $ids) {
                               $CI->db->where('id', $ids);
                               $product_title = $CI->db->get('products')->row()->name; 
                               echo '<li>'. $product_title .'</li>';
                            }
                            ?>
                           </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            
            <div id="lightbox--edit_promotion" class="white-popup-block mfp-hide">
              <form action="" method="post">

                <div class="col-xs-12 form-group">
                  <label class="control-label">Titulo</label>
                  <input type="text" name="title" value="<?= $promo->title ?>" class="form-control" required>
                  <input type="hidden" name="promotionID" value="<?= $promo->id ?>">
                </div>

                <div class="col-lg-6">
                  <div class="form-group">
                    <label class="control-label">Produtos</label><br>
                    <select class="selectpicker" name="products[]" data-width="100%" data-style="form-control" data-title="Selecione os produtos" data-max-options="3" multiple required>
                      <?php
                      $get = $CI->db->get('category');
                      if ($get->num_rows() > 0) {
                        foreach ($get->result() as $category):
                          $CI->db->where('status', '1');
                          $CI->db->where('category_id', $category->id);
                          $got = $CI->db->get('products');

                          if ($got->num_rows() > 0) {
                            echo '<optgroup label="'.strtoupper($category->name).'">';  
                            foreach ($got->result() as $product) {
                              if (in_array($product->id, $explode_items)) {
                                echo '<option value="'. $product->id .'" selected>'. ucwords($product->name) .'</option>';
                              }else{
                                echo '<option value="'. $product->id .'">'. ucwords($product->name) .'</option>';
                              }
                            }
                            echo '</optgroup>';
                          }

                        endforeach;
                      }else{
                        echo '<option selected disabled>Sem categorias</option>';
                      }
                      ?>
                    </select>
                  </div>
                </div>

                <div class="col-lg-6">
                  <div class="form-group">
                    <label>Preço promocional</label>
                    <input type="text" name="price" value="<?= $price_format ?>" class="form-control" required>
                  </div>
                </div>

                <p class="col-xs-12 hidden"><code>OBS: A duração das promoções diárias duram 24h. Você pode prolongá-la definindo abaixo.</code></p>
                <div class="col-lg-6 hidden">
                  <label>Hora</label>
                  <div class="input-group clockpicker">
                    <input type="text" name="hour" class="form-control" placeholder="00:00"> <span class="input-group-addon"> <span class="glyphicon glyphicon-time"></span> </span>
                  </div>
                </div>

                <div class="col-lg-6 hidden">
                  <label>Data</label>
                  <div class="input-group">
                    <input type="text" name="date" class="form-control mydatepicker" placeholder="dd/mm/aaaa"> <span class="input-group-addon"><i class="icon-calender"></i></span> 
                  </div>
                </div>

                <div class="col-xs-12 m-t-15 text-right">
                 <button type="submit" class="btn btn-info m-l-15">Editar promoção</button>
               </div>

             </form>

             <div class="clearfix"></div>
           </div>
          <?php else: ?>
          
          <div class="col-xs-12 text-center">
              <p>O que você tem de especial hoje? Crie uma promoção.</p>
              <a href="#lightbox--create_promotion" class="popup-with-form btn btn-success btn-sm">Criar promoção</a>
          </div>
          <div class="clearfix"></div>
          <?php endif ?>

          <div id="lightbox--create_promotion" class="white-popup-block mfp-hide">
            <form method="post" role="save_promotion">
          
              <div class="col-xs-12 form-group">
                <label class="control-label">Titulo</label>
                <input type="text" name="title" class="form-control" required>
              </div>

              <div class="col-lg-6">
                  <div class="form-group">
                    <label class="control-label">Produtos</label><br>
                    <select class="selectpicker" name="products[]" data-width="100%" data-style="form-control" data-title="Selecione os produtos" data-max-options="3" multiple required>
                        <?php
                          $get = $CI->db->get('category');
                          if ($get->num_rows() > 0) {
                            foreach ($get->result() as $category):
                              $CI->db->where('status', '1');
                              $CI->db->where('category_id', $category->id);
                              $got = $CI->db->get('products');

                              if ($got->num_rows() > 0) {
                                echo '<optgroup label="'.strtoupper($category->name).'">';  
                                foreach ($got->result() as $product) {
                                  echo '<option value="'. $product->id .'">'. ucwords($product->name) .'</option>';
                                }
                                echo '</optgroup>';
                              }

                            endforeach;
                          }else{
                            echo '<option selected disabled>Sem categorias</option>';
                          }
                        ?>
                    </select>
                  </div>
              </div>

              <div class="col-lg-6">
                  <div class="form-group">
                      <label>Preço promocional</label>
                      <input type="text" name="price" class="form-control" required>
                  </div>
              </div>
              
              <p class="col-xs-12 hidden"><code>OBS: A duração das promoções diárias duram 24h. Você pode prolongá-la definindo abaixo.</code></p>
              <div class="col-lg-6 hidden">
                  <label>Hora</label>
                  <div class="input-group clockpicker">
                    <input type="text" name="hour" class="form-control" placeholder="00:00"> <span class="input-group-addon"> <span class="glyphicon glyphicon-time"></span> </span>
                  </div>
              </div>

              <div class="col-lg-6 hidden">
                  <label>Data</label>
                  <div class="input-group">
                    <input type="text" name="date" class="form-control mydatepicker" placeholder="dd/mm/aaaa"> <span class="input-group-addon"><i class="icon-calender"></i></span> 
                  </div>
              </div>
          
              <div class="col-xs-12 m-t-15 text-right">
                  <span class="p-r-15">
                     <input id="active_promotion" type="checkbox" name="active" value="1" checked class="js-switch" data-color="#2cabe3" data-size="small" />
                     <label for="active_promotion"> Publicar</label>
                  </span>
                  <button type="submit" class="btn btn-info m-l-15">+ Criar promoção</button>
              </div>

            </form>
            
            <div class="clearfix"></div>
          </div>

        </div>

    </div>
</div>

</div>