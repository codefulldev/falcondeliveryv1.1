<?php
 $CI =& get_instance();
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<base href="<?php echo base_url(); ?>">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" type="image/png" sizes="16x16" href="themes/plugins/images/favicon.png">
	<title>Instalação Automática</title>
	<!-- Bootstrap Core CSS -->
	<link href="themes/ampleadmin/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<!--alerts CSS -->
	<link href="themes/plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
	<!-- Menu CSS -->
	<link href="themes/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
	<!-- Wizard CSS -->
	<link href="themes/plugins/bower_components/jquery-wizard-master/css/wizard.css" rel="stylesheet">
	<!-- animation CSS -->
	<link href="themes/ampleadmin/css/animate.css" rel="stylesheet">
	<!-- Custom CSS -->
	<link href="themes/ampleadmin/css/style.css" rel="stylesheet">
	<!-- color CSS -->
	<link href="themes/ampleadmin/css/colors/blue-dark.css" id="theme" rel="stylesheet">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>

	<div id="wrapper">
		<div id="page-wrapper" style="margin:0;padding:30px;">
			<div class="section-sm-100">
				<!-- .row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Configurações do sistema</h3>
                            <p class="text-muted m-b-30 font-13"> Preencha os dados corretamente, as configurações não poderão ser disfeitas.</p>
                            <div id="configurations" class="wizard">
                                <ul class="wizard-steps" role="tablist">
                                    <li class="active" role="tab">
                                        <h4><span><i class="ti-user"></i></span>Banco de dados</h4> </li>
                                    <li role="tab">
                                        <h4><span><i class="ti-credit-card"></i></span>Sistema</h4> </li>
                                    <li role="tab">
                                        <h4><span><i class="ti-check"></i></span>Finalizar</h4> </li>
                                </ul>
                                <form id="validation" method="post" class="form-horizontal">
                                    <div class="wizard-content">
                                        <div class="wizard-pane active" role="tabpanel">
											
                                        	<div class="col-lg-6 col-xs-12 form-group">
                                        		<div class="p-10">
                                        			<label class="control-label">Host</label>
                                        			<input type="text" class="form-control" name="host" placeholder="Padrão: localhost" /> 
                                        		</div>
                                        	</div>
                                        	<div class="col-lg-6 col-xs-12 form-group">
                                        		<div class="p-10">
                                        			<label class="control-label">Usuário</label>
                                        			<input type="text" class="form-control" name="username" /> 
                                        		</div>
                                        	</div>
                                        	<div class="col-lg-6 col-xs-12 form-group">
                                        		<div class="p-10">
                                        			<label class="control-label">Senha</label>
                                        			<input type="text" class="form-control" name="password" /> 
                                        		</div>
                                        	</div>
                                        	<div class="col-lg-6 col-xs-12 form-group">
                                        		<div class="p-10">
                                        			<label class="control-label">Banco de dados</label>
                                        			<input type="text" class="form-control" name="database" /> 
                                        		</div>
                                        	</div>
											<div class="clearfix"></div>
                                        </div>

                                        <div class="wizard-pane" role="tabpanel">

                                        	<p><strong>Configurações gerais</strong></p>
                                            <div class="col-lg-6 col-xs-12 form-group">
                                        		<div class="p-10">
                                        			<label class="control-label">E-mail</label>
                                        			<input type="text" class="form-control" name="email" /> 
                                        		</div>
                                        	</div>

                                            <div class="col-lg-6 col-xs-12 form-group">
                                        		<div class="p-10">
                                        			<label class="control-label">Telefone</label>
                                        			<input type="text" class="form-control" name="phone" /> 
                                        		</div>
                                        	</div>

                                        	<p><strong>Configurações de endereço</strong></p>
                                            <div class="col-lg-6 col-xs-12 form-group">
                                        		<div class="p-10">
                                        			<label class="control-label">CEP</label>
                                        			<input type="text" class="form-control" name="cep" /> 
                                        		</div>
                                        	</div>

                                            <div class="col-lg-6 col-xs-12 form-group">
                                        		<div class="p-10">
                                        			<label class="control-label">Endereço</label>
                                        			<input type="text" class="form-control" name="address" /> 
                                        		</div>
                                        	</div>

                                            <div class="col-lg-6 col-xs-12 form-group">
                                        		<div class="p-10">
                                        			<label class="control-label">Complemento</label>
                                        			<input type="text" class="form-control" name="complement" /> 
                                        		</div>
                                        	</div>

                                            <div class="col-lg-6 col-xs-12 form-group">
                                        		<div class="p-10">
                                        			<label class="control-label">Número</label>
                                        			<input type="text" class="form-control" name="number" /> 
                                        		</div>
                                        	</div>

                                            <div class="col-lg-6 col-xs-12">
                                        		<div id="maps"></div>
                                        	</div>

                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="wizard-pane" role="tabpanel">

                                            <div class="form-group">
                                                <label class="control-label">Password</label>
                                                <input type="password" class="form-control" name="password" /> 
                                            </div>

                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->


			</div>
		</div>
	</div>


	<!-- jQuery -->
	<script src="themes/plugins/bower_components/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap Core JavaScript -->
	<script src="themes/ampleadmin/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- Menu Plugin JavaScript -->
	<script src="themes/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
	<!--slimscroll JavaScript -->
	<script src="themes/ampleadmin/js/jquery.slimscroll.js"></script>
	<!--Wave Effects -->
	<script src="themes/ampleadmin/js/waves.js"></script>
	<!-- Form Wizard JavaScript -->
	<script src="themes/plugins/bower_components/jquery-wizard-master/dist/jquery-wizard.min.js"></script>
	<!-- FormValidation -->
	<link rel="stylesheet" href="themes/plugins/bower_components/jquery-wizard-master/libs/formvalidation/formValidation.min.css">
	<!-- FormValidation plugin and the class supports validating Bootstrap form -->
	<script src="themes/plugins/bower_components/jquery-wizard-master/libs/formvalidation/formValidation.min.js"></script>
	<script src="themes/plugins/bower_components/jquery-wizard-master/libs/formvalidation/bootstrap.min.js"></script>
	<!-- Custom Theme JavaScript -->
	<script src="themes/ampleadmin/js/custom.min.js"></script>
	<!-- Sweet-Alert  -->
	<script src="themes/plugins/bower_components/sweetalert/sweetalert.min.js"></script>
	<script type="text/javascript">
		(function () {

			$('#configurations').wizard({
				onInit: function () {
					$('#validation').formValidation({
						framework: 'bootstrap'
						, fields: {
							username: {
								validators: {
									notEmpty: {
										message: 'O usuário precisa ser preenchido'
									}
								}
							}
							, database: {
								validators: {
									notEmpty: {
										message: 'O banco de dados precisa ser preenchido'
									}
								}
							}
							, cep: {
								validators: {
									notEmpty: {
										message: 'O cep é obrigatório'
									}
								}
							}
							, address: {
								validators: {
									notEmpty: {
										message: 'O endreço é obrigatório'
									}
								}
							}
							, complement: {
								validators: {
									notEmpty: {
										message: 'Informe um complemento'
									}
								}
							}
							, number: {
								validators: {
									notEmpty: {
										message: 'Informe um número, se não tiver, digite: S/N'
									}
								}
							}
						}
					});
				}
				, validator: function () {
					var fv = $('#validation').data('formValidation');
					var $this = $(this);
                    // Validate the container
                    fv.validateContainer($this);
                    var isValidStep = fv.isValidContainer($this);
                    if (isValidStep === false || isValidStep === null) {
                    	return false;
                    }
                    return true;
                }
            });

            $(document).on('click', '.wizard-finish', function(e){
                e.preventDefault();
                var data = $("#validation").serialize();
                $.ajax({
                    url: 'install',
                    type: 'post',
                    data: data,
                    success: function(response){
                        console.log(response);
                    }
                })

            });
		})();
	</script>
	<!--Style Switcher -->
	<script src="themes/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>

</body>
</html>