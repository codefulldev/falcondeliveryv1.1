<?php
$CI =& get_instance();
?>
<div class="col-xs-12">
  <div class="table-responsive">
    <table class="table table-primary">
      <thead>
        <tr>
          <th>#ID</th>
          <th>Data</th>
          <th>Status</th>
          <th>Total</th>
        </tr>
      </thead>
      <tbody>
        <?php $CI->client_orders_table() ?>
      </tbody>
      <tfoot>
        <tr>
          <td>Todos os pedidos</td>
          <td></td>
          <td>Total</td>
          <td><?= $CI->client_orders_table_total() ?></td>
        </tr>
      </tfoot>
    </table>
  </div>
</div>