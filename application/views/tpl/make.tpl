<div class="col-lg-12 col-xs-12">
  <div class="form-group text-center">
      <input id="choose-1" name="choose-qtd" class="chooses" value="1" type="radio" checked="">
      <label for="choose-1"> Inteira </label>
      <input id="choose-2" name="choose-qtd" class="chooses" value="2" type="radio">
      <label for="choose-2"> Meia </label>
  </div>
</div>

<div class="col-lg-12 col-xs-12">
  <div id="mount" class="pieContainer">
    <div id="1" class="pie big" data-start="1" data-value="360" style="background-image: url(<?php echo $thumbnail ?>);background-repeat: no-repeat; background-size: cover; background-position: left top; border-radius: 100%;moz-border-radius: 100%;webkit-border-radius: 100%;o-border-radius: 100%;"></div>
   <div id="pieBackground" class="pieBackground-1"></div>
 </div>
</div>

<div class="col-lg-12 col-xs-12">
  
  <div id="chooses">

    <div id="second-flavor" class="col-lg-6" style="display:none;"">

      <div class="clearfix"></div>
      <div class="sidepizza-front-left col-xs-12" style="display:none;margin:0;padding:0;">
        <div class="white-box">
          <div id="ingredients-left" class="box-title">
            Ingredientes
          </div>
          <ul class="ingredientsStyles"></ul>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
     <div class="clearfix"></div>
    <div class="col-lg-6">
      <div id="getID" data-id="<?= $product->id ?>"></div>
      <div id="choose-<?= $product->id ?>" class="box-flavor first-flavor">
        <div class="left-img">
          <img src="<?php echo $thumbnail ?>" alt="Primeiro sabor">
        </div>
        <div class="before-info">
          <p><?= $product->name.' - R$'.number_format($product->price, 2, ',', '.'); ?></p>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="visible-xs visible-sm">
        <div class="clearfix"></div>
      </div>
      <div class="sidepizza-front-right col-xs-12" style="display:none;margin:0;padding:0;">
        <div class="white-box">
          <div id="ingredients-right" class="box-title">
            Ingredientes
          </div>
          <ul class="ingredientsStyles"></ul>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="clearfix"></div>
    </div>

  </div>

</div>


<div id="choose-flavor" class="modal fade" style="margin-top:5%;" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-body">
        <div class="search-list">
          <input id="search-list" type="text" class="form-control" placeholder="Encontrar sabor">
          <div class="search-list-icon">
            <i class="fa fa-search"></i>
          </div>
        </div>
        <ul id="list-menu"></ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>

  </div>
</div>
