<?php
$CI =& get_instance();
$user = $CI->user();
?>
<h5>Minha conta</h5>
<p>Bem-vindo, <?= $user->name ?>.</p>

<div class="row">

	<div class="table-responsive">
		<table class="table">
			<tbody>
				<tr>
					<td>Cliente desde:</td>
					<td><?= date('d/m/Y H:s', strtotime($user->since)) ?></td>
				</tr>
				<tr>
					<td>E-mail:</td>
					<td><?= $user->email ?></td>
				</tr>
				<tr>
					<td>Telefone:</td>
					<td><?= $CI->helps->formatPhone($user->phone) ?></td>
				</tr>
				<tr>
					<td>Compras completadas:</td>
					<td><?= $CI->helps->orders($user->id, 1); ?></td>
				</tr>
				<tr>
					<td>Pedidos aprovados:</td>
					<td><?= $CI->helps->orders($user->id, 1); ?></td>
				</tr>
				<tr>
					<td>Pedidos pendentes:</td>
					<td><?= $CI->helps->orders($user->id, 0); ?></td>
				</tr>
				<tr>
					<td>Pedidos cancelados:</td>
					<td><?= $CI->helps->orders($user->id, 2); ?></td>
				</tr>
			</tbody>
		</table>
	</div>

</div>