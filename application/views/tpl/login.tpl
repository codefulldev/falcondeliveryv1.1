<?php
  $input = '<input type="hidden" name="request" value="'.$_SERVER['REQUEST_URI'].'">';
?>
<section id="socialSignin" class="bg-white text-center section-50 section-sm-100">
  <div class="shell">
    <div class="range range-xs-center">
      <div class="cell-sm-8 cell-lg-6">
        <div class="responsive-tabs responsive-tabs-modern responsive-tabs-modern-mod-1 responsive-tabs-horizontal">
          <ul class="resp-tabs-list">
            <li>Entrar</li>
            <li>Criar conta grátis</li>
          </ul>
          <div class="resp-tabs-container">
            <div>
              <div class="group-sm">
                <a href="javascript:void(0);" class="btn btn-facebook btn-icon btn-icon-left" data-oauth="facebook" data-appid="<?= $appid ?>" data-version="<?= $version ?>">
                  <span class="icon icon-xs fa-facebook"></span><span>Facebook</span>
                </a>
                <a href="javascript:void(0);" id="google_oauth" rel="<?= $clientid ?>" class="btn btn-google-plus btn-icon btn-icon-left">
                  <span class="icon icon-xs fa-google-plus"></span><span>Google +</span>
                </a>
              </div>
              <div class="divider-custom offset-top-30">
                <p class="small text-uppercase">Ou</p>
              </div>
              <!-- RD Mailform-->
              <form role="lclients" class="rd-mailform form-register text-center offset-top-20">
                <div class="form-group">
                  <label for="client-email" class="form-label form-label-outside">E-mail</label>
                  <input id="client-email" type="email" name="email" data-constraints="@Required @Email" class="form-control">
                </div>
                <div class="form-group">
                  <label for="client-password" class="form-label form-label-outside">Senha</label>
                  <input id="client-password" type="password" name="password" data-constraints="@Required" class="form-control">
                </div>
                <div class="form-group text-right">
                  <label><a href="javascript: void(0)" id="recoverypassword" data-target="#lightbox--recoverypassword">Redefinir minha senha</a></label>
                </div>

                <?= $input ?>
                <div class="offset-top-50">
                  <button type="submit" class="btn btn-primary-lighter btn-fullwidth btn-shadow">Entrar</button>
                </div>
              </form>

              <div id="lightbox--recoverypassword" class="white-popup-block mfp-hide text-center">
               <h4 class="text-center">Redefinir senha</h4>
               <p class="text-center">Informe o seu e-mail e em seguida nós enviaremos uma nova senha.</p>
               <div id="recoveryDetails" class="form-group text-center"></div>
               <div class="form-group">
                <input type="text" class="form-control" name="recoveryemail">
               </div>
               <div class="form-group text-center">
                 <button type="button" id="recoverybutton" class="btn btn-danger">Redefinir</button>
               </div>
              </div>

            </div>
            <div>
              <!-- RD Mailform-->
              <form role="rclients" class="rd-mailform form-register text-center offset-top-20">
                <div class="form-group">
                  <label for="register-form-name" class="form-label form-label-outside">Nome</label>
                  <input id="register-form-name" type="text" name="name" data-constraints="@Required" class="form-control">
                </div>
                <div class="form-group">
                  <label for="register-form-phone" class="form-label form-label-outside">Telefone</label>
                  <input id="register-form-phone" type="text" name="phone" data-constraints="@Required" class="form-control">
                </div>
                <div class="form-group">
                  <label for="register-form-email" class="form-label form-label-outside">E-mail</label>
                  <input id="register-form-email" type="email" name="email" data-constraints="@Required @Email" class="form-control">
                </div>
                <div class="form-group">
                  <label for="register-form-password" class="form-label form-label-outside">Senha</label>
                  <input id="register-form-password" type="password" name="password" data-constraints="@Required" class="form-control">
                </div>
                <div class="form-group">
                  <label for="register-form-password-confirm" class="form-label form-label-outside">Confirmar senha</label>
                  <input id="register-form-password-confirm" type="password" name="password-confirm" data-constraints="@Required" class="form-control">
                </div>
                <?php echo $input ?>
                <div class="offset-top-50">
                  <button type="submit" class="btn btn-primary-lighter btn-fullwidth btn-shadow">Criar conta</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>