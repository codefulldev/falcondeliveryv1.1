<?php
$CI =& get_instance();
$address = $CI->config_address();
$city = explode(" - ", $address->city);
$user = $CI->user();
$addr = explode("|", $user->address);
$districts = isset($address->districts) ? explode("|", $address->districts) : null; 
$districts_array = array();
if (!is_null($districts)) {
	foreach ($districts as $id_bairro) {
		$CI->db->where("id_bairro", $id_bairro);
		$get = $CI->db->get("cepbr_bairro");
		if ($get->num_rows() > 0) {
			array_push($districts_array, $get->row()->bairro);
		}
	}
}
?>
<h5>Meus endereços</h5>
<section>

	<div class="modal fade" id="addAddress" tabindex="-1" role="dialog" aria-labelledby="Adicionar endereço">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form action="<?= base_url().'address' ?>" method="post" role="account_address">
					<div class="modal-header">
						<h5 class="modal-title">Adicionar endereço</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<input type="hidden" name="activerecord" value="true">
						<div class="form-group">
							<label>Nome</label>
							<input type="text" class="form-control" name="name" required="">
							<small class="text-danger">* Você pode nomear seus endereços, exemplo: Casa, Trabalho, etc.</small>
						</div>

						<div class="form-group">
							<label>CEP</label>
							<input type="text" class="form-control" name="cep" data-location="<?= trim($city[0]) ?>" data-districts="<?= join(",", $districts_array) ?>" required="">
						</div>

						<div class="form-group">
							<label>Endereço</label>
							<input type="text" class="form-control" name="address" required="" readonly="true">
						</div>

						<div class="form-group">
							<label>Número</label>
							<input type="text" class="form-control" name="address_number" required="">
						</div>

						<div class="form-group">
							<label>Referência</label>
							<input type="text" class="form-control" name="reff" required="">
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary">Adicionar</button>
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="shell">
		<div class="range range-xs-center">
			<div class="cell-xs-12">
				<div class="responsive-tabs responsive-tabs-button responsive-tabs-vertical">
					<ul class="resp-tabs-list">
						<?php
							$CI->db->where('clients_id', $user->id);
							$get = $CI->db->get('clients_address');
							if($get->num_rows() > 0){
							  foreach($get->result() as $row){
							  	echo '<li id="navs-'.$row->id.'">'.$row->name.'</li>';
							  }
							}else{
							  echo 'Nenhum endereço!';
							}
						?>
						<a href="javascript:void(0)" data-toggle="modal" data-target="#addAddress">Adicionar</a>
					</ul>
					<div class="resp-tabs-container text-left">
						<?php
						  if($get->num_rows() > 0){
							foreach($get->result() as $row){
							$adr = explode("|", $row->address);
						?>
						<div id="tabs-<?= $row->id ?>">
							<form action="<?= base_url().'address' ?>" method="post" role="account_address">
								<input type="hidden" name="id" value="<?= $row->id ?>">
								<div class="form-group">
									<label>Nome</label>
									<input type="text" class="form-control" name="name" value="<?= $row->name ?>" required="">
								</div>

								<div class="form-group">
									<label>CEP</label>
									<input type="text" class="form-control" name="cep" data-location="<?= trim($city[0]) ?>" value="<?= $user->cep ?>" required="">
								</div>

								<div class="form-group">
									<label>Endereço</label>
									<input type="text" class="form-control" name="address" value="<?php echo isset($adr[0]) ? $adr[0] : '' ?>" required="" readonly="true">
								</div>

								<div class="form-group">
									<label>Número</label>
									<input type="text" class="form-control" name="address_number" value="<?= isset($adr[1]) ? $adr[1] : '' ?>" required="">
								</div>

								<div class="form-group">
									<label>Referência</label>
									<input type="text" class="form-control" name="reff" value="<?= isset($adr[2]) ? $adr[2] : '' ?>" required="">
								</div>

								<div class="form-group">
									<button type="submit" class="btn btn-primary">Salvar</button>
									<button type="button" class="btn removeaddress btn-danger" data-id="<?= $row->id ?>">Excluir</button>
								</div>

							</form>
						</div>
						<?php
							}
						  }else{
						?>
						<form action="<?= base_url().'address' ?>" method="post" role="account_address">
							<p>Adicione pelo menos 1(um) endereço</p>
							<input type="hidden" name="activerecord" value="true">
							<div class="form-group">
								<label>Nome</label>
								<input type="text" class="form-control" name="name" required="">
								<small class="text-danger">* Você pode nomear seus endereços, exemplo: Casa, Trabalho, etc.</small>
							</div>

							<div class="form-group">
								<label>CEP</label>
								<input type="text" class="form-control" name="cep" data-location="<?= trim($city[0]) ?>" data-districts="<?= join(",", $districts_array) ?>" required="">
							</div>

							<div class="form-group">
								<label>Endereço</label>
								<input type="text" class="form-control" name="address" required="" readonly="true">
							</div>

							<div class="form-group">
								<label>Número</label>
								<input type="text" class="form-control" name="address_number" required="">
							</div>

							<div class="form-group">
								<label>Referência</label>
								<input type="text" class="form-control" name="reff" required="">
							</div>

							<div class="form-group">
								<button type="submit" class="btn btn-primary">Adicionar</button>
							</div>
						</form>
						<?php
						}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>