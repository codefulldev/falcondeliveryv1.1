<?php
$CI =& get_instance();
$user = $CI->user();
?>
<form action="<?= base_url().'settings' ?>" method="post" role="account_settings">
	<p>
		Cadastrado em <?= date('d/m/Y H:i', strtotime($user->since)) ?>
	</p>
	<div class="form-group">
		<label>Nome</label>
		<input type="text" class="form-control" name="name" value="<?= $user->name ?>" required="">
	</div>

	<div class="form-group">
		<label>E-mail</label>
		<input type="text" class="form-control" name="email" value="<?= $user->email ?>" required="">
	</div>

	<div class="form-group">
		<label>Senha</label>
		<input type="password" class="form-control" name="password" placeholder="*****">
	</div>

	<div class="form-group">
		<label>Telefone</label>
		<input type="text" class="form-control" name="phone" value="<?= $user->phone ?>" required="">
	</div>

	<div class="form-group text-right">
		<a href="<?= $_SERVER['REQUEST_URI'].'/delete' ?>">Excluir minha conta</a>
	</div>

	<div class="form-group">
		<button type="submit" class="btn btn-primary">Salvar</button>
	</div>

</form>