<link rel="stylesheet" href="<?php echo base_url() ?>themes/ampleadmin/bootstrap/dist/css/bootstrap.min.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>themes/ampleadmin/css/style.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>themes/ampleadmin/css/colors/blue-dark.css" />
<?php 
  $CI      = & get_instance();
  $config  = $CI->config_website();
  $address = $CI->config_address();

  if ($order->shippingData) {
    list($distance, $duration) = explode("|", $order->shippingData);
  }else{
    list($distance, $duration) = array('', '');
  }

  $deliveryType = (!empty($distance) && $distance != '') ? '<span class="label label-success">DELIVERY</span>' : '<span class="label label-info">RETIRADA</span>';
  $shippingPrice = (!empty($distance) && $distance != '') ? $CI->helps->shippingPrice($distance, $address->delivery_price) : 0;
  
?>
<div id="printArea" class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Detalhes do pedido</h4> </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="white-box printableArea">
                    <h3><b>PEDIDO <?= $deliveryType ?></b> <span class="pull-right">#<?= $order->id ?></span></h3>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="pull-left"> <address>
                              <h3> &nbsp;<b class="text-danger"><?php echo $config->title ?></b></h3>
                              <p class="text-muted m-l-5"><?= $address->street ?>, Nº <?= $address->number ?>.<br/>
                              <?= $address->locality ?>, <?= $address->city ?>. <br/>
                              CEP: <?= $address->cep ?>, <br>
                              Referência: <?= $address->complement ?>
                            </p>
                        </address> </div>
                        <div class="pull-right text-right"> <address>
                          <h3>Cliente,</h3>
                          <h4 class="font-bold"><?= ucwords($client->name) ?>,</h4>
                          <p class="text-muted m-l-30"><?php if ($client->address): $addr = explode("|", $client->address); $cAddress = explode(",", $addr[0]); $number = $addr[1];?>
                          <?= $cAddress[0] ?>, <?= $number ?>.<br/>
                         <?= $cAddress[1] ?>, <?= $cAddress[2] ?>.<br/>
                          CEP: <?= $client->cep ?> <br/>
                          Complemento: <?= $client->refer ?></p>
                          <?php endif ?>
                        <?php if (!empty($order->date)): ?>
                            <p class="m-t-30"><b>Data do pedido:</b> <i class="fa fa-calendar"></i> <?php echo date('d/m/Y H:i', strtotime($order->date)) ?></p>
                        <?php endif ?>
                        <?php if (!empty($order->date_agend)): ?>
                            <p><b>Data da entrega:</b> <i class="fa fa-calendar"></i> <?php echo date('d/m/Y H:i', strtotime($order->date_agend)) ?></p>
                        <?php endif ?>
                    </address> </div>
                </div>
                <div class="col-md-12">
                    <div class="table-responsive m-t-40" style="clear: both;">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th>Produto</th>
                                    <th class="text-right">Quantidade</th>
                                    <th class="text-right">Preço</th>
                                    <th class="text-right">Subtotal</th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php 
                                $total = 0;
                                if ($orders_details->num_rows() < 1) {
                                   redirect($base.$CI->ordersURI);
                                }
                                foreach ($orders_details->result() as $detail): 
                                $total += $detail->subtotal;

                                $CI->db->where('id', $detail->product_id);
                                $product = $CI->db->get('products')->row();

                                $flavors = array();
                                if($detail->flavors != '') {
                                  $explore_falvors = explode("|", $detail->flavors);
                                  $price_flavor = array();
                                  foreach ($explore_falvors as $findex => $flv) {
                                    $this->db->where('id', $flv);
                                    $get_flavor = $this->db->get('products');
                                    if ($get_flavor->num_rows() > 0) {
                                     $flavor = $get_flavor->row();
                                     $price_flavor[] = $flavor->price;
                                     if ($flavor->id !== $detail->product_id) {
                                      $flavors_index = $findex + 1;
                                      $flavors[$flavor->id][] = '<span class="label label-warning label-lg">'.$flavors_index.'º Sabor: <strong>'. $flavor->name .'</strong></span>';
                                    }
                                  }
                                }

                                }

                                $addons = array();
                                $addons_amount = 0;
                                if ($detail->addons != '') {
                                    foreach (explode('|', $detail->addons) as $id) {
                                      $this->db->where('id', $id);
                                      $addon = $CI->db->get('addons');
                                      if ($addon->num_rows() > 0) {
                                        $addons_amount += $addon->row()->price * $detail->quantity;
                                        $addons[] = '<span class="label label-warning">'. ucfirst($addon->row()->name) .'</span>';
                                      }
                                  }
                                }

                                $optionals = array();
                                $optionals_amount = 0;
                                if($detail->optionals != ''):
                                  $opts = explode("|", $detail->optionals);
                                  foreach ($opts as $opt) {
                                    list($option_id, $odetail_id) = explode(":", $opt);
                                    $this->db->where('id', $option_id);
                                    $get_optional = $this->db->get('optionals');
                                    if ($get_optional->num_rows() > 0) {
                                      $optional = $get_optional->row();
                                      $this->db->where('id', $odetail_id);
                                      $optional_detail = $this->db->get('optionals_options')->row();
                                      $cost = ($optional->price > 0) ? 'R$'.$CI->helps->format_money($optional->price) : 'Grátis';
                                      $optionals[] = '<span class="label label-info">'.$optional->name.': '.$optional_detail->name.' - '.$cost.'</span>';
                                    }
                                  }
                                endif;

                                $takes = array();
                                if ($detail->takes != '') {
                                  foreach (explode(",", $detail->takes) as $ing) {
                                   if(count(explode("-", $ing)) > 1){
                                     list($prod_ID, $ingredients_join) = explode("-", $ing);
                                     $this->db->where_in("id", explode("|", $ingredients_join));
                                     $ingredients = $this->db->get('ingredients');
                                     if ($ingredients->num_rows() > 0) {
                                       foreach ($ingredients->result() as $ingredient) {
                                         $takes[$prod_ID][] = '<span class="label label-danger">'. ucfirst($ingredient->name) .'</span>';
                                       }
                                     }
                                    }
                                  }
                                }

                              ?>
                                <tr>
                                    <td class="text-center"><?= $order->id ?></td>
                                    <td>
                                        <?= ucfirst($product->name) ?>
                                        <?php if (!empty($detail->schendule) && $detail->schendule != ''): ?>
                                          <span class="btn btn-inverse btn-rounded btn-xs" data-toggle="tooltip" title="" data-original-title="Entrega agendada">
                                            <i class="icon-clock"></i>
                                            <?= $detail->schendule ?>
                                          </span>
                                        <?php endif ?>
                                        
                                        <?php if (count($takes) > 0 && !empty($takes[$product->id])): ?>
                                         <div>
                                             <p><strong>Tirar ingredientes</strong> <br> <?= join(' ', $takes[$product->id]) ?></p>
                                         </div>
                                        <?php endif ?>

                                        <?php if (count($flavors) > 0): ?>
                                         <div>
                                            <p><strong>Sabores</strong> <br>
                                            <?php
                                             foreach ($flavors as $id_f => $flav) {
                                                $i=0;
                                                $takes_verify = isset($takes[$id_f]) ? 'Tirar: '. join(" ", $takes[$id_f]) : '';
                                                echo $flav[$i] . $takes_verify;
                                                $i++;
                                              }
                                            ?>
                                            </p>
                                         </div>
                                        <?php endif ?>
                                        
                                        <?php if (count($optionals) > 0): ?>
                                         <div>
                                             <p><strong>Opcionais</strong><br> <?= join(' ', $optionals) ?></p>
                                         </div>
                                        <?php endif ?>
                                        
                                        <?php if (count($addons) > 0): ?>
                                         <div>
                                             <p><strong>Adicionais</strong> <small>R$<?php echo $CI->helps->format_money($addons_amount) ?></small> <br> <?= join(' ', $addons) ?></p>
                                         </div>
                                        <?php endif ?>
                                    </td>
                                    <td class="text-right"> <?= $detail->quantity ?> </td>
                                    <td class="text-right"> R$<?= number_format($product->price, 2, ",", ".") ?> </td>
                                    <td class="text-right"> R$<?= number_format($detail->subtotal, 2, ",", ".") ?> </td>
                                </tr>
                              <?php 
                                endforeach 
                              ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="pull-right m-t-30 text-right">
                        <p>Taxa de entrega: R$<?php echo number_format($shippingPrice, 2, ",", ".") ?></p>
                        <hr>
                        <h3><b>Total :</b> R$<?php echo number_format($total + $shippingPrice, 2, ",", ".") ?></h3> </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>