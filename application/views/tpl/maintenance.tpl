<section class="section-50 section-sm-100">
  <div class="shell">
    <div class="range range-condensed range-xs-center">
      <div class="cell-md-8">
        <h5 class="font-secondary text-gray-light text-italic">Our website is under maintenance</h5>
        <h3 class="offset-top-0">This page will appear in</h3>
        <div class="countdown-wrap">
          <!-- Countdown-->
          <div data-type="until" data-date="2018-01-14 00:00:00" data-format="wdhms" data-color="#fccb56" data-bg="#f7f7f7" data-bg-width="0.8" data-width="0.025" class="DateCountdown DateCountdown-1"></div>
        </div>
        <div class="offset-top-40">
          <p class="text-base">Stay tuned and subscribe to our newsletter</p>
          <div class="form-bg-transparent">
            <form data-form-output="form-output-global" data-form-type="subscribe" method="post" action="http://livedemo00.template-help.com/wt_61177/bat/rd-mailform.php" class="rd-mailform form-subscribe form-inline-flex-xs">
              <div class="form-group">
                <input placeholder="Your Email..." type="email" name="email" data-constraints="@Required @Email" class="form-control"/>
              </div>
              <button type="submit" class="btn btn-burnt-sienna btn-shape-circle">Subscribe</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>