<?php
$CI =& get_instance();
$user = $CI->user();
$website = $CI->config_website();
$address = $CI->config_address();
$title_header = current( str_word_count( $website->title , 2 ) );
$app_define = isset($CI->session->user) ? 'ng-app="RangusApp"' : '';
$networks = $CI->db->get('networks');
$helps = $CI->helps;
$logo = $CI->helps->verify_logo($website->site_logo);
?>
<!DOCTYPE html>
<html lang="en" class="wide wow-animation smoothscroll" <?= $app_define ?> prefix="og: http://ogp.me/ns#">
<head>
  <!-- Site Title-->
  <title><?= $website->title ?></title>
  <base href="<?php echo base_url(); ?>">
  <meta name="format-detection" content="telephone=no">
  <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta charset="utf-8">

  <meta name="description" content="" />
  <meta name="keywords" content="" />

  <meta name="author" content="Codefull" />
  <meta name="copyright" content="FalconDelivery" />
  <meta name="application-name" content="<?= $website->title ?>" />

  <?php if (!empty($oauth->appid)): $thumbnail = $helps->verify_thumbnail($product->thumbnail, 0); $oauth = $CI->db->get('oauth')->row(); ?>
  <meta property="fb:app_id" content="<?= $oauth->appid ?>" /> 
  <meta property="og:url"           content="<?= base_url().'product/'.$product->id ?>" />
  <meta property="og:type"          content="article" />
  <meta property="og:title"         content="<?= ucwords($product->name).' - R$'.$helps->format_money($product->price) ?>" />
  <meta property="og:description"   content="<?= $product->description ?>" />
  <meta property="og:image"         content="<?= base_url().$thumbnail ?>" />
  <meta property="og:site_name"     content="<?= $website->title ?>" />
  <?php endif ?>

  <link rel="icon" type="image/png" sizes="16x16" href="<?= $logo ?>">
  <!-- Stylesheets-->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Changa+One:400,400i%7CGrand+Hotel%7CLato:300,400,400italic,700">
  <link rel="stylesheet" href="assets/css/style.css">

  <link href="themes/plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet">
  <script src="https://apis.google.com/js/api:client.js"></script>

  <?php foreach ($stylesheets as $file): ?>
    <link rel="stylesheet" href="<?php echo $file ?>">
  <?php endforeach ?>

<link rel="stylesheet" href="assets/css/chat.css">

		<!--[if lt IE 10]>
    <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    <script src="js/html5shiv.min.js"></script>
  <![endif]-->
</head>
<body ng-controller="GeneralController">
  <div id="pn" data-address="<?= $website->ipdomain ?>" data-port="<?= $website->port ?>"></div>
  <div class="page">
    <div class="page-loader">
      <div class="page-loader-body">
        <div class="cssload-container">
          <div class="cssload-speeding-wheel"></div>
        </div>
        <p>Carregando...</p>
      </div>
    </div>
  </div>
  <!-- Page-->
  <div class="page text-center">
    <!-- Page Header-->
    <header class="page-head">
      <!-- RD Navbar-->
      <div class="rd-navbar-wrap rd-navbar-default">
        <nav data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-static" data-stick-up-clone="false" data-md-stick-up-offset="100px" data-lg-stick-up-offset="150px" class="rd-navbar">
          <div class="shell shell-fluid">
            <div class="rd-navbar-inner">
              <div class="rd-navbar-nav-wrap-outer">
                <div class="rd-navbar-aside">
                  <div class="rd-navbar-aside-content">
                    <!-- RD Navbar Panel-->
                    <div class="rd-navbar-panel rd-navbar-aside-left">
                      <!-- RD Navbar Toggle-->
                      <button data-rd-navbar-toggle=".rd-navbar-nav-wrap" class="rd-navbar-toggle"><span></span></button>
                      <!-- RD Navbar Brand-->
                      <script>
                        if(screen.width < 760){
                          document.write('<a id="logo-navbar-brand" href="<?php echo base_url(); ?>" style="z-index:999;margin:0 auto;padding: 7px 0 0 0;display:block"><div class="" style="width: 100%;"> <img id="logo-img-brand" src="<?php echo $logo ?>" width="auto" height="35" alt="<?= $website->title ?>">');
                        }else{
                          document.write('<a id="logo-navbar-brand" href="<?php echo base_url(); ?>"><div class="" style="width: 100%;"> <img id="logo-img-brand" src="<?php echo $logo ?>" width="auto" height="50" alt="<?= $website->title ?>">');
                        }
                      </script>
                        </div>
                      </a>
                      </div>
                      <div class="rd-navbar-aside-right">
                        <!-- RD Navbar Aside-->
                        <ul class="list-links list-inline text-left">
                          <li>
                            <div class="unit unit-horizontal unit-middle unit-spacing-xs">
                              <div class="unit-left"><span class="icon icon-circle icon-burnt-sienna icon-xxs mdi mdi-map-marker text-middle"></span></div>
                              <div class="unit-body">
                                <address class="contact-info"><a href="#" class="link-default link-xs"><?= $address->street.', '.$address->number ?> <br class="visible-md visible-lg">
                                  <?= $address->locality.', '.$address->city ?></a></address>
                                </div>
                              </div>
                            </li>
                            <li>
                              <div class="unit unit-horizontal unit-middle unit-spacing-xs">
                                <div class="unit-left"><span class="icon icon-circle icon-burnt-sienna icon-xxs mdi mdi-phone text-middle"></span></div>
                                <div class="unit-body">
                                  <address class="contact-info">
                                    <a href="tel:<?= $website->phone ?>" class="link-default link-xs">
                                     <?= $CI->helps->formatPhone($website->phone); ?>
                                   </a>
                                   <?php 
                                   $funcionally = json_decode($CI->funcionally(), true);
                                   if (!file_exists("open")): ?>
                                     <span class="reveal-block link-xs label label-danger">Fechado</span>
                                   <?php else: ?>
                                     <span class="reveal-block link-xs label label-success">Aberto</span>
                                   <?php endif ?>
                                 </address>
                               </div>
                             </div>
                           </li>
                           <li>
                            <ul class="list-inline list-inline-sm">
                              <?php if ($networks->num_rows() > 0): ?>
                                <?php foreach ($networks->row() as $index => $social): ?>
                                  <?php if ($index != 'id'): ?>
                                    <li><a href="<?= $social ?>" class="link-silver-light"><span class="icon icon-sm-mod-1 fa fa-<?= $index ?>"></span></a></li>
                                  <?php endif ?>
                                <?php endforeach ?>
                              <?php endif ?>
                            </ul>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- RD Navbar Nav-->
                <div class="rd-navbar-nav-wrap">
                  <!-- RD Navbar Shop-->
                  <div class="rd-navbar-shop">
                    <a href="<?php echo base_url().'cart' ?>" class="link link-shop link-default">
                      <span class="big text-gray-light">Carrinho</span>
                      <span class="icon icon-sm mdi mdi-cart-outline"></span>
                      <span class="label-inline big"><?php echo count($CI->session->shopcart); ?></span>
                    </a>
                  </div>
                  <!-- RD Navbar Nav-->
                  <ul class="rd-navbar-nav">
                    <li class="active">
                      <a href="<?php echo base_url(); ?>">Home</a>
                    </li>
                    <?php if (isset($CI->session->user)): ?>
                      <li>
                        <a href="<?php echo base_url().'account' ?>">Minha conta</a>
                      </li>
                      <li>
                        <a href="<?php echo base_url().'logout' ?>">Sair</a>
                      </li>
                    <?php else: ?>
                      <li>
                        <a href="<?php echo base_url().'account' ?>">Login</a>
                      </li>
                    <?php endif ?>                    
                  </ul>
                  <div class="rd-navbar-aside-right">
                    <ul class="list-links list-inline text-left">
                      <li>
                        <div class="unit unit-horizontal unit-middle unit-spacing-xs">
                          <div class="unit-left"><span class="icon icon-circle icon-burnt-sienna icon-xxs mdi mdi-map-marker text-middle"></span></div>
                          <div class="unit-body">
                            <address class="contact-info"><a href="#" class="link-default link-xs"><?= $address->street.', '.$address->number ?> <br class="visible-md visible-lg">
                              <?= $address->locality.', '.$address->city ?></a></address>
                            </div>
                          </div>
                        </li>
                        <li>
                          <div class="unit unit-horizontal unit-middle unit-spacing-xs">
                            <div class="unit-left"><span class="icon icon-circle icon-burnt-sienna icon-xxs mdi mdi-phone text-middle"></span></div>
                            <div class="unit-body">
                              <address class="contact-info">
                                <a href="tel:<?= $website->phone ?>" class="link-default link-xs">
                                 <?= $CI->helps->formatPhone($website->phone); ?>
                               </a>
                               <span class="reveal-block link-xs label label-success">Aberto</span>
                             </address>
                           </div>
                         </div>
                       </li>
                       <li>
                        <ul class="list-inline list-inline-sm">
                          <?php if ($networks->num_rows() > 0): ?>
                            <?php foreach ($networks->row() as $index => $social): ?>
                              <?php if ($index != 'id'): ?>
                                <li><a href="<?= $social ?>" class="link-silver-light"><span class="icon icon-sm-mod-1 fa fa-<?= $index ?>"></span></a></li>
                              <?php endif ?>
                            <?php endforeach ?>
                          <?php endif ?>
                        </ul>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </nav>
        </div>
      </header>
      <!-- Page Content-->
      <main class="page-content">
        <?php $this->load->view('pages/'.$page); ?>
      </main>
      <!-- Page Footer -->
      <footer class="page-foot text-sm-left ">
        <section class="bg-gray-darker section-top-55 section-bottom-60 hidden">
          <div class="shell">
            <div class="range border-left-cell">
              <div class="cell-sm-6 cell-md-3 cell-lg-4">
                <a href="<?= base_url() ?>" class="brand brand-inverse">
                  <svg x="0px" y="0px" width="auto" height="41px" viewbox="0 0 -1 41" preserveAspectRatio="xMidYMid meet">
                    <text transform="matrix(1 0 0 1 1.144409e-004 32)" fill="#2C2D2F" font-family="'Grand Hotel'" font-size="45.22"><?= $website->title ?></text>
                  </svg>
                </a>
                <ul class="list-unstyled contact-info offset-top-5">
                  <li>
                    <div class="unit unit-horizontal unit-top unit-spacing-xxs">
                      <div class="unit-left"><span class="text-white">Address:</span></div>
                      <div class="unit-body text-left text-gray-light">
                        <p>267 Park Avenue  New York, NY 90210</p>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="unit unit-horizontal unit-top unit-spacing-xxs">
                      <div class="unit-left"><span class="text-white">Email:</span></div>
                      <div class="unit-body"><a href="mailto:#" class="link-gray-light">info@demolink.org</a></div>
                    </div>
                  </li>
                </ul>
              </div>
              <div class="cell-sm-6 cell-md-3 offset-top-50 offset-sm-top-0">
                <h4 class="text-uppercase">Our menu</h4>
                <ul class="list-tags offset-top-15">
                  <li class="text-gray-light"><a href="menu-single.html" class="link-gray-light">burgers</a></li>
                  <li class="text-gray-light"><a href="menu-single.html" class="link-gray-light">pizzas</a></li>
                  <li class="text-gray-light"><a href="menu-single.html" class="link-gray-light">toasts</a></li>
                  <li class="text-gray-light"><a href="menu-single.html" class="link-gray-light">Salads</a></li>
                  <li class="text-gray-light"><a href="menu-single.html" class="link-gray-light">drinks</a></li>
                  <li class="text-gray-light"><a href="menu-single.html" class="link-gray-light">desserts</a></li>
                </ul>
              </div>
              <div class="cell-sm-10 cell-lg-5 offset-top-50 offset-md-top-0 cell-md-6">
                <h4 class="text-uppercase">newsletter</h4>
                <div class="offset-top-20">
                  <form data-form-output="form-output-global" data-form-type="subscribe" method="post" action="http://livedemo00.template-help.com/wt_61177/bat/rd-mailform.php" class="rd-mailform form-subscribe form-inline-flex-xs">
                    <div class="form-group">
                      <input placeholder="Your Email" type="email" name="email" data-constraints="@Required @Email" class="form-control">
                    </div>
                    <button type="submit" class="btn btn-burnt-sienna btn-shape-circle">Subscribe</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="section-20 bg-white">
          <div class="shell">
            <div class="range range-xs-center range-sm-justify">
              <div class="cell-sm-5 offset-top-26 text-md-left">
                <p class="copyright">
                  <?= $website->title ?>
                  &nbsp;&#169;&nbsp;<span id="copyright-year"></span>&nbsp;<br class="veil-sm"><a href="privacy.html">Termos</a>
                  <!-- {%FOOTER_LINK}-->
                </p>
              </div>
              <div class="cell-sm-4 offset-top-30 offset-sm-top-0 text-md-right">
                <ul class="list-inline list-inline-sizing-1">
                  <?php if ($networks->num_rows() > 0): ?>
                    <?php foreach ($networks->row() as $index => $social): ?>
                      <?php if ($index != 'id'): ?>
                        <li><a href="<?= $social ?>" class="link-silver-light"><span class="icon icon-sm-mod-1 fa fa-<?= $index ?>"></span></a></li>
                      <?php endif ?>
                    <?php endforeach ?>
                  <?php endif ?>
                </ul>
              </div>
            </div>
          </div>
        </section>

      </footer>
    </div>
    <!-- Global Mailform Output-->
    <div id="form-output-global" class="snackbars"></div>
    <!-- PhotoSwipe Gallery-->
    <div tabindex="-1" role="dialog" aria-hidden="true" class="pswp">
      <div class="pswp__bg"></div>
      <div class="pswp__scroll-wrap">
        <div class="pswp__container">
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
          <div class="pswp__top-bar">
            <div class="pswp__counter"></div>
            <button title="Close (Esc)" class="pswp__button pswp__button--close"></button>
            <button title="Share" class="pswp__button pswp__button--share"></button>
            <button title="Toggle fullscreen" class="pswp__button pswp__button--fs"></button>
            <button title="Zoom in/out" class="pswp__button pswp__button--zoom"></button>
            <div class="pswp__preloader">
              <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                  <div class="pswp__preloader__donut"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip"></div>
          </div>
          <button title="Previous (arrow left)" class="pswp__button pswp__button--arrow--left"></button>
          <button title="Next (arrow right)" class="pswp__button pswp__button--arrow--right"></button>
          <div class="pswp__caption">
            <div class="pswp__caption__cent"></div>
          </div>
        </div>
      </div>
    </div>
     
    
    <?php if ($CI->session->user && file_exists("open")): ?>
      <button class="btn btn-success btn-around do-you-need-help user_logged__<?=$CI->session->user?>">Precisa de ajuda?</button>
      <div id="start__messages__chat" class="user_logged__<?=$CI->session->user?>">

       <div class="chat-main-box">

        <div class="chat-right-aside">
          <div class="chat-main-header">
            <div class="p-20 b-b">
              <h4 class="box-title">Atendimento Online <span class="close-support-chat text-primary">&times</span></h4> </div>
            </div>
            <div class="chat-box">
              <ul class="chat-list slimscroll p-t-30">

                <?php $id = $CI->session->user; 
                if ($CI->get_chatting($id) != false) {
                foreach ($CI->get_chatting($id) as $row): ?>
                <?php if ($row->send == 1): 
                $CI->db->where('id', $id);
                $get_client = $CI->db->get('clients');

                if ($get_client->num_rows() > 0) {
                $client = $get_client->row();
                ?>
                  <li id="box__<?= $row->id ?>" data-chat="<?= $client->id ?>" class="odd">
                    <div class="chat-image"> <img alt="<?= $client->name ?>" src="<?= $CI->helps->verify_client_thumbnail($client->thumbnail) ?>"> </div>
                    <div class="chat-body">
                      <div class="chat-text">
                        <h4><strong><?= $client->name ?></strong></h4>
                        <p> <?= $row->message ?> </p> <b><span class="time" title="<?= $row->date ?>"></span></b> 
                      </div>
                    </div>
                  </li>
                <?php } ?>

                <?php else: 

                $CI->db->where('id', $row->func_id);
                $get_admin = $CI->db->get('admin');

                if ($get_admin->num_rows() > 0) {
                  $admin = $get_admin->row();
                ?>
                  <li id="box__<?= $row->id ?>" data-chat="<?= $admin->id ?>">
                    <div class="chat-image"> <img alt="<?= $admin->name ?>" src="<?= $CI->helps->verify_client_thumbnail($admin->thumbnail) ?>"> </div>
                    <div class="chat-body">
                      <div class="chat-text">
                        <h4><strong><?= $admin->name ?></strong></h4>
                        <p> <?= $row->message ?> </p> <b><span class="time" title="<?= $row->date ?>"></span></b> 
                      </div>
                    </div>
                  </li>
                <?php } endif ?>
              <?php endforeach; } ?>

              </ul>
              <div class="row send-chat-box">
                <div class="col-sm-12">
                  <textarea id="sendtextarea" class="form-control" placeholder="Em que podemos ajuda-lo?"></textarea>
                  <div class="custom-send"><a href="javacript:void(0)" class="cst-icon" data-toggle="tooltip" title="Insert Emojis"><i class="ti-face-smile"></i></a> <a href="javacript:void(0)" class="cst-icon" data-toggle="tooltip" title="File Attachment"><i class="fa fa-paperclip"></i></a>
                    <button id="sendbutton" class="btn btn-danger btn-rounded" type="button">Enviar</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- .chat-right-panel -->
        </div>

      </div>
    <?php endif ?>

    <!-- JavaScript--></script-->
    <script src="assets/js/core.min.js"></script>

    <!--  AngularJS  -->
    <script src="themes/plugins/bower_components/angular/angular.min.js"></script>
    <?php foreach ($javascripts as $file): ?>
    <script src="<?php echo $file ?>"></script>
    <?php endforeach ?>

    <script src="themes/plugins/bower_components/sweetalert/sweetalert.min.js"></script>
    <script src="assets/js/script.js"></script>
    <script src="assets/js/devices.js"></script>

    <?php if ($CI->session->user): ?>
    <script src="node_modules/socket.io-client/dist/socket.io.js"></script>
    <script src="node_modules/angular-socket-io/socket.js"></script>
    <script src="assets/js/app.js"></script>
    <?php endif ?>

    <?php if ($CI->session->user): ?>
    <script src="themes/ampleadmin/js/jquery.slimscroll.js"></script>
    <script src="assets/js/chat.js"></script>
    <?php endif ?>

    <?php foreach ($externalsjs as $file): ?>
    <script src="<?php echo $file ?>"></script>
    <?php endforeach ?>

    <script src="assets/plugins/timeago/jquery.livequery.js"></script>
    <script src="assets/plugins/timeago/jquery.timeago.js"></script>
    <script>
      jQuery(document).ready(function($) {
        $(".time").timeago();
      });
    </script>

    <?php if(isset($paymentesMethods)): foreach ($paymentesMethods as $file): ?>
      <script type="text/javascript" src="<?php echo $file ?>"></script>
    <?php endforeach; endif; ?>
    

  </body>
  </html>