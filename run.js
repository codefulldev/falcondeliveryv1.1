const mysql      = require("mysql");
const app        = require('express')();
const http       = require('http').Server(app);
const io         = require('socket.io')(http);
const date       = new Date();
const moment	 = require('moment');
const $now 		 = moment(date).format('YYYY-MM-DD HH:mm:ss'); 

app.get('/', function(req, res){
  res.send('Rangus Delivery Server is running!');
});

// define arrays variables
var user 		= [];
var users 	    = [];
var connections = [];
var nicknames   = [];
/*// connect database application
const conn = mysql.createConnection({
    host     : "localhost",
    user     : "root",
    password : "",
    database : "delivery"
});*/

http.listen(3100, function(){
  console.log('listening on port 3100');
});

io.sockets.on("connection", function (socket) {

	socket.on('checkout finish', function(response){
		io.emit('new order', response);
	});

	socket.on('get connections', function(){
		allUsersConnections();
	});

	socket.on("user connected", function(user){

		var uid = user;

		var index = connections.indexOf(uid);
		if(index > -1) { 
		   delete users[socket.id];
		   connections.splice(index, 1);
		}

		users[socket.id] = uid;
		connections.push(uid);

		allUsersConnections();

	});

	socket.on('client send message', function(id) {
		io.emit('admin receive message', id);
	});

	socket.on('admin send message', function(id) {
		io.emit('client receive message', id);
	});

	socket.on('order status', function(res) {
		io.emit('order status changed', res);
	});

	socket.on('checkout', function(checkout) {
		if(checkout == true){
			checkout(users[socket.id]);
		}
		console.log(checkout);
	});

	function checkout(uid){
		io.emit('new checkout', uid);
	}

	function allUsersConnections(){
		io.emit('users connections', connections);
	}

	socket.on('disconnect', () => {
		io.emit('close connection', users[socket.id]);
		delete users[socket.id];
    });

});