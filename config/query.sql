/*
Navicat MySQL Data Transfer

Source Server         : Wamp Server
Source Server Version : 50714
Source Host           : localhost:3306
Source Database       : delivery

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2017-10-25 14:48:05
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for addons
-- ----------------------------
DROP TABLE IF EXISTS `addons`;
CREATE TABLE `addons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(70) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(70) DEFAULT NULL,
  `status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for clients
-- ----------------------------
DROP TABLE IF EXISTS `clients`;
CREATE TABLE `clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `status` enum('1','0') DEFAULT '1',
  `thumbnail` varchar(250) DEFAULT NULL,
  `cep` int(11) DEFAULT NULL,
  `address` text,
  `refer` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for config
-- ----------------------------
DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text,
  `slogan` text,
  `url` text,
  `email_logo` varchar(250) DEFAULT NULL,
  `site_logo` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for config_address
-- ----------------------------
DROP TABLE IF EXISTS `config_address`;
CREATE TABLE `config_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` text,
  `street` text,
  `number` int(11) DEFAULT NULL,
  `locality` text,
  `complement` text,
  `coord_y` varchar(30) DEFAULT NULL,
  `coord_x` varchar(30) DEFAULT NULL,
  `cep` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for config_emails
-- ----------------------------
DROP TABLE IF EXISTS `config_emails`;
CREATE TABLE `config_emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` text,
  `define` varchar(70) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for config_payments
-- ----------------------------
DROP TABLE IF EXISTS `config_payments`;
CREATE TABLE `config_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `payments` varchar(50) DEFAULT NULL,
  `payments_x` int(11) DEFAULT NULL,
  `payments_url` text,
  `payments_token` varchar(250) DEFAULT NULL,
  `payments_email` varchar(200) DEFAULT NULL,
  `payments_type` enum('1','0') DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for gallery
-- ----------------------------
DROP TABLE IF EXISTS `gallery`;
CREATE TABLE `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alt` text,
  `thumbnail` varchar(250) DEFAULT NULL,
  `products_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_gallery_products_idx` (`products_id`),
  CONSTRAINT `fk_gallery_products` FOREIGN KEY (`products_id`) REFERENCES `products` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ingredients
-- ----------------------------
DROP TABLE IF EXISTS `ingredients`;
CREATE TABLE `ingredients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(70) DEFAULT NULL,
  `about` text,
  `price` decimal(10,2) DEFAULT NULL,
  `status` enum('0','1') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for optionals
-- ----------------------------
DROP TABLE IF EXISTS `optionals`;
CREATE TABLE `optionals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) DEFAULT NULL,
  `obrigatory` enum('1','0') DEFAULT '0',
  `price` decimal(10,2) DEFAULT NULL,
  `status` enum('1','0') DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for optionals_options
-- ----------------------------
DROP TABLE IF EXISTS `optionals_options`;
CREATE TABLE `optionals_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `optionals_id` int(11) DEFAULT NULL,
  `name` varchar(70) DEFAULT NULL,
  `status` enum('1','0') DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clients_id` int(11) DEFAULT NULL,
  `payment` varchar(30) DEFAULT NULL,
  `receiver` decimal(10,2) DEFAULT NULL,
  `send` decimal(10,2) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `status` enum('2','1','0') DEFAULT '0',
  `date_agend` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for orders_details
-- ----------------------------
DROP TABLE IF EXISTS `orders_details`;
CREATE TABLE `orders_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `addons` text,
  `takes` text,
  `flavors` text,
  `quantity` int(11) DEFAULT NULL,
  `subtotal` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `description` text,
  `thumbnail` varchar(250) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `ingredients` varchar(100) DEFAULT NULL,
  `flavors` varchar(100) DEFAULT NULL,
  `addons` varchar(100) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `sells` int(11) DEFAULT '0',
  `pizza` enum('1','0') DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_products_category1_idx` (`category_id`),
  CONSTRAINT `fk_products_category1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
