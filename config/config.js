(function () {

 $('#configurations').wizard({
    buttonLabels: {
        next: 'Avançar',
        back: 'Voltar',
        finish: 'Salvar'
    },
    onInit: function () {
       $('#validation').formValidation({
          framework: 'bootstrap'
          , fields: {
            key: {
                validators: {
                    notEmpty: {
                        message: 'O serial é necessário.'
                    }
                }
            }
            ,
            username: {
                validators: {
                   notEmpty: {
                      message: 'O usuário precisa ser preenchido.'
                  }
              }
            }
          , database: {
            validators: {
               notEmpty: {
                  message: 'O banco de dados precisa ser preenchido.'
              }
          }
      }
      , cep: {
        validators: {
           notEmpty: {
              message: 'O cep é obrigatório.'
          }
      }
  }
  , address: {
    validators: {
       notEmpty: {
          message: 'O endreço é obrigatório.'
      }
  }
}
, complement: {
    validators: {
       notEmpty: {
          message: 'Informe um complemento.'
      }
  }
}
, number: {
    validators: {
       notEmpty: {
          message: 'Informe um número, se não tive, digite: S/N.'
      }
  }
}
, title: {
    validators: {
       notEmpty: {
          message: 'Informe um título para a sua loja.'
      }
  }
}
, thumbnail: {
    validators: {
       notEmpty: {
          message: 'Faça o upload da sua marca.'
      }
  }
}
}
});
   }
   , validator: function () {
     var fv = $('#validation').data('formValidation');
     var $this = $(this);
     fv.validateContainer($this);
     var isValidStep = fv.isValidContainer($this);
         if (isValidStep === false || isValidStep === null) {
           return false;
          }
         return true;
     },
    onFinish: function(){
        $("#validation").submit();
    }

});

 $(document).on('blur', 'input[name="cep"]', function(e){
    e.preventDefault();

    var cep = $(this).val().replace(/\D/g, '');

    if (cep != "") {

        var validacep = /^[0-9]{8}$/;

        if(validacep.test(cep)) {
            $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(response) {

            if (!("erro" in response)) {
                    var address = response.logradouro+', '+response.bairro+', '+response.localidade+' - '+response.uf;
                    var shotaddress = response.bairro+', '+response.localidade+' - '+response.uf;
                    $('input[name="address"]').val(address);
            }
            else {
                swal("CEP não encontrado!", "verifique se você digitou corretamente o cep.", "warning");
            }
        });
        }else{
            swal("CEP inválido!", "verifique o formato do cep, ex: 45200000.", "warning");
        }
    }

});


$(document).on('blur', 'input[name="key"]', function(e){
    var key = $(this).val();
    if(key.length > 0){
        $.post('/a088972718e5202c662b958ac389d6b8', {'key': key}, function(response){

            if(response.error != true){
              if($(".wizard-next").length < 1){
                $('<a class="wizard-next" href="#configurations" data-wizard="next" role="button">Avançar</a>').insertBefore($('.wizard-finish'));
              }
            }else{
              $(".wizard-next").remove();
              swal("Serial inválido!", "verifique se você digitou o serial corretamente.", "warning");  
            }

        }, 'json');
    }
});

})();