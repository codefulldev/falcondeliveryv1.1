<?php
    $ssl = empty($_SERVER['HTTPS']) ? 'http:' : 'https:';
    $url = $ssl.'//'.$_SERVER['SERVER_NAME'].'/';
    if(file_exists(__DIR__.'/INSTALL')){
        header('location: ./');
    }
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<base href="<?php echo $url; ?>">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" type="image/png" sizes="16x16" href="themes/plugins/images/favicon.png">
	<title>Instalação Automática</title>
	<!-- Bootstrap Core CSS -->
	<link href="themes/ampleadmin/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<!--alerts CSS -->
	<link href="themes/plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
	<!-- Menu CSS -->
	<link href="themes/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
	<!-- Wizard CSS -->
	<link href="themes/plugins/bower_components/jquery-wizard-master/css/wizard.css" rel="stylesheet">
	<!-- animation CSS -->
	<link href="themes/ampleadmin/css/animate.css" rel="stylesheet">
	<!-- Custom CSS -->
	<link href="themes/ampleadmin/css/style.css" rel="stylesheet">
	<!-- color CSS -->
	<link href="themes/ampleadmin/css/colors/blue-dark.css" id="theme" rel="stylesheet">
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>

	<div id="wrapper">
		<div id="page-wrapper" style="margin:0;padding:30px;">
			<div class="section-sm-100">
				<!-- .row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Configurações do sistema</h3>
                            <p class="text-muted m-b-30 font-13"> Preencha os dados corretamente, as configurações não poderão ser disfeitas.</p>
                            <?php if (file_exists('config.json')): ?>
                            <div id="configurations" class="wizard">
                                <ul class="wizard-steps" role="tablist">
                                    <li class="active" role="tab">
                                        <h4><span><i class="ti-credit-card"></i></span>Sistema</h4> </li>
                                    <li role="tab">
                                        <h4><span><i class="ti-check"></i></span>Finalizar</h4> </li>
                                </ul>
                                <form id="validation" method="post" class="form-horizontal" enctype="multipart/form-data">
                                    <div class="wizard-content">

                                        <div class="wizard-pane active" role="tabpanel">

                                            <p><strong>Licença</strong></p>
                                            <div class="col-lg-12 col-xs-12 form-group">
                                                <div class="p-10">
                                                    <label class="control-label">Número de Serial</label>
                                                    <input type="text" class="form-control" name="key" /> 
                                                </div>
                                            </div>

                                        	<p><strong>Configurações gerais</strong></p>

                                            <div class="col-lg-6 col-xs-12 form-group">
                                                <div class="p-10">
                                                    <label class="control-label">Titulo da loja</label>
                                                    <input type="text" class="form-control" name="title" /> 
                                                </div>
                                            </div>

                                            <div class="col-lg-6 col-xs-12 form-group">
                                                <div class="p-10">
                                                    <label class="control-label">Slogan da loja</label>
                                                    <input type="text" class="form-control" name="slogan" /> 
                                                </div>
                                            </div>

                                            <div class="col-lg-6 col-xs-12 form-group">
                                                <div class="p-10">
                                                    <label class="control-label">URL da loja</label>
                                                    <input type="text" class="form-control" name="url" /> 
                                                </div>
                                            </div>

                                            <div class="col-lg-6 col-xs-12 form-group">
                                                <div class="p-10">
                                                    <label class="control-label">Logotipo</label>
                                                    <input type="file" class="form-control" name="thumbnail" /> 
                                                </div>
                                            </div>

                                            <div class="col-lg-6 col-xs-12 form-group">
                                        		<div class="p-10">
                                        			<label class="control-label">E-mail</label>
                                        			<input type="text" class="form-control" name="email" /> 
                                        		</div>
                                        	</div>

                                            <div class="col-lg-6 col-xs-12 form-group">
                                        		<div class="p-10">
                                        			<label class="control-label">Telefone</label>
                                        			<input type="text" class="form-control" name="phone" /> 
                                        		</div>
                                        	</div>

                                        	<p><strong>Configurações de endereço</strong></p>
                                            <div class="col-lg-6 col-xs-12 form-group">
                                        		<div class="p-10">
                                        			<label class="control-label">CEP</label>
                                        			<input type="text" class="form-control" name="cep" /> 
                                        		</div>
                                        	</div>

                                            <div class="col-lg-6 col-xs-12 form-group">
                                        		<div class="p-10">
                                        			<label class="control-label">Endereço</label>
                                        			<input type="text" class="form-control" name="address" /> 
                                        		</div>
                                        	</div>

                                            <div class="col-lg-6 col-xs-12 form-group">
                                        		<div class="p-10">
                                        			<label class="control-label">Complemento</label>
                                        			<input type="text" class="form-control" name="complement" /> 
                                        		</div>
                                        	</div>

                                            <div class="col-lg-6 col-xs-12 form-group">
                                        		<div class="p-10">
                                        			<label class="control-label">Número</label>
                                        			<input type="text" class="form-control" name="number" /> 
                                        		</div>
                                        	</div>

                                            <div class="col-lg-6 col-xs-12">
                                        		<div id="maps"></div>
                                        	</div>

                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="wizard-pane finisher" role="tabpanel">

                                            <p><strong>Configurações de Administrador</strong></p>
                                            <div class="col-lg-6 col-xs-12 form-group">
                                                <div class="p-10">
                                                    <label class="control-label">Nome do administrador</label>
                                                    <input type="text" class="form-control" name="adminName" /> 
                                                </div>
                                            </div>

                                            <div class="col-lg-6 col-xs-12 form-group">
                                                <div class="p-10">
                                                    <label class="control-label">E-mail</label>
                                                    <input type="email" class="form-control" name="adminEmail" /> 
                                                </div>
                                            </div>

                                            <div class="col-lg-6 col-xs-12 form-group">
                                                <div class="p-10">
                                                    <label class="control-label">Senha</label>
                                                    <input type="password" class="form-control" name="adminPassword" /> 
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>
                                        </div>

                                    </div>
                                    <input type="submit" id="submit" class="hidden">
                                </form>
                            </div>
                            <?php else: 

                             if (!empty($_POST)) {
                                 
                             
                                $dirr = $_SERVER['DOCUMENT_ROOT'];

                                if (isset($_POST['host'])) {
                                    $host = $_POST['host'];
                                }else{
                                    $host = 'localhost';
                                }
                                $username = $_POST['username'];
                                $password = isset($_POST['password']) ? $_POST['password'] : '';
                                $database = $_POST['database'];

                                $database = array(
                                    'host' => $host,
                                    'user' => $username,
                                    'pass' => $password,
                                    'db'   => $database
                                );

                                if(file_put_contents($dirr.'/config/config.json', json_encode($database))){
                                    echo '<script>window.location = "config";</script>';
                                }

                              }

                            ?>
                                <div class="wizard-pane active" role="tabpanel">
                                  <form action="" method="post" accept-charset="utf-8">
                                    
                                    <p><strong>Banco de dados</strong></p>
                                    <div class="col-lg-6 col-xs-12 form-group">
                                        <div class="p-10">
                                            <label class="control-label">Host</label>
                                            <input type="text" class="form-control" name="host" placeholder="Padrão: localhost" value="localhost" /> 
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-xs-12 form-group">
                                        <div class="p-10">
                                            <label class="control-label">Usuário</label>
                                            <input type="text" class="form-control" name="username" /> 
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-xs-12 form-group">
                                        <div class="p-10">
                                            <label class="control-label">Senha</label>
                                            <input type="text" class="form-control" name="password" /> 
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-xs-12 form-group">
                                        <div class="p-10">
                                            <label class="control-label">Banco de dados</label>
                                            <input type="text" class="form-control" name="database" /> 
                                        </div>
                                    </div>

                                    <div class="row text-right">
                                        <button type="submit" class="btn btn-primary">Conectar</button>
                                    </div>
                                    <div class="clearfix"></div>
                                  </form>
                                </div>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
                <!-- /.row -->


			</div>
		</div>
	</div>


	<!-- jQuery -->
	<script src="themes/plugins/bower_components/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap Core JavaScript -->
	<script src="themes/ampleadmin/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- Menu Plugin JavaScript -->
	<script src="themes/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
	<!--slimscroll JavaScript -->
	<script src="themes/ampleadmin/js/jquery.slimscroll.js"></script>
	<!--Wave Effects -->
	<script src="themes/ampleadmin/js/waves.js"></script>
	<!-- Form Wizard JavaScript -->
	<script src="themes/plugins/bower_components/jquery-wizard-master/dist/jquery-wizard.min.js"></script>
	<!-- FormValidation -->
	<link rel="stylesheet" href="themes/plugins/bower_components/jquery-wizard-master/libs/formvalidation/formValidation.min.css">
	<!-- FormValidation plugin and the class supports validating Bootstrap form -->
	<script src="themes/plugins/bower_components/jquery-wizard-master/libs/formvalidation/formValidation.min.js"></script>
	<script src="themes/plugins/bower_components/jquery-wizard-master/libs/formvalidation/bootstrap.min.js"></script>
	<!-- Custom Theme JavaScript -->
	<script src="themes/ampleadmin/js/custom.min.js"></script>
	<!-- Sweet-Alert  -->
	<script src="themes/plugins/bower_components/sweetalert/sweetalert.min.js"></script>
	<script src="config/config.js"></script>
	<!--Style Switcher -->
	<script src="themes/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    <script src="themes/plugins/bower_components/blockUI/jquery.blockUI.js"></script>
    <script>
        jQuery(document).ready(function($) {

            jQuery(document).on('submit','#validation', function(e) {     
                e.preventDefault();

                $('div.finisher').block({
                    message: '<h3>Carregando...</h3>'
                    , css: {
                        border: '1px solid #fff'
                    }
                });
                var formData = new FormData(this);  

                $.ajax({
                    url     : 'install',
                    type    : 'post',
                    dataType: 'json',
                    data    : formData,
                    success : function( response ) {

                        $('div.finisher').unblock();

                        if(response.install == true){
                            swal("Sistema instalado!", "Você será redirecionado para o painel de controle.", "success");
                            setTimeout(function(){
                                window.location = window.location.href+"/control";
                            }, 3500);
                        }else{
                            swal("Algo de errado aconteceu!", "Tente novamente! Se o problema persistir, entre em contato com a nossa equipe.", "warning");
                        }

                    },
                    contentType: false,
                    processData: false
                });    

            });

        });
    </script>
</body>
</html>