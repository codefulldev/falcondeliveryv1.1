$(document).ready(function(){
	$(".select2").select2();
	//$.fn.datepicker.defaults.language = 'pt-BR';
	$('.datepicker-autoclose').datepicker({
		language: "pt-BR",
		autoclose: true,
		todayHighlight: true,
		format: 'dd/mm/yyyy'
    });

	$("input[type='checkbox'], input[type='radio']").iCheck({
		checkboxClass: 'icheckbox_square-orange',
		radioClass: 'iradio_square-orange',
		increaseArea: '100%'
	});

	$('.selectpicker').selectpicker();

	// Settings Min Buy
	$(document).on("click", ".define", function(e){
		e.preventDefault();
		var target = $(this).data("target");
		var table  = $(this).data("table");
		var row    = $(this).data("row");
		var value  = $(target).val();
		$.post('ajaxReq/6c24039a1b7fcdbae18a1957bafee95903adf179', {table: table, row: row, value: value}, function(response) {
			if(response.error == false){
				swal('Os dados foram salvos!', '', 'success');
			}else{
				swal('Ocorreu um erro!', 'Um erro inesperado aconteceu, tente novamente.', 'error');
			}
		}, 'json');
	});

    // Delivery
    if($('.choose-taxas').is(':checked')){
        var option = $('.choose-taxas:checked').val();
        if (option == 1) {
            $("#kms").show();
            $("#districtsC").hide();
        } else {
            $("#kms").hide();
            $("#districtsC").show();
            list_districts();
        }
    }
    $('.choose-taxas').on('ifChanged', function(){
        var option = $(this).val();
        if(option == 1){
            $("#kms").show();
            $("#districtsC").hide();
        }else{
            $("#kms").hide();
            $("#districtsC").show();
            list_districts();
        }
    });

    $(document).on('click', '.delete-district', function(e){
        e.preventDefault();
        var id = $(this).data('id');
        $.post('ajaxReq/0b4f7ae1da771df6b0877903560fa43528f1b9g1', { 'id': id }, function (response) {
            if (!response.error){
                list_districts();
            }
        }, 'json');
    });

    $(document).on('click', '.add-district', function(e){
        e.preventDefault();
        get_districts();
    });

    $(document).on('click', '.add-districtAction', function(e){
        e.preventDefault();
        var id = $("select[name='id'] option:selected").val();
        var price = $("input[name='price']").val();

        $.post('ajaxReq/0b4f7ae1da771df6b0877903560fa43528f1b9f9', {'id': id, 'price': price}, function(response){
            list_districts();
            $("select[name='id'] option:selected").remove();
            $("input[name='price']").val('');
        }, 'json');
    });

    function get_districts(){
        var arr = [];
        $("select.districts option:selected").each(function () {
            arr.push(this.value);
        });
        $.post('ajaxReq/0b4f7ae1da771df6b0877903560fa43528f1b9f8', { 'districts': arr }, function (response) {
            var content = $("#add-districts");
            var element = '<li style="list-style: none;">';
            element = element + '<div class="col-lg-4"><select id="list-districts-options" class="form-control" name="id"></select></div>';
            element = element + '<div class="col-lg-4"><input type="text" class="form-control" name="price" placeholder="Preço"></div>';
            element = element + '<div class="col-lg-4"><button type="button" class="btn btn-xs btn-success add-districtAction">Adicionar</button></div>';
            element = element + '</li>';
            content.html(element);

            $.each(response, function (i, item) {
                $('#list-districts-options').append($('<option>', {
                    value: item.id,
                    text: item.name
                }));
            });

        }, 'json');
    }

    function list_districts(){
        var content = $("#list-districts");
        $.get('ajaxReq/0b4f7ae1da771df6b0877903560fa43528f1c1g1', function(response){
            content.empty();
            $.each(response, function(i, item){
                content.append('<li id="'+ item.id +'" class="list-group-item">'+ item.name +' - R$'+ item.price +' <div data-id="'+ item.id +'" class="btn btn-xs btn-warning pull-right delete-district">Deletar</div></li>');
            });
        }, 'json');
    }

	// Settings Payments
	$(document).on('submit', '[role="pagseguro-payment"]', function(e){
		e.preventDefault();
		var data = $(this).serialize();
		$.post('ajaxReq/6c24039a1b7fcdbae18a1957bafee95903adf177', data, function(response){
			if(response.error == false){
				swal('Os dados foram salvos!', '', 'success');
			}else{
				swal('Ocorreu um erro!', 'Um erro inesperado aconteceu, tente novamente.', 'error');
			}
		}, 'json');
	});

	// Settings Funcionally
	$(".more-fields").on('click', function(e){
		e.preventDefault();
		var id = $(".hour-fields").length;
		if(id <= 4){
			var element = '<div id="field-'+ id +'" class="form-group p-t-10 hour-fields">';
			element = element + '<a href="#" class="pull-right remove" data-id="'+ id +'">apagar</a>';
			element = element + '<label>Dias de funcionamento. <i class="fa fa-info-circle" data-toggle="tooltip" title="Você pode selecionar de 1 à 7 dias, de acordo com o funcionamento do restaurante."></i></label>';
			element = element + '<select name="days['+ id +'][]" class="selectpicker form-control" title="Selecione os dias" multiple>';
			element = element + '<option value="0">Domingo</option>';
			element = element + '<option value="1">Segunda-feira</option>';
			element = element + '<option value="2">Terça-feira</option>';
			element = element + '<option value="3">Quarta-feira</option>';
			element = element + '<option value="4">Quinta-feira</option>';
			element = element + '<option value="5">Sexta-feira</option>';
			element = element + '<option value="6">Sábado</option>';
			element = element + '</select>';
			element = element + '<label>Hora de funcionamento</label>';
			element = element + '<input type="text" name="hour['+ id +'][]" class="form-control hour-changed" placeholder="00:00, 00:00" maxlength="12" required="">';
			element = element + '<input type="hidden" name="id['+ id +'][]" value="">';
			element = element + '</div>';
			$("#hours-container").append(element);
			$('.selectpicker').selectpicker('show');
		}

	});

	$(document).on('blur', 'input[name="cep"]', function(e){
		e.preventDefault();
		findCEP($(this).val());
	});

	function findCEP(digitedCEP){
		var cep = digitedCEP.replace(/\D/g, '');
		var location = $('input[name="cep"]').data('location');

		if (cep != "") {

			var validacep = /^[0-9]{8}$/;

			if(validacep.test(cep)) {
				$.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(response) {

					if (!("erro" in response)) {

						var address = response.logradouro+', '+response.bairro+', '+response.localidade+' - '+response.uf;
						var shotaddress = response.bairro+', '+response.localidade+' - '+response.uf;
						$('input[name="address"]').val(address);
						initialize(address);
					
				}
				else {
					swal("CEP não encontrado!", "verifique se você digitou corretamente o cep.", "warning");
				}
			});
			}else{
				swal("CEP inválido!", "verifique o formato do cep, ex: 45200000.", "warning");
			}
		}
	}

	var latInit = ($('#lat').val()) ? $('#lat').val() : 12.971599;
	var lngInit = ($('#lng').val()) ? $('#lng').val() : 77.594563;

	var map;
	var markersArray = [];
	var mapOptions = {
		center: new google.maps.LatLng(latInit, lngInit),
		zoom: 19,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	}
	var marker;

	function createMarker(latLng) {
		if ( !! marker && !! marker.setMap) {
			marker.setPosition(latLng);
		} else {
			marker = new google.maps.Marker({
				map: map,
				position: latLng,
				draggable: true
			});
		}
		document.getElementById('lat').value = marker.getPosition().lat().toFixed(6);
		document.getElementById('lng').value = marker.getPosition().lng().toFixed(6);

		google.maps.event.addListener(marker, "dragend", function () {
			var lat = marker.getPosition().lat().toFixed(6);
			var lng = marker.getPosition().lng().toFixed(6);
		});
	}

	function initialize(address) {
		geocoder = new google.maps.Geocoder();
		map = new google.maps.Map(document.getElementById('findCEP'), mapOptions);
		codeAddress(address);
		google.maps.event.addListener(map, 'click', function (event) {
			map.panTo(event.latLng);
			map.setCenter(event.latLng);
			createMarker(event.latLng);
		});
	}

	function codeAddress(address) {
		geocoder.geocode({
			'address': address
		}, function (results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				map.setCenter(results[0].geometry.location);
				createMarker(results[0].geometry.location);
			} else {
				document.getElementById("findCEP").innerHTML = "Não encontramos seu endereço no mapa.";
			}
		});
	}

	$(document).on('submit', '[role="save_ecommerce_settings"]', function(e){
		e.preventDefault();

		var form = $(this).serialize();
		$.post('ajaxReq/2f06d50b643e4edd7891e1b37c636ab5542fc97e', form, function(response){
			if (response.error == false) {
				swal('Salvo com sucesso!', '', 'success');
			}else{
				swal('Ocorreu um erro!', '', 'warning');
			}
		}, 'json');

	});

	$(document).on('click', '.remove', function(e){
		e.preventDefault();
		var id = $(this).data('id');
		$("#field-"+ id).remove().fadeOut('fast');
	});

	$(document).on('click', '.delete', function(e){
		e.preventDefault();
		var id = $(this).data('id');
		$("#field-"+ id).remove().fadeOut('fast');
		$.post('ajaxReq/4f62f9ef775556d839098f4946f916a0d986d639', {'id': id}, function(response){

			if (response.count == 0) {
				var element = '<div class="form-group p-t-10 hour-fields">';
				element = element + '<label>Dias de funcionamento. <i class="fa fa-info-circle" data-toggle="tooltip" title="Você pode selecionar de 1 à 7 dias, de acordo com o funcionamento do restaurante."></i></label>';
				element = element + '<select name="days['+ response.count +'][]" class="selectpicker form-control" title="Selecione os dias" multiple>';
				element = element + '<option value="0">Domingo</option>';
				element = element + '<option value="1">Segunda-feira</option>';
				element = element + '<option value="2">Terça-feira</option>';
				element = element + '<option value="3">Quarta-feira</option>';
				element = element + '<option value="4">Quinta-feira</option>';
				element = element + '<option value="5">Sexta-feira</option>';
				element = element + '<option value="6">Sábado</option>';
				element = element + '</select>';
				element = element + '<label>Hora de funcionamento</label>';
				element = element + '<input type="text" name="hour['+ response.count +'][]" class="form-control hour-changed" placeholder="00:00, 00:00" maxlength="12" required="">';
				element = element + '</div>';
				$("#hours-container").append(element);
				$('.selectpicker').selectpicker('refresh');
			}

		}, 'json');
	});

	$(document).on('change', '.hour-changed', function(e){
		$(".submit-block").removeAttr('disabled');
	});

	$('[role="funcionally"]').on('submit', function(e){
		e.preventDefault();
		var data = $(this).serialize();
		$.post('ajaxReq/0c6dc74dee68bcde2c4f57f77627a34f68f3f4ac', data, function(response){
			swal('Salvo com sucesso!', '', 'success');
			$(".submit-block").attr('disabled', '');
		}, 'json');
	});

	$('[role="generalSettings"]').on('submit', function(e){
		e.preventDefault();
		var data = new FormData(this);
		$.ajax({
			url: 'ajaxReq/01309050243a5b2125d4dc1d3cff01e7c77fe4bc',
			method: 'POST',
			data: data,
			processData: false,
			contentType: false,
			success: function(response){
				if (response.error == false) {
					swal('Salvo com sucesso!', '', 'success');
				} else {
					swal('Ocorreu um erro!', '', 'warning');
				}
			},
			dataType: 'json'
		})
	});

	// Settings Emails
	$("#change_logo").change(function(file){

		var logo_load = $("#logo_load");

		var fileTypes = ["jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP"];
		var extension = file.target.files[0].name.split('.').pop().toLowerCase(),
		isSuccess = fileTypes.indexOf(extension) > -1;

		var reader = new FileReader();

		if (isSuccess) {

			reader.onload = function(file){

				var fileContent = file.target.result;  
				logo_load.html('<img src="'+ fileContent +'" height="120" />');
				$("#logo_emails").submit();
			}
			reader.readAsDataURL(this.files[0]);

		}else{
			swal('Formato inválido!', 'O arquivo precisa ser uma imagem.', 'error');
		}

	});

	$(document).on('submit', "#logo_emails", function(e){
		e.preventDefault();
		var data = new FormData(this);
		data.append('EmailsThumbnail', $('#change_logo')[0].files[0]);
		$.ajax({
			url: 'ajaxReq/794c3b095cb888292dac59c6ed07ca4a6e898d74',
			data: data,
			contentType: false,
			processData: false,
			type: 'POST',
			success: function(response){
				if (response.error == false) {
					swal('Salvo!', 'A logo foi salva com sucesso!', 'success');
				}else{
					swal('Erro!', 'Ocorreu algum erro, estamos verificando.', 'warning');
				}
			},
			dataType: 'json'
		})

	});

	$('.submit').click(function(){
		var id = $(this).data('id');
		var alter = $("#"+id).val();
		var subj = $("#"+id+"-subject").val();

		$.ajax({
			url: 'ajaxReq/5f20365ba3812df8ed36b1075959e0c10600b831',
			type: 'post',
			data: {'id': id, 'body': alter, 'subject': subj},
			success: function(response){
				console.log(response);
				if (response.save == true) {
					swal('Salvo!', 'Conteúdo salvo com sucesso!', 'success');
				}else{
					swal('Erro!', 'Ocorreu algum erro, estamos verificando.', 'warning');
				}
				$("#box-"+id).hide('fast');
			},
			dataType: 'json'
		})

	});

	$(document).on('submit', '[role="active_maintenance"]', function(e){
		e.preventDefault();
		var form = $(this).serialize();
		$.post('ajaxReq/64336dfc11c12c7501046194505d3f77167494a5', form, function(response){
			if (response.save == true) {
				swal('Salvo!', response.message, 'success');
			}else{
				swal('Erro!', 'Ocorreu algum erro, estamos verificando.', 'warning');
			}
		}, 'json');
	});

	$(document).on('submit', '[role="active_comingsoon"]', function(e){
		e.preventDefault();
		var form = $(this).serialize();
		$.post('ajaxReq/3d6c182d0fa59a0b128384c4ea4178f69819e37f', form, function(response){
			if (response.save == true) {
				swal('Salvo!', response.message, 'success');
			}else{
				swal('Erro!', 'Ocorreu algum erro, estamos verificando.', 'warning');
			}
		}, 'json');
	});

	$(document).on('submit', '[role="oauthconfig"]', function(e){
		e.preventDefault();
		var form = $(this).serialize();
		$.post('ajaxReq/dd43b276cc26cbf4f7a9b45962c77f80a7264097', form, function(response){
			if (response.save == true) {
				swal('Salvo!', response.message, 'success');
			}else{
				swal('Erro!', 'Ocorreu algum erro, estamos verificando.', 'warning');
			}
		}, 'json');
	});

	$(document).on('submit', '[role="socialconfig"]', function(e){
		e.preventDefault();
		var form = $(this).serialize();
		$.post('ajaxReq/0b4f7ae1da771df6b0877903560fa43528f1f9b7', form, function(response){
			if (response.save == true) {
				swal('Salvo!', response.message, 'success');
			}else{
				swal('Erro!', 'Ocorreu algum erro, estamos verificando.', 'warning');
			}
		}, 'json');
	});

});