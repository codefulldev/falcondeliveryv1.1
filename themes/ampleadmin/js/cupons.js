$(document).ready(function() {


	$(document).on('click', '.change_status', function(e){
		e.preventDefault();

		if($(this).data('provider') == 1){
			$(this).data('provider', '0');
			$(this).removeClass('label-success');
			$(this).addClass('label-danger');
			$(this).text('Indisponível');
			var data = {'id': $(this).data('id'), 'type': $(this).data('type'), 'provider': '0'};
		}else{
			$(this).data('provider', '1');
			$(this).removeClass('label-danger');
			$(this).addClass('label-success');
			$(this).text('Disponível');
			var data = {'id': $(this).data('id'), 'type': $(this).data('type'), 'provider': '1'};
		}

		var params = {
			url: 'ajaxReq/a937c9941c1df0c6e917269c95700803ce1cac52',
			type: 'post',
			data: data,
			success: function(response){

			}
		}

		$.ajax(params);

	});

	$(document).on('click', '.remove', function(e){
		e.preventDefault();
		var row = $(this).data('id');

		swal({   
			title: "Deseja remover?",   
			text: "O cupom serÃ¡ removido permanentemente.",   
			type: "warning",   
			showCancelButton: true,   
			confirmButtonColor: "#DD6B55",   
			confirmButtonText: "Sim",   
			cancelButtonText: "NÃ£o",   
			closeOnConfirm: false,   
			closeOnCancel: true 
		}, function(isConfirm){   
			if (isConfirm) {     
				$.post('ajaxReq/dd0096da614d1e8b5d44748664173f8896287e07', {'id': row, 'type': 'cupons'}, function(){
					$("#"+row).fadeOut();
				});
				swal("Removido!", "", "success");   
			}
		});

	});

	$(document).on('submit', '[role="cupom"]', function(e){
		e.preventDefault();
		var data = $(this).serialize();

		var params = {
			url: 'ajaxReq/3f3ddad64f6e3a77d2eb49573ccd7e0592d1b69d',
			type: 'post',
			data: data,
			success: function(response){

				if(response.error == true){

				}else{
					var status = (response[0].status == 1) ? '<span class="label label-success change_status" data-toggle="tooltip" title="Mudar status" data-id="'+ response[0].id +'" data-type="cupons" data-provider="'+ response[0].status +'" style="cursor:pointer;">Disponível</span>' : '<span class="label label-danger change_status" data-toggle="tooltip" title="Mudar status" data-id="'+ response[0].id +'" data-type="cupons" data-provider="'+ response[0].status +'" style="cursor:pointer;">Indisponível</span>';
					$("#addCupom").modal('hide');
					swal("Adicionado!", "A categoria foi adicionada com sucesso!", "success");
					var element = '<tr id="'+ response[0].id +'">';
					element = element + '<td>'+ response[0].genered +'</td>';
					element = element + '<td>'+ response[0].title +'</td>';
					element = element + '<td>'+ response[0].value +'%</td>';
					element = element + '<td>'+ response[0].expire +'</td>';
					element = element + '<td>';
					element = element + status;
					element = element + '</td>';
					element = element + '<td>';
					element = element + '<button type="button" data-id="'+ response[0].id +'" data-role="cupons" data-toggle="modal" data-target="#editItem" class="btn btn-info btn-outline btn-circle btn-xs m-r-5 edit"> <i class="ti-pencil"></i> </button>';
					element = element + '<button type="button" data-id="'+ response[0].id +'" class="btn btn-info btn-outline btn-circle btn-xs m-r-5 remove"><i class="ti-trash"></i></button>';
					element = element + '</td>';
					element = element + '</tr>';
					$("#cupom").prepend(element);
					$('[role="cupom"]')[0].reset();
				}
			},
			dataType: 'json'
		}
		$.ajax(params);

	});

});