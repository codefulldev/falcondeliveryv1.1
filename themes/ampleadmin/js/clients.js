$(document).ready(function() {
	
	$(".remove").on('click', function(e){
    	var id = $(this).data('id');

        swal({   
            title: "Deseja remover?",   
            text: "Todos os dados do cliente serão removidos.",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Sim, remover.",    
            cancelButtonText: "Não",   
            closeOnConfirm: false 
        }, function(){   
        	$.post('ajaxReq/a9e98764722534ba8ab10b264d44717be1a5f788', {'client': id}, function(response){
        		console.log(response);
    			swal("Removido!", "O cliente foi removido do sistema.", "success"); 
    			$("#client_"+id).fadeOut('fast');
    		});
        });

    });

});
