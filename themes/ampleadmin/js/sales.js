!function($){"use strict";$.browser||($.browser={},$.browser.mozilla=/mozilla/.test(navigator.userAgent.toLowerCase())&&!/webkit/.test(navigator.userAgent.toLowerCase()),$.browser.webkit=/webkit/.test(navigator.userAgent.toLowerCase()),$.browser.opera=/opera/.test(navigator.userAgent.toLowerCase()),$.browser.msie=/msie/.test(navigator.userAgent.toLowerCase()));var a={destroy:function(){return $(this).unbind(".maskMoney"),$.browser.msie&&(this.onpaste=null),this},mask:function(a){return this.each(function(){var b,c=$(this);return"number"==typeof a&&(c.trigger("mask"),b=$(c.val().split(/\D/)).last()[0].length,a=a.toFixed(b),c.val(a)),c.trigger("mask")})},unmasked:function(){return this.map(function(){var a,b=$(this).val()||"0",c=-1!==b.indexOf("-");return $(b.split(/\D/).reverse()).each(function(b,c){return c?(a=c,!1):void 0}),b=b.replace(/\D/g,""),b=b.replace(new RegExp(a+"$"),"."+a),c&&(b="-"+b),parseFloat(b)})},init:function(a){return a=$.extend({prefix:"",suffix:"",affixesStay:!0,thousands:",",decimal:".",precision:2,allowZero:!1,allowNegative:!1},a),this.each(function(){function b(){var a,b,c,d,e,f=r.get(0),g=0,h=0;return"number"==typeof f.selectionStart&&"number"==typeof f.selectionEnd?(g=f.selectionStart,h=f.selectionEnd):(b=document.selection.createRange(),b&&b.parentElement()===f&&(d=f.value.length,a=f.value.replace(/\r\n/g,"\n"),c=f.createTextRange(),c.moveToBookmark(b.getBookmark()),e=f.createTextRange(),e.collapse(!1),c.compareEndPoints("StartToEnd",e)>-1?g=h=d:(g=-c.moveStart("character",-d),g+=a.slice(0,g).split("\n").length-1,c.compareEndPoints("EndToEnd",e)>-1?h=d:(h=-c.moveEnd("character",-d),h+=a.slice(0,h).split("\n").length-1)))),{start:g,end:h}}function c(){var a=!(r.val().length>=r.attr("maxlength")&&r.attr("maxlength")>=0),c=b(),d=c.start,e=c.end,f=c.start!==c.end&&r.val().substring(d,e).match(/\d/)?!0:!1,g="0"===r.val().substring(0,1);return a||f||g}function d(a){r.each(function(b,c){if(c.setSelectionRange)c.focus(),c.setSelectionRange(a,a);else if(c.createTextRange){var d=c.createTextRange();d.collapse(!0),d.moveEnd("character",a),d.moveStart("character",a),d.select()}})}function e(b){var c="";return b.indexOf("-")>-1&&(b=b.replace("-",""),c="-"),c+a.prefix+b+a.suffix}function f(b){var c,d,f,g=b.indexOf("-")>-1?"-":"",h=b.replace(/[^0-9]/g,""),i=h.slice(0,h.length-a.precision);return i=i.replace(/^0/g,""),i=i.replace(/\B(?=(\d{3})+(?!\d))/g,a.thousands),""===i&&(i="0"),c=g+i,a.precision>0&&(d=h.slice(h.length-a.precision),f=new Array(a.precision+1-d.length).join(0),c+=a.decimal+f+d),e(c)}function g(a){var b,c=r.val().length;r.val(f(r.val())),b=r.val().length,a-=c-b,d(a)}function h(){var a=r.val();r.val(f(a))}function i(){var b=r.val();return a.allowNegative?""!==b&&"-"===b.charAt(0)?b.replace("-",""):"-"+b:b}function j(a){a.preventDefault?a.preventDefault():a.returnValue=!1}function k(a){a=a||window.event;var d,e,f,h,k,l=a.which||a.charCode||a.keyCode;return void 0===l?!1:48>l||l>57?45===l?(r.val(i()),!1):43===l?(r.val(r.val().replace("-","")),!1):13===l||9===l?!0:!$.browser.mozilla||37!==l&&39!==l||0!==a.charCode?(j(a),!0):!0:c()?(j(a),d=String.fromCharCode(l),e=b(),f=e.start,h=e.end,k=r.val(),r.val(k.substring(0,f)+d+k.substring(h,k.length)),g(f+1),!1):!1}function l(c){c=c||window.event;var d,e,f,h,i,k=c.which||c.charCode||c.keyCode;return void 0===k?!1:(d=b(),e=d.start,f=d.end,8===k||46===k||63272===k?(j(c),h=r.val(),e===f&&(8===k?""===a.suffix?e-=1:(i=h.split("").reverse().join("").search(/\d/),e=h.length-i-1,f=e+1):f+=1),r.val(h.substring(0,e)+h.substring(f,h.length)),g(e),!1):9===k?!0:!0)}function m(){q=r.val(),h();var a,b=r.get(0);b.createTextRange&&(a=b.createTextRange(),a.collapse(!1),a.select())}function n(){var b=parseFloat("0")/Math.pow(10,a.precision);return b.toFixed(a.precision).replace(new RegExp("\\.","g"),a.decimal)}function o(b){if($.browser.msie&&k(b),""===r.val()||r.val()===e(n()))a.allowZero?a.affixesStay?r.val(e(n())):r.val(n()):r.val("");else if(!a.affixesStay){var c=r.val().replace(a.prefix,"").replace(a.suffix,"");r.val(c)}r.val()!==q&&r.change()}function p(){var a,b=r.get(0);b.setSelectionRange?(a=r.val().length,b.setSelectionRange(a,a)):r.val(r.val())}var q,r=$(this);a=$.extend(a,r.data()),r.unbind(".maskMoney"),r.bind("keypress.maskMoney",k),r.bind("keydown.maskMoney",l),r.bind("blur.maskMoney",o),r.bind("focus.maskMoney",m),r.bind("click.maskMoney",p),r.bind("mask.maskMoney",h)})}};$.fn.maskMoney=function(b){return a[b]?a[b].apply(this,Array.prototype.slice.call(arguments,1)):"object"!=typeof b&&b?($.error("Method "+b+" does not exist on jQuery.maskMoney"),void 0):a.init.apply(this,arguments)}}(window.jQuery||window.Zepto);
jQuery(document).ready(function($) {

	$(".select2").select2({
		formatNoMatches: function () {
			return "Nada encontrado!";
		}
	});

	//$("#amount").maskMoney();
	$("#product- name").on('keyup', function(e){
		var name = $(this).val();
		$(this).attr("autocomplete", "off");

		if (name.length > 0) {
			$("#autocomplete").show('fast');
			$("#autocomplete ul").html('<li class="text-center">Carregando ...</li>');
			$.post('ajaxReq/89181b16a83f599c365eb1e44e5338e7d1ac01da', {'productname': name}, function(response){
				if (response.error != true) {
					$("#autocomplete ul").empty();
					for (var i = 0; i < response.length; i++) {
						$.each(response[i], function(index, val) {
							$("#autocomplete ul").append('<li class="product-li" data-id="'+ val.id +'" data-price="'+ val.price +'">'+ val.name +'</li>');
						});
					}
				}else{
					$("#autocomplete ul").html('<li>Produto não encontrado!</li>');
				}
			}, 'json');
		}else{
			$("#autocomplete").hide('fast');
		}
	});

	$(document).on('click', '.product-li', function(e){
		e.preventDefault();
		var id = $(this).data('id');
		var pc = $(this).data('price');
		var tx = $(this).text();
		$("#product-name").val(tx);
		$("#product-name").attr('data-id', id);
		$("#product-name").attr('data-price', pc);
		$("#autocomplete ul").empty();
		$("#autocomplete").hide('fast');
	});

	$("#product-name").on("change", function(e){
		var id = $('option:selected', this).data('id');
		var ingredients = $('option:selected', this).data('ingredients');
		var addons = JSON.parse(JSON.stringify($('option:selected', this).data('addons')));
		var optionals = JSON.parse(JSON.stringify($('option:selected', this).data('optionals')));
		var falvors = $('option:selected', this).data('falvors');
		var pizza = $('option:selected', this).data('pizza');
		var input = $("#ingredients");

		input.tagsinput({ 
			allowDuplicates: false,
			tagClass: 'label label-danger',
			itemValue: 'id',
			itemText: 'name'
		});

		$.post('ajaxReq/a79e7133d9318261a9d8098804a4df1b9a3dca68', {'value': ingredients}, function(response){

			$.each(response, function(index, val) {
				input.tagsinput('add', response[index]);
			});

			$(".bootstrap-tagsinput").attr('style', 'background:none;border:none;box-shadow:none;');
			$(".bootstrap-tagsinput input").remove();

		}, 'json');

		if (pizza == 1) {
			$.getJSON('ajaxReq/b882641720be020b3a6b4cab2719d6c47b814b5a', function(json) {
				console.log(json);
				if (json.length > 0) {
					$.each(json, function(index, response) {
						$("#flavors").show();
						if(response.id != id){
							var option = document.createElement("option");
							option.text = response.name+' - R$'+response.price;
							option.value = response.id;
							option.dataset.ingredients = response.ingredients;
							option.dataset.addons = response.addons;
							option.dataset.thumbnail = response.thumbnail;
							//option.selected = "selected";
							var select = document.getElementById("flavors_select");
							select.appendChild(option);
						}
					});
				}
			});
		}

		if (addons.length > 0) {
			var element = "<p>Adicionais: <i class='fa fa-info-circle' data-toggle='tooltip' title='Você pode escolher alguns produtos adicionais, de acordo com o valor.'></i></p>";
			$.each(addons, function(i, obj) {
				var price = (obj.price != 'Grátis') ? 'R$'+obj.price : '<b>Grátis</b>';
                element = element + '<div class="form-group text-left checkbox checkbox-info">';
                element = element + '<input name="addons[]" value="'+ obj.id +'" id="addon'+ i +'" type="checkbox">';
                element = element + '<label for="addon'+ i +'"> '+ obj.name +' </label>';
                element = element + '<span class="pull-right">'+ price +'</span> </div>';
			});
			$("#addons").empty();
			$("#addons").append(element);
		}

		if (optionals.length > 0) {
			var element = '<p>Opcionais: <i class="fa fa-info-circle" data-toggle="tooltip" title="Você pode/deve escolher algumas opções do produto. Algumas, a seleção será obrigatória."></i></p>';
			$.each(optionals, function(i, obj) {
				
				var price = (obj.price != 'Grátis') ? 'R$'+obj.price : '<b>Grátis</b>';
				element = element + '<div class="col-xs-6">';
                element = element + '<p>'+ obj.name +' - <span><b>'+ price +'</b></span></p>';
                element = element + '<ul>';

                $.each(obj.options, function(index, opt) {
                	var val = opt;
                	element = element + '<li class="form-group radio radio-info">';
                	element = element + '<input name="optionals['+ obj.id +'][]" value="'+ val.id +'" id="optional'+ index +'" type="radio">';
                	element = element + '<label for="optional'+ index +'">'+ val.name +'</label>';
                	element = element + '</li>';
                });
                element = element + '</ul>';
                element = element + '</div>';

			});
			$("#options").empty();
			$("#options").append(element);
		}
		$("[data-toggle='tooltip']").tooltip();
		//console.log(optionals);

	});


	$("#flavors_select").on("change", function(e){
		var id       	= $('option:selected', this).data('value');
		var name        = $('option:selected', this).text();
		var ingredients = $('option:selected', this).data('ingredients');
		var addons      = $('option:selected', this).data('addons');
		var thumbnail   = $('option:selected', this).data('thumbnail');

		var div = $("#selected_flavors ul");

		var element = '<li id="flavor__'+ id +'" class="list-group-item">';
		var element = element + '<img src="'+ thumbnail +'" alt="'+ name +'" width="35" height="35" />';
		var element = element + '<span> '+ name +'</span>';
		var element = element + '<p>'+ ingredients +'</p>';
		var element = element + '</li>';

		div.append(element);

	});

	$("#product-sales").on('submit', function(e){
		e.preventDefault();
		$("#product-modal").modal('hide');
		var id = $('option:selected', this).data('id');
		var name = $("#product-name").val();
		var price = $('option:selected', this).data('price');
		var quantity = $("#product-quantity").val();
		var multi = quantity * price;
		var total = multi.toFixed(2);

		var element = '<tr id="product-'+ id +'">';
        element = element + '<td class="text-center">'+ id +'</td>';
        element = element + '<td> <span class="font-medium">'+ name +'</span> </td>';
        element = element + '<td> '+ quantity +'</td>';
        element = element + '<td> R$'+ price +'</td>';
        element = element + '<td> R$'+ total +'</td>';
        element = element + '<td> <button type="button" class="btn btn-info btn-outline btn-circle btn-remove btn-xs m-r-5" data-id="'+id+'" data-total="'+ total +'"><i class="ti-trash"></i></button> </td>';
        element = element + '</tr>';
		$("#products-list").append(element);
		$("#amount").removeAttr('disabled');

		if ($("#products-list tr").length > 0 && $("#products-list tr").length < 2) {
			$("#total-sell").show();
			$("#total-sell .total-buy").text(total);
		}else{
			var sell = $("#total-sell .total-buy").text();
			var some = parseFloat(total) + parseFloat(sell);
			$("#total-sell .total-buy").text(some.toFixed(2));
		}

		var data = new FormData( this );
		data.append('productID', id );
		data.append('quantity', quantity );
		$.ajax({
			url: 'sales/add_product',
			type: 'POST',
			processData: false,
			contentType: false,
			data: data,
			success: function(response){
				console.log(response);
			}
		});

		$("#addons").empty();
		$("#options").empty();
		$("#product-sales")[0].reset();

	});

	$(document).on("submit", "#client-sales", function(e){
		e.preventDefault();
		$.post("ajaxReq/ce3350785304253a0dcf8d3567a4d38884564df6", $(this).serialize(), function(response){
			console.log(response);
			if (response.error == false) {
				var option = document.createElement("option");
				option.text = response.name+' - '+response.phone;
				option.value = response.id;
				option.dataset.picture = response.picture;
				option.dataset.address = response.address;
				option.dataset.phone = response.phone;
				option.selected = "selected";

				var select = document.getElementById("sales-client");
				select.appendChild(option);
				$("#sales-client").select2().trigger("change");

				$("#client-sales")[0].reset();
				$('#client-modal').modal('toggle');
				swal('Cliente adicionado com sucesso!', '', 'success');
			}
		}, 'json');
	});

	$(document).on('blur', 'input[name="cep"]', function(e){
		e.preventDefault();

		var cep = $(this).val().replace(/\D/g, '');
		if (cep != "") {

			var validacep = /^[0-9]{8}$/;
			if(validacep.test(cep)) {
				$.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(response) {
					if (!("erro" in response)) {
						
						var address = response.logradouro+', '+response.bairro+', '+response.localidade+' - '+response.uf;
						var shotaddress = response.bairro+', '+response.localidade+' - '+response.uf;
						$('input[name="address"]').val(address);

					}else {
						swal("CEP não encontrado!", "verifique se você digitou corretamente o cep.", "warning");
					}
				});
			}else{
				swal("CEP inválido!", "verifique o formato do cep, ex: 45200000.", "warning");
			}
		}

	});

	$(document).on('click', '.btn-remove', function(e){
		e.preventDefault();
		var i = $(this).attr('id');
		var id = $(this).data('id');
		var total = $(this).data('total');
		var total_buy = $(".total-buy").text();
		var subtract = (parseFloat(total) - parseFloat(total_buy)).toFixed(2);
		$("#product-"+i).fadeOut();
		$("#total-sell .total-buy").text(subtract.replace("-", ""));
		$.post("sales/remove_item_cart", {id: id}, function(response){

		});

	});

	$(document).on("change", "#sales-client", function(e){
		e.preventDefault();
		var id 		= $(this).val();
		var picture = $('option:selected', this).data('picture');
		var address = $('option:selected', this).data('address');
		var phone   = $('option:selected', this).data('phone');

		$('#client_thumb').attr('src', picture);
		$('#client_tel').attr('href', 'tel:'+phone);
		$('#client_id').data('id', id);
		$('#client_address').text(address);
		//$('#client_outers').attr();
		$('#ingredients').tagsinput('add', 'some tag');

		$("#client_data").show('fast');

	});

	if($("#products-list tr").length < 1){
		$("#amount").attr('disabled', '');
	}

	$('#amount').on('blur', function(e){
		var amount = $(this).val();
		var total_buy = $(".total-buy").text();

		if (parseFloat(amount).toFixed(2) > parseFloat(total_buy).toFixed(2)) {
			if(amount.length > 0){
				var balance = parseFloat(amount) - parseFloat(total_buy).toFixed(2);
				$("#balance input").val('R$'+balance);

				if ($("[name='payment']").val() == 'delivery') {
					$("#balance").show('fast');
				}else{
					$("#balance").hide('fast');
				}
			}else{
				$("#balance input").val('');
				$("#balance").hide('fast');
			}
		}else{
			if (!$("#complete_amount").is(":checked")) {
				swal('O valor recebido é menor que o total da compra.', '', 'warning');
			}
		}

	});

	$(document).on('submit', '#complete-order', function(e){
		e.preventDefault();
		var data = $(this).serialize();
		$.ajax({
			url: 'sales/complete_sale',
			type: 'POST',
			data: data,
			success: function(response){
				console.log(response);
				if(response.checkout == true){
					$("#complete-order")[0].reset();
					$("#products-list").empty();
					var element = '<div id="lightbox--process_order" class="white-popup-block mfp-hide text-center">';
			        element = element + '<center><p>O pedido foi realizado com sucesso!</p>';
			        element = element + '<button id="cupom" class="btn btn-default btn-outline cupomPrinter" data-via="client" data-id="'+ response.order +'" type="button">';
                    element = element + '<span><i class="fa fa-print"></i> Via Cliente</span>';
                    element = element + '</button>';
			        element = element + '<button id="cupom" class="btn btn-default btn-outline cupomPrinter" data-via="kitchen" data-id="'+ response.order +'" type="button">';
                    element = element + '<span><i class="fa fa-print"></i> Via Cozinha</span>';
                    element = element + '</button>';
			        element = element + '</center></div>';
			        $('body').append(element);
			        $.magnificPopup.open({
			        	items: {
			        		src: '#lightbox--process_order',
			        		midClick: true,
			        		closeOnBgClick: false,
			        		closeOnContentClick: false,
			        		mainClass: 'mfp-3d-unfold'
			        	},
			        	type: 'inline'
			        });
				}
			},
			//dataType: 'json'
		});
	})

});
