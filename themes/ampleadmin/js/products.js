$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip();
	$('.selectpicker').selectpicker();
	$('#pre-selected-options').multiSelect();
	

	if(window.File && window.FileList && window.FileReader)
	{
		var filesInput = document.getElementById("thumbnailUploads");

		filesInput.addEventListener("change", function(event){

	            var files = event.target.files; //FileList object
	            var output = document.getElementById("gallery-content-center");
	            
	            for(var i = 0; i< files.length; i++)
	            {
	            	var file = files[i];

	                //Only pics
	                if(!file.type.match('image'))
	                	continue;
	                
	                var picReader = new FileReader();
	                
	                picReader.addEventListener("load",function(event){

	                	var picFile = event.target;
	                	var div = '<img src="' + picFile.result + '" height="220" class="all img-thumbnail" alt="gallery" />'; 

	                	var $container = $('#gallery-content-center');
    					var $boxes = $( div );
    					$container.prepend( $boxes )
						$(window).resize(function() {
						   $container.masonry("reload");
						});

	                });
	                
	                 //Read the image
	                 picReader.readAsDataURL(file);
	             }                               

	    });

		var Thumbnail = document.getElementById("thumbnailUpload");

		Thumbnail.addEventListener("change", function(event){

	            var files = event.target.files; //FileList object
	            var output = document.getElementById("resultsThumbnail");
	            
	            for(var i = 0; i< files.length; i++)
	            {
	            	var file = files[i];

	                //Only pics
	                if(!file.type.match('image'))
	                	continue;
	                
	                var picReader = new FileReader();
	                
	                picReader.addEventListener("load",function(event){

	                	var picFile = event.target;
	                	var div = '<img src="'+ picFile.result +'" height="220" alt="" />';
	                	output.innerHTML = div;

	                });
	                
	                 //Read the image
	                 picReader.readAsDataURL(file);
	             }
                            

	    });

	}
	else
	{
		console.log("Your browser does not support File Reader API");
	}

	$('#open-image').click(function (e) {
		e.preventDefault();
		$(this).ekkoLightbox();
	});

	var pizza = document.getElementById('pizza');
	pizza.addEventListener("change", function(event){
		if ($('input#pizza').is(':checked')) {
			$("#pizzaID").show('fast');
		}else{
			$("#pizzaID").hide('fast');
			$("input#flavors").val('');
		}

	});

	$(document).on('click', '.delete-row', function(e){
		var id = $(this).data('id');
		swal({   
            title: "Apagar produto?",   
            text: "O produto será apagado permanentemente.",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Sim, apagar.",   
            cancelButtonText: "Cancelar.",   
            closeOnConfirm: false 
        }, function(){   
        	$.post('ajaxReq/2e10734440a96be1248ac60bc5655bb88762a894', {id: id}, function(response){
        		if (response.error == false) {
        			$("[data-row='table-row-"+ id +"']").fadeOut();
        			swal("Apagado!", "", "success"); 
        		}else{
        			swal("Erro!", "", "error"); 
        		}
        	}, 'json');
        });
	});

	$(document).on('click', '.change_status', function(e){
		e.preventDefault();

		if($(this).data('provider') == 1){
			$(this).data('provider', '0');
			$(this).removeClass('label-success');
			$(this).addClass('label-danger');
			$(this).text('Indisponível');
			var data = {'id': $(this).data('id'), 'type': $(this).data('type'), 'provider': '0'};
		}else{
			$(this).data('provider', '1');
			$(this).removeClass('label-danger');
			$(this).addClass('label-success');
			$(this).text('Disponível');
			var data = {'id': $(this).data('id'), 'type': $(this).data('type'), 'provider': '1'};
		}

		var params = {
			url: 'ajaxReq/a937c9941c1df0c6e917269c95700803ce1cac52',
			type: 'post',
			data: data,
			success: function(response){
			}
		}

		$.ajax(params);

	});

	$(document).on('submit', '[role="056cf7d70d3010713f4384149c6efd1f078f90f9"]', function(e){
		e.preventDefault();
		var data = new FormData(this);
		
		var params = {
			url: 'ajaxReq/056cf7d70d3010713f4384149c6efd1f078f90f9',
			method: 'post',
			data: data,
			cache: false,
    		contentType: false,
    		processData: false,
			success: function(success){
				console.log(success);
				if(success.error == false){
					swal("Produto adicionado!", "O produto "+ success.id +" foi adicionado.", "success");
					$("#056cf7d70d3010713f4384149c6efd1f078f90f9")[0].reset();
					$('#resultsThumbnail').empty();
					$('#gallery-content-center').empty();
					$(".selectpicker").val('').selectpicker('refresh');
				}else{
					swal("Ocorreu um erro", "Um erro inesperado aconteceu, por favor, tenta novamente.", "warning")
				}
			},
			dataType: 'json'
		}
		$.ajax(params);

	});

	$(document).on('submit', '[role="13f5a48ed1d5991d5d0bd3c2cf598f1ff1ff4520"]', function(e){
		e.preventDefault();
		var data = new FormData(this);
		
		var params = {
			url: 'ajaxReq/13f5a48ed1d5991d5d0bd3c2cf598f1ff1ff4520',
			method: 'post',
			data: data,
			cache: false,
    		contentType: false,
    		processData: false,
			success: function(success){
				console.log(success);
				if(success.error == false){
					swal("Produto atualizado!", "O produto "+ success.id +" foi atualizado.", "success");
					$(".selectpicker").val('').selectpicker('refresh');
				}else{
					swal("Ocorreu um erro", "Um erro inesperado aconteceu, por favor, tenta novamente.", "warning")
				}
			},
			dataType: 'json'
		}
		$.ajax(params);

	});

	$(document).on('click', '.actions-btn', function(){
		var target = $(this).data('target');
		$(".tabber").hide('fast');
		$("."+target).show('fast');
	});

});