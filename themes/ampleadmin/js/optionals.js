$('#demo-foo-addrow').editableTableWidget();

$(document).on('change', '#demo-foo-addrow .editableRow', function(evt, newValue) {

	var value = 0;

	if($(this).data('type') == 'price'){
		if(newValue == ''){
			value = 0;
		}else{
			value = newValue;
			$(this).html('R$'+value);
		}
	}else {
		value = newValue;
	}
	var params = {
		'id' : $(this).data('id'),
		'type' : $(this).data('type'),
		'table' : 'optionals',
		'value' : value
	}

	$.post('ajaxReq/7e75d824c17cbc175a56479a7ad38b4212cbf0da', params, function(response){

	});

});

jQuery(document).ready(function($) {

	$('td[readonly]').click(function(e) {
		e.preventDefault();
		e.stopPropagation();
		alert();
	});

	$(document).on('click', '.add-field', function(){
		var div = $(".optional").length + 1;
		var func = $(this).data('function');
		var element = '<li id="box-field_'+ div +'" class="list-group-item"> <a href="#" data-id="'+ div +'" class="pull-right remove-field"><i class="fa fa-times"></i></a><label>Nome opção</label> <input type="text" name="optionals[]" class="form-control input-sm optional" required> </li>';
		(func == 'edit') ? $("#options_list_edit").append(element) : $("#options_list").append(element);

	});

	$(document).on('click', '.remove-field', function(e){
		e.preventDefault();
		var id = $(this).data('id');
		$("#box-field_"+ id).fadeOut('fast');
	});

	$(document).on('submit', '[role="optionals"]', function(e){
		e.preventDefault();
		var data = $(this).serialize();
		$.ajax({
			url: 'ajaxReq/7827095cec8bb21a40199ca4429d7a90a0c59c8c',
			type: 'post',
			dataType: 'json',
			data: data,
			success: function(response){
				if(response.error != true){

					$("#addOptionals").modal('hide');
					swal("Adicionado!", "O opcional foi adicionado com sucesso!", "success");
					var element = '<tr id="'+ response.id +'">';
					element = element + '<td>'+ response.name +'</td>';
					element = element + '<td>';
					element = element + '<span class="label label-success change_status" data-toggle="tooltip" title="Mudar status" data-id="'+ response.id +'" data-type="category" data-provider="'+ response.status +'" style="cursor:pointer;">Disponível</span>';
					element = element + '</td>';
					element = element + '<td>';
					element = element + '<button type="button" data-id="'+ response.id +'" class="btn btn-info btn-outline btn-circle btn-xs m-r-5 remove"><i class="ti-trash"></i></button>';
					element = element + '</td>';
					element = element + '</tr>';
					$("#optionals").prepend(element);
					$('[role="optionals"]')[0].reset();

				}else{
					swal("Erro!", "Ocorreu um erro, tente novamente!", "warning");
				}
			}
		});
		
	});

	$(document).on('click', '.change_status', function(e){
		e.preventDefault();

		if($(this).data('provider') == 1){
			$(this).data('provider', '0');
			$(this).removeClass('label-success');
			$(this).addClass('label-danger');
			$(this).text('Indisponível');
			var data = {'id': $(this).data('id'), 'type': $(this).data('type'), 'provider': '0'};
		}else{
			$(this).data('provider', '1');
			$(this).removeClass('label-danger');
			$(this).addClass('label-success');
			$(this).text('Disponível');
			var data = {'id': $(this).data('id'), 'type': $(this).data('type'), 'provider': '1'};
		}

		var params = {
			url: 'ajaxReq/a937c9941c1df0c6e917269c95700803ce1cac52',
			type: 'post',
			data: data,
			success: function(response){
			}
		}

		$.ajax(params);

	});

	$(document).on('click', '.remove', function(e){
		e.preventDefault();
		var row = $(this).data('id');

		swal({   
			title: "Deseja remover?",   
			text: "O adicional será removido permanentemente.",   
			type: "warning",   
			showCancelButton: true,   
			confirmButtonColor: "#DD6B55",   
			confirmButtonText: "Sim",   
			cancelButtonText: "Não",   
			closeOnConfirm: false,   
			closeOnCancel: true 
		}, function(isConfirm){   
			if (isConfirm) {     
				$.post('ajaxReq/56ddcaf23a35e960be2717844a84b1f2981e76f4', {'id': row, 'type': 'optionals'}, function(){
					$("#"+row).fadeOut();
				});
				swal("Removido!", "", "success");   
			}
		});

	});

});