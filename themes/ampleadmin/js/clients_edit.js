$(document).ready(function() {

	$("#change_profile__picture").change(function(file){

		var src = $("#profile-pic");
		var bgd = $("#background-pic");
		var fileTypes = ["jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP"];
		var extension = file.target.files[0].name.split('.').pop().toLowerCase(),
		isSuccess = fileTypes.indexOf(extension) > -1;
		var reader = new FileReader();

		if (isSuccess) {
			reader.onload = function(onfiles){
				var fileContent = onfiles.target.result;  
				src.attr('src', fileContent);
				bgd.attr('src', fileContent);

				var data = new FormData();
				data.append('clientsThumbnail', file.target.files[0]);
				data.append('clientsEmail', $('input[name="thumbnail"]').val());

				$.ajax({
					url: 'ajaxReq/9290ee36ac14af3432423b78a923ba8cd12d7a1r',
					data: data,
					contentType: false,
					processData: false,
					type: 'POST',
					success: function(response){
						console.log(response);
						if (response.error == false) {
							swal('Salvo!', 'A imagem foi salva com sucesso!', 'success');
						}else{
							swal('Erro!', 'Ocorreu algum erro, estamos verificando.', 'warning');
						}
					},
					dataType: 'json'
				})

			}
			reader.readAsDataURL(file.target.files[0]);
		}else{
			swal('Formato invÃ¡lido!', 'O arquivo precisa ser uma imagem.', 'error');
		}

	});

	$(document).on('submit', '[role="clients_account"]', function(e){
		e.preventDefault();
		var data = $(this).serialize();
		$.post('ajaxReq/6bd2a7db18a5d9d9cb6a5f60dec688b08a7e6e26', data, function(response){
			if (response.error == false) {
				$('.json-name').text(response.name);
				$('.json-email').text(response.email);
				swal('Salvo com sucesso!', '', 'success');
			}else{
				swal('Ocorreu um erro!', '', 'warning');
			}
		}, 'json');
	});

    $(document).on('click', '.editclient', function(e){
        e.preventDefault();
        $('#editline').fadeIn();
        var id          = $(this).data('id');
        var cep         = $(this).data('cep');
        var name        = $(this).data('name');
        var address     = $(this).data('address');
        var number      = $(this).data('number');
        var complement  = $(this).data('complement');
        $("#idinput").val(id);
        $("#cepinput").val(cep);
        $("#nameinput").val(name);
        $("#addressinput").val(address);
        $("#numberinput").val(number);
        $("#complementinput").val(complement);

	});

    $(document).on('submit', '[role="editclientaddress"]', function(e){
        e.preventDefault();
        var serialize = $(this).serialize();
        var button = $(".save-submit");
        button.text('Salvando...');
        button.prop('disabled', true);
        $.post('ajaxReq/9290ee36ac14af3432423b78a923ba8cd12d7a1s', serialize, function(response){
            if (response.error == false){
                button.text('Salvar');
                button.prop('disabled', false);
                swal('Salvo com sucesso!', '', 'success');
                $('[role="editclientaddress"]')[0].reset();
                $('#editline').fadeOut();
                $('.name'+response.data.id).text(response.data.name);
                $('.address'+response.data.id).text(response.data.street+', '+response.data.district+', Nº '+response.data.number);
                $('.city'+response.data.id).text(response.data.city);
                $('.cep'+response.data.id).text(response.data.cep);
                $('.editable'+response.data.id).data('name', response.data.name);
                $('.editable'+response.data.id).data('address', response.data.address);
                $('.editable'+response.data.id).data('number', response.data.number);
                $('.editable'+response.data.id).data('complement', response.data.complement);
                $('.editable'+response.data.id).data('cep', response.data.cep);
            }else{
                swal('Ocorreu um erro.', '', 'error');
                button.text('Salvar');
                button.prop('disabled', false);
            }
        }, 'json');
    });

    $(document).on('click', '.deleteclient', function(e){
		e.preventDefault();
        var id = $(this).data('id');
        $.post('ajaxReq/9290ee36ac14af3432423b78a923ba8cd12d7a1t', {'id': id}, function(response){
            if(!response.error){
                $("#row_client--"+id).remove();
                swal('Apagado com sucesso!', '', 'success');
            }else{
                swal('Ocorreu um erro.', '', 'error');
            }
        }, 'json');
	});

    $(document).on('blur', 'input[name="cep"]', function (e) {
        e.preventDefault();

        var cep = $(this).val().replace(/\D/g, '');
        var location = $(this).data('location').trim();
        var list = $(this).data('districts');

        if (cep != "") {

            var validacep = /^[0-9]{8}$/;

            if (validacep.test(cep)) {
                $.getJSON("//viacep.com.br/ws/" + cep + "/json/?callback=?", function (response) {
                    var split = list.split(",");
                    if (!("erro" in response)) {
                        var localidade = response.localidade;

                        switch (localidade) {
                            case location:
                                if (split.indexOf(response.bairro) === -1) {
                                    var split = (list != '') ? "Bairros que entregamos: " + list.replace(",", "\n\r") : "";
                                    swal("Fora de area!", "Desculpe, mas nós não entregamos em seu bairro. " + split, "warning");
                                } else {
                                    var address = response.logradouro + ', ' + response.bairro + ', ' + response.localidade + ' - ' + response.uf;
                                    var shotaddress = response.bairro + ', ' + response.localidade + ' - ' + response.uf;
                                    $('input[name="address"]').val(address);
                                }
                                break;
                            default:
                                $('input[name="address"]').val('');
                                $('input[name="cep"]').val('');
                                swal("Fora de area!", "Desculpe, mas nós só entregamos na cidade de " + location, "warning");
                                break;
                        }

                    }
                    else {
                        swal("CEP não encontrado!", "verifique se você digitou corretamente o cep.", "warning");
                    }
                });
            } else {
                swal("CEP inválido!", "verifique o formato do cep, ex: 45200000.", "warning");
            }
        }

    });

});