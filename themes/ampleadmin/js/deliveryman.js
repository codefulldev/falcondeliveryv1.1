$(document).ready(function() {

    $(document).on('click', ".remove", function(e) {
        e.preventDefault();
        var id = $(this).data('id');

        swal({
            title: "Deseja remover?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sim, remover.",
            cancelButtonText: "NÃ£o",
            closeOnConfirm: false
        }, function() {
            $.post('ajaxReq/a9e98764722534ba8ab10b264d44717be1a5f789', { 'id': id }, function(response) {
                swal("Removido!", "O entregador foi removido do sistema.", "success");
                $("#deliveryman_" + id).fadeOut('fast');
            });
        });

    });

    $(document).on('click', '.reports', function(e) {
        e.preventDefault();
        var id = $(this).data('id');
        $.post('control/deliveryman_reports', { 'id': id }, function(response) {
            $('#reports_load').html(response);
        });
    });

    $(document).on('submit', '[role="close_day"]', function(e) {
        e.preventDefault();
        $.post('control/deliveryman_reports', $(this).serialize(), function(response) {
            if (response.error == false) {
                swal('Fechamento realizado!', '', 'success');
                $('#reports_load').empty();
                $('#reports').modal('toggle');
            } else {
                swal('Ocorreu um erro!', 'Algo de errado aconteceu, estamos resolvendo. Tente novamente!', 'warning');
            }
        }, 'json');
    });

    $(document).on('blur', '#inputCoast', function(e) {
        e.preventDefault();
        var coast = $(this).val();
        if (coast == '') {
            return;
        }
        coast = parseFloat(coast.replace(/\.|,/g, '.'));
        var total = $("#inputEntry").val();
        total = parseFloat(total.replace("R$", ""));

        var real_coast = coast.toFixed(2);
        var real_total = total.toFixed(2);

        var comission = parseFloat(real_total * real_coast);
        $("#totalComission").removeClass('hidden');
        $("#inputReceiver").val('R$' + comission.toFixed(2));

    });

    $(document).on('click', '.change-status', function(e) {
        e.preventDefault();
        var id = $(this).data('id');
        var status = $(this).attr('rel');
        var change = (status == 1) ? 0 : 1;

        if (status == 1) {
            $(this).attr('rel', '0');
            $(".status_" + id + " span").removeClass('label-success');
            $(".status_" + id + " span").addClass('label-warning');
            $(".status_" + id + " span").text('Em entrega');
        } else {
            $(this).attr('rel', '1');
            $(".status_" + id + " span").removeClass('label-warning');
            $(".status_" + id + " span").addClass('label-success');
            $(".status_" + id + " span").text('Livre');
        }

        $.post('ajaxReq/a9e98764722534ba8ab10b264d44717be1a5f790', { 'id': id, 'status': change }, function(response) {
            console.log(response);
        });
    });

    $(document).on('submit', '[role="deliveryman"]', function(e) {
        e.preventDefault();
        $.post('ajaxReq/a9e98764722534ba8ab10b264d44717be1a5f791', $(this).serialize(), function(response) {
            if (!response.error) {
                swal("Sucesso!", "O entregador foi cadastrado com sucesso.", "success");
                var element = '<tr id="deliveryman_' + response.id + '">';
                element = element + '<td><a href="javascript:void(0)">' + response.id + '</a></td>';
                element = element + '<td>' + response.name + '</td>';
                element = element + '<td>' + response.cpf + '</td>';
                element = element + '<td>' + response.board + '</td>';
                element = element + '<td class="status_' + response.id + '"><span class="label label-success">Livre</span></td>';
                element = element + '<td>';
                element = element + '<button type="button" data-id="' + response.id + '" data-status="' + response.status + '" class="btn btn-info btn-outline btn-circle btn-xs m-r-5 change-status" data-toggle="tooltip" title="Mudar status"> <i class="ti-arrows-horizontal"></i> </button>';
                element = element + '<button type="button" data-id="' + response.id + '" class="btn btn-info btn-outline btn-circle btn-xs m-r-5 remove" data-toggle="tooltip" title="Apagar entregador"> <i class="ti-trash"></i> </button>';
                element = element + '</td>';
                element = element + '</tr>';
                $("#deliveryman").append(element);
            } else {

            }
        });
    });

});