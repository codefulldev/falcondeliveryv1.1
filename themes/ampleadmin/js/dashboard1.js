$(document).ready(function () {

$('.selectpicker').selectpicker();
$.toast({
  heading: 'Welcome to Ample admin'
  , text: 'Use the predefined ones, or specify a custom position object.'
  , position: 'top-right'
  , loaderBg: '#fff'
  , icon: 'warning'
  , hideAfter: 3500
  , stack: 6
})

$('.clockpicker').clockpicker({
  donetext: 'Definir'
  , }).find('input').change(function () {

  });
jQuery('.mydatepicker, #datepicker').datepicker({
  format: 'dd/mm/yyyy',
  language: 'pt-BR',
});

$('#calendar').fullCalendar('option', 'height', 745); 

if ($(".js-switch").length) {
  var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
  $('.js-switch').each(function() {
    new Switchery($(this)[0], $(this).data());
  });
}

$(document).on('submit', '[role="save_promotion"]', function(e){
  e.preventDefault();
  var form = $(this).serialize();
  var params = {
    url: 'ajaxReq/79b5318a00f32befd597920d2f4834cebfc85e48',
    type: 'post',
    data: form,
    success: function(response){
      if(response.error == false){
        $('[role="save_promotion"]')[0].reset();
        swal('Criada com sucesso!', 'A promoção do dia foi criada e já está disponível.', 'success');
        window.location = window.location.href;
      }
    },
    dataType: 'json'
  }
  $.ajax(params);
});

if ($('#choose__month').length > 0) {

  var month = $('#choose__month').val();
  $.post('ajaxReq/79972e2375517a97ef7679d1c46dbffe238fb3da', {'month': month}, function(response){
      console.log(response);
      var month_name = $('#choose__month')[0].selectedOptions[0].innerHTML;

      $("#month_text").text(month_name);
      $("#total_orders").text('R$'+response.total_orders);
      $("#orders_list").empty();

      $.each(response.orders, function(index, val) {

         var date = moment(val.order_date).format('DD/MM/YYYY, HH:mm');
         var date_table = moment(val.order_date).format('MM/YYYY');
         var status;
         switch (val.order_status) {
           case '0':
             status = '<span class="label label-warning label-rouded">Pendente</span>';
             break;
           case '1':
             status = '<span class="label label-success label-rouded">Confirmado</span>';
             break;
           default:
             status = '<span class="label label-danger label-rouded">Cancelado</span>';
             break;
         }
         var element = '<tr>';
         element  = element + '<td><a href="control/invoice/'+ val.order_id +'">'+ val.order_id +'</a></td>';
         element  = element + '<td class="txt-oflo">'+ val.client_name +'</td>';
         element  = element + '<td>'+ status +'</td>';
         element  = element + '<td class="txt-oflo">'+ date +'</td>';
         element  = element + '<td><span class="text-success">R$'+ val.order_total +'</span></td>';
         element  = element + '</tr>';
         $("#orders_list").append(element);

      });

      $('#tableMonths').DataTable({
          dom: 'Bfrtip',
          buttons: [
            {
                extend: 'copy',
                title: 'Relatório de vendas mensal - '+month_name
            },
            {
                extend: 'csv',
                title: 'Relatório de vendas mensal - '+month_name
            },
            {
                extend: 'excel',
                title: 'Relatório de vendas mensal - '+month_name
            },
            {
                extend: 'pdf',
                title: 'Relatório de vendas mensal - '+month_name
            },
            {
                extend: 'print',
                title: 'Relatório de vendas mensal - '+month_name
            }
          ],
          searching: false
      });
  }, 'json');

}

$('#choose__month').on('change', function(e){

  var option = e.target.options[e.target.options.selectedIndex].innerHTML;
  var month = $(this).val();

  $.post('ajaxReq/79972e2375517a97ef7679d1c46dbffe238fb3da', {'month': month}, function(response){

      $("#month_text").text(option);
      $("#total_orders").text('R$'+response.total_orders);
      $("#orders_list").empty();
      $.each(response.orders, function(index, val) {
         var date = moment(val.order_date).format('DD/MM/YYYY, HH:mm');
         var status;
         switch (val.order_status) {
           case '0':
             status = '<span class="label label-warning label-rouded">Pendente</span>';
             break;
           case '1':
             status = '<span class="label label-success label-rouded">Confirmado</span>';
             break;
           default:
             status = '<span class="label label-danger label-rouded">Cancelado</span>';
             break;
         }
         var element = '<tr>';
         element  = element + '<td><a href="control/invoice/'+ val.order_id +'">'+ val.order_id +'</a></td>';
         element  = element + '<td class="txt-oflo">'+ val.client_name +'</td>';
         element  = element + '<td>'+ status +'</td>';
         element  = element + '<td class="txt-oflo">'+ date +'</td>';
         element  = element + '<td><span class="text-success">R$'+ val.order_total +'</span></td>';
         element  = element + '</tr>';
         $("#orders_list").append(element);

      });
  }, 'json');

});

$('[name="choose_promotion"]').on('change', function(e){
  var id = $(this).val();
  $.post('ajaxReq/a1e270684eb9551f60c7338cde7eb862f5f93bd5', {'id': id, 'type': 'update'}, function(response){
    if (response.error == false) {
      window.location = '/control';
    }
  }, 'json');
});

$(".js-switch").on('change', function(e){
  var id = $(this).data('id');
  if ($(this).is(':checked')) {
    var type = 'update';
  }else{
    var type = 'unupdate';
  }
  $.post('ajaxReq/a1e270684eb9551f60c7338cde7eb862f5f93bd5', {'id': id, 'type': type}, function(response){
    console.log(response);
  }, 'json');
});
// Dashboard 1 Morris-chart
/*Morris.Area({
    element: 'morris-area-chart2'
    , data: [{
            period: '2010'
            , SiteA: 50
            , SiteB: 0
        , }, {
            period: '2011'
            , SiteA: 160
            , SiteB: 100
        , }, {
            period: '2012'
            , SiteA: 110
            , SiteB: 60
        , }, {
            period: '2013'
            , SiteA: 60
            , SiteB: 200
        , }, {
            period: '2014'
            , SiteA: 130
            , SiteB: 150
        , }, {
            period: '2015'
            , SiteA: 200
            , SiteB: 90
        , }
        , {
            period: '2016'
            , SiteA: 100
            , SiteB: 150
        , }]
    , xkey: 'period'
    , ykeys: ['SiteA', 'SiteB']
    , labels: ['Site A', 'Site B']
    , pointSize: 0
    , fillOpacity: 0.1
    , pointStrokeColors: ['#79e580', '#2cabe3']
    , behaveLikeLine: true
    , gridLineColor: '#ffffff'
    , lineWidth: 2
    , smooth: true
    , hideHover: 'auto'
    , lineColors: ['#79e580', '#2cabe3']
    , resize: true
});

//ct-bar-chart
new Chartist.Bar('#ct-daily-sales', {
  labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
  series: [
    [5, 4, 3, 7, 5, 2, 3]
    
  ]
}, {
  axisX: {
    showLabel: false,
    showGrid: false,
    // On the x-axis start means top and end means bottom
    position: 'start'
  },
  
  chartPadding: {
    top:-20,
    left:45,
  },
  axisY: {
    showLabel: false,
    showGrid: false,
    // On the y-axis start means left and end means right
    position: 'end'
  },
  height:335,
  plugins: [
    Chartist.plugins.tooltip()
  ]
});

// ct-weather
var chart = new Chartist.Line('#ct-weather', {
  labels: ['1', '2', '3', '4', '5', '6'],
  series: [
    [1, 0, 5, 3, 2, 2.5]
    
  ]
}, {
  showArea: true,
  showPoint: false,
  
  chartPadding: {
    left: -20
  },
  axisX: {
    showLabel: false,
    showGrid: false
  },
  axisY: {
    showLabel: false,
    showGrid: true
  },
  fullWidth: true,
  
});
//ct-visits
new Chartist.Line('#ct-visits', {
  labels: ['2008', '2009','2010', '2011', '2012', '2013', '2014', '2015'],
  series: [
    [5, 2, 7, 4, 5, 3, 5, 4],
    [2, 5, 2, 6, 2, 5, 2, 4]
  ]
}, {
  top:0,
  
  low:1,
  showPoint: true,
  
  fullWidth: true,
  plugins: [
    Chartist.plugins.tooltip()
  ],
  axisY: {
    labelInterpolationFnc: function(value) {
      return (value / 1) + 'k';
    }
  },
  showArea: true
});*/
// counter
$(".counter").counterUp({
        delay: 100,
        time: 1200
    });

});