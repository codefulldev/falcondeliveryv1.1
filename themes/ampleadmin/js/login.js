jQuery(document).ready(function($) {

	$(document).on('submit', "#loginform", function(e){
		e.preventDefault();
		var data = $(this).serialize();

		$.post('ajaxReq/c9196710bc35fa7806e74c0fb9d92eba11c6ae8a', data, function(response){
			console.log(response);
			if (response.access == true) {
				window.location = response.uri;
			}else{
				swal("Dados incorretos!", "Desculpe, mas os dados informados não conferem.", "warning");
			}
		}, 'json');
	});


	$(document).on('submit', '#recoverform', function(e){
		e.preventDefault();
		var data = $(this).serialize();
		$.post('ajaxReq/1c4239bbec8c8ca08c5007468377cac563c154f6', data, function(response){

			var element = '<img src="'+ response.thumbnail +'" width="30" height="30" />';
			element = element + '<h4>'+ response.name +'</h4>';
			element = element + '<p>'+ response.email +'</p>';
			element = element + '<span>Uma nova senha foi enviada para seu e-mail.</span>';
			$("#recoveryDetails").html(element);

			$("#recoverform")[0].reset();
			setTimeout(function() {
				$("#recoveryDetails").empty();
			}, 5000)

		}, 'json');
	});

	$(document).on('click', '#loginformReturn', function(e){
		e.preventDefault();
		$('#loginform').fadeToggle();
		$('#recoverform').hide();
	});

});