var $mount = $("#mount");

$(document).ready(function() {

	$('.selectpicker').selectpicker();

	$(".select2").select2();

	$(".touchSpin").TouchSpin();

	$("#chat-open-sidebar").css('left', '80px');

	// Switchery

	var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

	$('.js-switch').each(function() {

		new Switchery($(this)[0], $(this).data());

	});



	$('#slimtest1').slimScroll({

		height: '600px'

	});



	$("#salesList").load('sales/saleslist');



	$(".appearDiv").addClass("desable-div");



	setTimeout(function(){

		if ($(".appearTr").length > 0) {

			$(".appearDiv").removeClass("desable-div");

		}else{

			$(".appearDiv").addClass("desable-div");

		}

	}, 3000);



	$('.btn-loader').on('click', function() {

		var $this = $(this);

		$this.button('loading');

	});



	$(".doneBuy").on("click", function(e){

		e.preventDefault();

		$(".appearDone").show();

		$.get('sales/sales_total', function(response){

			if ($('.shipping').text().length > 0) {

				var shipping = $('.shipping').text();

				var total = parseFloat(response.total) + parseFloat(shipping);

			}else{

				var total = response.total;

			}



			$('.subtotal').text(response.subtotal);

			$('.total-buy').text(total.toFixed(2));

			$('.total-buy').data('total', total.toFixed(2));

			$('#amount').val(total.toFixed(2));

		}, 'json');

	});



	$(document).on("submit", "#client-sales", function(e){

		e.preventDefault();

		$.post("ajaxReq/ce3350785304253a0dcf8d3567a4d38884564df6", $(this).serialize(), function(response){

			console.log(response);

			if (response.error == false) {

				var option = document.createElement("option");

				option.text = response.name+' - '+response.phone;

				option.value = response.id;

				option.dataset.picture = response.picture;

				option.dataset.address = response.address;

				option.dataset.phone = response.phone;

				option.selected = "selected";



				var select = document.getElementById("sales-client");

				select.appendChild(option);

				$("#sales-client").select2().trigger("change");



				$("#client-sales")[0].reset();

				$('#client-modal').modal('toggle');

				swal('Cliente adicionado com sucesso!', '', 'success');

			}else{

				swal('Cliente já existe!', '', 'warning');

			}

		}, 'json');

	});



    $(document).on('blur', 'input[name="cep"]', function (e) {
        e.preventDefault();

        var cep = $(this).val().replace(/\D/g, '');
        var location = $(this).data('location').trim();
        var list = $(this).data('districts');

        if (cep != "") {

            var validacep = /^[0-9]{8}$/;

            if (validacep.test(cep)) {
                $.getJSON("//viacep.com.br/ws/" + cep + "/json/?callback=?", function (response) {
                    var split = list.split(",");
                    if (!("erro" in response)) {
                        var localidade = response.localidade;

                        switch (localidade) {
                            case location:
                                if (split.indexOf(response.bairro) === -1) {
                                    var split = (list != '') ? "Bairros que entregamos: " + list.replace(",", "\n\r") : "";
                                    swal("Fora de area!", "Desculpe, mas nós não entregamos em seu bairro. " + split, "warning");
                                } else {
                                    var address = response.logradouro + ', ' + response.bairro + ', ' + response.localidade + ' - ' + response.uf;
                                    var shotaddress = response.bairro + ', ' + response.localidade + ' - ' + response.uf;
                                    $('input[name="address"]').val(address);
                                }
                                break;
                            default:
                                $('input[name="address"]').val('');
                                $('input[name="cep"]').val('');
                                swal("Fora de area!", "Desculpe, mas nós só entregamos na cidade de " + location, "warning");
                                break;
                        }

                    }
                    else {
                        swal("CEP não encontrado!", "verifique se você digitou corretamente o cep.", "warning");
                    }
                });
            } else {
                swal("CEP inválido!", "verifique o formato do cep, ex: 45200000.", "warning");
            }
        }

    });



	$(document).on('change', '.methods', function(e){

		var type = $(this).val();

		if (type == 'delivery') {

			var id = $("#sales-client").val();
			$("#deliveryman").show();

			$.post('sales/address_client', {'id': id}, function(response){

				if (response.error == true) {

					

				}else{

					

					$("#sales-client").select2().trigger("change");

				}

			}, 'json');



			if ($(".payments").val() == 1 || $(".payments").val() == 2) {

				$("#payment_brand").show();

			}else{

				$("#payment_brand").hide();

			}

		}else{

			$("#client-address").empty();

			$("#payment_brand").hide();

			$("#deliveryman").hide();

			



			var delivery = $('.shipping').text();

			var total = parseFloat($('.total-buy').text()) - parseFloat(delivery);

			$('.total-buy').text(total.toFixed(2));

			$('.total-buy').data('total', total.toFixed(2));

			$('#shipping').hide();

		}

	});



	$(document).on('change', '.payments', function(e){

		var index = $(this).val();

		if (index == 0) {

			$('#payment_insert').show('fast');

			$('#payment_insert input').attr('required','required');

			$("#payment_brand").hide();

		}else{

			if (index == 1 || index == 2) {

				if ($(".methods").val() == 'delivery') {

					$("#payment_brand").show();

				}else{

					$("#payment_brand").hide();

				}

			}

			

			$('#payment_insert').hide('fast');

			$('#payment_insert input').removeAttr('required');

			$('#money_back').hide('fast');

			$('.money_back').text('00.00');

		}

	});



	$(document).on('keyup', '#moneycash', function(e){

		var price = $(this).val();

		if (price.indexOf(",") > -1) {

			$(this).val(price.replace(",", "."))

		}

	});



	$(document).on('change', '.quantity', function(e){

		var index = $(this).data('id');

		var quantity = $(this).val();

		if (quantity > 0 && quantity != '') {

			$.post('sales/change_quantity', {'index': index, 'quantity': quantity}, function(response){

				$(".subtotal_"+response.index).text(response.subtotal);

				$.get('sales/sales_total', function(response){

					$('.subtotal').text(response.subtotal);

					$('.total-buy').text(response.total);

					$('.total-buy').data('total', response.total);

					$('#amount').val(response.total);

				}, 'json');

				if($("#moneycash").val() !== ''){

					var amount = $("#moneycash").val();

					var totalBuy = ($(".total-buy").text().length > 0) ? $(".total-buy").text() : 0;

					var shipping = ($(".shipping").text().length > 0) ? $(".shipping").text() : 0;



					$.post('sales/back_money', {'amount': amount, 'shipping': shipping, 'total': totalBuy}, function(response){

						var back = response.back;

						if (response.valid != true) {

							$("#money_back").hide();

							swal('Valor mínimo!', 'O valor '+amount.toFixed(2)+' é menor do que o total.', 'warning');

							

							$("#balance").val('');

						}else{

							$("#money_back").show();

							$(".money_back").text(back);

							$("#balance").val(back);

							

						}

					}, 'json');



				}

			}, 'json');

		}

	});



	$(document).on('change', '#inputCupom', function(e){

		e.preventDefault();

		var uid = $("#sales-client").val();

		var cupom = $(this).val();

		$.post('sales/checkout_cupom_verify', {uid: uid, cupom: cupom}, function(response){

			switch (response.status){

				case 'used':

				swal('Você já usou este cupom!', '', 'warning');

				$("#cupom").val('');

				break;

				case 'expired':

				swal('O cupom já expirou, desculpe!', '', 'warning');

				$("#cupom").val('');

				break;

				case 'invalid':

				swal('O cupom é inválido, tente outro!', '', 'warning');

				$("#cupom").val('');

				break;

				default:

				swal('Verificado!', 'O cupom é válido até '+response.expire+', aproveite!', 'warning');



				var shipping = ($(".shipping").text().length > 0) ? $(".shipping").text() : 0;

				var subtotal = ($(".subtotal").text().length > 0) ? $(".subtotal").text() : 0;

				var tots = parseFloat(subtotal);



				var descount = parseFloat(tots) * parseFloat(response.value);

				var descountsTotal = parseFloat(tots) - parseFloat(descount);

				var total = parseFloat(tots) - parseFloat(descount);

				total = parseFloat(total) + parseFloat(shipping);

				$(".descounts").html(descount.toFixed(2)+' - '+response.descount+'%');

				$(".descounted").val(descount.toFixed(2))

				$(".total-buy").text(total.toFixed(2).replace("-", ""));

				$("#amount").val(total.toFixed(2).replace("-", ""));

				$(".descountsTotal").text(descountsTotal.toFixed(2).replace("-", ""));



				if($("#moneycash").val() !== ''){

					var amount = $("#moneycash").val();

					var totalBuy = ($(".total-buy").text().length > 0) ? $(".total-buy").text() : 0;

					var shipping = ($(".shipping").text().length > 0) ? $(".shipping").text() : 0;

					$.post('sales/back_money', {'amount': amount, 'shipping': shipping, 'total': totalBuy}, function(response){

						var back = response.back;

						if (response.valid != true) {

							$("#money_back").hide();

							swal('Valor mínimo!', 'Por favor, informe um novo valor na quantia recebida.', 'warning');

							

							$("#balance").val('');

						}else{

							$("#money_back").show();

							$(".money_back").text(back);

							$("#balance").val(back);

							

						}

					}, 'json');



				}

				

				break;

			}

			if (response.status != 'free') {



				var totalBuy = ($(".total-buy").text().length > 0) ? $(".total-buy").text() : 0;

				var shipping = ($(".shipping").text().length > 0) ? $(".shipping").text() : 0;

				var descount = ($(".descounts").text().length > 0) ? $(".descounts").text().split(" - ") : 0;

				var total = parseFloat(totalBuy) - parseFloat(descount[0]);



				if($("#moneycash").val() !== ''){

					var amount = $("#moneycash").val();

					$.post('sales/back_money', {'amount': amount, 'shipping': shipping, 'total': total}, function(response){

						var back = response.back;

						if (response.valid != true) {

							$("#money_back").hide();

							swal('Valor mínimo!', 'Por favor, informe um novo valor na quantia recebida.', 'warning');

							

							$("#balance").val('');

						}else{

							$(".descounts").html("00.00");

							$(".descounted").val();

							$("#money_back").show();

							$(".money_back").text(back);

							$("#balance").val(back);

							

						}

					}, 'json');



				}



				$('.total-buy').text(total.toFixed(2));

				$('.total-buy').data('total', total.toFixed(2));

				$('#amount').val(total.toFixed(2));

				$(".descountsTotal").text('00.00');



			}

		}, 'json');

	});



	$(document).on('blur', '#moneycash', function(e){

		var amount = $(this).val();

		var totalBuy = ($(".total-buy").text().length > 0) ? $(".total-buy").text() : 0;

		var shipping = ($(".shipping").text().length > 0) ? $(".shipping").text() : 0;



		$.post('sales/back_money', {'amount': amount, 'shipping': shipping, 'total': totalBuy}, function(response){

			var back = response.back;

			if (response.valid != true) {

				$("#money_back").hide();

				swal('Valor mínimo!', 'O valor '+amount.toFixed(2)+' é menor do que o total.', 'warning');

				

				$("#balance").val('');

			}else{

				$("#money_back").show();

				$(".money_back").text(back);

				$("#balance").val(back);

				

			}

		}, 'json');



	});



	$(document).on('change', '#inputProductName', function(e){

		e.preventDefault();

		var id = $(this).val();



		$.post('sales/get_product', {'getID': id}, function(response){

			var pizza       = response.pizza;

			var addons      = response.addons;

			var optionals   = response.optionals;

			var ingredients = response.ingredients;



			$("#inputQuantity").attr('max', response.stock);

			$("#product_ID").data('id', response.id);



			// Is Pizza

			if(pizza == '1'){

				$("#slice-1").remove();

				$("#slice-2").remove();

				$("#flavors").show('fast');

				$(".first-flavor").data('thumbnail', response.thumbnail);

				$("#1").attr('style', 'background-image: url("'+response.thumbnail+'");background-repeat: no-repeat; background-size: cover; background-position: left top; border-radius: 100%;moz-border-radius: 100%;webkit-border-radius: 100%;o-border-radius: 100%;');



				if ($("#choose-2").is(":checked")) {

					$("#choose-1").prop("checked", true);

				}



				$mount.prepend('<div id="slice-1" class="pie big" data-start="1" data-value="360"></div>');

				var style = stylesheet_body(1, response.thumbnail, 'center center');

				$('head').append(style);

				$(".sidepizza-left ul").empty();

				$(".sidepizza-left").hide();



				$( "style" ).each(function(index) {

					if (index > 0) {

						if ($( this ).text().indexOf(".pie:nth-of-type("+ index +")") !== -1) {

							$(this).remove();

						}

					}

				});



				// Ingredients Each

				if (ingredients.length > 0) {

					$('.ingredients_div').hide('fast');

					$(".sidepizza-right").show();

					$("#ingredients-right").html(response.name+' - R$'+response.price);

					var $sideright = $(".sidepizza-right ul");

					var $sideleft  = $(".sidepizza-left ul");

					$sideright.empty();



					var countSwichy = 0;

					$.each(ingredients, function(i, data) {

						$sideright.append('<li>'+ data.name +' <input type="checkbox" id="switchery'+ data.id + countSwichy+'" name="ingredients[pizza]['+ response.id +'][]" value="'+ data.id +'" checked class="js-switch pull-right" data-color="#f96262" data-size="small" /></li>');

						var elems = document.getElementById('switchery'+ data.id + countSwichy);  

						var switchery = new Switchery(elems, {size: 'small'});

						countSwichy++;

					});

				}else{

					$(".sidepizza-right").hide();

					$("#ingredients-right").empty();

					$("#ingredients").empty();

					$('.ingredients_div').hide('fast');

				}

			}else{

				$("#flavors").hide('fast');

				$("#flavors .flv").remove();

				// Ingredients Each

				if (ingredients.length > 0) {

					$("#ingredients").empty();

					$('.ingredients_div').show('fast');

					$.each(ingredients, function(i, data) {

						$("#ingredients").append('<option value="'+ data.id +'" selected>'+ data.name +'</option>');

					});

					$("#ingredients").selectpicker('refresh');

				}else{

					$("#ingredients").empty();

					$('.ingredients_div').hide('fast');

				}

			}



			// Addons Each

			if (addons.length > 0) {

				var element = "<p>Adicionais: <i class='fa fa-info-circle' data-toggle='tooltip' title='Você pode escolher alguns produtos adicionais, de acordo com o valor.'></i></p>";

				$.each(addons, function(i, obj) {

					var price = (obj.price != 'Grátis') ? 'R$'+obj.price : '<b>Grátis</b>';

					element = element + '<div class="form-group text-left checkbox checkbox-info">';

					element = element + '<input name="addons[]" value="'+ obj.id +'" id="addon'+ i +'" type="checkbox">';

					element = element + '<label for="addon'+ i +'"> '+ obj.name +' </label>';

					element = element + '<span class="pull-right">'+ price +'</span> </div>';

				});

				$("#addons").empty();

				$("#addons").append(element);

			}else{

				$("#addons").empty();

			}



			// Optionals Each

			if (optionals.length > 0) {

				$("#options").empty();

				var element = '<p>Opcionais: <i class="fa fa-info-circle" data-toggle="tooltip" title="Você pode/deve escolher algumas opções do produto. Algumas, a seleção será obrigatória."></i></p>';

				$.each(optionals, function(i, obj) {



					var price = (obj.price != 'Grátis') ? 'R$'+obj.price : '<b>Grátis</b>';

					element = element + '<div id="optionals_'+ i +'" class="col-xs-6">';

					element = element + '<p>'+ obj.name +' - <span><b>'+ price +'</b></span></p>';



					$.each(obj.optionals_options[0], function(index, val) {

						element = element + '<div class="form-group text-left radio radio-info">';

						element = element + '<input name="optionals['+ obj.id +'][]" value="'+ val.id +'" id="optional'+ index +'" type="radio">';

						element = element + '<label for="optional'+ index +'">'+ val.name +'</label>';

						element = element + '</div>';

					});



					element = element + '</div>';



				});

				$("#options").append(element);

			}else{

				$("#options").empty();

			}



		}, 'json');



});



$('[name="choose-qtd"]').on('change', function(e) {

	var qtd = $(this).val();

	var thumbnail = $(".first-flavor").data('thumbnail');

	$qtd = qtd;

	$(".pie").remove();



	switch (qtd) {

		case '1':

		$mount.prepend('<div id="slice-1" class="pie big" data-start="1" data-value="360"></div>');

		var style = stylesheet_body(1, thumbnail, 'center center');

		$('head').append(style);

		$(".sidepizza-left ul").empty();

		$(".sidepizza-left").hide();



		$( "style" ).each(function(index) {

			if (index > 0) {

				if ($( this ).text().indexOf(".pie:nth-of-type(2)") !== -1) {

					$(this).remove();

				}

			}

		});

		break;

		case '2':

		var html = '<div id="slice-1" class="pie" data-start="0" data-value="180"></div>';

		html = html + '<div id="slice-2" class="pie" data-start="180" data-value="180"><i data-id="2" class="addlink fa fa-plus"></i></div>';

		$mount.prepend(html);

		var style = stylesheet_body(1, thumbnail, 'left top');

		$('head').append(style);

		break;

		case '3':

		var html = '<div id="slice-1" class="pie clip" data-start="0" data-value="120"></div>';

		html = html + '<div id="slice-2" class="pie clip" data-start="120" data-value="120"><i data-id="2" class="addlink fa fa-plus"></i></div>';

		html = html + '<div id="slice-3" class="pie clip last-three" data-start="240" data-value="120"><i data-id="3" class="addlink fa fa-plus"></i></div>';

		$mount.prepend(html);

		var style = stylesheet_body(1, thumbnail, 'left top');

		$('head').append(style);

		break;

		default:

		var html = '<div id="slice-1" class="pie" data-start="0" data-value="90"></div>';

		html = html + '<div id="slice-2" class="pie" data-start="90" data-value="90"><i data-id="2" class="addlink fa fa-plus"></i></div>';

		html = html + '<div id="slice-3"class="pie" data-start="180" data-value="90"><i data-id="3" class="addlink fa fa-plus"></i></div>';

		html = html + '<div id="slice-4" class="pie last-four" data-start="270" data-value="90"><i data-id="4" class="addlink fa fa-plus"></i></div>';

		$mount.prepend(html);

		var style = stylesheet_body(1, thumbnail, 'left top');

		$('head').append(style);

		break;

	}

	$bgs.removeAttr('class');

	$bgs.addClass('pieBackground-'+qtd);



});



$(document).on('click', '.mount-button', function(e){

	e.preventDefault();

	var id          = $(this).attr('id');

	var name        = $(this).data('name');

	var price       = $(this).data('price');

	var ingredients = $("#ocultIgredients"+id).val();



	if ($("#flavors"+id).length > 0) {

		$("#flavors"+id).remove();

	}

	$(".flv").remove();

	$("#flavors").prepend('<input type="hidden" class="flv" name="flavors[]" value="'+ id +'" />')



	$.post('ajaxReq/a79e7133d9318261a9d8098804a4df1b9a3dca68', {'value': ingredients}, function(response){

		$(".sidepizza-left ul").empty();

		$(".sidepizza-left").show();

		$("#ingredients-left").html(name+' - R$'+ price );

		var $sideright = $(".sidepizza-left ul");

		$sideright.empty();

		var countSwichy = 0;

		$.each(response, function(i, data) {

			$sideright.append('<li>'+ data.name +' <input type="checkbox" id="switchery'+ data.id + countSwichy+'" name="ingredients[pizza]['+ id +'][]" value="'+ data.id +'" checked class="js-switch" data-color="#f96262" data-size="small" /></li>');

			var elems = document.getElementById('switchery'+ data.id + countSwichy);  

			var switchery = new Switchery(elems, {size: 'small'});

			countSwichy++;

		});

	}, 'json');

});



$(document).on('click', '.removeSale', function(e){

	e.preventDefault();

	var id = $(this).data('id');

	var index = $(this).data('index');



	$.post('sales/remove_item_cart', {'id': id}, function(response){

		$(".session_quantity").html(response.count);



		if (response.count == 0) {

			$(".appearDiv").addClass("desable-div");

		}



		$.get('sales/sales_total', function(data){

			$('.subtotal').text(data.subtotal);

			$('.total-buy').text(data.total);

			$('.total-buy').data('total', data.total);

			$('#amount').val(data.total);

			$("#salesList").load('sales/saleslist');

		}, 'json');



		if($("#moneycash").val() !== ''){

			var amount = $("#moneycash").val();
			var totalBuy = ($(".total-buy").text().length > 0) ? $(".total-buy").text() : 0;
			var shipping = ($(".shipping").text().length > 0) ? $(".shipping").text() : 0;

			$.post('sales/back_money', {'amount': amount, 'shipping': shipping, 'total': totalBuy}, function(response){

				var back = response.back;
				if (response.valid != true) {
					$("#money_back").hide();
					swal('Valor mínimo!', 'O valor '+amount.toFixed(2)+' é menor do que o total.', 'warning');
					$("#balance").val('');
				}else{
					$("#money_back").show();
					$(".money_back").text(back);
					$("#balance").val(back);
				}

			}, 'json');



		}



	}, 'json');



});



$(document).on("submit", "#sales-client_create", function(e){

	e.preventDefault();

	$.post('sales/create_client', $(this).serialize(), function(response){

		if (response.error == true) {

			swal(response.message, '', 'warnign');

		}else{

			var option = document.createElement("option");

			option.text = response.name+' - '+response.phone;

			option.value = response.id;

			option.selected = "selected";



			var select = document.getElementById("sales-client");

			select.appendChild(option);

			$("#sales-client").select2().trigger("change");



			$("#sales-client_create")[0].reset();

			$('#client-modal').modal('toggle');

			swal('Cliente cadastrado com sucesso!', '', 'success');



			$(".newaddress").show();

			$(".newaddress").attr('id', response.id);

		}

	}, 'json');

});



$(document).on('submit', '#sales-client_newaddress', function(e){

	e.preventDefault();

	$.post('sales/create_newaddress', $(this).serialize(), function(response){

		if (response.error == true) {
			swal(response.message, '', 'warnign');
		}else{
			$("#sales-client").select2().trigger("change");
			$("#sales-client_newaddress")[0].reset();
			$('#client-newaddress-modal').modal('toggle');
		}

	}, 'json');

});



$(".newaddress").on('click', function(e){

	var id = $(this).attr('id');

	$(".clientidaddress").val(id);

});



$(document).on("change", "#sales-client", function(e){

	var id = $(this).val();

	if (id == 'default') {

		var delivery = $('.shipping').text();

		var total = parseFloat($('.total-buy').text()) - parseFloat(delivery);

		$('.total-buy').text(total.toFixed(2));

		$('.total-buy').data('total', total.toFixed(2));

		$(".newaddress").hide();

		$('#shipping').hide();

		$("#client-address").empty();



		if ($("#moneycash").is(":visible")) {

			if($("#moneycash").val() !== ''){

				var amount = $("#moneycash").val();

				var totalBuy = ($(".total-buy").text().length > 0) ? $(".total-buy").text() : 0;

				var shipping = ($(".shipping").text().length > 0) ? $(".shipping").text() : 0;



				$.post('sales/back_money', {'amount': amount, 'shipping': shipping, 'total': totalBuy}, function(response){

					var back = response.back;

					if (response.valid != true) {

						$("#money_back").hide();

						swal('Valor mínimo!', 'O valor '+amount.toFixed(2)+' é menor do que o total.', 'warning');

						

						$("#balance").val('');

					}else{

						$("#money_back").show();

						$(".money_back").text(back);

						$("#balance").val(back);

						

					}

				}, 'json');



			}

		}

		

	}else{

		$.post('sales/address_client', {'id': id}, function(response){

			$("#client-address").empty();

			if (response.error == true) {

				$("#client-address").html('<p class="text-center">'+ response.message +'</p>');

				$(".newaddress").show();

			}else{

				var element = '<p><strong>Endreços</strong></p>';

				$.each(response, function(i, data) {

					var address = data.address.split("|");

					element = element + '<div id="content'+ i +'" class="text-left radio radio-info">';

					element = element + '<input name="chooseAddress" value="'+ data.id +'" data-address="'+ data.address +'" data-index="'+ i +'" id="address'+ i +'" type="radio">';

					element = element + '<label for="address'+ i +'">'+ data.name +'</label>';

					element = element + '</div>';

				});

				$("#client-address").append(element);

				$(".newaddress").show();

				$(".newaddress").attr('id', id);

			}

		}, 'json');

	}

});



$(document).on("change", "input[name='chooseAddress']", function(e){

	e.preventDefault();

	var id = $(this).val();

	var index = $(this).data('index');

	var addressData = $(this).data('address');

	var address = addressData.split("|");

	$(".addressNamed").remove();

	$("#content"+index).append('<br><small class="addressNamed">'+address[0]+', '+address[1]+', '+address[2]+'</small>');

	$.post('sales/address_calculate', {'id': id}, function(response){

		var shipping = response.shipping;
		var subtotal = ($(".subtotal").text().length > 0) ? $(".subtotal").text() : 0;
		var descount = ($(".descounts").text().length > 0) ? $(".descounts").text().split(" - ") : 0;

		if (descount != 0) {
			var tots = parseFloat(subtotal) - parseFloat(descount[0]);
		}else{
			var tots = parseFloat(subtotal);
		}

		if (shipping.length > 0) {

			var total = parseFloat(subtotal) + parseFloat(shipping);

			$('#shipping').show();

			$('.shipping').text(response.shipping);

			$('.total-buy').text(total.toFixed(2));

			$('.total-buy').data('total', total.toFixed(2));

			$('#amount').val(total.toFixed(2));

		}else{

			var delivery = $('.shipping').text();

			var total = parseFloat(subtotal) - parseFloat(delivery);

			$('.total-buy').text(total.toFixed(2));

			$('.total-buy').data('total', total.toFixed(2));

			$('#shipping').hide();

		}



		if($("#moneycash").val() !== ''){

			var amount = $("#moneycash").val();

			var totalBuy = ($(".total-buy").text().length > 0) ? $(".total-buy").text() : 0;

			var shipping = ($(".shipping").text().length > 0) ? $(".shipping").text() : 0;



			$.post('sales/back_money', {'amount': amount, 'shipping': shipping, 'total': totalBuy}, function(response){

				var back = response.back;

				if (response.valid != true) {

					$("#money_back").hide();

					swal('Valor mínimo!', 'O valor '+amount.toFixed(2)+' é menor do que o total.', 'warning');

					

					$("#balance").val('');

				}else{

					$("#money_back").show();

					$(".money_back").text(back);

					$("#balance").val(back);

					

				}

			}, 'json');



		}

	}, 'json');

});



$(document).on('submit', '#add_product', function(e){

	$.post('sales/add_products', $(this).serialize(), function(response){



		$.magnificPopup.open({

			items: {

				src: '#lightbox--add_product',

				midClick: true,

				closeOnBgClick: false,

				closeOnContentClick: false,

				mainClass: 'mfp-3d-unfold'

			},

			type: 'inline'

		});



		if (response.added == true) {

			$("#add_product").get(0).reset();

			$("#flavors").hide('fast');

			$("#flavors .flv").remove();

			$(".add--product").modal('hide');

			$("#lightbox--add_product").html('<img src="assets/images/icon-check.png" width="30" height="30" /> Produto adicionado!');

			$(".btn-loader").button('reset');

		}else{

			$("#lightbox--add_product").html('<img src="assets/images/close_red.png" width="30" height="30" /> Produto não adicionado!');

			$(".btn-loader").button('reset');

		}



		setTimeout(function(){

			$.magnificPopup.close();

			$("#salesList").load('sales/saleslist');

			$(".session_quantity").html(parseInt($(".session_quantity").text()) + parseInt(1));

			if($(".desable-div").length > 0){

				$(".appearDiv").removeClass('desable-div');

			}

		}, 2000);



		$.get('sales/sales_total', function(response){

			$('.subtotal').text(response.subtotal);

			$('.total-buy').text(response.total);

			$('.total-buy').data('total', response.total);

			$('#amount').val(response.total);

		}, 'json');



		if($("#moneycash").val() !== ''){

			var amount = $("#moneycash").val();

			var totalBuy = ($(".total-buy").text().length > 0) ? $(".total-buy").text() : 0;

			var shipping = ($(".shipping").text().length > 0) ? $(".shipping").text() : 0;



			$.post('sales/back_money', {'amount': amount, 'shipping': shipping, 'total': totalBuy}, function(response){

				var back = response.back;

				if (response.valid != true) {

					$("#money_back").hide();

					swal('Valor mínimo!', 'O valor '+amount.toFixed(2)+' é menor do que o total.', 'warning');

					

					$("#balance").val('');

				}else{

					$("#money_back").show();

					$(".money_back").text(back);

					$("#balance").val(back);

					

				}

			}, 'json');



		}



	}, 'json');

});



$(document).on('click', '.cancel-sale', function(e){

	e.preventDefault();

	$.get('sales/cancel', function(response){

		window.location = window.location.href;

	});

});



$(document).on('submit', '#sales_submit', function(e){

	$.post('sales/complete_sale', $(this).serialize(), function(response){

        console.log(response);

		$(".done-sale").button('loading');

		var $content = $('#lightbox--complete_sales');



		$.magnificPopup.open({

			items: {

				src: '#lightbox--complete_sales',

				midClick: true,

				closeOnBgClick: false,

				closeOnContentClick: false,

				mainClass: 'mfp-3d-unfold'

			},

			type: 'inline'

		});



		var $amount = ($("#moneycash").val() != '') ? $("#moneycash").val() : '00.00'; 



		if (response.checkout == true) {

			var element = '<p class="text-center"><img src="assets/images/icon-check.png" width="30" height="30" /> Pedido realizado com sucesso!</p>';

			element = element + '<p class="text-center"><small class="text-danger">Imprima os comprovantes de pedido.</small></p>';

			element = element + '<div class="text-center"><button class="btn btn-info cupomPrinter" data-id="'+ response.order +'" data-via="client" data-where="sales" data-amount="'+ $amount +'"><i class="fa fa fa-print"></i> Imprimir via cliente</button>';

			element = element + '&nbsp;&nbsp;&nbsp;<button class="btn btn-danger cupomPrinter" data-id="'+ response.order +'" data-via="kitchen" data-where="sales" data-amount="'+ $amount +'"><i class="fa fa fa-print"></i> Imprimir via cozinha</button></div>';

			element = element + '<div class="text-center m-t-20"><button class="done-order btn btn-xs btn-success"><i class="fa fa fa-check"></i> Concluir</button></div>';

			$content.html(element);

		}else{

			var element = '<p class="text-center"><img src="assets/images/close_red.png" width="30" height="30" /> Pedido não foi realizado, tente novamente!</p>';

			element = element + '<small class="text-center text-danger">ocorreu algum erro no processamento do pedido.</small>';

			element = element + '<div class="text-center"><button class="try-order btn btn-xs btn-warning"><i class="fa fa fa-check"></i> Tentar novamente</button></div>';

			$content.html(element);

		}

		$(".btn-loader").button('reset');



	}, 'json');

});



$(document).on("click", ".done-order", function(e){

	window.location = window.location.href;

});



$(document).on("click", ".try-order", function(e){

	$("#sales_submit").submit();

});



});



function stylesheet_body(index, url, positions){

	return '<style>.pie:nth-of-type('+ index +'):AFTER, .pie:nth-of-type('+ index +'):BEFORE {background-color: rgba(0, 0, 0, .5); cursor: pointer; text-align: center; vertical-align: middle; background-image: url('+ url +'); background-repeat: no-repeat; background-size: cover; background-position: '+ positions +'}</style>';

}