$(document).ready(function() {
    $("#print").click(function() {
        var mode = 'iframe';
        var close = mode == "popup";
        var options = {
            mode: mode,
            popClose: close
        };
        $("div.printableArea").printArea(options);
    });

    $("#trash").on('click', function(e){
    	var id = $(this).data('id');

        swal({   
            title: "Deseja apagar?",   
            text: "O pedido será apagado totalmente.",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Sim, apagar.",    
            cancelButtonText: "Não",   
            closeOnConfirm: false 
        }, function(){   
        	$.post('ajaxReq/a2b7f359a0e3d17ddd381f07f5f1c43350826cea', {'order': id}, function(response){
        		console.log(response);
    			swal("Apagado!", "O pedido foi apagado do sistema.", "success"); 
    			setTimeout(function(){
    				window.location = response.uri_redirect;
    			},2000)
    		}, 'json');
        });

    });

    $("#deliverymen").on('change', function(e){
        var id = $(this, 'option:selected').val();
        var order_id = $(this).data('order');
        $.post('ajaxReq/0b4f7ae1da771df6b0877903560fa43528f1b9g2', {id: id, order: order_id}, function(response){
            if (response.error == false){
                swal('Alterado!', 'Entregador alterado com sucesso.', 'success');
            }else{
                swal('Erro!', 'Ocorreu um erro interno, tente novamente.', 'error');
            }
        }, 'json');
    });
    
});