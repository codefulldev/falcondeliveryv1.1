$(document).ready(function() {

	$("#change_profile__picture").change(function(file){

		var src = $("#profile-pic");
		var bgd = $("#background-pic");
		var fileTypes = ["jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP"];
		var extension = file.target.files[0].name.split('.').pop().toLowerCase(),
		isSuccess = fileTypes.indexOf(extension) > -1;
		var reader = new FileReader();

		if (isSuccess) {
			reader.onload = function(onfiles){
				var fileContent = onfiles.target.result;  
				src.attr('src', fileContent);
				bgd.attr('src', fileContent);

				var data = new FormData();
				data.append('clientsThumbnail', file.target.files[0]);
				data.append('clientsEmail', $('input[name="thumbnail"]').val());

				$.ajax({
					url: 'ajaxReq/9290ee36ac14af3432423b78a923ba8cd12d7a1r',
					data: data,
					contentType: false,
					processData: false,
					type: 'POST',
					success: function(response){
						console.log(response);
						if (response.error == false) {
							swal('Salvo!', 'A imagem foi salva com sucesso!', 'success');
						}else{
							swal('Erro!', 'Ocorreu algum erro, estamos verificando.', 'warning');
						}
					},
					dataType: 'json'
				})

			}
			reader.readAsDataURL(file.target.files[0]);
		}else{
			swal('Formato invÃ¡lido!', 'O arquivo precisa ser uma imagem.', 'error');
		}

	});

	$(document).on('submit', '[role="clients_account"]', function(e){
		e.preventDefault();
		var data = $(this).serialize();
		$.post('ajaxReq/6bd2a7db18a5d9d9cb6a5f60dec688b08a7e6e26', data, function(response){
			if (response.error == false) {
				swal('Salvo com sucesso!', '', 'success');
			}else{
				swal('Ocorreu um erro!', '', 'warning');
			}
		}, 'json');
	});

});