$(document).ready(function() {
	$(document).on("click", ".edit", function(e){
		e.preventDefault();
		var id = $(this).data('id');
		var role = $(this).data('role');
		var prefixId = 'editItem';

		$.post('ajaxReq/dc5712026f8e040e3cd541e0de5a5f1ba7c40861', {id: id, role: role}, function(response){

			switch (role) {
				case 'cupons':
					console.log(response);
					$("#"+prefixId+" .title").val(response.title);
					$("#"+prefixId+" .value").val(response.value);
					if (response.expire.length > 0) {
						var dd = response.expire.split(" ");
						$("#"+prefixId+" .expire").val(dd[0]);
					}
					$('[role="editItems"]').attr('id', response.id);
					break;
				case 'category':
					$("#"+prefixId+" .name").val(response.name);
					$('[role="editItems"]').attr('id', response.id);
					break;
				case 'addons':
					$("#"+prefixId+" .name").val(response.name);
					$("#"+prefixId+" .price").val(response.price);
					$('[role="editItems"]').attr('id', response.id);
					break;
				case 'optionals':
					$(".list-group-item").remove();
					$("#"+prefixId+" .name").val(response.optional.name);
					$("#"+prefixId+" .price").val(response.optional.price);
					$('[role="editItems"]').attr('id', response.optional.id);
					if (response.optional.obrigatory == 1) {
						$(".obrigatory-inner").html('<input id="checkbox-11" name="obrigatory" class="obrigatory" type="checkbox" value="1" checked="true"> <label for="checkbox-55"> Obrigatório </label>');
					}else{
						$(".obrigatory-inner").html('<input id="checkbox-11" name="obrigatory" class="obrigatory" type="checkbox" value="0"> <label for="checkbox-55"> Obrigatório </label>');
					}
					$.each(response.options, function(index, val) {
						var data = val;
						var element = '<li id="box-field_'+ data.id +'" class="list-group-item"> <a href="#" data-id="'+ data.id +'" data-delete="optionals_options" class="pull-right delete-field"><i class="fa fa-times"></i></a><label>Nome opção</label> <input type="text" name="optionals[]['+ data.id +']" class="form-control input-sm optional" value="'+ data.name +'"> </li>';
						$("#options_list_edit").append(element);
					});

					break;
				default:
					$("#"+prefixId+" .name").val(response.name);
					$("#"+prefixId+" .price").val(response.price);
					$("#"+prefixId+" .about").val(response.about);
					$('[role="editItems"]').attr('id', response.id);
					break;
			}
		}, 'json');

	});

	if ($(".obrigatory").length > 0) {
		$(document).on('click', ".obrigatory", function(e){
			e.preventDefault();
			if ($(".obrigatory").is(":checked")) {
				$('.obrigatory').attr('checked', true);
			}else{
				$('.obrigatory').attr('checked', false);
			}
		});
		
	}

	$(document).on('click', '.delete-field', function(e){
		e.preventDefault();
		var id = $(this).data('id');
		var del = $(this).data('delete');
		$.post('ajaxReq/dc5712026f8e040e3cd541e0de5a5f1ba7c40861', {id: id, delete: del}, function(response){	
			$("#box-field_"+ id).fadeOut('fast');
		});
	});

	$(document).on('submit', '[role="editItems"]', function(e){
		e.preventDefault();
		var table = $(this).data('table');
		var id = $(this).attr('id');
		$.post('ajaxReq/dc5712026f8e040e3cd541e0de5a5f1ba7c40861?table='+table+':'+id, $(this).serialize(), function(response){
			if (response.update == true) {
				swal('Salvo com sucesso!', '', 'success');
				setTimeout(function() {
					window.location = window.location.href;
				}, 2000)
			}else{
				swal('Ocorreu um erro!', '', 'error');
			}
		}, 'json');
	});

});
