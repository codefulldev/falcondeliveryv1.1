jQuery(document).ready(function($) {

	$("[data-alternate='tooltip']").tooltip();

	$(".change_order_status").on('change', function(e){
		var id = $(this).data('id');
		var ht = $("#order_status_html_"+id);
		var st = $(this).val();

		switch (st) {
			case '0':
				var status = 'Pendente';
				var sclass = 'label label-warning';
			break;
			case '1':
				var status = 'Completo';
				var sclass = 'label label-success';
			break;
			default:
				var status = 'Cancelado';
				var sclass = 'label label-danger';
			break;
		}

		$.post('ajaxReq/13107df1de65ac459e9ce39ee2d304ba739ae08e', {'id': id, 'status': st}, function(response){
			if(response.error == false){
				ht.html(status);
				ht.removeAttr('class');
				ht.attr('class', sclass);
			}
		}, 'json');

	});

	$(".remove").on('click', function(e){
    	var id = $(this).data('id');

        swal({   
            title: "Deseja apagar?",   
            text: "O pedido será apagado totalmente.",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Sim, apagar.",    
            cancelButtonText: "Não",   
            closeOnConfirm: false 
        }, function(){   
        	$.post('ajaxReq/a2b7f359a0e3d17ddd381f07f5f1c43350826cea', {'order': id}, function(response){
        		console.log(response);
    			swal("Apagado!", "O pedido foi apagado do sistema.", "success"); 
    			$("#order_"+id).fadeOut('fast');
    		}, 'json');
        });

    });

});