$('#demo-foo-addrow').editableTableWidget().find('td:first').focus();

$(document).on('change', '#demo-foo-addrow td', function(evt, newValue) {
	// do something with the new cell value 
	var value = 0;

	if($(this).data('type') == 'price'){
		if(newValue == ''){
			value = 0;
		}else{
			value = newValue.replace("R$", "");
			$(this).html('R$'+value);
		}
	}else {
		value = newValue;
	}
	var params = {
		'id' : $(this).data('id'),
		'type' : $(this).data('type'),
		'table' : 'ingredients',
		'value' : value
	}

	$.post('ajaxReq/7e75d824c17cbc175a56479a7ad38b4212cbf0da', params, function(response){
		//console.log(response);
	});

});

$(document).on('click', '.change_status', function(e){
	e.preventDefault();

	if($(this).data('provider') == 1){
		$(this).data('provider', '0');
		$(this).removeClass('label-success');
		$(this).addClass('label-danger');
		$(this).text('Indisponível');
		var data = {'id': $(this).data('id'), 'type': $(this).data('type'), 'provider': '0'};
	}else{
		$(this).data('provider', '1');
		$(this).removeClass('label-danger');
		$(this).addClass('label-success');
		$(this).text('Disponível');
		var data = {'id': $(this).data('id'), 'type': $(this).data('type'), 'provider': '1'};
	}

	var params = {
		url: 'ajaxReq/a937c9941c1df0c6e917269c95700803ce1cac52',
		type: 'post',
		data: data,
		success: function(response){
			//console.log(response);
		}
	}

	$.ajax(params);

});

$(document).on('click', '.remove', function(e){
	e.preventDefault();
	var row = $(this).data('id');

	swal({   
		title: "Deseja remover?",   
		text: "O ingrediente será removido permanentemente.",   
		type: "warning",   
		showCancelButton: true,   
		confirmButtonColor: "#DD6B55",   
		confirmButtonText: "Sim",   
		cancelButtonText: "Não",   
		closeOnConfirm: false,   
		closeOnCancel: true 
	}, function(isConfirm){   
		if (isConfirm) {     
			$.post('ajaxReq/078798c32e5ac6b71924b425b305229c9cacd859', {'id': row, 'type': 'ingredients'}, function(){
				$("#"+row).fadeOut();
			});
			swal("Removido!", "", "success");   
		}
	});
	
});

$(document).on('submit', '[role="ingredients"]', function(e){
	e.preventDefault();
	var data = $(this).serialize();

	var params = {
		url: 'ajaxReq/6f48716b9f950a4d2597c90199ed029e4290f2b1',
		type: 'post',
		data: data,
		success: function(response){
			//console.log(response[0]);
			if(response.error == true){
			   
			}else{
			   $("#addIngredients").modal('hide');
			   swal("Adicionado!", "O ingrediente foi adicionado com sucesso!", "success");
			   var element = '<tr id="'+ response[0].id +'">';
				   element = element + '<td>'+ response[0].name +'</td>';
				   element = element + '<td>';
				   element = element + '<span class="label label-success change_status" data-toggle="tooltip" title="Mudar status" data-id="'+ response[0].id +'" data-type="category" data-provider="'+ response[0].status +'" style="cursor:pointer;">Disponível</span>';
				   element = element + '</td>';
				   element = element + '<td>';
				   element = element + '<button type="button" data-id="'+ response[0].id +'" class="btn btn-info btn-outline btn-circle btn-xs m-r-5 remove"><i class="ti-trash"></i></button>';
				   element = element + '</td>';
				   element = element + '</tr>';
				$("#ingredients").prepend(element);
				$('[role="ingredients"]')[0].reset();
			}
		},
		dataType: 'json'
	}
	$.ajax(params);

});