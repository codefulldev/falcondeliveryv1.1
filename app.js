const app = angular.module('FoodApplication', ['btford.socket-io']);


app.factory('socket', function(socketFactory) {

    var ip = angular.element(document.querySelector('#pn')).data('address');
    var port = angular.element(document.querySelector('#pn')).data('port');

    var connect = io.connect('//' + ip + ':' + port + '/');

    socket = socketFactory({
        ioSocket: connect
    });
    return socket;

});



app.filter('formatDate', function($filter) {

    return function(ele, dateFormat) {

        return $filter('date')(new Date(ele), dateFormat);

    }

})



app.controller('GeneralController', ['$scope', '$http', '$filter', '$interval', '$timeout', 'socket', function($scope, $http, $filter, $interval, $timeout, socket) {

    $scope.cliente_name = '[{cliente_name}]';

    $scope.ecommerce_title = '[{e_title}]';

    $scope.ecommerce_phone = '[{e.phone}]';

    $scope.ecommerce_email = '[{e.email}]';



}])



app.controller('ProductsController', ['$scope', '$http', function($scope, $http) {

    $scope.productPageTitle = 'Produtos';



    var products = reqGet('ajaxReq/cf38f5ec33c37247808351d567d3d31acb719699', 'application/json');

    $http(products)

    .then(function(success) {

        $scope.products = success.data.products;

    }, function(error) {});



    $scope.viewProduct = function(productID) {



        var product = reqGet('ajaxReq/cf38f5ec33c37247808351d567d3d31acb719699?id=' + productID, 'application/json');

        $http(product)

        .then(function(success) {

            console.log(success.data);

            var data = success.data.product[0];

            $scope.productName = data.name;

        }, function(error) {});



    }



}])



app.controller('CategoryController', ['$scope', '$http', function($scope, $http) {

    $scope.categoryPageTitle = 'Categorias';



    var category = reqGet('ajaxReq/5c192f0a5198906d65f24dcf39b3ca4d006ec70a', 'application/json');

    $http(category)

    .then(function(success) {

        $scope.categories = success.data;

    }, function(error) {});



}])



app.controller('CupomController', ['$scope', '$http', function($scope, $http) {

    $scope.cupomPageTitle = 'Cupons';



    var cupom = reqGet('ajaxReq/133e18fd36ada046f9e51206b34d63fa54838d45', 'application/json');

    $http(cupom)

    .then(function(success) {

        $scope.cupons = success.data;

    }, function(error) {});



}])



app.controller('AddonsController', ['$scope', '$http', function($scope, $http) {

    $scope.addonsPageTitle = 'Adicionais';



    var addons = reqGet('ajaxReq/177d988094f6753022ed763cf3943f4f31054aa4', 'application/json');

    $http(addons)

    .then(function(success) {

        $scope.addons = success.data;

    }, function(error) {});



}])



app.controller('OptionalsController', ['$scope', '$http', function($scope, $http) {

    $scope.optionalsPageTitle = 'Opcionais';



    var optionals = reqGet('ajaxReq/88df9223964eb4cd2e67c78b41236a8cd08c7094', 'application/json');

    $http(optionals)

    .then(function(success) {

        $scope.optionals = success.data;

    }, function(error) {});



}])



app.controller('IngredientsController', ['$scope', '$http', function($scope, $http) {

    $scope.ingredientsPageTitle = 'Ingredientes';



    var ingredients = reqGet('ajaxReq/5f59d72ce004b5f3c518208b41a0e0c733995bfe', 'application/json');

    $http(ingredients)

    .then(function(success) {

        $scope.ingredients = success.data;

    }, function(error) {});



}])



app.controller('OrdersController', ['$scope', '$http', function($scope, $http) {

    $scope.ordersPageTitle = 'Pedidos';



    var orders = reqGet('ajaxReq/5f59d72ce004b5f3c518208b41a0e0c733995bfe', 'application/json');

    $http(orders)

    .then(function(success) {

        $scope.orders = success.data;

    }, function(error) {});



}])



app.controller('ClientsController', ['$scope', '$http', function($scope, $http) {

    $scope.clientsPageTitle = 'Clientes';



}])



function reqPost(url, data, content) {



    var req = {

        method: 'POST',

        url: url,

        headers: {

            'Content-Type': content

        },

        data: data

    }



    return req;



}



function reqGet(url, content) {



    var req = {

        method: 'GET',

        url: url,

        headers: {

            'Content-Type': content

        }

    }



    return req;



}